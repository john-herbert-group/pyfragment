import unittest
from io import StringIO, TextIOWrapper
from typing import Match

import numpy as np

from fragment.properties.core import (
    PartialMatrix,
    PropertySet,
    add_property,
    remove_property,
)
from fragment.properties.extraction import (
    Extractor,
    PropertyExtractorMixin,
    SharedStreamDataSource,
    calc_property,
)
from tests._util import DBTestCase

# Psudo atom types for testing
DUMMY = -1
PHYSICAL = 0
PROXY = 1


class AMatrix(PartialMatrix):
    dtype = "int64"
    window = (3, 3)
    extensive = (False, True)


class BMatrix(PartialMatrix):
    dtype = "int64"
    window = (1, 2)
    extensive = (True, True)


class PartialMatrixTestCases(unittest.TestCase):
    def test_shape(self):
        A = AMatrix.zeros([2, 6, 3])
        self.assertEqual(A.data.shape, (3, 9))
        self.assertListEqual(A.el_types, [DUMMY] * 3)
        self.assertDictEqual(A.lookup, {2: 0, 3: 1, 6: 2})

        B = BMatrix.zeros([2, 6, 3])
        self.assertEqual(B.data.shape, (3, 6))
        self.assertListEqual(B.el_types, [DUMMY] * 3)
        self.assertDictEqual(B.lookup, {2: 0, 3: 1, 6: 2})

    def test_masks(self):
        # Test with a 3 x 3A matrix
        A = AMatrix.zeros(elements=[1, 2, 3])

        # Check that we get a proper mask in d1 direction
        d1 = A.axis_indexs({1, 3}, 0)
        self.assertEqual(d1, slice(None, None, None))

        # Check the d2 direction
        d2 = A.axis_indexs({1, 3}, 1)
        self.assertListEqual(d2.tolist(), [0, 1, 2, 6, 7, 8])

        # Check the mask product
        mask = A.get_mask({1, 3})
        for i, j in zip(mask, (d1, d2)):
            if isinstance(i, slice):
                self.assertEqual(i, j)
            else:
                self.assertTrue((i == j).all())

        # Check result for an elements that do not exist
        with self.assertRaises(KeyError):
            d3 = A.axis_indexs({1, 2, 5}, 1)

        # Now rerun it with a 2A x 1A matrix
        B = BMatrix.zeros(elements=[1, 2, 3])

        # Check Axis 1 in terms of a window=1
        d3 = B.axis_indexs({1, 3}, 0)
        self.assertListEqual(d3.tolist(), [0, 2])

        mask = B.get_mask({1, 3})
        d3 = (0, 0, 0, 0, 2, 2, 2, 2)
        d4 = (0, 1, 4, 5, 0, 1, 4, 5)

        # Result should be a product of dmasks
        for i, j in zip(mask, (d3, d4)):
            self.assertEqual(tuple(i), j)

    def test_matrix_add_into(self):
        COEF = 3
        REF = COEF * np.array(
            [
                [1, 1, 0, 0, 1, 1, 0, 0, 1, 1],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [1, 1, 0, 0, 1, 1, 0, 0, 1, 1],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [1, 1, 0, 0, 1, 1, 0, 0, 1, 1],
            ]
        )

        A = BMatrix(
            list(range(5)),
            [PHYSICAL] * 5,
            np.zeros((5, 10), "int64"),
        )

        B = BMatrix(
            [0, 2, 4],
            [PHYSICAL] * 3,
            np.ones((3, 6), "int64"),
        )

        A.add_into(B, COEF)
        self.assertTrue((REF == A.data).all())

    def test_matrix_zeros(self):
        A = AMatrix(
            list(range(5)),
            [PHYSICAL] * 5,
            np.zeros((3, 15), "int64"),
        )

        B = AMatrix(
            list(range(4, 7)),
            [PROXY] * 3,  # physical should take priority
            np.ones((3, 9), "int64"),
        )

        C = AMatrix.union_zeros(A, B)
        self.assertEqual(C.data.shape, (3, 21))
        self.assertListEqual(C.elements, list(range(0, 7)))
        self.assertListEqual(C.el_types, [PHYSICAL] * 5 + [PROXY] * 2)

    def test_matrix_add_together(self):
        A = BMatrix(
            list(range(2)),
            [PHYSICAL] * 2,
            np.ones((2, 4), "int64"),
        )

        B = BMatrix(
            list(range(1, 3)),
            [PROXY] * 2,  # physical should take priority
            np.ones((2, 4), "int64") * 2,
        )

        C = A + B
        REF = np.array(
            [
                [1, 1, 1, 1, 0, 0],
                [1, 1, 3, 3, 2, 2],
                [0, 0, 2, 2, 2, 2],
            ]
        )
        self.assertTrue((REF == C.data).all())

    def test_to_dict(self):
        A = BMatrix(
            list(range(2)),
            [PHYSICAL] * 2,
            np.arange(0, 8).reshape(2, 4),
        )
        d = A.to_dict()
        self.assertDictEqual(
            d,
            {
                "elements": [0, 1],
                "el_types": [0, 0],
                "shape": (2, 4),
                "dtype": "int64",
                "data": [0, 1, 2, 3, 4, 5, 6, 7],
            },
        )
        B = BMatrix.from_dict(d)
        self.assertTrue((A.data == B.data).all())


class PropertySetTestCases(unittest.TestCase):
    def setUp(self) -> None:
        add_property(name="A", t=AMatrix, use_coef=True, help="Matrix property")
        add_property(name="B", t=float, use_coef=True, help="Float property")
        add_property(
            name="C", t=int, use_coef=False, help="Int property that does not use coefs"
        )

    def tearDown(self) -> None:
        remove_property("A")
        remove_property("B")
        remove_property("C")

    def test_construct(self):
        A = AMatrix.zeros([1, 2])

        p = PropertySet({"A": A, "B": 1.2, "C": 8})
        p.validate_types()

        # C is not an integer
        p = PropertySet({"A": A, "B": 1.2, "C": 8.0})
        with self.assertRaises(ValueError):
            p.validate_types()

        p = PropertySet({"A": A, "B": 1.2, "C": 8, "BLARG": 3})
        with self.assertRaises(KeyError):
            p.validate_types()

    def test_zeros(self):
        p = PropertySet.zeros(["A", "B", "C"], [1, 2, 3])

        self.assertTrue((p["A"].data == np.zeros((3, 9), dtype=int)).all())
        self.assertEqual(p["B"], 0.0)
        self.assertEqual(p["C"], 0)

    def test_add_into(self):
        REF = np.array(
            [
                [1, 1, 1, 0, 0, 0, 1, 1, 1],
                [1, 1, 1, 0, 0, 0, 1, 1, 1],
                [1, 1, 1, 0, 0, 0, 1, 1, 1],
            ]
        )
        A = AMatrix([1, 3], [DUMMY] * 2, np.ones((3, 6), dtype="int64"))

        P1 = PropertySet.zeros(["A", "B", "C"], [1, 2, 3])
        P2 = PropertySet({"A": A, "B": 1.2, "C": 1})

        P1.add_into(P2, 1)
        self.assertTrue((P1["A"].data == REF).all())
        self.assertEqual(P1["B"], 1.2)
        self.assertEqual(P1["C"], 1)

        P1.add_into(P2, -1)
        self.assertTrue((P1["A"].data == 0 * REF).all())
        self.assertEqual(P1["B"], 0.0)

        # Does not use coef so it increments
        self.assertEqual(P1["C"], 2)

    def test_add_together(self):
        REF = np.array(
            [
                [2, 2, 2, 1, 1, 1, 1, 1, 1],
                [2, 2, 2, 1, 1, 1, 1, 1, 1],
                [2, 2, 2, 1, 1, 1, 1, 1, 1],
            ]
        )

        P1 = PropertySet(
            {
                "A": AMatrix([1, 3], [DUMMY] * 2, np.ones((3, 6), dtype="int64")),
                "B": 1.2,
                "C": 1,
            }
        )
        P2 = PropertySet(
            {
                "A": AMatrix([1, 2], [DUMMY] * 2, np.ones((3, 6), dtype="int64")),
                "B": 1.8,
                "C": 2,
            }
        )

        P3 = P1 + P2

        self.assertTrue((P3["A"].data == REF).all())
        self.assertEqual(P3["B"], 3.0)
        self.assertEqual(P3["C"], 3)

        REF = np.array(
            [
                [0, 0, 0, -1, -1, -1, 1, 1, 1],
                [0, 0, 0, -1, -1, -1, 1, 1, 1],
                [0, 0, 0, -1, -1, -1, 1, 1, 1],
            ]
        )

        P4 = P1 - P2

        self.assertTrue((P4["A"].data == REF).all())
        self.assertAlmostEqual(P4["B"], -0.6)
        self.assertEqual(P4["C"], 3)

    def test_to_dict(self):
        P1 = PropertySet.zeros(["A", "B", "C"], [1, 2, 3])
        d = P1.to_dict()
        self.assertDictEqual(
            d,
            {
                "A": {
                    "elements": [1, 2, 3],
                    "el_types": [DUMMY, DUMMY, DUMMY],
                    "shape": (3, 9),
                    "dtype": "int64",
                    "data": [
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                    ],
                },
                "B": 0.0,
                "C": 0,
            },
        )

        P2 = PropertySet.from_dict(d)
        self.assertTrue((P1["A"].data == P2["A"].data).all())


class PropertySetTest(DBTestCase):
    def setUp(self) -> None:
        add_property(
            name="coef_prop", t=float, use_coef=True, help="Coef dependent property"
        )
        add_property(
            name="no_coef_prop",
            t=float,
            use_coef=False,
            help="Coef independent property",
        )

    def tearDown(self) -> None:
        remove_property("coef_prop")
        remove_property("no_coef_prop")

    def test_construct(self):
        class PropertiedClass(PropertyExtractorMixin):
            @calc_property(prop_name="no_coef_prop")
            def prop_tttime(self, ctx) -> float:
                """Docs"""
                return 1.0

            @calc_property()
            def prop_coef_prop(self, ctx):
                return 1.0

        prop = PropertiedClass()

        self.assertIsInstance(prop.prop_tttime, Extractor)
        self.assertEqual(prop.prop_tttime.property_name, "no_coef_prop")

        self.assertDictEqual(
            prop.get_properties(None, []).property_values,
            {"no_coef_prop": 1.0, "coef_prop": 1.0},
        )

    def test_available_props(self):
        class PropertiedClass(PropertyExtractorMixin):
            @calc_property(source="file")
            def prop_no_coef_prop(self, ctx, line, file):
                return 1

            @calc_property(source="regex", patterns=[r"blah"])
            def prop_coef_prop(self, ctx, match):
                return 1

        prop = PropertiedClass()
        self.assertSetEqual(prop.available_properties(), {"no_coef_prop", "coef_prop"})
        self.assertIsInstance(prop._data_providers[0], SharedStreamDataSource)
        self.assertListEqual(
            prop._data_providers[0].extractors,
            [prop.prop_coef_prop, prop.prop_no_coef_prop],
        )

    def test_file_source(self):
        class PropertiedClass(PropertyExtractorMixin):
            def __init__(self) -> None:
                super().__init__()
                self.coef_prop_ctr = 0.0
                self.no_coef_prop_ctr = 0.0

            @calc_property(source="file")
            def prop_coef_prop(self, ctx, line, file_handle: TextIOWrapper):
                self.coef_prop_ctr += 1.0
                assert len(line) != 0

            @calc_property(source="file")
            def prop_no_coef_prop(self, ctx, line, file_handle: TextIOWrapper):
                self.no_coef_prop_ctr += 1
                assert len(line) != 0
                if self.no_coef_prop_ctr == 5.0:
                    return 5.0

        prop = PropertiedClass()
        prop_vals = prop.get_properties(None, [StringIO("Not blank\n" * 23)])
        self.assertDictEqual(prop_vals.property_values, {"no_coef_prop": 5.0})
        self.assertEqual(prop.no_coef_prop_ctr, 5.0)
        self.assertEqual(prop.coef_prop_ctr, 23.0)

    def test_re_source(self):
        class PropertiedClass(PropertyExtractorMixin):
            def __init__(self) -> None:
                super().__init__()
                self.call_counter = 0
                self.matches = []

            @calc_property(
                source="regex", patterns=[r"\W+method:\W+(\w+)", r"\W+name:\W+(\w+)"]
            )
            def prop_no_coef_prop(self, ctx, match: Match, _):
                self.matches.append(match[1])
                self.call_counter += 1

        prop = PropertiedClass()
        _ = prop.get_properties(
            None,
            [
                StringIO(
                    """
            method: PDB
            method: Dummy 
            method: PDB
            name: RCaps
            method: Dummy
        """
                )
            ],
        )
        self.assertEqual(prop.call_counter, 5)
        self.assertListEqual(prop.matches, ["PDB", "Dummy", "PDB", "RCaps", "Dummy"])
