import unittest
from hashlib import sha256
from os import path
from tempfile import TemporaryDirectory

import networkx as nx

from fragment.calculations.common import JobStatus
from fragment.calculations.models import Calculation, Job, Layer
from fragment.conf.models import ConfigModel
from fragment.core.project import FileDatabaseManager, Project, TransientDatabaseManager
from fragment.db.bootstrap import DATABASE_FILE_NAME
from fragment.examples.strategies import WATER6_STRATEGY_PATH
from fragment.examples.systems import WATER6_SYS_PATH
from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.systems.models import Atom, System
from tests._util import TestFragmenter, TestQMBackend, TestQMShellBackend, make_system

UUID_STR = "00000000-0000-0000-0000-000000000000"


class TransientDBMTestCases(unittest.TestCase):
    def test_create(self):
        dbm = TransientDatabaseManager()
        dbm.init()

        with dbm:
            Atom.create(t="H", x=1, y=2, z=3)
        dbm.cleanup_lifecycle()


class FileDBMTestCases(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = TemporaryDirectory()
        self.dbm = FileDatabaseManager(basepath=self.tmp_dir.name)
        self.dbm.init()

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()
        self.dbm.cleanup_lifecycle()

    def test_create(self):
        # with self.assertRaises(Exception):
        #     Atom.create(t='H', x=1,y=2,z=3)

        with self.dbm:
            Atom.create(t="H", x=1, y=2, z=3)

        self.assertTrue(path.exists(path.join(self.tmp_dir.name, "fragment.db")))

    def test_decorator(self):
        @self.dbm
        def decorated():
            Atom.create(t="H", x=1, y=2, z=3)

        # Should not error out
        decorated()

    def test_nested(self):
        with self.dbm:
            with self.dbm:
                Atom.create(t="H", x=1, y=2, z=3)
            Atom.create(t="H", x=1, y=2, z=3)

        # with self.assertRaises(Exception):
        #     Atom.create(t='H', x=1,y=2,z=3)


CONFIG_FIXTURE = [
    (
        "supersystem",
        "ConfigType.FRAGMENTER",
        "SupersystemFragmenter",
        [],
        {"mods": None, "combinator": None, "bu_missing": 0},
    ),
    (
        "distance_8A",
        "ConfigType.MOD",
        "DistanceMod",
        [],
        {"distance": 8.0, "method": None, "min_distance": None, "max_distance": None},
    ),
    (
        "waterMBE",
        "ConfigType.FRAGMENTER",
        "WaterFragmenter",
        [],
        {"mods": None, "combinator": None, "bu_missing": 0},
    ),
    (
        "supersystem2",
        "ConfigType.FRAGMENTER",
        "SupersystemFragmenter",
        [],
        {"mods": None, "combinator": None, "bu_missing": 0},
    ),
    (
        "pm7",
        "ConfigType.QM_BACKEND",
        "MOPACBackend",
        [],
        {"template": "PM7 XYZ PRECISE NOSYM NOREOR GEO-OK\n\n\n{geometry}\n"},
    ),
    (
        "hf3c",
        "ConfigType.QM_BACKEND",
        "QChemBackend",
        [],
        {
            "template": "! Name: {name}\n$molecule\n{charge} {mult} \n{geometry}\n$end\n\n$rem\n    basis minix\n    method hf3c\n    job_type sp\n$end\n"
        },
    ),
]


class ProjectInitTestCases(unittest.TestCase):
    def setUp(self) -> None:
        self.tmpdir = TemporaryDirectory()

    def tearDown(self) -> None:
        self.tmpdir.cleanup()

    def test_in_memory(self):
        project = Project(self.tmpdir.name, in_memory_db=True)
        project.init()

        self.assertIsInstance(project.dbm, TransientDatabaseManager)
        self.assertFalse((project.basepath / DATABASE_FILE_NAME).exists())
        self.assertTrue(project.basepath.exists())
        self.assertTrue(project.scratchpath.exists())
        self.assertTrue(project.file_archive.exists())

    def test_file(self):
        project = Project(self.tmpdir.name)
        project.init()

        self.assertIsInstance(project.dbm, FileDatabaseManager)
        self.assertTrue((project.basepath / DATABASE_FILE_NAME).exists())
        self.assertTrue(project.basepath.exists())
        self.assertTrue(project.scratchpath.exists())
        self.assertTrue(project.file_archive.exists())


class ProjectTestCases(unittest.TestCase):
    def setUp(self) -> None:
        self.tmpdir = TemporaryDirectory()
        self.project = Project(self.tmpdir.name)
        self.project.init()

    def tearDown(self) -> None:
        self.tmpdir.cleanup()

    def test_add_system(self):
        self.project.add_system(
            WATER6_SYS_PATH,
            "water6",
            "A Note",
            charges={1: 1, 4: -1},  # First and second oxygens
        )

        with self.project.dbm:
            sys: System = System.get(1)
            self.assertEqual("water6", sys.labels.first().name)
            self.assertEqual("A Note", sys.labels.first().note)
            self.assertEqual(sys.atoms.count(), 18)
            self.assertEqual(sys.atoms[0].charge, 1)
            self.assertEqual(sys.atoms[3].charge, -1)
            self.assertEqual(nx.number_connected_components(sys.bonds), 6)

        with self.project.open_archive("r") as tf:
            self.assertListEqual(
                ["sources/water6.xyz"], [f.name for f in tf.getmembers()]
            )

    def test_add_strategy(self):
        self.project.add_strategy_file(WATER6_STRATEGY_PATH)

        with self.project.dbm:
            conf_data = [
                (s.name, s.config_type, s.config_class, s.args, s.kwargs)
                for s in ConfigModel.select()
            ]
            self.assertListEqual(conf_data, CONFIG_FIXTURE)

            sys = System.get(1)
            self.assertEqual(len(sys.atoms), 18)

            self.assertEqual(Calculation.select().count(), 1)
            self.assertListEqual(
                Calculation.select().first().job_structure,
                [
                    {"coef": 1, "layer_id": 1},
                    {"coef": -1, "layer_id": 3},
                    {"coef": 1, "layer_id": 2},
                ],
            )

        # Do it a second time to make sure it's idempotent
        self.project.add_strategy_file(WATER6_STRATEGY_PATH)

    def test_archive_job(self):
        FILE_LIST = [
            "qm_calcs/0/1/2/1/00",
            "qm_calcs/0/1/2/1/00/frag_1.2.1.blog",
            "qm_calcs/0/1/2/1/00/frag_1.2.1.csv",
            "qm_calcs/0/1/2/1/00/frag_1.2.1.txt",
        ]

        with self.project.dbm:
            sys = make_system(atoms=3)
            backend = TestQMShellBackend("qm", "", save_config=True)

            job = Job.create(
                system_id=sys.id,
                backend__id=backend.conf_id,
                # mod_stack_id=Stack.from_names().id
                key=UUID_STR,
            )

        self.project.run_job(job)  # Calls archive job at the end
        with self.project.dbm:
            job = Job.get_by_id(job.id)  # Refresh from the DB
            with self.project.open_archive("r") as f:
                self.assertListEqual(f.getnames(), FILE_LIST)
            self.assertDictEqual(
                job.properties.to_dict(),
                {"a": 1.0, "b": 2.0, "wall_time": 1.0, "total_energy": 0.2},
            )

        # Test compress and uncompress workflow
        self.assertEqual(self.project.file_archive.name, "file_archive.tar")
        self.assertFalse(self.project.is_compressed)

        sha_1 = sha256()
        with self.project.file_archive.open("rb") as f:
            sha_1.update(f.read())

        # Check that the archive name is changed
        self.project.compress()
        self.assertEqual(self.project.file_archive.name, "file_archive.tar.bz2")
        self.assertFalse(self.project._file_archive.exists())  # Was it deleted?
        self.assertTrue(self.project.is_compressed)  # Are we marked as compressed?

        # Check that it's readable but not writeable
        with self.project.open_archive("r") as f:
            self.assertListEqual(f.getnames(), FILE_LIST)

        with self.assertRaises(RuntimeError):
            self.project.open_archive("a")

        # Check that we get everything back out
        self.project.uncompress()
        self.assertEqual(self.project.file_archive.name, "file_archive.tar")
        self.assertFalse(self.project._compressed_archive.exists())  # Was it deleted?
        self.assertFalse(self.project.is_compressed)  # Are we marked as compressed?

        sha_2 = sha256()
        with self.project.file_archive.open("rb") as f:
            sha_2.update(f.read())

        self.assertEqual(sha_2.digest(), sha_1.digest())

    def test_project_run(self):
        # Setup the calculation
        with self.project.dbm:
            fragmenter = TestFragmenter("fragmenter", "", save_config=True)
            ssf = AbstractFragmenter.obj_from_name("supersystem")
            backend = TestQMBackend("qm", "", save_config=True)

            sys = make_system(atoms=6)
            frag_view = fragmenter.aux_from_system(sys, order=2, save=True)
            supersystem = ssf.aux_from_system(sys, order=1, save=True)

            l1, _ = Layer.get_or_create(view=frag_view, backend__id=backend.conf_id)
            l2, _ = Layer.get_or_create(view=supersystem, backend__id=backend.conf_id)

            calc = Calculation(name="test_calc", note="Test Calc")
            calc.add_layer(l1)
            calc.add_layer(l2)
            calc.save()

            job: Job = Job.select().first()
            job.key = UUID_STR  # Zero this out since it dirs depend on it
            job.save()
            layer: Layer = Layer.select().first()

        pending_query = Job.select().where(Job.status == JobStatus.PENDING)

        with self.project.dbm:
            self.assertEqual(pending_query.count(), 18)

        # Test a single job
        self.project.run_job(job)
        with self.project.dbm:
            job = Job.get_by_id(job.id)  # Refresh from the DB
            self.assertEqual(pending_query.count(), 17)
            with self.project.open_archive("r") as f:
                # This backend is in fileless mode
                self.assertListEqual(f.getnames(), [])
            self.assertEqual(list(self.project.scratchpath.iterdir()), [])
            self.assertDictEqual(
                job.properties.to_dict(),
                {"a": 1.0, "b": 2.0, "total_energy": 0.2, "wall_time": 1.0},
            )

        # Test layers
        self.project.run_layer(layer)
        with self.project.dbm:
            layer = Layer.get(id=layer.id)
            self.assertEqual(pending_query.count(), 1)

            ref = {"a": 17.0, "b": 2.0, "total_energy": 0.2, "wall_time": 17.0}

            data = layer.properties.to_dict()
            self.assertEqual(len(ref), len(ref))
            for k, v in data.items():
                self.assertAlmostEqual(v, data[k], 5)

        # Test calculation
        self.project.run_calc(calc.name)
        with self.project.dbm:
            calc = Calculation.get(id=calc.id)
            self.assertEqual(pending_query.count(), 0)

            # b uses coefs and layers 1 and 2 cancel out so it's
            # just the supersystem
            self.assertDictEqual(
                calc.properties.to_dict(),
                {"a": 35.0, "b": 2.0, "total_energy": 0.2, "wall_time": 35.0},
            )
