import unittest
from functools import cached_property

from fragment.core.util import invalidate_cache, reset_cache


class CachedProperty(unittest.TestCase):
    def setUp(self) -> None:
        class CachedClass(object):
            def __init__(self) -> None:
                self.val = 1

            @cached_property
            def test_prop(self):
                self.val += 1
                return self.val

            @invalidate_cache
            def invalidate(self):
                pass

        self.cc = CachedClass()

    def test_cache(self):
        v = self.cc.test_prop
        self.assertEqual(v, 2)
        self.assertEqual(v, self.cc.test_prop)

    def test_reset(self):
        v = self.cc.test_prop
        self.assertEqual(v, self.cc.test_prop)

        reset_cache(self.cc)

        self.assertEqual(3, self.cc.test_prop)
        self.assertNotEqual(v, self.cc.test_prop)

    def test_invalidate(self):
        v = self.cc.test_prop
        self.assertEqual(v, self.cc.test_prop)
        self.cc.invalidate()
        self.assertNotEqual(v, self.cc.test_prop)

    def test_setter(self):
        self.cc.test_prop = 42
        v = self.cc.test_prop
        self.assertEqual(v, 42)

    def test_deleter(self):
        self.cc.test_prop = 42
        self.assertEqual(self.cc.test_prop, 42)
        del self.cc.test_prop
        self.assertEqual(self.cc.test_prop, 2)
