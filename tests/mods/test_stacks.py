from fragment.mods.abstract import FilterBaseClass, PostAuxFragBase, PreTemplateBase
from fragment.mods.models import Stack
from fragment.systems.models import System
from tests._util import DBTestCase, TestCOMMod, make_system


class NotEven(FilterBaseClass):
    def run(self, num) -> bool:
        if num == 0:
            return True
        return not (num % 2 == 0)


class NotThree(FilterBaseClass):
    def run(self, num) -> bool:
        if num == 0:
            return True
        return not (num % 3 == 0)


class StackTestCases(DBTestCase):
    def setUp(self) -> None:
        make_system()
        capper1 = TestCOMMod("capper1", "", save_config=True)
        capper2 = TestCOMMod("capper2", "", save_config=True)
        self.stack = Stack(capper2, capper1)
        self.stack.save()

    def test_stack(self):
        stack = Stack.get(self.stack.id)
        self.assertEqual(stack.key, (3, 2))

    def test_mod_stack(self):
        stack = Stack.get(self.stack.id)
        self.assertListEqual([m.name for m in stack.mods], ["capper2", "capper1"])

    def test_is_empty(self):
        stack = Stack.get(self.stack.id)
        system = System.get(1)

        not_empty = PreTemplateBase.get_applicator(stack.mods, system)
        self.assertFalse(not_empty.is_empty())

        empty = PostAuxFragBase.get_applicator(stack.mods, system)
        self.assertTrue(empty.is_empty())

    def test_filter(self):
        a = [(i,) for i in range(0, 13)]
        app = FilterBaseClass.get_applicator(
            [NotEven("even", ""), NotThree("three", "")]
        )

        self.assertListEqual(
            [(0,), (1,), (5,), (7,), (11,)], [i for i in app.filter(a)]
        )

    def test_fn_stack(self):
        stack = Stack.get(self.stack.id)
        system = System.get(1)
        stack_app = PreTemplateBase.get_applicator(stack.mods, system)
        stack_app(system)
        # Make sure two capping atoms are added
        self.assertEqual(system.atoms.count(), 2)

    def test_get_stack(self):
        stack1 = Stack.from_names("capper1", "capper2")
        self.assertEqual(stack1.id, 3)

        stack2 = Stack.from_names("capper2", "capper1")
        self.assertEqual(stack2.id, 2)
