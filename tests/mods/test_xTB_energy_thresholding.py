import unittest
from time import time

from fragment.backends.libxtb import LibXTBBackend
from fragment.examples.systems import WATER6_FRAG_PATH
from fragment.fragmenting.water import WaterFragmenter
from fragment.mods.xtb_filter import EnergyNodeTrimming, XTBEnergyMod
from fragment.systems.geometry_readers.util import read_geometry
from tests._util import DBTestCase


class EnergyTestCases(DBTestCase):
    def setUp(self) -> None:
        # This gives a good balance of top-level nodes to be exclude
        # while leaving in a few nodes that should be screened out
        THRESHOLDS = {4: 0.005, 3: 0.3, 2: 0.7}
        self.order = 4

        self.backend = LibXTBBackend("xtb", "", save_config=True)
        # Set some high thresholds. We want most of the fragments to be discarded
        self.filter = XTBEnergyMod(
            "energy_filter", "", backend="xtb", thresholds=THRESHOLDS, save_config=True
        )
        self.trimmer = EnergyNodeTrimming(
            "energy_trimmer", "", backend="xtb", thresholds=THRESHOLDS, save_config=True
        )
        self.filter_only = WaterFragmenter(
            "wf_1", "water_MBE", save_config=True, mods=["energy_filter"]
        )
        self.trimmer_only = WaterFragmenter(
            "wf_2", "water_MBE", save_config=True, mods=["energy_trimmer"]
        )
        self.filter_and_trimmer = WaterFragmenter(
            "wf_3",
            "water_MBE",
            save_config=True,
            mods=["energy_filter", "energy_trimmer"],
        )
        self.sys = read_geometry("water6", "Water 6 system", path=WATER6_FRAG_PATH)

    @unittest.skipIf(not LibXTBBackend.is_available(), "Could not find xTB exe")
    def test_filter(self):
        aux_frags = self.filter_only.aux_from_system(self.sys, self.order, save=True)
        self.assertEqual(len(aux_frags.dep_tree.tree), 50)
        self.assertEqual(len(aux_frags.fragments), 49)
        self.assertTrue(aux_frags.dep_tree.is_complete())

    @unittest.skipIf(not LibXTBBackend.is_available(), "Could not find xTB exe")
    def test_trimmer(self):
        aux_frags = self.trimmer_only.aux_from_system(self.sys, self.order, save=True)
        self.assertEqual(len(aux_frags.dep_tree.tree), 32)
        self.assertEqual(len(aux_frags.fragments), 31)
        self.assertTrue(aux_frags.dep_tree.is_complete())

    @unittest.skipIf(not LibXTBBackend.is_available(), "Could not find xTB exe")
    def test_filter_trimmer(self):
        aux_frags = self.filter_and_trimmer.aux_from_system(
            self.sys, self.order, save=True
        )
        self.assertEqual(len(aux_frags.dep_tree.tree), 32)
        self.assertEqual(len(aux_frags.fragments), 31)
        self.assertTrue(aux_frags.dep_tree.is_complete())
