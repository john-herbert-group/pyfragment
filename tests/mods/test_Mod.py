from fragment.systems.models import View
from tests._util import DBTestCase, TestCOMMod, make_system


class ModSchemeTests(DBTestCase):
    def setUp(self) -> None:
        self.sys = make_system(atoms=3, make_primary_frags=True)
        self.capper = TestCOMMod("test_capper", "")
        self.capper.setUp(self.sys)
        self.view = View.select()[0]

    def test_pre_template(self):
        frag = list(self.view.fragments)[0]
        self.assertEqual(len(frag.atoms), 2)
        self.capper.run(frag)
        self.assertEqual(len(frag.atoms), 3)
