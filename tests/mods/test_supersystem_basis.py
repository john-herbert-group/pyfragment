from fragment.examples.systems import WATER6_SYS_PATH
from fragment.fragmenting.util import get_fragmenter
from fragment.fragmenting.water import WaterFragmenter
from fragment.mods.util import get_mod
from fragment.systems.common import AtomType
from fragment.systems.geometry_readers.util import read_geometry
from tests._util import DBTestCase


class UseSupersystemBasis(DBTestCase):
    def test_water6(self):
        get_mod("UseSupersystemBasis")("use_supersystem_mod", "", save_config=True)
        supersys = read_geometry("water6", path=WATER6_SYS_PATH)

        real_atoms = set(supersys.key)

        fragmenter: WaterFragmenter = get_fragmenter("Water")(
            "water_frag", "", mods=["use_supersystem_mod"]
        )
        aux_frags = fragmenter.aux_from_system(supersys, 2, save=True)

        for f in aux_frags.fragments:
            self.assertEqual(len(f.key), 6 * 3)

            num_real_atoms = len(real_atoms.intersection(set(f.key)))
            if f.viewcoef.coef == 2:
                self.assertEqual(num_real_atoms, 1)
            if f.viewcoef.coef == -5:
                self.assertEqual(num_real_atoms, -5)

    def test_water6_job(self):
        supersys = read_geometry("water6", path=WATER6_SYS_PATH)
        real_atoms = set(supersys.key)

        mod = get_mod("ClusterBasis")("...", "...", save_config=True)
        fragmenter: WaterFragmenter = get_fragmenter("Water")("...", "...")
        p_frags = fragmenter.primary_fragments(supersys)
        mod.setUp(supersys)

        ghosts = {a.id: a.convert_to(AtomType.GHOST) for a in supersys.atoms}
        ghost_set = {g.id for g in ghosts.values()}

        for f in p_frags.fragments:
            real_atoms = f.key
            mod.run(f)
            modded_atoms = {a.id for a in f.atoms}
            self.assertEqual(len(modded_atoms), 6 * 3)
            self.assertSetEqual(real_atoms, modded_atoms.difference(ghost_set))
