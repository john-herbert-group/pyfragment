from fragment.examples.systems import WATER6_SYS_PATH
from fragment.fragmenting.util import get_fragmenter
from fragment.fragmenting.water import WaterFragmenter
from fragment.mods.util import get_mod
from fragment.systems.common import AtomType
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import Atom
from tests._util import DBTestCase


class UseCloudBasis(DBTestCase):
    def test_water6(self):
        # 3.5 A is small so only use it for testing purposes.
        CUTOFF = 3.5
        get_mod("UseCloudBasis")(
            "use_cloud_basis_mod", "", cutoff=CUTOFF, save_config=True
        )
        supersys = read_geometry("water6", path=WATER6_SYS_PATH)

        fragmenter: WaterFragmenter = get_fragmenter("Water")(
            "water_frag", "", mods=["use_cloud_basis_mod"]
        )
        aux_frags = fragmenter.aux_from_system(supersys, 2, save=True)

        for f in aux_frags.fragments:
            for a1 in f.atoms.filter(type=AtomType.PHYSICAL):
                shortest = CUTOFF * 2  # This will fail the check
                for a2 in f.atoms.filter(type=AtomType.GHOST):
                    d = Atom.distance(a1, a2)
                    shortest = d if shortest >= d else shortest
                self.assertLessEqual(shortest, CUTOFF)
