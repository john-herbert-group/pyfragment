from fragment.db.models import BaseModel
from fragment.mods.abstract import PreTemplateBase
from fragment.mods.mixins import ModableMixin, UnsupportedModError
from fragment.mods.models import Stack
from tests._util import DBTestCase, TestCOMMod, UnsuportedMod


class ModdedClass(BaseModel, ModableMixin):
    SUPPORTED_MODS = (PreTemplateBase,)


class ModableMixinTestCases(DBTestCase):
    def setUp(self) -> None:
        ModdedClass.create_table()
        self.capper1 = TestCOMMod("c1", "", save_config=True)
        self.capper2 = TestCOMMod("c2", "", save_config=True)
        self.stack = Stack(self.capper1, self.capper2)  # NOTE: this is unsaved

    def test_modless(self):
        mc = ModdedClass()
        mc.save()

    def test_mod(self):
        mc = ModdedClass(mod_stack=self.stack)
        mc.save()

        mc = ModdedClass.get(1)
        for m, name in zip(mc.mod_stack.mods, ["c1", "c2"]):
            self.assertTrue(isinstance(m, TestCOMMod))
            self.assertEqual(m.name, name)

    def test_save_multiple(self):
        ModdedClass(mod_stack=self.stack).save()
        ModdedClass(mod_stack=self.stack).save()

    def test_unsupported_mods(self):
        m = UnsuportedMod("m1", "", save_config=True)
        with self.assertRaises(UnsupportedModError):
            ModdedClass(mod_stack=Stack.from_names("m1"))
