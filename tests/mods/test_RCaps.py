from fragment.examples.systems import (
    ETHANOL_SYS_PATH,
    FORMALDEHYDE_SYS_PATH,
    SMALL_PROTIEN_SYS_PATH,
    TWO_AA_SYS_PATH,
    WATER6_SYS_PATH,
)
from fragment.fragmenting.util import get_fragmenter
from fragment.mods.util import get_mod
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import Atom, System, View
from tests._util import DBTestCase


class RCapperTestCases(DBTestCase):
    def setUp(self) -> None:
        self.capper = get_mod("RCaps")("test_capper", "")

    def test_synthetic(self):
        """Synthetic test containing carbon, oxygen, and nitrogen,
        and hydrogen
        """
        sys1 = System(
            [
                Atom("C", r=(0, 0, 0)),
                Atom("C", r=(1.5, 0, 0)),  # Single bond
                Atom("C", r=(-1.3, 0, 0)),  # Double bond (which does not make sense)
                Atom("N", r=(0, 1.37, 0)),  # Single CN Bond
                Atom("N", r=(0, -1.07, 0)),  # Single CH Bond
            ]
        )
        sys1.save()

        central_atom = sys1.atoms[0]
        sys2 = System([central_atom])
        sys2.save()

        self.capper.setUp(sys1)
        capped_sys = self.capper.run(sys2)
        capped_atoms = [a for a in capped_sys.atoms if a.meta]
        capped_atoms.sort(key=lambda a: a.meta["proxy_for"])
        self.assertListEqual(
            [Atom.distance(central_atom, a) for a in capped_atoms],
            [1.07, 1.07, 1.02, 1.02],
        )

    def test_ignore_charged(self):
        """Test of charged systems that should NOT be capped"""
        self.ic_capper = get_mod("RCaps")(
            "test_ic_capper", "Test ignore charged", ignore_charged=True
        )

        charged_sys = System(
            [
                Atom("O", r=(0, 0, 0), charge=-1),
                Atom("H", r=(1.0, 0, 0)),
            ]
        )
        charged_sys.save()

        # Check that O was not capped
        central_atom1 = charged_sys.atoms[0]
        o_atom1 = System([central_atom1])
        o_atom1.save()
        self.ic_capper.setUp(charged_sys)
        capped_sys1 = self.ic_capper.run(o_atom1)

        added_atoms1 = list(capped_sys1.atoms.filter(saved=False))
        self.assertEqual(len(added_atoms1), 0)

        # Check that H was capped
        proton1 = charged_sys.atoms[1]
        h_atom1 = System([proton1])
        h_atom1.save()
        capped_sys2 = self.ic_capper.run(h_atom1)

        added_atoms2 = list(capped_sys2.atoms.filter(saved=False))
        self.assertEqual(len(added_atoms2), 0)

        # OH radical test
        neutral_sys = System(
            [
                Atom("O", r=(0, 0, 0)),
                Atom("H", r=(1.0, 0, 0)),
            ]
        )
        neutral_sys.save()

        # Check that O was capped
        central_atom2 = neutral_sys.atoms[0]
        o_atom2 = System([central_atom2])
        o_atom2.save()
        self.ic_capper.setUp(neutral_sys)
        capped_sys3 = self.ic_capper.run(o_atom2)

        added_atoms3 = list(capped_sys3.atoms.filter(saved=False))
        self.assertEqual(len(added_atoms3), 1)

        # Check that H was capped
        proton2 = neutral_sys.atoms[1]
        h_atom2 = System([proton2])
        h_atom2.save()
        capped_sys4 = self.ic_capper.run(h_atom2)

        added_atoms4 = list(capped_sys4.atoms.filter(saved=False))
        self.assertEqual(len(added_atoms4), 1)

    def test_metal_center(self):
        """Metal coordination test, should not cap between fragments"""
        self.ic_capper = get_mod("RCaps")(
            "test_ic_capper", "Test ignore charged", ignore_charged=True
        )

        metal_center = System(
            [
                Atom("Mg", r=(0, 0, 0), charge=2),
            ]
        )
        metal_center.save()

        oh_group1 = System(
            [
                Atom("O", r=(-1.8, 0, 0.3), charge=-1),
                Atom("H", r=(-2.6, 0, 0.2)),
            ]
        )
        oh_group1.save()

        oh_group2 = System(
            [
                Atom("O", r=(1.8, 0, -0.3), charge=-1),
                Atom("H", r=(2.6, 0, 0.2)),
            ]
        )
        oh_group2.save()

        super_sys = System.merge(metal_center, oh_group1, oh_group2)
        self.ic_capper.setUp(super_sys)
        view = View(fragments=[metal_center, oh_group1, oh_group2])

        total_caps = 0
        for frag in view.fragments:
            self.ic_capper.run(frag)
            total_caps += frag.atoms.count(saved=False)

        # Assert that the atom list has NOT been changed
        self.assertEqual(total_caps, 0)

    def test_ethanol(self):
        sys = read_geometry("ethanol", path=ETHANOL_SYS_PATH)
        self.capper.setUp(sys)

        # Test capping the terminal CH3
        CH3 = System(sys.atoms.filter(ids=[1, 4, 5, 6]))
        CH3_cap = self.capper.run(CH3)
        added_atoms = list(CH3_cap.atoms.filter(saved=False))

        self.assertEqual(len(added_atoms), 1)
        self.assertDictEqual(added_atoms[0].meta, {"cap_for": [1, 2], "proxy_for": 2})
        self.assertAlmostEqual(Atom.distance(added_atoms[0], Atom.get(1)), 1.07, 4)

        # Test capping the intermediate CH2
        CH2 = System(sys.atoms.filter(ids=[2, 7, 8]))
        CH2_cap = self.capper.run(CH2)
        added_atoms = list(CH2_cap.atoms.filter(saved=False))

        self.assertEqual(len(added_atoms), 2)
        self.assertDictEqual(added_atoms[0].meta, {"cap_for": [2, 3], "proxy_for": 3})
        self.assertDictEqual(added_atoms[-1].meta, {"cap_for": [2, 1], "proxy_for": 1})
        self.assertAlmostEqual(
            Atom.distance(
                added_atoms[0],
                Atom.get(id=2),
            ),
            0.97,
            4,
        )
        self.assertAlmostEqual(
            Atom.distance(
                added_atoms[-1],
                Atom.get(id=2),
            ),
            1.07,
            4,
        )

        # Terminal OH group. Let's turn this to water
        OH = System(sys.atoms.filter(ids=[3, 9]))
        OH_cap = self.capper.run(OH)
        added_atoms = list(OH_cap.atoms.filter(saved=False))

        self.assertEqual(len(added_atoms), 1)
        self.assertDictEqual(added_atoms[0].meta, {"cap_for": [3, 2], "proxy_for": 2})
        self.assertAlmostEqual(
            Atom.distance(
                added_atoms[0],
                Atom.get(id=3),
            ),
            1.07,
            4,
        )

    def test_formaldehyde(self):
        sys = read_geometry("formaldehyde", path=FORMALDEHYDE_SYS_PATH)
        self.capper.setUp(sys)

        O = System(sys.atoms.filter(ids=[1]))  # O
        O_cap = self.capper.run(O)
        added_atoms = list(O.atoms.filter(saved=False))

        self.assertEqual(len(added_atoms), 1)
        self.assertAlmostEqual(Atom.distance(added_atoms[0], Atom.get(1)), 1.07, 4)

        CH2 = System(sys.atoms.filter(ids=[2, 3, 4]))
        CH2_cap = self.capper.run(CH2)
        added_atoms = list(CH2.atoms.filter(saved=False))

        self.assertEqual(len(added_atoms), 1)
        self.assertAlmostEqual(Atom.distance(added_atoms[0], Atom.get(2)), 0.97, 4)

    def test_two_AA(self):
        sys = read_geometry("two_aa", path=TWO_AA_SYS_PATH)
        self.capper.setUp(sys)

        p1 = System(sys.atoms.filter(ids=[1, 2, 3, 4, 5, 11, 12, 13, 14]))
        p1_cap = self.capper.run(p1)
        added_atoms = list(p1.atoms.filter(saved=False))

        self.assertEqual(len(added_atoms), 1)
        self.assertAlmostEqual(Atom.distance(added_atoms[0], Atom.get(5)), 1.019999, 4)

        p2 = System(sys.atoms.filter(ids=[6, 7, 8, 9, 10, 15, 16, 17, 18]))
        p2_cap = self.capper.run(p2)
        added_atoms = list(p2.atoms.filter(saved=False))

        self.assertEqual(len(added_atoms), 1)
        self.assertAlmostEqual(Atom.distance(added_atoms[0], Atom.get(6)), 0.97, 4)

    def test_CappingPDB(self):
        supersys = read_geometry("small_protien", path=SMALL_PROTIEN_SYS_PATH)
        self.capper.setUp(supersys)
        fragmenter = get_fragmenter("PDB")("pdb_frag", "")
        p_frags = fragmenter.primary_fragments(supersys)
        aux_frags = fragmenter.aux_fragments(p_frags, order=3)

        total_caps = 0
        for frag in aux_frags.fragments:
            self.capper.run(frag)
            total_caps += frag.atoms.count(saved=False)

        self.assertEqual(total_caps, 160)

    def test_CappingWater(self):
        supersys = read_geometry("water6", path=WATER6_SYS_PATH)
        self.capper.setUp(supersys)
        fragmenter = get_fragmenter("Water")("water_frag", "")
        p_frags = fragmenter.primary_fragments(supersys)
        aux_frags = fragmenter.aux_fragments(p_frags, order=3)

        total_caps = 0
        for frag in aux_frags.fragments:
            self.capper.run(frag)
            total_caps += frag.atoms.count(saved=False)

        self.assertEqual(total_caps, 0)
