from fragment.mods.distance import DistanceMod
from fragment.systems.models import Atom, System, View
from tests._util import DBTestCase


class TestDistance(DBTestCase):
    def setUp(self) -> None:
        systems = [System([Atom("H", r=(i, 0, 0))]) for i in range(4)]
        systems.append(System([Atom("H", r=(-4, 0, 0))]))
        self.s1, self.s2, self.s3, self.s4, self.s5 = systems

        for s in systems:
            s.save()

        self.supersystem = System.merge(*systems)
        self.view = View(systems, [0 for _ in systems])

    def test_max_distance(self):
        dfilter = DistanceMod(
            "df",
            "note",
            # Pydantic returns this with string mapping keys
            max_distance={"2": 2.5, "3": 2.5},
        )
        dfilter.setUp(self.supersystem, self.view, 3, 0)

        # Test two-body thresholds
        self.assertTrue(dfilter.run(self.s1, self.s2))
        self.assertTrue(dfilter.run(self.s1, self.s3))
        self.assertFalse(dfilter.run(self.s1, self.s4))

        # Test three-body thresholds
        self.assertFalse(dfilter.run(self.s1, self.s2, self.s4))  # Outside max
        self.assertTrue(dfilter.run(self.s1, self.s2, self.s3))  # Outside min

    def test_min_distance(self):
        dfilter = DistanceMod(
            "df",
            "note",
            min_distance={"2": 2.5, "3": 2.5},
        )
        dfilter.setUp(self.supersystem, self.view, 3, 0)

        # Test two-body thresholds
        self.assertTrue(dfilter.run(self.s1, self.s2))
        self.assertTrue(dfilter.run(self.s1, self.s3))
        self.assertFalse(dfilter.run(self.s1, self.s4))

        # Test three-body thresholds
        self.assertTrue(dfilter.run(self.s1, self.s2, self.s4))
        self.assertTrue(dfilter.run(self.s1, self.s2, self.s3))
        self.assertFalse(dfilter.run(self.s1, self.s4, self.s5))  # outisde min
