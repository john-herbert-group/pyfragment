import numpy as np

from fragment.examples.systems import WATER6_SYS_PATH
from fragment.fragmenting.water import WaterFragmenter
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import Atom, System
from fragment.util.spatial import (
    Delaunay_graph,
    neighbor_cloud,
    neighbors,
    system_Delaunay_graph,
    view_Delaunay_graph,
)
from tests._util import DBTestCase


class SpatialTestCases(DBTestCase):
    def setUp(self) -> None:
        self.outside = System(
            [
                Atom("H", r=(0.0, 0, 0)),  # ID 1
                Atom("H", r=(1.0, 0, 0)),  # ID 2
                Atom("H", r=(2.0, 0, 0)),  # ID 3
                Atom("H", r=(3.0, 0, 0)),  # ID 4
            ]
        )
        self.outside.save()

        self.inside = System(
            [self.outside.atoms[0], Atom("H", r=(4.0, 0, 0))]  # ID 1  # ID 5
        )
        self.inside.save()
        self.assertEqual(self.inside.atoms[0].x, 0.0)

    def test_neighbors(self) -> None:
        # Get all them atoms
        nn = neighbors(self.inside, self.outside, cutoff=10)
        self.assertEqual(len(nn), 2)

        # For atom 1 in inside
        row = nn[0]
        self.assertEqual(row[0], 0)
        self.assertListEqual(row[1], [1, 2, 3])
        self.assertListEqual(row[2], [1.0, 2.0, 3.0])

        # For atom 5 in inside
        row = nn[1]
        self.assertEqual(row[0], 1)
        self.assertListEqual(row[1], [3, 2, 1])
        self.assertListEqual(row[2], [1.0, 2.0, 3.0])

    def test_neighbor_cloud(self) -> None:
        cloud = neighbor_cloud(self.inside, self.outside, cutoff=10)
        self.assertSetEqual(set([a.id for a in cloud]), {2, 3, 4})

        cloud = neighbor_cloud(self.inside, self.outside, cutoff=1.5)
        self.assertSetEqual(set([a.id for a in cloud]), {2, 4})

        cloud = neighbor_cloud(self.inside, self.outside, cutoff=0.99)
        self.assertSetEqual(set([a.id for a in cloud]), set())

    def test_DGraph(self):
        """Uses unit cube"""
        G = Delaunay_graph(
            np.array(
                [
                    [0.0, 0.0, 0.0],
                    [1.0, 0.0, 0.0],
                    [0.0, 1.0, 0.0],
                    [1.0, 1.0, 0.0],
                    [0.0, 0.0, 1.0],
                    [1.0, 0.0, 1.0],
                    [0.0, 1.0, 1.0],
                    [1.0, 1.0, 1.0],
                ]
            )
        )

        self.assertSequenceEqual(set(G.nodes), set(range(0, 8)))
        self.assertSequenceEqual(
            set(G.edges),
            {
                (0, 4),
                (1, 0),
                (1, 3),
                (1, 4),
                (1, 5),
                (1, 7),
                (2, 0),
                (2, 1),
                (2, 3),
                (4, 5),
                (5, 7),
                (6, 0),
                (6, 1),
                (6, 2),
                (6, 3),
                (6, 4),
                (6, 5),
                (6, 7),
                (7, 3),
            },
        )

    def test_View_DGraph(self):
        sys = read_geometry("water6", path=WATER6_SYS_PATH)
        fragmenter = WaterFragmenter("water_frag", "water fragmenter")
        p_frags = fragmenter.primary_fragments(sys)
        G = view_Delaunay_graph(p_frags)

        self.assertEqual(len(G.nodes), 6)
        self.assertEqual(len(G.edges), 14)

        # Code for visually inspecting system
        # from matplotlib import pyplot as plt

        # ax = plt.axes(projection="3d")
        # plot_colors = {"H": "white", "O": "red", "N": "blue", "C": "grey"}
        # colors = [plot_colors[a.t] for a in sys.atoms]
        # sizes = [150 * np.pi * a.vdw_radius**2 for a in sys.atoms]
        # ax.scatter(
        #     sys.r_matrix[:, 0],
        #     sys.r_matrix[:, 1],
        #     sys.r_matrix[:, 2],
        #     c=colors,
        #     s=sizes,
        #     edgecolor="k",
        # )

        # for i, j in G.edges:
        #     _r = p_frags.COM_r_matrix[[i, j], :]
        #     ax.plot(_r[:, 0], _r[:, 1], _r[:, 2], color="blue")
        # plt.show()

    def test_system_DGraph(self):
        sys = read_geometry("water6", path=WATER6_SYS_PATH)
        G = system_Delaunay_graph(sys)
        self.assertEqual(len(G.nodes), 6 * 3)
        self.assertEqual(len(G.edges), 84)

        # Code for visually inspecting system
        # from matplotlib import pyplot as plt

        # ax = plt.axes(projection="3d")
        # plot_colors = {"H": "white", "O": "red", "N": "blue", "C": "grey"}
        # colors = [plot_colors[a.t] for a in sys.atoms]
        # sizes = [150 * np.pi * a.vdw_radius**2 for a in sys.atoms]
        # ax.scatter(
        #     sys.r_matrix[:, 0],
        #     sys.r_matrix[:, 1],
        #     sys.r_matrix[:, 2],
        #     c=colors,
        #     s=sizes,
        #     edgecolor="k",
        # )

        # for i, j in G.edges:
        #     _r = sys.r_matrix[[i, j], :]
        #     ax.plot(_r[:, 0], _r[:, 1], _r[:, 2], color="blue")
        # plt.show()
