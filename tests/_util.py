import unittest
from os import path
from tempfile import TemporaryDirectory
from typing import Any, List, Match, Optional, Tuple

from fragment.backends import templating as tmp
from fragment.backends.abstract import QMBackend, QMShellCommand
from fragment.backends.common import RunContext
from fragment.backends.exceptions import BackendRunException
from fragment.core.project import TransientDatabaseManager
from fragment.core.quickPIE import quickNodeList, quickNodeSet
from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.mods.abstract import AuxFragGenBase, ModBaseClass, PreTemplateBase
from fragment.properties.core import add_property
from fragment.properties.extraction import calc_property
from fragment.registry import REGISTRY
from fragment.systems.models import Atom, AtomType, System, SystemLabel, View


def QNList(*kl: Any) -> quickNodeList:
    return [frozenset(i) for i in kl]


def QNSet(*ks: Any) -> quickNodeSet:
    return {frozenset(i) for i in ks}


def QNSet_ints(*ks: Any) -> quickNodeSet:
    return {frozenset({i}) for i in ks}


class DBTestCase(unittest.TestCase):
    def __init_subclass__(
        cls,
        enable_tmpdir=False,
        enable_dbm=True,
    ) -> None:
        cls.enable_tmpdir = enable_tmpdir
        cls.enable_dbm = enable_dbm

    def run(self, *args, **kwargs):
        # Construct the supporting structure
        try:
            if self.enable_dbm:
                self.dbm = TransientDatabaseManager()
                self.dbm.init()
                self.dbm.start_session()
            if self.enable_tmpdir:
                self.tmpdir = TemporaryDirectory()

            return super().run(*args, **kwargs)
        finally:
            if self.enable_dbm:
                self.dbm.cleanup_session()
                self.dbm.cleanup_lifecycle()
            if self.enable_tmpdir:
                self.tmpdir.cleanup()


class TestFragmenter(AbstractFragmenter):
    """
    A simple fragmenter which does a boxcar overlap
    of atoms
    """

    def generate_primary_fragments(
        self, system: System, view: Optional[View] = None
    ) -> View:
        p_frags = super().generate_primary_fragments(system, view)
        atoms = list(system.atoms)
        p_frags.fragments.add(
            *(System([atoms[i], atoms[i + 1]]) for i in range(len(atoms) - 1))
        )
        return p_frags


REGISTRY.add("tests._util.TestFragmenter", "Fragmenter")


class TestMBEFragmenter(AbstractFragmenter):
    """
    A simple fragmenter which does a boxcar overlap
    of atoms
    """

    def generate_primary_fragments(
        self, system: System, view: Optional[View] = None
    ) -> View:
        p_frags = super().generate_primary_fragments(system, view)
        atoms = list(system.atoms)
        p_frags.fragments.add(*(System([a]) for a in atoms))
        return p_frags


REGISTRY.add("tests._util.TestMBEFragmenter", "Fragmenter")


class No123Mod(AuxFragGenBase):
    """Dummy mod to get rid of fragment with key (1,2,3)"""

    def run(self, *systems: System) -> bool:
        k = frozenset.union(*(s.key for s in systems))
        if k in {frozenset({1, 2, 3, 4}), frozenset({1, 2, 4, 5})}:
            return False
        return True


REGISTRY.add("tests._util.No123Mod", "Mod")


class TestCOMMod(PreTemplateBase):
    """
    Modifies the system by adding a Hydrogen at the COM
    """

    def run(self, frag: System) -> System:
        if frag.mass:
            r = frag.COM
        else:
            r = [0, 0, 0]

        cap_atom = Atom("H", r)
        frag.atoms.add(cap_atom)
        return frag


REGISTRY.add("tests._util.TestCOMMod", "Mod")


class UnsuportedMod(ModBaseClass):
    def run(self):
        pass


REGISTRY.add("tests._util.UnsuportedMod", "Mod")


add_property(name="a", t=float, use_coef=False, help="test property a")
add_property(name="b", t=float, use_coef=True, help="test property b")


class TestQMShellBackend(QMShellCommand):
    DEFAULT_TEMPLATE_PARAMS = {
        "template": tmp.DEFAULT_SYSTEM,
        "atom": tmp.DEFAULT_ATOM,
        "ghost_atom": tmp.DEFAULT_GHOST_ATOM,
    }
    FILE_MANIFEST = {
        "input": ".txt",
        "output": ".csv",
        "log": ".blog",
    }
    RUN_CMD = "python"

    def __init__(self, *args, fail=False, **kwargs) -> None:
        super().__init__(*args, fail=fail, **kwargs)
        self.fail = fail

    def get_run_cmd(self, ctx: RunContext) -> Tuple[str, List[str]]:
        # File path defined relative to this files
        script_path = path.join(
            path.split(__file__)[0], "backends", "QMBackend_runner.py"
        )
        args = [
            script_path,
            ctx.files["input"] if not self.fail else "paththatdoesnotexist",
            ctx.files["output"],
        ]
        return self.RUN_CMD, args

    @calc_property(source="re_file", patterns=[r"total energy: (\d+.\d+)"])
    def prop_total_energy(self, ctx, match: Match, _):
        return float(match.groups()[0])

    @calc_property()
    def prop_a(self, ctx):
        return 1.0

    @calc_property()
    def prop_b(self, ctx):
        return 2.0

    @calc_property()
    def prop_wall_time(self, ctx):
        return 1.0


REGISTRY.add("tests._util.TestQMShellBackend", "Backend")


class TestQMBackend(QMBackend):
    def __init__(self, *args, fail=False, energy=0.2, **kwargs) -> None:
        super().__init__(*args, fail=fail, energy=energy, **kwargs)
        self.fail = fail
        self.energy = energy

    def run_calc(self, ctx: RunContext):
        if self.fail:
            raise BackendRunException("Test exception")

    @calc_property()
    def prop_total_energy(self, ctx):
        return self.energy

    @calc_property()
    def prop_a(self, ctx):
        return 1.0

    @calc_property()
    def prop_b(self, ctx):
        return 2.0

    @calc_property()
    def prop_wall_time(self, ctx):
        return 1.0


REGISTRY.add("tests._util.TestQMBackend", "Backend")


def make_atoms(n, type=AtomType.PHYSICAL):
    if n == 0:
        return []

    atoms = [Atom("H", (float(i), float(i), float(i)), type=type) for i in range(n)]
    return atoms


def make_system(
    atoms=0,
    ghost_atoms=0,
    make_primary_frags=False,
    make_aux_frags=False,
) -> System:
    _atoms = make_atoms(atoms)
    _ghost_atoms = make_atoms(ghost_atoms, type=AtomType.GHOST)

    sys = System(
        _atoms + _ghost_atoms,
    )
    sys.save()

    SystemLabel.create(
        system=sys,
        name="test_system",
        note="auto-generated",
    )

    if make_primary_frags:
        fragmenter = TestFragmenter("test_frag", "", order=2, save_config=True)
        primary_frags = fragmenter.primary_fragments(sys)
        primary_frags.save()
    if make_primary_frags and make_aux_frags:
        aux_frags = fragmenter.aux_fragments(primary_frags, 2)
        aux_frags.save()

    return sys
