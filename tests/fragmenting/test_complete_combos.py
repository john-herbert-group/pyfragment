from fragment.core.PIE_common import Keys_from_NDList, NDs_from_KeySet
from fragment.core.quickPIE import compress_up
from fragment.fragmenting.abstract import complete_combos
from fragment.mods.abstract import AuxFragGenBase
from fragment.mods.distance import DistanceMod
from fragment.systems.models import Atom, System, View
from tests._util import DBTestCase, QNList, QNSet


class TestSupersystem(DBTestCase):
    def test_complete_combos(self):
        systems = [System([Atom("H", r=(i, 0, 0))]) for i in range(4)]

        for s in systems:
            s.save()

        supersystem = System.merge(*systems)
        view = View(systems, [0 for _ in systems])

        dfilter = DistanceMod("df", "", distance=2.01)
        filter = AuxFragGenBase.get_applicator([dfilter], supersystem, view, 3, 0)

        filtered = complete_combos(systems, 3, filter)
        self.assertSetEqual(
            filtered,
            QNSet((2, 4), (1, 2), (3, 4), (1, 2, 3), (4,), (1,), (2, 3, 4), (1, 3)),
        )

        self.assertSetEqual(
            Keys_from_NDList(compress_up(NDs_from_KeySet(filtered))),
            QNSet((1, 2, 3), (2, 3, 4)),
        )

        dfilter = DistanceMod("df", "", distance=9.0)
        filter = AuxFragGenBase.get_applicator([dfilter], supersystem, view, 3, 0)
        filtered = complete_combos(systems, 3, filter)
        self.assertSetEqual(filtered, QNSet((2, 3, 4), (1, 2, 3), (1, 2, 4), (1, 3, 4)))
