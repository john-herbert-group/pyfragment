from fragment.examples.systems import WATER6_SYS_PATH
from fragment.fragmenting.MBCP import MBCPFragmenter
from fragment.fragmenting.util import get_fragmenter
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import System
from tests._util import DBTestCase


class MBCPTestCases(DBTestCase):
    def setUp(self) -> None:
        self.mbcp2: MBCPFragmenter = get_fragmenter("MBCP")("MBCP", "")

    def test_mbcp_water6(self) -> None:
        supersys = read_geometry("water6", path=WATER6_SYS_PATH)
        aux_frags = self.mbcp2.aux_from_system(supersys, 2, save=True)

        # Supersystem, 6 waters, 30 CP terms
        self.assertEqual(System.select().count(), 1 + 6 + 5 * 6)

        for f in aux_frags.fragments:
            if len(f.key) == 3:
                self.assertEqual(f.viewcoef.coef, 5)
            if len(f.key) == 6:
                self.assertEqual(f.viewcoef.coef, -1)
