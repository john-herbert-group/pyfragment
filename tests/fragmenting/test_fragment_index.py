import unittest

from fragment.util.indexing import IDToIndex, KeyToIndex


class ToIndexTestCases(unittest.TestCase):
    def test_key(self):
        k = KeyToIndex([(1,), (2,)])
        self.assertEqual(k.size, 2)

        self.assertEqual(k((1,)), 0)
        self.assertEqual(k((2,)), 1)

        self.assertEqual(k.back(0), (1,))
        self.assertEqual(k.back(1), (2,))

    def test_id(self):
        k = IDToIndex([1, 2])
        self.assertEqual(k.size, 2)

        self.assertEqual(k(1), 0)
        self.assertEqual(k(2), 1)

        self.assertEqual(k.back(0), 1)
        self.assertEqual(k.back(1), 2)
