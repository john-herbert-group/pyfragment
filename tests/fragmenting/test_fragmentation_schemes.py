from copy import deepcopy
from itertools import combinations
from random import random, seed
from typing import Dict, Iterable, Tuple
from unittest.mock import patch

from fragment.core.quickPIE import quickNode, quickNodeSet
from fragment.fragmenting.abstract import PIETree
from fragment.systems.models import Atom, System, View, ViewType
from tests._util import DBTestCase, No123Mod, QNList, TestFragmenter, make_system


class FragmentedViewTestCases(DBTestCase):
    def setUp(self):
        self.sys = make_system(5)
        self.conf = TestFragmenter("test_fragmenter", "", save_config=True)
        self.p_frags = self.conf.primary_fragments(self.sys)

    def test_primary_fragments(self):
        self.assertEqual(len(self.p_frags.fragments), 4)
        self.assertListEqual(
            [f.key for f in self.p_frags.fragments],
            QNList((1, 2), (2, 3), (3, 4), (4, 5)),
        )

    def test_aux_fragments(self):
        fragments = {
            frozenset({1, 2, 3, 4}): (1, 3),
            frozenset({1, 2, 4, 5}): (1, 2),
            frozenset({2, 3, 4, 5}): (1, 3),
            frozenset({1, 2, 4}): (-1, 1),
            frozenset({2, 3, 4}): (-1, 2),
            frozenset({2, 4, 5}): (-1, 1),
            frozenset({2, 4}): (1, 0),
        }
        aux_frags = self.conf.aux_fragments(self.p_frags, order=2)

        for f in aux_frags.fragments:
            self.assertEqual(fragments[f.key][0], f.coef)
            self.assertEqual(fragments[f.key][1], f.viewcoef.order)
        self.assertSetEqual(aux_frags.fragments.keys, {k for k in fragments.keys()})

    @patch.object(PIETree, "from_MBE_primary_frags")
    def test_aux_MBE(self, mock):
        view = View(
            [System([Atom(id=i, t="H", r=(i, 0, 0))]) for i in range(1, 7)],
            supersystem=self.sys,
            type=ViewType.PRIMARY,
        )
        try:
            self.conf.aux_fragments(view, order=2)
        except:
            pass  # We are just testing that it uses MBE instead of GMBE
        self.assertTrue(mock.called)

    def test_TD_v_BU(self):
        no_123 = No123Mod("n124", "", save_config=True)

        # Make fragmenters
        TD = TestFragmenter(
            "top_down",
            "",
            combinator="top_down",
            save_config=True,
        )
        TD_mod = TestFragmenter(
            "top_down_moded",
            "",
            mods=[no_123.name],
            combinator="top_down",
            save_config=True,
        )
        BU = TestFragmenter(
            "bottom_up",
            "",
            combinator="bottom_up",
            save_config=True,
        )
        BU_mod = TestFragmenter(
            "bottom_up_moded",
            "",
            mods=[no_123.name],
            combinator="bottom_up",
            save_config=True,
        )
        BU_mod_MC = TestFragmenter(
            "bottom_up_moded_missing_children",
            "",
            mods=[no_123.name],
            combinator="bottom_up",
            bu_missing=1,
            save_config=True,
        )

        # Two appraoches should give the same results w/o filters
        tree_TD = TD.aux_fragments(self.p_frags, 3)
        tree_TD.dep_tree.clean_zeros()
        tree_BU = BU.aux_fragments(self.p_frags, 3)
        tree_BU.dep_tree.clean_zeros()

        self.assertTrue(
            PIETree.is_equal(
                tree_TD.dep_tree, tree_BU.dep_tree, check_coef=True, debug=True
            )
        )

        # Results will be different for modded approach
        tree_TD_mod = TD_mod.aux_fragments(self.p_frags, 3)
        tree_TD_mod.dep_tree.clean_zeros()
        tree_BU_mod = BU_mod.aux_fragments(self.p_frags, 3)
        tree_BU_mod.dep_tree.clean_zeros()
        tree_BU_mod_MC = BU_mod_MC.aux_fragments(self.p_frags, 3)
        tree_BU_mod_MC.dep_tree.clean_zeros()

        self.assertFalse(
            PIETree.is_equal(
                tree_TD_mod.dep_tree, tree_BU_mod.dep_tree, check_coef=True
            )
        )

        self.assertTrue(
            PIETree.is_equal(
                tree_TD_mod.dep_tree, tree_BU_mod_MC.dep_tree, check_coef=True
            )
        )

        fragments = {
            frozenset({2, 3, 4, 5}): (1, 3),
            frozenset({1, 2, 3}): (1, 2),
            frozenset({2, 3}): (-1, 1),
        }

        for f in tree_BU_mod.fragments:
            if f.viewcoef.coef == 0:
                continue
            self.assertEqual(fragments[f.key][0], f.coef)
            self.assertEqual(fragments[f.key][1], f.viewcoef.order)
