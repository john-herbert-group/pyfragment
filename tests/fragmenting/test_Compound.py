from itertools import combinations

from fragment.examples.systems import F_WATER10_FRAG_PATH
from fragment.fragmenting.util import get_fragmenter
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import AtomType
from tests._util import DBTestCase


class TestCompound(DBTestCase):
    def setUp(self) -> None:
        self.sys = read_geometry("Fw10", path=F_WATER10_FRAG_PATH)

        get_fragmenter("Water")("water", "water fragmenter").save_config()
        get_fragmenter("Compound")
        self.fragmenter = get_fragmenter("Compound")(
            "explicit_water",
            "splits systems into fragments with explicit water",
            fragments={"default": "supersystem", "last": "water"},
        )

    def test_primary_fragments(self):
        p_frags = self.fragmenter.primary_fragments(self.sys)
        self.assertEqual(len(p_frags.fragments), 11)

        # Check each is unique
        for f1, f2 in combinations(p_frags.fragments, 2):
            self.assertEqual(
                len(set.union(set(f1.key), set(f2.key))),
                f1.atoms.count(type=AtomType.PHYSICAL)
                + f2.atoms.count(type=AtomType.PHYSICAL),
            )

    def test_aux_fragments(self):
        p_frags = self.fragmenter.primary_fragments(self.sys)
        aux_frags = self.fragmenter.aux_fragments(p_frags, 4)

        self.assertEqual(len(aux_frags.fragments), 561)
