import networkx as nx

from fragment.core.PIETree import ROOT
from fragment.examples.systems import WATER6_SYS_PATH
from fragment.fragmenting.BSSE_balance import BSSEBalancedFragmenter
from fragment.fragmenting.util import get_fragmenter
from fragment.systems.geometry_readers.util import read_geometry
from tests._util import DBTestCase


class BSSEBalancedTestCases(DBTestCase):
    def setUp(self) -> None:
        self.balancer: BSSEBalancedFragmenter = get_fragmenter("BSSEBalanced")(
            "balancer", ""
        )

    def test_bsse_balancer_water6(self) -> None:
        supersys = read_geometry("water6", path=WATER6_SYS_PATH)
        aux_frags = self.balancer.aux_from_system(supersys, 3, save=True)

        ss = aux_frags.supersystem._key
        counts = aux_frags.dep_tree.count_members()
        for s in ss:
            self.assertEqual(counts[s], 1)

        monomer_count = 0
        dimer_count = 0
        trimer_count = 0

        for l1 in aux_frags.dep_tree.tree[ROOT]:
            len_l1 = len(l1)
            desc = list(nx.descendants(aux_frags.dep_tree.tree, l1))

            if len_l1 == 3:
                monomer_count += 1
                self.assertEqual(len(desc), 0)
            elif len_l1 == 6:
                dimer_count += 1
                self.assertEqual(len(desc), 2)
            elif len_l1 == 9:
                trimer_count += 1
                self.assertEqual(len(desc), 6)

            for d in nx.descendants(aux_frags.dep_tree.tree, l1):
                self.assertEqual(len(d), len_l1)

        # This fragmentation method is extrememly inefficient
        # Each calculation in the expansion is it's own fragment
        for _, d in aux_frags.dep_tree:
            self.assertEqual(abs(d["coef"]), 1)

        self.assertEqual(monomer_count, 6)
        self.assertEqual(dimer_count, 15)
        self.assertEqual(trimer_count, 20)
