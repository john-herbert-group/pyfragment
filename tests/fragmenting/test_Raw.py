from itertools import combinations

from fragment.examples.systems import WATER6_FRAG_PATH
from fragment.fragmenting.util import get_fragmenter
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import AtomType
from tests._util import DBTestCase


class TestWater(DBTestCase):
    def setUp(self) -> None:
        self.sys = read_geometry("water6", path=WATER6_FRAG_PATH)
        self.fragmenter = get_fragmenter("Raw")(
            "raw_fragmenter", "splits systems into explicit fragments"
        )

    def test_primary_fragments(self):
        p_frags = self.fragmenter.primary_fragments(self.sys)
        self.assertEqual(len(p_frags.fragments), 6)

        # Check each is unique
        for f1, f2 in combinations(p_frags.fragments, 2):
            self.assertEqual(
                len(set.union(set(f1.key), set(f2.key))),
                f1.atoms.count(type=AtomType.PHYSICAL)
                + f2.atoms.count(type=AtomType.PHYSICAL),
            )

    def test_aux_fragments(self):
        p_frags = self.fragmenter.primary_fragments(self.sys)
        aux_frags = self.fragmenter.aux_fragments(p_frags, 4)

        self.assertEqual(len(aux_frags.fragments), 56)
        for f in aux_frags.fragments:
            atom_count = len(f.atoms)
            if atom_count == 12:
                self.assertEqual(f.coef, 1)
            elif atom_count == 9:
                self.assertEqual(f.coef, -2)
            elif atom_count == 6:
                self.assertEqual(f.coef, 3)
            elif atom_count == 3:
                self.assertEqual(f.coef, -4)
