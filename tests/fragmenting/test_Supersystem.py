from fragment.examples.systems import SMALL_PROTIEN_SYS_PATH
from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.systems.geometry_readers.util import read_geometry
from tests._util import DBTestCase


class TestSupersystem(DBTestCase):
    def setUp(self):
        self.sys = read_geometry("small_protien", path=SMALL_PROTIEN_SYS_PATH)
        # Supersystem fragmenter is automatically added
        # self.fragmenter = get_fragmenter('Supersystem')('supersystem', '', order=1)
        self.fragmenter = AbstractFragmenter.obj_from_name("supersystem")
        self.p_frags = self.fragmenter.primary_fragments(self.sys)
        self.aux_frags = self.fragmenter.aux_fragments(self.p_frags, 1)

    def test_primary_fragments(self):
        frag = list(self.p_frags.fragments)[0]
        self.assertEqual(len(self.p_frags.fragments), 1)
        self.assertEqual(len(frag.atoms), 51)

    def test_aux_fragments(self):
        frag = list(self.aux_frags.fragments)[0]
        self.assertEqual(len(self.aux_frags.fragments), 1)
        self.assertEqual(len(frag.atoms), 51)
