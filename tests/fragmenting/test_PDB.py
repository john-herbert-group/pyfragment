from unittest.case import skip

from fragment.examples.systems import SMALL_PROTIEN_SYS_PATH
from fragment.fragmenting.util import get_fragmenter
from fragment.systems.geometry_readers.util import read_geometry
from tests._util import DBTestCase, QNList

FRAGMENT_LENGTHS = [4, 8, 7, 9, 1, 9, 13]


class TestPDB(DBTestCase):
    def setUp(self):
        self.sys = read_geometry("small_protien", path=SMALL_PROTIEN_SYS_PATH)
        self.fragmenter = get_fragmenter("PDB")("pdb_frag", "")
        self.p_frags = self.fragmenter.primary_fragments(self.sys)

    def test_primary_fragments(self):
        self.assertEqual(len(self.p_frags.fragments), 7)

        # print([len(f.atoms) for f in self.p_frags.fragments])
        for f, length in zip(self.p_frags.fragments, FRAGMENT_LENGTHS):
            self.assertEqual(len(f.atoms), length)

    def test_aux_fragments(self):
        aux_frags = self.fragmenter.aux_fragments(self.p_frags, order=2)
        self.assertEqual(len(aux_frags.fragments), 28)

    def test_window(self):
        fragmenter = get_fragmenter("PDB")("windowed_frag", "", order=2, window=2)
        p_frags = fragmenter.primary_fragments(self.sys)

        self.assertEqual(len(p_frags.fragments), 6)

        frag_atom_ids = [f.key for f in p_frags.fragments]
        # print(frag_atom_ids)
        self.assertListEqual(
            frag_atom_ids,
            QNList(
                (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                (5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19),
                (13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28),
                (20, 21, 22, 23, 24, 25, 26, 27, 28, 29),
                (29, 30, 31, 32, 33, 34, 35, 36, 37, 38),
                (
                    30,
                    31,
                    32,
                    33,
                    34,
                    35,
                    36,
                    37,
                    38,
                    39,
                    40,
                    41,
                    42,
                    43,
                    44,
                    45,
                    46,
                    47,
                    48,
                    49,
                    50,
                    51,
                ),
            ),
        )

    @skip("We need to change this interface")
    def test_charged_atoms(self):
        fragmenter = get_fragmenter("PDB")("pdb", "", charged_atoms={38: 1})
        fragmenter.primary_fragments(self.sys)
        # need to check charge/multiplicty value in fragment
        charge = 1
        self.assertEqual(charge, view.primary_fragments[4].charge)
