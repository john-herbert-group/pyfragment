from peewee import IntegrityError

from fragment.conf.models import ConfigModel
from fragment.conf.util import SavableConfig
from fragment.registry import REGISTRY
from tests._util import DBTestCase


class TestConf(SavableConfig):
    CONFIG_TYPE = "test_config"

    def __init__(self, name, note, *args, test_param, **kwargs) -> None:
        self.test_param = test_param
        super().__init__(name, note, *args, test_param=test_param, **kwargs)


REGISTRY.add("tests.conf.test_confs.TestConf", "Fragmenter")


class ConfigModelTestCases(DBTestCase):
    def setUp(self):
        self.tc = ConfigModel.create(
            name="test_conf",
            config_type="test",
            config_class="test",
            args=[1, 2, 3],
            kwargs={"a": 1, "b": 2},
        )

    def test_ConfigModel(self):
        conf = ConfigModel.get(name="test_conf")
        self.assertEqual("test_conf", conf.name)
        self.assertListEqual([1, 2, 3], conf.args)
        self.assertDictEqual({"a": 1, "b": 2}, conf.kwargs)
        self.assertEqual("test", conf.config_class)
        self.assertEqual("test", conf.config_type)

    def test_unique(self):
        with self.assertRaises(IntegrityError):
            ConfigModel.create(
                name="test_conf",
                config_type="test",
                config_class="test",
                args=[],
                kwargs={},
            )


class SavableConfigTestCases(DBTestCase):
    def setUp(self) -> None:
        self.conf = TestConf("test", "this is a test", "arg1", test_param=2)
        self.conf.save_config()

    def test_create(self):
        # Extract underlying model and see that it's saved
        conf = ConfigModel.get(name="test")
        self.assertEqual("test", conf.name)
        self.assertListEqual(["arg1"], conf.args)
        self.assertDictEqual({"test_param": 2}, conf.kwargs)

    def test_get_config(self):
        restored_conf = SavableConfig.obj_from_name("test", "test_config")

        self.assertTrue(isinstance(restored_conf, TestConf))
        self.assertEqual(self.conf.conf_id, restored_conf.conf_id)
        self.assertEqual(self.conf.name, restored_conf.name)
        self.assertEqual(self.conf.note, restored_conf.note)
        self.assertListEqual(self.conf.args, restored_conf.args)
        self.assertDictEqual(self.conf.kwargs, restored_conf.kwargs)

        # Test resave
        self.conf.args = [1, 2]
        self.conf.save_config()

        restored_conf = SavableConfig.obj_from_name("test", "test_config")
        self.assertEqual(self.conf.conf_id, restored_conf.conf_id)
        self.assertListEqual(self.conf.args, restored_conf.args)
