from fragment.systems.models import Atom, AtomToAtom, AtomType
from tests._util import DBTestCase


class AtomToAtomTestCases(DBTestCase):
    def setUp(self) -> None:
        self.atom: Atom = Atom.create(t="H", x=0, y=0, z=0)
        self.atom.save()

    def test_convert_to(self) -> None:
        a2 = self.atom.convert_to(AtomType.GHOST)
        self.assertEqual(AtomToAtom.select().count(), 1)
        self.assertEqual(a2.type, AtomType.GHOST)
        self.assertNotEqual(self.atom.id, a2.id)

        a2_again = self.atom.convert_to(AtomType.GHOST)
        self.assertEqual(a2.id, a2_again.id)

        a1 = self.atom.convert_to(AtomType.PHYSICAL)
        self.assertEqual(a1.id, self.atom.id)
