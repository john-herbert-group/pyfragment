from fragment.systems.models import Atom, AtomType
from tests._util import DBTestCase, make_atoms


class AtomTestCases(DBTestCase):
    def setUp(self) -> None:
        Atom.bulk_create(make_atoms(3))
        Atom.bulk_create(make_atoms(1, type=AtomType.GHOST))

    def test_atom(self):
        atom = Atom.select()[0]
        self.assertEqual(atom.t, "H")
        self.assertEqual(atom.x, 0)
        self.assertEqual(atom.type, AtomType.PHYSICAL)
        self.assertEqual(Atom.select().count(), 4)

    def test_distance(self):
        self.assertAlmostEqual(Atom.distance(Atom.get(1), Atom.get(2)), 1.73, 2)

    def test_ghost(self):
        ghost = Atom.select().where(Atom.type == AtomType.GHOST)[0]
        self.assertEqual(ghost.type, AtomType.GHOST)

    def test_bulk_add(self):
        atoms = [Atom("H", (0, 0, 0)), Atom.get(2), Atom("H", (1, 1, 1))]

        Atom.save_many(atoms)

        self.assertListEqual([a.id for a in atoms], [5, 2, 6])
