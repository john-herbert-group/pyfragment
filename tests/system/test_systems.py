from peewee import IntegrityError, Value

from fragment.core.util import reset_cache
from fragment.examples.systems import (
    BENZENE_SYS_PATH,
    ETHANE_SYS_PATH,
    ETHANOL_SYS_PATH,
)
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import Atom, AtomManager, AtomToSystem, AtomType, System
from tests._util import DBTestCase, make_system


class AtomManagerTestCases(DBTestCase):
    def setUp(self):
        # Create the atoms of different typs
        atoms = [
            Atom.create(t="G", x=0, y=0, z=0, type=AtomType.GHOST),
            Atom.create(t="P", x=0, y=0, z=0, type=AtomType.PHYSICAL),
            Atom.create(t="C", x=0, y=0, z=0, type=AtomType.PROXY),
        ]

        # Create the system
        sys = System()
        sys.key = (1, 2, 3)  # Do this manually or this code will depend on itself
        sys.save()

        # Create the relationships
        AtomToSystem.insert_many(
            (
                (
                    a.id,
                    sys.id,
                )
                for a in atoms
            ),
            fields=[AtomToSystem.atom_id, AtomToSystem.system_id],
        ).execute()

    def test_init(self):
        # Test instantiated system
        m = AtomManager(System.get(1))
        self.assertEqual(len(m), 3)

        # Test uninstantiated system
        m = AtomManager(System())
        self.assertEqual(len(m), 0)

    def test_add_atoms(self):
        # Test adding a new_atom
        m = AtomManager(System.get(1))
        m.add(Atom.get(1))  # Let's not add duplicates
        self.assertEqual(len(m), 3)

        # Add an existing atom
        atom = Atom("H", (0, 0, 0))
        atom.save()
        m.add(atom)
        self.assertEqual(len(m), 4)
        self.assertEqual(len(m.new_atoms), 0)

        # Add a new atom
        m.add(Atom("H", (1, 1, 1)))
        self.assertEqual(len(m.new_atoms), 1)

    def test_discard(self):
        m = AtomManager(System.get(1))
        m.discard(Atom.get(1))

        # Make sure atom was removed from the DB too
        self.assertEqual(len(m), 2)
        self.assertEqual(len(AtomManager(System.get(1))), 2)

    def test_discard__empty(self):
        new_atom = Atom("H", (0, 0, 0))
        m = AtomManager(System())
        m.add(Atom("H", (0, 0, 0)))
        m.add(new_atom)
        m.add(Atom("H", (0, 0, 0)))
        self.assertEqual(len(m), 3)
        m.discard(new_atom)
        # Check that the database is still intact
        self.assertEqual(Atom.select().count(), 3)
        self.assertEqual(AtomToSystem.select().count(), 3)
        # Check that the deletion went to plan
        self.assertEqual(len(m), 2)
        self.assertEqual(len(m.new_atoms), 2)

    def test_iter(self):
        m = AtomManager(System.get(1))
        m.add(Atom("H", (0, 0, 0)))
        m.add(Atom("H", (0, 0, 0), type=AtomType.GHOST))
        self.assertListEqual([a.id for a in m], [2, None, 3, 1, None])

    def test_key(self):
        m = AtomManager(System.get(1))
        self.assertSetEqual(m.make_key(), {1, 2, 3})
        m.add(Atom())
        with self.assertRaises(AttributeError):
            self.assertEqual(m.make_key(), None)

    def test_filter(self):
        m = AtomManager(System.get(1))
        # Test type selection
        self.assertListEqual([a.id for a in m.filter(type=AtomType.PHYSICAL)], [2])

        # Test ids
        self.assertListEqual([a.id for a in m.filter(ids=[1, 2])], [2, 1])

        # Test saved
        m.add(Atom())
        self.assertListEqual([a.id for a in m.filter(saved=False)], [None])

    def test_get_atoms(self):
        m = AtomManager(System.get(1))
        self.assertEqual(m[0].id, 1)
        self.assertListEqual([a.id for a in m[1:3]], [2, 3])
        self.assertListEqual([a.id for a in m[0, 2]], [1, 3])


class SystemTestCases(DBTestCase):
    def setUp(self):
        self.sys = make_system(atoms=4, ghost_atoms=3)
        self.sys.save()
        self.atoms = list(Atom.select())

    def test_system(self):
        g0 = System([self.atoms[0], self.atoms[1], self.atoms[2]])
        self.assertSetEqual(g0.key, {1, 2, 3})

        g1 = System([self.atoms[0], self.atoms[1]])
        self.assertSetEqual(g1.key, {1, 2})

        g2 = System([self.atoms[2], self.atoms[1]])
        self.assertSetEqual(g2.key, {2, 3})

        # Make sure that an atom cannot be added twice
        g1.atoms.add(self.atoms[0])
        self.assertSetEqual(g1.key, {1, 2})

        self.assertTrue(len(g0.atoms), 3)
        self.assertTrue(len(g1.atoms), 2)
        self.assertTrue(len(g2.atoms), 2)

    def test_retrieve(self):
        sys: System = System.get(1)
        self.assertSetEqual(sys.key, {1, 2, 3, 4, 5, 6, 7})
        self.assertEqual(len(sys.atoms), 7)

    def test_unique_key(self):
        new_sys = System(self.atoms)
        with self.assertRaises(IntegrityError):
            new_sys.save()

    def test_get_by_key(self):
        with self.assertRaises(System.DoesNotExist):
            sys = System.get(System._key == Value([1, 2], unpack=False))
        sys = System.get(System._key == Value([1, 2, 3, 4, 5, 6, 7], unpack=False))
        self.assertEqual(sys.id, 1)

    def test_add_atoms(self):
        self.assertEqual(len(self.sys.atoms), 7)
        self.sys.atoms.add(Atom("H", (0, 1, 2)))
        self.assertEqual(len(self.sys.atoms), 8)
        with self.assertRaises(AttributeError):
            self.sys.key

        self.sys.save()
        self.assertSetEqual(self.sys.key, {1, 2, 3, 4, 5, 6, 7, 8})

        self.sys.atoms.add(Atom("H", (0, 1, 2)))
        with self.assertRaises(AttributeError):
            self.sys.key

    def test_properties(self):
        self.assertEqual(0, self.sys.charge)
        self.assertEqual(1, self.sys.multiplicity)
        self.assertListEqual([1.5, 1.5, 1.5], list(self.sys.COM))

        # Test changing charge of atom
        a = self.sys.atoms._atoms[
            0
        ]  # This is testing. Would be taboo in normal operation
        a.charge += 1
        # This may be an issue that needs addressing.
        reset_cache(self.sys, keep=lambda k: getattr(self.sys, k) is self.sys.atoms)
        self.assertEqual(self.sys.charge, 1)
        self.assertEqual(self.sys.multiplicity, 2)

        new_a = Atom("H", (0, 0, 0))
        self.sys.atoms.add(new_a)
        self.assertListEqual([1.2, 1.2, 1.2], list(self.sys.COM))

    def test_canonical_transform(self):
        sys = read_geometry("ETH", "", path=ETHANOL_SYS_PATH)
        I = sys.moment_of_inertia_tensor()
        self.assertListEqual(
            I.reshape(-1).tolist(),
            [
                51.67323440799469,
                -15.543370921872675,
                -1.5777349738709088,
                -15.543370921872675,
                22.14725647283357,
                4.741983228525845,
                -1.5777349738709088,
                4.741983228525845,
                55.64730941530539,
            ],
        )

        r = sys.canonical_coords()
        self.assertListEqual(
            r.reshape(-1).tolist(),
            [
                -1.2592126907330499,
                -0.25873868975283465,
                -0.007338009553014893,
                0.01823241078814772,
                0.5704755387451805,
                0.02443756204340739,
                1.1431487747993838,
                -0.25998737323214904,
                -0.06969108214093739,
                -2.1424286360333813,
                0.41194671405130073,
                0.047419285952016374,
                -1.2829527976879669,
                -0.9557641081067346,
                0.8569323709183048,
                -1.308805299066733,
                -0.8444976905220195,
                -0.9496342173219725,
                0.06268459814245539,
                1.1840502212192163,
                0.952844173570961,
                0.02240094036388603,
                1.271962987446489,
                -0.8363442519989002,
                1.2826401692601976,
                -0.6533070981159552,
                0.83122924381514,
            ],
        )
        # print(sys.r_matrix)

    def test_r_matrix(self):
        self.assertListEqual(
            self.sys.r_matrix.reshape(-1).tolist(),
            [
                0.0,
                0.0,
                0.0,
                1.0,
                1.0,
                1.0,
                2.0,
                2.0,
                2.0,
                3.0,
                3.0,
                3.0,
                0.0,
                0.0,
                0.0,
                1.0,
                1.0,
                1.0,
                2.0,
                2.0,
                2.0,
            ],
        )

    def test_masses(self):
        self.assertListEqual(list(self.sys.masses), [1.00782503223] * 4 + [0.0] * 3)

    def test_kd_tree(self):
        tree = self.sys.KDTree
        dm = tree.query_ball_point([0.0, 0.0, 0.0], 4.0)
        self.assertEqual(dm, [0, 1, 2, 4, 5, 6])

    def test_save_many(self):
        # Create a sample system
        sys = System([Atom.get(1)])
        sys.save()

        systems = [
            System(Atom.select()[:]),  # Duplicate of an existing system
            System([Atom.get(1), Atom("H", (0, 0, 0))]),  # New system
            sys,  # An initialized and existing system,
            System([Atom.get(2)]),  # New system
            System([Atom.get(2)]),  # Duplicate of new system
        ]

        System.save_many(systems)

        self.assertListEqual([s.id for s in systems], [1, 3, 2, 4, 4])
