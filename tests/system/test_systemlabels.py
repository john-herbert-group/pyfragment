from fragment.core.util import ValidationError
from fragment.systems.models import SystemLabel
from tests._util import DBTestCase, make_system


class SystemLabelTestCases(DBTestCase):
    def setUp(self):
        self.sys = make_system(atoms=3)
        self.sys.save()

    def test_supersystem(self):
        self.sys
        ss1 = SystemLabel(system=self.sys, name="test_name", note="Blah")

        with self.assertRaises(ValidationError):
            ss2 = SystemLabel(system=self.sys, name="test/_name*(#!")
            ss2.save()

        # Now check and retrieve this
        ss1.save()
        ss1 = SystemLabel.get(ss1.id)
        self.assertEqual(ss1.name, "test_name")
