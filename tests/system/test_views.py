from itertools import combinations
from os import environ
from unittest.case import skip, skipIf

from fragment.core.legacy_PIE import PIEPlan
from fragment.core.legacy_PIETree import make_PIETree
from fragment.core.PIETree import tree_equal
from fragment.systems.models import (
    Atom,
    AtomToSystem,
    FragmentManager,
    System,
    View,
    ViewCoef,
    ViewType,
)
from tests._util import DBTestCase, QNList, TestMBEFragmenter, make_system


class FragmentManagerTestCases(DBTestCase):
    def setUp(self):
        sys1 = System(atoms=[Atom("H", (0, 0, 0))])
        sys2 = System(atoms=[Atom("H", (0, 0, 0))])
        sys3 = System(atoms=[Atom("H", (0, 0, 0))])
        sys1.save()
        sys2.save()
        sys3.save()
        view = View(name="name", order=1, type=ViewType.PRIMARY, fragmenter_id=0)
        view.save()
        ViewCoef.create(view=view, fragment=sys1, coef=1)
        ViewCoef.create(view=view, fragment=sys2, coef=1)
        ViewCoef.create(view=view, fragment=sys3, coef=0)
        self.fm = FragmentManager(view)

    def test_init(self):
        self.assertEqual(len(self.fm), 3)

    def test_add(self):
        new_sys = System(atoms=[Atom("H", (0, 0, 0))])
        self.fm.add(System.get(1), coefs=[1])
        self.fm.add(System.get(1), coefs=[1])
        self.fm.add(new_sys, coefs=[1])

        self.assertListEqual(list(self.fm.new_systems), [new_sys])
        new_sys.save()
        new_sys.introspect_key()
        self.assertListEqual(self.fm.new_view_coefs, [(1, 1, 2, None), (1, 4, 1, None)])

    def test_discard(self):
        new_system = System()
        self.fm.add(new_system, coefs=[1])
        self.assertEqual(len(self.fm), 4)

        self.fm.discard(new_system)
        self.assertEqual(len(self.fm), 3)

        self.fm.discard(System.get(1))
        self.assertEqual(len(self.fm), 2)

    def test_coef(self):
        self.fm.add(System(), System(), coefs=[2, 3])
        self.assertListEqual([s.viewcoef.coef for s in self.fm], [1, 1, 0, 2, 3])

    def test_dup_resolution(self):
        view = View.get(1)
        view.fragments.add(System.get(1), coefs=[1])
        view.save()

        view = View.get(1)
        # sys1 should be in the view twice
        self.assertListEqual([f.viewcoef.coef for f in view.fragments], [2, 1, 0])

    @skip("Depricated")
    def test_rm_zeros(self):
        self.assertEqual(len(self.fm), 2)  # Contains 3 but coef == 0

        new_sys = System()
        zero_sys = System()
        self.fm.add(new_sys, zero_sys, coefs=[1, 0])

        self.assertEqual(len(self.fm), 3)  # Adds two but one has coef == 0
        self.assertListEqual(
            [s for s in self.fm],
            [System.get(1), System.get(2), new_sys],
        )

        self.assertListEqual(
            [s for s in self.fm.all],
            [System.get(1), System.get(2), System.get(3), new_sys, zero_sys],
        )


class ViewTestCases(DBTestCase):
    def setUp(self):
        self.sys = make_system(atoms=5, make_primary_frags=True, make_aux_frags=True)
        self.view: View = View.get(1)

    def test_COM_r_matrix(self):
        self.assertListEqual(
            list(self.view.COM_r_matrix.reshape(1, 12)[0]),
            [0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 2.5, 2.5, 2.5, 3.5, 3.5, 3.5],
        )

    def test_create(self):
        """
        Much of this logic is handled by the fragment manager
        """
        view = View(name="test_view", supersystem=self.sys, type=ViewType.PRIMARY)
        view.fragments.add(self.sys, coefs=[1])
        self.assertEqual(len(view.fragments), 1)

    def test_view(self):
        self.assertEqual(len(self.view.supersystem.atoms), 5)
        self.assertListEqual(
            [frag.key for frag in self.view.fragments],
            QNList((1, 2), (2, 3), (3, 4), (4, 5)),
        )

    def test_PIE_methods(self):
        with self.assertRaises(AttributeError):
            self.view.PIEPlan

        aux_view: View = View.get(type=ViewType.AUXILIARY)
        parents = self.view.fragments.sets

        # Retreive the trees from the database
        plan_dict = aux_view.quickPlan

        # Do the calculations manually
        actual_plan = PIEPlan()
        for frags in combinations(self.view.fragments, aux_view.order):
            actual_plan.add_node(set.union(*(set(f.key) for f in frags)))

        # Check that we got the same coefs
        act_plan_dict = {
            frozenset(p.key): p.coef for p in actual_plan.nodes.values() if p.coef != 0
        }
        self.assertDictEqual(plan_dict, act_plan_dict)

        # Check on the PIE Tree
        _act_tree = make_PIETree(actual_plan, parents)

        # Compatablity code for old PIE code (should add this to PIE legacy)
        import networkx as nx

        act_tree = nx.DiGraph()
        act_tree.add_nodes_from(
            ((frozenset(n) if n else 0, d) for n, d in _act_tree.nodes(data=True))
        )

        act_tree.add_edges_from(
            ((frozenset(u) if u else 0, frozenset(v)) for u, v in _act_tree.edges)
        )
        fresh_tree = aux_view._make_dep_tree().tree
        cached_tree = aux_view._get_dep_tree().tree

        self.assertTrue(tree_equal(act_tree, fresh_tree, check_coefs=True, debug=True))
        self.assertTrue(
            tree_equal(fresh_tree, cached_tree, check_coefs=True, debug=True)
        )

        keys = (
            ViewCoef.select(System._key)
            .join(System)
            .where(ViewCoef.view == aux_view, ViewCoef.coef != 0)
        )

        for (k,) in keys.tuples():
            self.assertDictEqual(fresh_tree.nodes[k], cached_tree.nodes[k])


@skipIf(not "FULL_TEST" in environ, "Disabled for general testing")
class StressTest(DBTestCase):
    def test_stress_test(self):
        # import logging
        # logger = logging.getLogger('peewee')
        # logger.addHandler(logging.StreamHandler())
        # logger.setLevel(logging.DEBUG)
        from time import time

        start = time()
        sys = make_system(30)
        print()
        print(f"Atoms: {Atom.select().count()}")

        fragmenter = TestMBEFragmenter("test", "", save_config=True)
        p_frags = fragmenter.primary_fragments(sys)
        p_frags.save()
        print(f"Save time: {time() - start:.4f}")
        start = time()
        print(f"P Fragments: {len(p_frags.fragments)} ({System.select().count()})")

        aux_frags = fragmenter.aux_fragments(p_frags, order=4)
        print(f"A Fragments: {System.select().count()}")
        print(f"Generate Time: {time() - start:.4f}")
        start = time()

        aux_frags.save()
        print(f"A Fragments: {System.select().count()}")
        print(f"Saveing Time: {time() - start:.4f}")
        start = time()
        print(f"Atom to System Rels: {AtomToSystem.select().count()}")
        print(f"Saveing Time: {time() - start:.4f}")
        start = time()
        print()
