from os import path

from fragment.examples.systems import (
    SMALL_PROTIEN_SYS_PATH,
    WATER6_FRAG_PATH,
    WATER6_SYS_PATH,
)
from fragment.systems.geometry_readers.util import SystemExists, read_geometry
from fragment.systems.models import System
from tests._util import DBTestCase


class FileImporterTestCases(DBTestCase, enable_tmpdir=True):
    def test_xyz(self):
        sys = read_geometry("water6", path=WATER6_SYS_PATH)
        self.assertListEqual(
            [a.t for a in sys.atoms],
            [
                "O",
                "H",
                "H",
                "O",
                "H",
                "H",
                "O",
                "H",
                "H",
                "O",
                "H",
                "H",
                "O",
                "H",
                "H",
                "O",
                "H",
                "H",
            ],
        )

    def test_xyz_charge(self):
        read_geometry("water6", path=WATER6_SYS_PATH, charges={1: -1, 4: 2}, save=True)
        sys = System.get(1)
        atoms = list(sys.atoms)
        self.assertEqual(atoms[0].charge, -1)
        self.assertEqual(atoms[3].charge, 2)
        self.assertEqual(sys.charge, 1)

    def test_pdb(self):
        sys = read_geometry("small_protien", path=SMALL_PROTIEN_SYS_PATH)
        atom_types = [a.t for a in sys.atoms]
        res_id = [a.meta["frag_group"] for a in sys.atoms]
        res_types = [a.meta["residue_type"] for a in sys.atoms]

        # Test fixtures
        # Atom Types
        self.assertEqual(
            atom_types,
            [
                "N",
                "C",
                "C",
                "O",
                "N",
                "C",
                "C",
                "O",
                "C",
                "C",
                "O",
                "O",
                "N",
                "C",
                "C",
                "O",
                "C",
                "O",
                "C",
                "N",
                "C",
                "C",
                "O",
                "C",
                "C",
                "C",
                "O",
                "O",
                "N",
                "N",
                "C",
                "C",
                "O",
                "C",
                "C",
                "C",
                "C",
                "N",
                "O",
                "C",
                "O",
                "C",
                "N",
                "O",
                "O",
                "C",
                "C",
                "N",
                "O",
                "O",
                "C",
            ],
        )
        # Residue IDs
        self.assertEqual(
            res_id, [1] * 4 + [2] * 8 + [3] * 7 + [4] * 9 + [5] + [6] * 9 + [7] * 13
        )
        # Residue Types
        self.assertEqual(
            res_types,
            [
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "atom",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
                "hetero",
            ],
        )

    def test_pdb_charge(self):
        read_geometry(
            "small_protien",
            path=SMALL_PROTIEN_SYS_PATH,
            charges={13: 1, 1727: -1, 1728: 1},
            save=True,
        )
        sys = System.get(1)
        atoms = list(sys.atoms)
        self.assertEqual(atoms[12].charge, 1)
        self.assertEqual(atoms[12].meta["pdb_atom_no"], 13)
        self.assertEqual(atoms[38].charge, -1)
        self.assertEqual(atoms[38].meta["pdb_atom_no"], 1727)
        self.assertEqual(atoms[39].charge, 1)
        self.assertEqual(atoms[39].meta["pdb_atom_no"], 1728)
        self.assertEqual(sys.charge, 1)

    def test_frag(self):
        read_geometry("water6", path=WATER6_FRAG_PATH, charges={4: -1}, save=True)
        sys = System.get(1)
        atoms = list(sys.atoms)
        self.assertListEqual(
            [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6],
            [a.meta["frag_group"] for a in atoms],
        )
        self.assertEqual(atoms[3].charge, -1)

    def test_file_dedup(self):
        sys1 = read_geometry("water6", path=WATER6_SYS_PATH, save=True)

        with self.assertRaises(SystemExists):
            _ = read_geometry("water6-2", path=WATER6_SYS_PATH, save=True)
