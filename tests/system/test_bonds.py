# from unittest import skip
# import networkx as nx

# from fragment.examples.systems import (
#     ACETYLENE_SYS_PATH,
#     BENZENE_SYS_PATH,
#     ETHANE_SYS_PATH,
#     ETHENE_SYS_PATH,
#     MG_ALDEHYDE,
#     MG_COMPLEX_SYS_PATH,
# )
# from fragment.systems.geometry_readers.util import read_geometry
# from fragment.systems.models import Bond
# from tests._util import DBTestCase

# ETHANE_BONDS = {
#     ("C", 1, "H", 4, 1),
#     ("C", 2, "H", 6, 1),
#     ("C", 2, "H", 8, 1),
#     ("C", 1, "C", 2, 1),
#     ("C", 1, "H", 3, 1),
#     ("C", 1, "H", 5, 1),
#     ("C", 2, "H", 7, 1),
# }

# ETHENE_BONDS = {
#     ("C", 1, "H", 3, 1),
#     ("C", 1, "C", 2, 2),
#     ("C", 1, "H", 4, 1),
#     ("C", 2, "H", 6, 1),
#     ("C", 2, "H", 5, 1),
# }

# ACETYLENE_BONDS = {("C", 1, "C", 2, 3), ("C", 2, "H", 4, 1), ("C", 1, "H", 3, 1)}

# BENZENE_BONDS = {
#     ("C", 1, "C", 2, 1),
#     ("C", 2, "C", 3, 2),
#     ("C", 3, "C", 4, 1),
#     ("C", 4, "C", 5, 2),
#     ("C", 5, "C", 6, 1),
#     ("C", 1, "C", 6, 2),
#     ("C", 1, "H", 10, 1),
#     ("C", 2, "H", 7, 1),
#     ("C", 3, "H", 8, 1),
#     ("C", 4, "H", 12, 1),
#     ("C", 5, "H", 11, 1),
#     ("C", 6, "H", 9, 1),
# }

# MG_ALDEHYDE_BONDS = {
#     ("O", 0, "C", 1, 2),
#     ("O", 0, "Mg", 4, 0),
#     ("C", 1, "H", 2, 1),
#     ("C", 1, "H", 3, 1),
# }

# MG_COMPLEX_BONDS = {
#     ("N", 1, "H", 69, 1),
#     ("N", 1, "C", 2, 1),
#     ("N", 1, "H", 12, 1),
#     ("C", 2, "H", 9, 1),
#     ("C", 2, "C", 3, 1),
#     ("C", 2, "C", 5, 1),
#     ("C", 3, "H", 70, 1),
#     ("C", 3, "H", 71, 1),
#     ("C", 3, "O", 4, 1),
#     ("O", 4, "H", 72, 1),
#     ("C", 5, "C", 6, 1),
#     ("C", 5, "H", 10, 1),
#     ("C", 5, "H", 11, 1),
#     ("C", 6, "O", 8, 1),
#     ("C", 6, "O", 7, 2),
#     ("O", 8, "Mg", 27, 0),
#     ("N", 13, "H", 73, 1),
#     ("N", 13, "C", 15, 1),
#     ("N", 13, "H", 21, 1),
#     ("N", 14, "H", 25, 1),
#     ("N", 14, "H", 26, 1),
#     ("N", 14, "C", 17, 1),
#     ("C", 15, "C", 18, 1),
#     ("C", 15, "H", 22, 1),
#     ("C", 15, "C", 16, 1),
#     ("C", 16, "C", 17, 1),
#     ("C", 16, "H", 24, 1),
#     ("C", 16, "H", 23, 1),
#     ("C", 17, "O", 19, 2),
#     ("C", 18, "H", 74, 1),
#     ("C", 18, "O", 20, 2),
#     ("O", 19, "Mg", 27, 0),
#     ("Mg", 27, "O", 60, 0),
#     ("Mg", 27, "O", 66, 0),
#     ("Mg", 27, "O", 59, 0),
#     ("N", 28, "H", 50, 1),
#     ("N", 28, "H", 52, 1),
#     ("N", 28, "H", 51, 1),
#     ("N", 28, "C", 29, 1),
#     ("C", 29, "C", 34, 1),
#     ("C", 29, "H", 49, 1),
#     ("C", 29, "C", 35, 1),
#     ("C", 30, "H", 76, 1),
#     ("C", 30, "C", 31, 1),
#     ("C", 30, "H", 75, 1),
#     ("C", 30, "H", 39, 1),
#     ("C", 31, "H", 40, 1),
#     ("C", 31, "H", 41, 1),
#     ("C", 31, "S", 38, 1),
#     ("C", 32, "H", 43, 1),
#     ("C", 32, "H", 42, 1),
#     ("C", 32, "H", 44, 1),
#     ("C", 33, "H", 45, 1),
#     ("C", 33, "S", 38, 1),
#     ("C", 33, "H", 46, 1),
#     ("C", 33, "C", 34, 1),
#     ("C", 34, "H", 48, 1),
#     ("C", 34, "H", 47, 1),
#     ("C", 35, "O", 37, 1),
#     ("C", 35, "O", 36, 2),
#     ("C", 53, "O", 59, 1),
#     ("C", 53, "C", 54, 1),
#     ("C", 53, "C", 58, 2),
#     ("C", 54, "C", 55, 2),
#     ("C", 54, "O", 60, 1),
#     ("C", 55, "H", 62, 1),
#     ("C", 55, "C", 56, 1),
#     ("C", 56, "H", 63, 1),
#     ("C", 56, "C", 57, 2),
#     ("C", 57, "H", 64, 1),
#     ("C", 57, "C", 58, 1),
#     ("C", 58, "H", 65, 1),
#     ("O", 59, "H", 61, 1),
#     ("O", 66, "H", 67, 1),
#     ("O", 66, "H", 68, 1),
# }


# @skip("BROKEN")
# class BondingTestCases(DBTestCase):
#     def test_ethane(self):
#         sys = read_geometry("ethane", path=ETHANE_SYS_PATH)
#         G = Bond.mk_bond_graph(sys)
#         bonds = {
#             (G.nodes[a1]["atom"].t, a1, G.nodes[a2]["atom"].t, a2, e_d["order"])
#             for a1, a2, e_d in G.edges(data=True)
#         }
#         self.assertSetEqual(bonds, ETHANE_BONDS)

#     def test_ethene(self):
#         sys = read_geometry("ethene", path=ETHENE_SYS_PATH)
#         G = Bond.mk_bond_graph(sys)
#         bonds = {
#             (G.nodes[a1]["atom"].t, a1, G.nodes[a2]["atom"].t, a2, e_d["order"])
#             for a1, a2, e_d in G.edges(data=True)
#         }
#         self.assertSetEqual(bonds, ETHENE_BONDS)

#     def test_acetylene(self):
#         sys = read_geometry("acetylene", path=ACETYLENE_SYS_PATH)
#         G = Bond.mk_bond_graph(sys)
#         bonds = {
#             (G.nodes[a1]["atom"].t, a1, G.nodes[a2]["atom"].t, a2, e_d["order"])
#             for a1, a2, e_d in G.edges(data=True)
#         }
#         self.assertSetEqual(bonds, ACETYLENE_BONDS)

#     def test_benzene(self):
#         sys = read_geometry("benzene", path=BENZENE_SYS_PATH)
#         G = Bond.mk_bond_graph(sys)
#         bonds = {
#             (G.nodes[a1]["atom"].t, a1, G.nodes[a2]["atom"].t, a2, e_d["order"])
#             for a1, a2, e_d in G.edges(data=True)
#         }
#         self.assertSetEqual(bonds, BENZENE_BONDS)

#     def test_mg_aldehyde(self):
#         sys = read_geometry("mg_aldehyde", path=MG_ALDEHYDE)
#         G = Bond.mk_bond_graph(sys)
#         bonds = {
#             (G.nodes[a1]["atom"].t, a1, G.nodes[a2]["atom"].t, a2, e_d["order"])
#             for a1, a2, e_d in G.edges(data=True)
#         }
#         self.assertSetEqual(bonds, MG_ALDEHYDE_BONDS)

#     def test_Mg_complex(self):
#         sys = read_geometry(
#             "Mg_complex",
#             path=MG_COMPLEX_SYS_PATH,
#             charges={8: -1, 27: 1, 28: 1, 37: 1, 60: -1},
#         )
#         G = Bond.mk_bond_graph(sys)
#         bonds = {
#             (G.nodes[a1]["atom"].t, a1, G.nodes[a2]["atom"].t, a2, e_d["order"])
#             for a1, a2, e_d in G.edges(data=True)
#         }
#         self.assertSetEqual(bonds, MG_COMPLEX_BONDS)

#     def test_get_graph(self):
#         sys = read_geometry("ethane", path=ETHANE_SYS_PATH)
#         G = Bond.mk_bond_graph(sys)

#         def edge_match(d1, d2):
#             return all(
#                 [
#                     d1["order"] == d2["order"],
#                     d1["length"] == d2["length"],
#                 ]
#             )

#         self.assertTrue(nx.is_isomorphic(G, sys.bonds, edge_match=(edge_match)))
