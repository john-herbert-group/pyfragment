from fragment.calculations.common import JobStatus
from fragment.calculations.models import Job
from fragment.schemas.workers import ServerState, ServerStatus
from tests._util import make_system

from .project_test_case import ProjectTestCase


class StatusTestCases(ProjectTestCase):
    def create_env(self):
        _ = make_system(atoms=5, make_primary_frags=True, make_aux_frags=True)
        Job.create(system_id=1, backend__id=1, status=JobStatus.PENDING)
        Job.create(system_id=2, backend__id=1, status=JobStatus.RUNNING)
        Job.create(system_id=3, backend__id=1, status=JobStatus.COMPLETED)
        Job.create(system_id=4, backend__id=1, status=JobStatus.FAILED)

    def test_status(self):
        resp = self.client.get("/status")
        self.assertEqual(resp.status_code, 200)
        stat = ServerStatus(**resp.json())

        self.assertFalse(stat.work_done)
        self.assertEqual(len(stat.workers), 1)
        self.assertEqual(stat.jobs[JobStatus.PENDING], 0)
        self.assertEqual(stat.jobs[JobStatus.RUNNING], 2)
        self.assertEqual(stat.jobs[JobStatus.COMPLETED], 1)
        self.assertEqual(stat.jobs[JobStatus.FAILED], 1)

        stat = ServerState(**self.client.get("status/state").json())
        self.assertEqual(stat.work_done, False)
        self.assertEqual(stat.active_workers, 1)

        resp = self.client.post("/status/halt")
        self.proj.get_worker(1).last_seen -= self.proj.timeout_delta
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get("/status")
        self.assertEqual(resp.status_code, 200)
        stat = ServerStatus(**resp.json())
        self.assertEqual(len(stat.workers), 0)
        self.assertTrue(stat.work_done)
