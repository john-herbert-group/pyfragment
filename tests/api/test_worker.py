import tests.api.project_test_case as ptc
from fragment.calculations.models import Worker
from fragment.schemas.workers import WorkerAssignment


class WorkerTestCases(ptc.ProjectTestCase):
    NUM_WORKERS = 0

    async def test_worker(self):
        resp = self.client.post(
            "/worker/register", json={"cores": 1, "hostname": "localhost", "info": {}}
        )
        self.assertEqual(resp.status_code, 200)
        assignment = WorkerAssignment(**resp.json())
        self.assertEqual(assignment.id, 1)
        worker_count = await self.proj.run_in_pool(Worker.select().count)
        self.assertEqual(worker_count, 1)
        self.assertEqual(self.proj.num_active_workers(), 1)
        last_seen = self.proj.get_worker(1).last_seen

        # Test check-in
        resp = self.client.post("/worker/check_in")
        self.assertEqual(resp.status_code, 400)

        self.client.headers["X-Worker-ID"] = str(assignment.id)
        resp = self.client.post("/worker/check_in")
        self.assertEqual(resp.status_code, 200)
        self.assertGreater(self.proj.get_worker(1).last_seen, last_seen)

        # Test deregister
        resp = self.client.post("/worker/deregister")
        self.assertEqual(self.proj.num_active_workers(), 0)
