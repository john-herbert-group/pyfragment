import unittest
from tempfile import TemporaryDirectory

from fastapi.testclient import TestClient

import fragment.app.api.db as app_common
import fragment.core.logging as flogger
from fragment.app.api.main import app, shutdown_event, startup_event
from fragment.calculations.models import Worker
from fragment.schemas.workers import ServerStatus


class ProjectTestCase(unittest.IsolatedAsyncioTestCase):
    NUM_WORKERS = 1

    def setUp(self) -> None:
        self.tmpfs = TemporaryDirectory()

    def create_env(self):
        pass

    async def asyncSetUp(self) -> None:
        app_common.PROJECT = await app_common.mk_project(
            self.tmpfs.name, make_db=True, config={"batch_size": 1, "batch_size_min": 1}
        )
        app_common.PROJECT.config.log_level = "ERROR"

        self.client = TestClient(app)
        await startup_event()

        self.proj = app_common.PROJECT
        await self.proj.run_in_pool(self.create_env)

        for _ in range(self.NUM_WORKERS):
            w = Worker(cores=1, hostname="localhost", info={})
            await self.proj.add_worker(w)

        if self.NUM_WORKERS:
            self.client.headers["X-Worker-ID"] = str(
                (await self.proj.run_in_pool(Worker.select().first)).id
            )

    def tearDown(self) -> None:
        self.tmpfs.cleanup()
        flogger.teardown_logger()

    async def asyncTearDown(self) -> None:
        await shutdown_event()
        await self.proj.stop.wait()
        app_common.PROJECT = None

    def status(self) -> ServerStatus:
        return ServerStatus(**self.client.get("/status").json())
