import asyncio
from datetime import datetime

from fragment.calculations.common import JobStatus
from fragment.calculations.models import Calculation, Job
from fragment.schemas.calculation import Batch, BatchResult, Result
from tests._util import TestQMBackend, make_system

from .project_test_case import ProjectTestCase


class CalculationTestCases(ProjectTestCase):
    def create_env(self):
        _ = make_system(atoms=6, make_primary_frags=True, make_aux_frags=True)
        _ = TestQMBackend("test", "note", save_config=True)
        Calculation.from_conf(
            "test",
            "note",
            [{"view": "test_system__test_frag__order-2", "backend": "test"}],
        )

    async def fill_bq(self) -> None:
        while (
            not self.proj.batch_queue.full() and not self.proj.job_batcher_task.done()
        ):
            await asyncio.sleep(0.0001)

    async def test_get_work(self):
        # Jobs should already be in the queue
        await self.fill_bq()
        status = self.status()
        self.assertLessEqual(status.jobs[JobStatus.PENDING], 12)

        self.assertEqual(self.proj.jobs_prepaired, 5)
        self.assertEqual(self.proj.batch_queue.qsize(), 5)

        res = self.client.get("/calculation/get_work")
        self.assertEqual(res.status_code, 200)
        data = Batch(**res.json())
        self.assertEqual(len(data.jobs), 1)

        self.assertEqual(self.proj.batch_queue.qsize(), 4)

    def test_report_work(self):
        r1 = Result(
            id=1,
            name="frag_11.3.1",
            status=JobStatus.COMPLETED,
            start_time=datetime.now(),
            end_time=datetime.now(),
            properties={"total_energy": 0.2},
        )
        r2 = Result(
            id=2,
            name="frag_12.3.1",
            status=JobStatus.FAILED,
            start_time=datetime.now(),
            end_time=datetime.now(),
            properties={},
        )
        results = BatchResult(
            results=[r1, r2], start_time=datetime.now(), end_time=datetime.now()
        )

        self.assertEqual(self.proj.report_queue.qsize(), 0)

        res = self.client.post("/calculation/report_work", content=results.json())
        self.assertEqual(res.status_code, 200)
        self.assertFalse(self.proj.stop.is_set())

        self.assertEqual(self.proj.report_queue.qsize(), 1)

        results = BatchResult(
            results=[], start_time=datetime.now(), end_time=datetime.now()
        )

    def test_upload_job_files(self):
        upload = self.proj.basepath / "text.txt"
        with upload.open("w") as f:
            f.write("Test data\n")

        with upload.open("rb") as f:
            resp = self.client.post(
                "/calculation/upload_archive", files={"archive": (upload.name, f)}
            )
        self.assertEqual(resp.status_code, 200)

        uploaded_upload = list(self.proj.uploadpath.iterdir())[0] / "text.txt"
        self.assertTrue(uploaded_upload.exists())
        with upload.open("r") as u:
            with uploaded_upload.open("r") as uu:
                self.assertEqual(u.read(), uu.read())

    async def test_next_task(self):
        await self.fill_bq()

        # HALT WITH A FULL QUEUE
        self.proj.stop.set()
        self.assertEqual(self.proj.batch_queue.qsize(), 5)
        resp = self.client.get("/worker/next_task")
        self.assertEqual(resp.status_code, 200)
        self.assertDictEqual(resp.json(), {"next": "halt"})

        self.proj.stop.clear()
        resp = self.client.get("/worker/next_task")
        self.assertEqual(resp.status_code, 200)
        self.assertDictEqual(resp.json(), {"next": "run_jobs"})

        # DRAIN THE QUEUE
        while not self.proj.batch_queue.empty():
            self.proj.batch_queue.get_nowait()
            self.proj.batch_queue.task_done()
        self.assertEqual(self.proj.batch_queue.qsize(), 0)

        resp = self.client.get("/worker/next_task")
        self.assertEqual(resp.status_code, 200)
        self.assertDictEqual(resp.json(), {"next": "sleep"})

        # MARK ALL JOBS AS RUNNING
        self.proj.job_batcher_task.cancel()
        try:
            await self.proj.job_batcher_task
        except:
            pass

        resp = self.client.get("/worker/next_task")
        self.assertEqual(resp.status_code, 200)
        self.assertDictEqual(resp.json(), {"next": "halt"})
