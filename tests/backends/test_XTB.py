import unittest
from io import StringIO
from pathlib import Path
from tempfile import TemporaryDirectory
from textwrap import dedent

from fragment.backends.util import get_backend
from fragment.backends.xtb import XTBBackend
from fragment.calculations.models import JobStatus
from fragment.systems.models import Atom
from tests._util import DBTestCase, make_system
from tests.backends.util import H2_job


class XTBTestCases(DBTestCase):
    def setUp(self) -> None:
        self.backend: XTBBackend = get_backend("xTB")("xTB", "")
        self.sys = make_system(atoms=2, ghost_atoms=2)

    def test_atom(self):
        atom = Atom.get(1)
        self.assertEqual(self.backend.template.atom_to_string(atom), "H    0.0 0.0 0.0")

    def test_template(self):
        out_templ = self.backend.template.system_to_string(self.sys, name="fragment_1")
        self.assertEqual(
            out_templ,
            "4\nFragment fragment_1; Charge=0; Multiplicity=1\nH    0.0 0.0 0.0\nH    1.0 1.0 1.0\n",
        )

    def test_properties(self):
        props = self.backend.get_properties(
            None,
            [
                "output",
                StringIO(
                    dedent(
                        """\
         TOT                       37832.9366   215.6412   321.2541  1344.1271
            -------------------------------------------------
          | TOTAL ENERGY             -164.948939611365 Eh   |
          | TOTAL ENTHALPY           -164.055281977461 Eh   |
         total:
        * wall-time:     0 d,  0 h,  0 min, 42.992 sec
        *  cpu-time:     0 d,  0 h,  4 min, 56.617 sec
        """
                    )
                ),
            ],
        )
        self.assertDictEqual(
            props.to_dict(),
            {
                "total_energy": -164.948939611365,
                "cpu_time": 296.617,
                "total_entropy": 321.2541,
                "total_enthalpy": -164.055281977461,
            },
        )

    @unittest.skipIf(not XTBBackend.is_available(), "Could not find xTB exe")
    def test_exec(self):
        self.backend.save_config()
        job = H2_job(self.backend)

        with TemporaryDirectory() as dir:
            res = self.backend.run(
                job.id, "test_job", job.modded_system, 1, workpath=Path(dir)
            )

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertAlmostEqual(res.properties["total_energy"], -0.97514, 5)
