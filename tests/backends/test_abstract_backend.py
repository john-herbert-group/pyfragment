from pathlib import Path

from fragment.backends.abstract import QMBackend
from fragment.backends.common import RunContext
from tests._util import DBTestCase, TestQMBackend, TestQMShellBackend


class QMBackendTestCases(DBTestCase):
    def test_retrieval(self):
        TestQMBackend("default", "", save_config=True)
        retrieved = QMBackend.obj_from_name("default")
        self.assertIsInstance(retrieved, TestQMBackend)

    def test_property_mixin(self):
        ctx = RunContext(
            (
                1,
                1,
                1,
            ),
            "test_job",
            None,
        )

        default_backend = TestQMBackend("default", "", energy=3.3888)
        self.assertDictEqual(
            default_backend.get_properties(ctx, []).to_dict(),
            dict(total_energy=3.3888, a=1, b=2, wall_time=1.0),
        )


class QMShellBackendTestCases(DBTestCase):
    def test_retrieval(self):
        TestQMShellBackend("default", "", template="foo", save_config=True)
        retrieved = QMBackend.obj_from_name("default")
        self.assertIsInstance(retrieved, TestQMShellBackend)
        self.assertEqual(retrieved.template.template, "foo")

    def test_file_manifest(self):
        backend = TestQMShellBackend("default", "")
        files = backend.file_manifest("frag_1", Path("test/path"))
        self.assertDictEqual(
            files,
            {
                "input": Path("test/path/frag_1.txt"),
                "log": Path("test/path/frag_1.blog"),
                "output": Path("test/path/frag_1.csv"),
            },
        )
