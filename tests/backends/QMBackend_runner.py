import sys
from os import path
from sys import argv


def main():
    try:
        input_file = argv[1]
    except IndexError:
        input_file = ""

    try:
        output_file = argv[2]
    except IndexError:
        output_file = None

    if not path.exists(input_file):
        print("No input file\n", file=sys.stderr)
        exit(-1)
    if output_file is None:
        print("No output file\n", file=sys.stderr)
        exit(-2)

    print("OUTPUT", file=sys.stderr)
    print("OUTPUT", file=sys.stdout)

    with open(input_file, "r") as in_file:
        with open(output_file, "a") as out_file:
            out_file.write(in_file.read().upper())
            out_file.write("total energy: 0.2\n")


if __name__ == "__main__":
    main()
