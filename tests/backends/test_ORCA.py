import unittest
from io import StringIO
from pathlib import Path
from tempfile import TemporaryDirectory
from textwrap import dedent

from fragment.backends.orca import OrcaBackend
from fragment.backends.util import get_backend
from fragment.calculations.models import JobStatus
from fragment.systems.models import Atom
from tests._util import DBTestCase, make_system
from tests.backends.util import H2_job


class OrcaTestCases(DBTestCase):
    def setUp(self) -> None:
        self.backend: OrcaBackend = get_backend("Orca")("Orca", "")
        self.sys = make_system(atoms=2, ghost_atoms=2)

    def test_atom(self):
        atom = Atom.get(1)
        self.assertEqual(self.backend.template.atom_to_string(atom), "H    0.0 0.0 0.0")

    def test_ghost_atom(self):
        atom = Atom.get(3)
        self.assertEqual(
            self.backend.template.atom_to_string(atom), "H:    0.0 0.0 0.0"
        )

    def test_template(self):
        out_templ = self.backend.template.system_to_string(self.sys, name="fragment_1")
        self.assertEqual(
            out_templ,
            "# Name: fragment_1\n! B3LYP SP 6-31G\n\n*xyz 0 1\nH    0.0 0.0 0.0\nH    1.0 1.0 1.0\nH:    0.0 0.0 0.0\nH:    1.0 1.0 1.0\n*\n",
        )

    def test_properties(self):
        props = self.backend.get_properties(
            None,
            [
                StringIO(
                    dedent(
                        """\
        Total Energy       :         -496.88645078 Eh          -13520.96772 eV
        TOTAL RUN TIME: 0 days 0 hours 1 minutes 11 seconds 2 msec
        Total Enthalpy                    ...   -157.06107433 Eh
        Final entropy term                ...      0.03354941 Eh     21.05 kcal/mol
        Final Gibbs free energy         ...   -157.09462374 Eh
        """
                    )
                )
            ],
        )
        self.assertDictEqual(
            props.to_dict(),
            {
                "total_energy": -496.88645078,
                "cpu_time": 71.002,
                "total_entropy": 0.03354941,
                "total_enthalpy": -157.06107433,
                "total_gibbs": -157.09462374,
            },
        )

    @unittest.skipIf(not OrcaBackend.is_available(), "Could not find ORCA exe")
    def test_exec(self):
        self.backend.save_config()
        job = H2_job(self.backend)

        with TemporaryDirectory() as dir:
            res = self.backend.run(
                job.id, "test_job", job.modded_system, 1, workpath=Path(dir)
            )

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertAlmostEqual(res.properties["total_energy"], -1.153897, 4)
