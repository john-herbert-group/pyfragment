import unittest
from io import StringIO
from pathlib import Path
from tempfile import TemporaryDirectory
from textwrap import dedent

from fragment.backends.mopac import MOPACBackend
from fragment.backends.util import get_backend
from fragment.calculations.models import JobStatus
from fragment.systems.models import Atom
from tests._util import DBTestCase, make_system
from tests.backends.util import H2_job


class MOPACTestCases(DBTestCase):
    def setUp(self) -> None:
        self.backend: MOPACBackend = get_backend("mopac")("mopac", "")
        self.sys = make_system(atoms=2, ghost_atoms=2)

    def test_atom(self):
        atom = Atom.get(1)
        self.assertEqual(self.backend.template.atom_to_string(atom), "H    0.0 0.0 0.0")

    def test_ghost_atom(self):
        atom = Atom.get(3)
        self.assertEqual(self.backend.template.atom_to_string(atom), None)

    def test_template(self):
        out_templ = self.backend.template.system_to_string(self.sys, name="fragment_1")
        self.assertEqual(
            out_templ,
            dedent(
                """\
            PM7 XYZ PRECISE NOSYM NOREOR GEO-OK


            H    0.0 0.0 0.0
            H    1.0 1.0 1.0
            """
            ),
        )

    def test_properties(self):
        props = self.backend.get_properties(
            None,
            [
                "output",
                StringIO(
                    dedent(
                        """\
        TOTAL ENERGY            =      -1915.74402 EV
        COMPUTATION TIME        =       0.05 SECONDS
        """
                    )
                ),
            ],
        )
        self.assertDictEqual(
            props.to_dict(),
            {
                "total_energy": -70.40229478174886,
                "cpu_time": 0.05,
            },
        )

    @unittest.skipIf(not MOPACBackend.is_available(), "Could not find MOPAC exe")
    def test_exec(self):
        self.backend.save_config()
        job = H2_job(self.backend)

        with TemporaryDirectory() as dir:
            res = self.backend.run(
                job.id, "test_job", job.modded_system, 1, workpath=Path(dir)
            )

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertAlmostEqual(res.properties["total_energy"], -1.030707, 5)
