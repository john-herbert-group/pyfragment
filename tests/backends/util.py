from fragment.backends.abstract import QMBackend
from fragment.calculations.models import Job
from fragment.systems.models import Atom, System


def H2() -> System:
    system = System(atoms=[Atom("H", r=(0, 0, 0)), Atom("H", r=(0.92, 0, 0))])
    system.save()
    return system


def H2_job(backend: QMBackend) -> Job:
    job = Job(system_id=H2().id, backend__id=backend.conf_id)
    job.save()
    return job
