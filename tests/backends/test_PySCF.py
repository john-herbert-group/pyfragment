import unittest

import numpy as np

from fragment.backends.abstract import QMBackend
from fragment.backends.common import RunContext
from fragment.backends.pyscf import PySCFBackend
from fragment.backends.util import get_backend
from fragment.calculations.models import JobStatus
from fragment.schemas.strategy import (
    PySCF_CCSD,
    PySCF_CCSD_T,
    PySCF_DFMP2,
    PySCF_HF,
    PySCF_KS,
    PySCF_MP2,
)
from tests._util import DBTestCase, make_system
from tests.backends.util import H2_job

ATOM_FIXTURE = [
    ("H", np.array([0.0, 0.0, 0.0])),
    ("H", np.array([1.0, 1.0, 1.0])),
    ("ghost:H", np.array([0.0, 0.0, 0.0])),
    ("ghost:H", np.array([1.0, 1.0, 1.0])),
]


class PySCFTestCases(DBTestCase):
    def setUp(self) -> None:
        self.backend: PySCFBackend = get_backend("PySCF")(
            "pyscf_HF", "", procedure=PySCF_HF(method="HF", basis="STO-3G")
        )
        self.sys = make_system(atoms=2, ghost_atoms=2)
        self.ctx = RunContext(id=1, name="test_calc", system=self.sys)

    def test_restore(self) -> None:
        self.backend.save_config()
        backend = QMBackend.obj_from_name("pyscf_HF")
        self.assertIsInstance(backend.procedure_args, PySCF_HF)

    @unittest.skipIf(not PySCFBackend.is_available(), "Could not find PySCF exe")
    def test_setup(self) -> None:
        self.backend.setup_calc(self.ctx)
        mol = self.ctx.scratch["mol"]
        self.assertEqual(mol.basis, "STO-3G")
        self.assertEqual(mol.charge, 0)
        self.assertEqual(mol.spin, 0)
        for a, a_ref in zip(self.ctx.scratch["mol"].atom, ATOM_FIXTURE):
            self.assertEqual(a[0], a_ref[0])
            self.assertTrue((a[1] == a_ref[1]).all())

    @unittest.skipIf(not PySCFBackend.is_available(), "Could not find PySCF exe")
    def test_exec_HF(self):
        backend = PySCFBackend(
            "pyscf_HF", "", procedure=PySCF_HF(method="HF", basis="STO-3G")
        )
        backend.save_config()
        job = H2_job(backend)

        res = backend.run(job.id, "test_job", job.modded_system, 1)

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertTrue("cpu_time" in res.properties)
        self.assertTrue("wall_time" in res.properties)

        PROPERTIES = {
            "nuclear_repulsion": 1.0869565217391304,
            "one_electron_int": -2.839560964360053,
            "total_energy": -1.0305550699899921,
            "total_scf_energy": -1.0305550699899921,
            "two_electron_int": 0.7220493726309302,
        }

        for p, v in PROPERTIES.items():
            self.assertAlmostEqual(res.properties[p], v, 5)

    @unittest.skipIf(not PySCFBackend.is_available(), "Could not find PySCF exe")
    def test_exec_KS(self):
        backend = PySCFBackend(
            "pyscf_KS", "", procedure=PySCF_KS(method="KS", basis="STO-3G", xc="b3lyp")
        )
        backend.save_config()
        job = H2_job(backend)

        res = backend.run(job.id, "test_job", job.modded_system, 1)

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertTrue("cpu_time" in res.properties)
        self.assertTrue("wall_time" in res.properties)

        PROPERTIES = {
            "dft_exchange": -0.758417107853926,
            "nuclear_repulsion": 1.0869565217391304,
            "one_electron_int": -2.8395609643600532,
            "total_coulomb_energy": 1.444098745261861,
            "total_energy": -1.0669228052129878,
            "total_scf_energy": -1.0669228052129878,
        }

        for p, v in PROPERTIES.items():
            self.assertAlmostEqual(res.properties[p], v, 5)

    @unittest.skipIf(not PySCFBackend.is_available(), "Could not find PySCF exe")
    def test_exec_MP(self):
        backend = PySCFBackend(
            "pyscf_MP2",
            "",
            procedure=PySCF_MP2(method="MP2", basis="STO-3G", xc="b3lyp"),
        )
        backend.save_config()
        job = H2_job(backend)

        res = backend.run(job.id, "test_job", job.modded_system, 1)

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertTrue("cpu_time" in res.properties)
        self.assertTrue("wall_time" in res.properties)

        PROPERTIES = {
            "nuclear_repulsion": 1.0869565217391304,
            "one_electron_int": -2.839560964360053,
            "total_correlation_energy": -0.008286823260946544,
            "total_energy": -1.0388418932509387,
            "total_scf_energy": -1.0305550699899921,
            "two_electron_int": 0.7220493726309302,
        }

        for p, v in PROPERTIES.items():
            self.assertAlmostEqual(res.properties[p], v, 5)

    @unittest.skipIf(not PySCFBackend.is_available(), "Could not find PySCF exe")
    def test_exec_DFMP(self):
        backend = PySCFBackend(
            "pyscf_DFMP2",
            "",
            procedure=PySCF_DFMP2(method="RIMP2", basis="cc-pVDZ"),
        )
        backend.save_config()
        job = H2_job(backend)

        res = backend.run(job.id, "test_job", job.modded_system, 1)

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertTrue("cpu_time" in res.properties)
        self.assertTrue("wall_time" in res.properties)

        PROPERTIES = {
            "nuclear_repulsion": 1.0869565217391304,
            "one_electron_int": -2.8814480362747235,
            "total_correlation_energy": -0.0238001845258694,
            "total_energy": -1.0604796952414164,
            "total_scf_energy": -1.036679510715547,
            "two_electron_int": 0.757812003820046,
        }

        for p, v in PROPERTIES.items():
            self.assertAlmostEqual(res.properties[p], v, 5)

    @unittest.skipIf(not PySCFBackend.is_available(), "Could not find PySCF exe")
    def test_exec_CCSD(self):
        backend = PySCFBackend(
            "pyscf_CCSD", "", procedure=PySCF_CCSD(method="CCSD", basis="STO-3G")
        )
        backend.save_config()
        job = H2_job(backend)

        res = backend.run(job.id, "test_job", job.modded_system, 1)

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertTrue("cpu_time" in res.properties)
        self.assertTrue("wall_time" in res.properties)

        PROPERTIES = {
            "total_energy": -1.0423720957128741,
            "CCSD_correlation": -0.011817025722882075,
            "MP2_correlation": -0.008286823260946546,
            "total_correlation_energy": -0.011817025722882075,
            "nuclear_repulsion": 1.0869565217391304,
            "total_scf_energy": -1.0305550699899921,
            "one_electron_int": -2.839560964360053,
            "two_electron_int": 0.7220493726309302,
        }

        for p, v in PROPERTIES.items():
            self.assertAlmostEqual(res.properties[p], v, 5)

    @unittest.skipIf(not PySCFBackend.is_available(), "Could not find PySCF exe")
    def test_exec_CCSD_T(self):
        backend = PySCFBackend(
            "pyscf_CCSD_T", "", procedure=PySCF_CCSD_T(method="CCSD(T)", basis="STO-3G")
        )
        backend.save_config()
        job = H2_job(backend)

        res = backend.run(job.id, "test_job", job.modded_system, 1)

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertTrue("cpu_time" in res.properties)
        self.assertTrue("wall_time" in res.properties)

        PROPERTIES = {
            "total_energy": -1.0423720957128741,
            "total_scf_energy": -1.0305550699899921,
            "MP2_correlation": -0.008286823260946546,
            "CCSD_correlation": -0.011817025722882075,
            "CC_T_correlation": -1.9014596366096455e-47,
            "total_correlation_energy": -0.011817025722882075,
            "nuclear_repulsion": 1.0869565217391304,
            "one_electron_int": -2.839560964360053,
            "two_electron_int": 0.7220493726309302,
        }

        for p, v in PROPERTIES.items():
            self.assertAlmostEqual(res.properties[p], v, 5)
