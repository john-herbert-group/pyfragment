from fragment.backends.util import get_backend
from fragment.systems.models import Atom
from tests._util import DBTestCase, make_system


class XYZTestCases(DBTestCase):
    def setUp(self) -> None:
        self.backend = get_backend("XYZ")("xyz", "")
        self.sys = make_system(atoms=2, ghost_atoms=2)

    def test_atom(self):
        atom = Atom.get(1)
        self.assertEqual(self.backend.template.atom_to_string(atom), "H    0.0 0.0 0.0")

    def test_ghost_atom(self):
        atom = Atom.get(3)
        self.assertIsNone(self.backend.template.atom_to_string(atom))

    def test_template(self):
        self.assertEqual(
            self.backend.template.system_to_string(self.sys, name="fragment_1"),
            "4\nFragment fragment_1; Charge=0; Multiplicity=1\nH    0.0 0.0 0.0\nH    1.0 1.0 1.0\n",
        )
