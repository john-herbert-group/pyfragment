import unittest
from pathlib import Path
from tempfile import TemporaryDirectory

from fragment.backends.libxtb import LibXTBBackend
from fragment.backends.util import get_backend
from fragment.calculations.models import JobStatus
from tests._util import DBTestCase, make_system
from tests.backends.util import H2_job


class LibXTBTestCases(DBTestCase):
    def setUp(self) -> None:
        self.backend: LibXTBBackend = get_backend("LibxTB")("xTB", "")
        self.sys = make_system(atoms=2, ghost_atoms=2)

    @unittest.skipIf(not LibXTBBackend.is_available(), "Could not find xTB exe")
    def test_exec(self):
        self.backend.save_config()
        job = H2_job(self.backend)

        res = self.backend.run(job.id, "test_job", job.modded_system, 1)

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertTrue("cpu_time" in res.properties)
        self.assertTrue("wall_time" in res.properties)
        self.assertAlmostEqual(res.properties["total_energy"], -0.97514, 5)

    @unittest.skipIf(not LibXTBBackend.is_available(), "Could not find xTB exe")
    def test_exec_save_output(self):
        backend = LibXTBBackend(
            name="name",
            note="note",
            method="gfn1",
            save_output=True,
            save_config=True,
        )
        job = H2_job(backend)

        with TemporaryDirectory() as dir:
            res = backend.run(
                job.id, "test_job", job.modded_system, 1, workpath=Path(dir)
            )

            with res.files["output"].open("r") as f:
                # TODO: For some reason binding files does not work :(
                #       Perhaps capture STDOUT?
                self.assertEqual(f.read(), "")

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertAlmostEqual(res.properties["total_energy"], -1.02343, 5)
