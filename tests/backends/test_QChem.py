import unittest
from pathlib import Path
from tempfile import TemporaryDirectory
from textwrap import dedent

from fragment.backends.common import RunContext
from fragment.backends.qchem import QChemBackend
from fragment.backends.util import get_backend
from fragment.calculations.models import JobStatus
from fragment.systems.models import Atom, System
from tests._util import DBTestCase, make_system
from tests.backends.qchem_outfiles import (
    TEST_CCSD_OUT,
    TEST_CCSDT_OUT,
    TEST_GRAD_OUT,
    TEST_MP2_OUT,
    TEST_WB97XV_OUT,
    TEST_WB97XV_OUT_NO_SCFMAN,
)
from tests.backends.util import H2_job


class QChemTestCases(DBTestCase):
    def setUp(self) -> None:
        self.backend: QChemBackend = get_backend("QChem")("qchem", "")
        self.sys = make_system(atoms=2, ghost_atoms=2)

    def test_atom(self):
        atom = Atom.get(1)
        self.assertEqual(
            self.backend.template.atom_to_string(atom),
            "H    0.00000000 0.00000000 0.00000000",
        )

    def test_ghost_atom(self):
        atom = Atom.get(3)
        self.assertEqual(
            self.backend.template.atom_to_string(atom),
            "@H    0.00000000 0.00000000 0.00000000",
        )

    def test_template(self):
        out_templ = self.backend.template.system_to_string(self.sys, name="fragment_1")
        self.assertEqual(
            out_templ,
            dedent(
                """\
            ! Name: fragment_1
            $molecule
            0 1 
            H    0.00000000 0.00000000 0.00000000
            H    1.00000000 1.00000000 1.00000000
            @H    0.00000000 0.00000000 0.00000000
            @H    1.00000000 1.00000000 1.00000000
            $end

            $rem
                basis 6-31G
                method HF
                job_type sp
            $end
            """
            ),
        )

    def test_DFT_properties(self):
        with open(TEST_WB97XV_OUT, "r") as f:
            props = self.backend.get_properties(None, [f])
        self.assertDictEqual(
            props.to_dict(),
            {
                "nuclear_repulsion": 0.34873964,
                "hf_exchange": -0.30736459065521,
                "dft_exchange": -0.19132616115182,
                "dft_correlation": -0.03594325243512,
                "total_coulomb_energy": 0.96514412043875,
                "one_electron_int": -1.82894628401205,
                "total_scf_energy": -1.0496965291,
                "total_energy": -1.0496965291,
                "cpu_time": 11.87,
            },
        )

    def test_gradient(self):
        sys = System(
            [
                Atom(
                    "O",
                    (
                        0.0000000000,
                        0.0000000000,
                        0.0000000000,
                    ),
                ),
                Atom(
                    "H",
                    (
                        0.0000000000,
                        0.0000000000,
                        0.9472690000,
                    ),
                ),
                Atom(
                    "H",
                    (
                        0.9226720317,
                        0.0000000000,
                        -0.2622325328,
                    ),
                ),
                Atom(
                    "O",
                    (
                        2.6471478408,
                        0.3844315763,
                        -0.9182749348,
                    ),
                ),
                Atom(
                    "H",
                    (
                        3.1174655443,
                        -0.2510444530,
                        -1.4399583931,
                    ),
                ),
                Atom(
                    "H",
                    (
                        2.4898165091,
                        1.1358441907,
                        -1.4932568902,
                    ),
                ),
                Atom(
                    "H",
                    (
                        -0.7410073392,
                        1.4056351087,
                        -1.0422511992,
                    ),
                ),
                Atom(
                    "O",
                    (
                        -0.8926504046,
                        2.1871551862,
                        -1.5770641709,
                    ),
                ),
                Atom(
                    "H",
                    (
                        -1.3211437399,
                        2.8135423867,
                        -1.0103863394,
                    ),
                ),
                Atom(
                    "H",
                    (
                        0.8272292940,
                        2.5359868450,
                        -2.2596898885,
                    ),
                ),
                Atom(
                    "O",
                    (
                        1.7290565094,
                        2.4955238008,
                        -2.5842220902,
                    ),
                ),
                Atom(
                    "H",
                    (
                        1.6540997866,
                        2.3948478946,
                        -3.5233223240,
                    ),
                ),
            ]
        )
        sys.save()
        ctx = RunContext(1, "test", sys)
        with open(TEST_GRAD_OUT, "r") as f:
            props = self.backend.get_properties(ctx, [f])
        self.assertDictEqual(
            props.to_dict(),
            {'nuclear_repulsion': 138.50530341, 'one_electron_int': -695.7229525654, 'total_coulomb_energy': 288.9931360924, 'hf_exchange': -35.8639156754, 'nuclear_attraction': -999.104564156, 'kinetic_energy': 303.3816115906, 'total_scf_energy': -304.08842873, 'total_energy': -304.08842873, 'scf_gradient': {'elements': [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16], 'el_types': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 'shape': (3, 12), 'dtype': 'float64', 'data': [2.54e-05, -0.000114, 4e-05, -2.42e-05, 5.7e-05, 5.93e-05, 0.0001755, -0.000121, -3.35e-05, -5.73e-05, 0.0001621, -0.0001693, 7.36e-05, 1.71e-05, -5.5e-05, 2.12e-05, 7.65e-05, -0.0001437, 7.9e-06, 0.0001165, -7.13e-05, 2.16e-05, 4.6e-05, -0.0001104, -0.0001867, 4.01e-05, 0.0002675, -2.59e-05, -6.99e-05, -5.73e-05, -0.0002081, 0.0001042, -1e-05, 6.93e-05, 7.28e-05, 3.9e-06]}, 'cpu_time': 14.0},  # fmt: skip
        )

    def test_no_scfman_properties(self):
        with open(TEST_WB97XV_OUT_NO_SCFMAN, "r") as f:
            props = self.backend.get_properties(None, [f])
            self.maxDiff = None
        self.assertDictEqual(
            props.to_dict(),
            {
                "nuclear_repulsion": 0.34873964,
                "hf_exchange": -0.3073645906,
                "dft_exchange": -0.1913261612,
                "dft_correlation": -0.0359432524,
                "total_coulomb_energy": 0.9651441204,
                "one_electron_int": -1.828946284,
                "kinetic_energy": 0.7760006951,
                "nuclear_attraction": -2.6049469792,
                "total_scf_energy": -1.04969653,
                "total_energy": -1.04969653,
                "cpu_time": 12.18,
            },
        )

    def test_MP2_properties(self):
        # TEST MP2 template
        self.backend = get_backend("QChem")(
            "qchem",
            "",
            template=dedent(
                """\
        ! Name: {name}
        $molecule
        0 1
        {geometry}
        $end
        $rem
            basis aug-cc-pVTZ
            method mp2
            job_type sp
        $end
        """
            ),
        )
        with open(TEST_MP2_OUT, "r") as f:
            props = self.backend.get_properties(None, [f])
        self.assertDictEqual(
            props.to_dict(),
            {
                "nuclear_repulsion": 0.34873964,
                "hf_exchange": -0.55072907976639,
                "one_electron_int": -1.80515692871430,
                "total_correlation_energy": -0.04659996,
                "total_coulomb_energy": 1.10145815953278,
                "total_energy": -0.95228817,
                "total_scf_energy": -0.9056882102,
                "cpu_time": 3.54,
            },
        )

    def test_CCSD_properties(self):
        self.backend = get_backend("QChem")(
            "qchem",
            "",
            template=dedent(
                """\
        ! Name: {name}
        $molecule
        0 1
        {geometry}
        $end
        $rem
            basis aug-cc-pVTZ
            method CCSD
            job_type sp
        $end
        """
            ),
        )
        with open(TEST_CCSD_OUT, "r") as f:
            props = self.backend.get_properties(None, [f])
        self.assertDictEqual(
            props.to_dict(),
            {
                "nuclear_repulsion": 0.34873964,
                "hf_exchange": -0.47394666363846,
                "one_electron_int": -1.82166189958895,
                "total_correlation_energy": -0.06033663,
                "total_coulomb_energy": 0.94789332727691,
                "total_energy": -1.05931223,
                "total_scf_energy": -0.9989755972,
                "cpu_time": 10.14,
            },
        )

    def test_CCSD_T_properties(self):
        self.backend = get_backend("QChem")(
            "qchem",
            "",
            template=dedent(
                """\
        ! Name: {name}
        $molecule
        0 1
        {geometry}
        $end
        $rem
            basis = aug-cc-pVTZ
            method = CCSD(T)
            job_type = sp
        $end
        """
            ),
        )
        with open(TEST_CCSDT_OUT, "r") as f:
            props = self.backend.get_properties(None, [f])
        self.assertDictEqual(
            props.to_dict(),
            {
                "nuclear_repulsion": 0.34873964,
                "hf_exchange": -0.55072907976639,
                "one_electron_int": -1.80515692871430,
                "total_correlation_energy": 0.00000000,
                "total_coulomb_energy": 1.10145815953278,
                "total_energy": -0.99540333,
                "total_scf_energy": -0.9056882102,
                "cpu_time": 9.04,
            },
        )

    @unittest.skipIf(not QChemBackend.is_available(), "Could not find QChem exe")
    def test_exec(self):
        self.backend.save_config()
        job = H2_job(self.backend)

        with TemporaryDirectory() as dir:
            res = self.backend.run(
                job.id, "test_job", job.modded_system, 1, workpath=Path(dir)
            )

        # Now checkout how our job went :)
        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertAlmostEqual(res.properties["total_energy"], -1.10859, 5)
