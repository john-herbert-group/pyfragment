from pathlib import Path
from textwrap import dedent

from peewee import IntegrityError

from fragment.calculations.models import Job, JobStatus
from fragment.mods.models import Stack
from fragment.systems.models import System
from tests._util import DBTestCase, TestCOMMod, TestQMShellBackend, make_system


class JobTestCases(DBTestCase, enable_tmpdir=True):
    def setUp(self) -> None:
        self.sys = make_system(atoms=2, ghost_atoms=2, make_primary_frags=True)
        self.mod = TestCOMMod("test", "", save_config=True)
        self.backend = TestQMShellBackend("test", "", save_config=True)

        self.job = Job.create(
            system=self.sys,
            mod_stack=Stack.from_names("test"),
            backend__id=self.backend.conf_id,
        )

        self.job_uncapped = Job.create(
            system=System.get(2), backend__id=self.backend.conf_id
        )

    def test_dedup(self):
        with self.assertRaises(IntegrityError):
            Job.create(
                fragment=self.sys,
                mod_stack=Stack.from_names("test"),
                backend=self.backend,
            )

    def test_calculation(self):
        job = Job.select()[0]
        self.assertEqual(job.system.id, 1)
        self.assertEqual(job.backend.name, "test")
        self.assertListEqual([m.name for m in job.mod_stack.mods], ["test"])
        self.assertEqual(job.status, JobStatus.PENDING)
        self.assertDictEqual(job.properties_, {})

    def test_run(self):
        backend = self.job.backend

        res = backend.run(
            self.job.id,
            "test_job",
            self.job.modded_system,
            1,
            workpath=Path(self.tmpdir.name),
        )

        self.assertEqual(res.status, JobStatus.COMPLETED)
        self.assertDictEqual(
            res.properties.to_dict(),
            {"a": 1.0, "b": 2.0, "wall_time": 1.0, "total_energy": 0.2},
        )

        with res.files["log"].open("r") as lf:
            self.assertEqual(
                dedent(
                    """\
                OUTPUT
                OUTPUT
                """
                ),
                lf.read(),
            )
        with res.files["output"].open("r") as of:
            self.assertEqual(
                dedent(
                    """\
                TEST_JOB: N_ATOMS=5; 

                0 2
                 H    0.0 0.0 0.0
                 H    1.0 1.0 1.0
                 H    0.5 0.5 0.5
                @H    0.0 0.0 0.0
                @H    1.0 1.0 1.0
                total energy: 0.2
                """
                ),
                of.read(),
            )
