from typing import Callable, Dict, Optional, Set, Tuple

import networkx as nx

from fragment.core.PIE_common import ROOT
from fragment.core.quickPIE import quickNode
from fragment.examples.systems import WATER6_FRAG_PATH
from fragment.fragmenting.water import WaterFragmenter
from fragment.systems.common import AtomType
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import Atom
from tests._util import DBTestCase


class MBCPTestCases(DBTestCase):
    def setUp(self) -> None:
        self.frag = WaterFragmenter("water", "water_MBE", save_config=True)
        self.sys = read_geometry("water6", "Water 6 system", path=WATER6_FRAG_PATH)
        self.aux_frags = self.frag.aux_from_system(self.sys, 3, save=True)

    def test_tree_extraction(self):
        supersystem = self.aux_frags.supersystem
        p_frags = self.aux_frags.parent

        atom_to_ghost: Dict[int, Atom] = {}
        for a in supersystem.atoms:
            ga = Atom(
                t=a.t,
                x=a.x,
                y=a.y,
                z=a.z,
                charge=a.charge,
                type=AtomType.GHOST,
                meta=a.meta,
            )
            ga.meta.update(proxy_for=a.id)
            ga.save()
            atom_to_ghost[a.id] = ga.id

        def to_ghosts_factory(p: quickNode, ghosts: Dict[int, int]):
            def to_ghost(f: quickNode) -> quickNode:
                _key = set()
                for a in f:
                    if a in p:
                        _key.add(a)
                    else:
                        _key.add(ghosts[a])
                return frozenset(_key)

            return to_ghost

        def converted_sub_tree(
            nodes: Set[int],
            G_old: nx.DiGraph,
            converter: Callable[[quickNode], Tuple[int, ...]],
            G: Optional[nx.DiGraph] = None,
        ) -> nx.DiGraph:
            if G is None:
                G = nx.DiGraph()
            for n in nodes:
                if n == ROOT:
                    continue

                d = G_old.nodes[n]
                k = converter(d["data"])
                G.add_node(k, **d)

                pred = G_old.predecessors(n)
                G.add_edges_from(
                    (converter(set(p)), k) for p in pred if (p != ROOT) and (p in nodes)
                )

            return G

        # TODO: be able to directly get primary fragments from a view.
        tree = self.aux_frags.dep_tree
        G = nx.DiGraph()
        for f in p_frags.fragments:
            k = f.key
            to_ghosts = to_ghosts_factory(k, atom_to_ghost)
            ancestors = nx.ancestors(tree.tree, k)
            ancestors.add(k)  # Include the node itself
            converted_sub_tree(ancestors, tree.tree, to_ghosts, G=G)
            G.add_edge(k, ROOT)
            break

        for n, d in G.nodes(data=True):
            if n == ROOT:
                continue
            d["coef"] *= -1

        G = G.reverse()

        # for n in nx.topological_sort(G):
        #     if n==ROOT:
        #         continue
        #     d = G.nodes[n]
        #     print(n, d['coef'])
        # nx.nx_pydot.write_dot(G, "CP.dot")
