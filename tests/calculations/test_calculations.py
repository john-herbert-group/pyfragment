from fragment.calculations.models import Calculation, Job
from fragment.examples.systems import WATER6_SYS_PATH
from fragment.fragmenting.util import AbstractFragmenter, get_fragmenter
from fragment.systems.geometry_readers.util import read_geometry
from fragment.systems.models import View
from tests._util import DBTestCase, TestQMBackend


class CalculationTestCases(DBTestCase):
    def setUp(self) -> None:
        system = read_geometry("water", "", path=WATER6_SYS_PATH)

        # Generate views
        get_fragmenter("water")("water", "", save_config=True).aux_from_system(
            system, order=2
        ).save()
        AbstractFragmenter.obj_from_name("supersystem").aux_from_system(
            system, order=1
        ).save()

        # Generate backend
        TestQMBackend("test", "", save_config=True)

        layer_conf = [
            {"view": View.get(1).name, "backend": "test"},
            {"view": View.get(2).name, "backend": "test"},
        ]

        self.calc: Calculation = Calculation.from_conf("test_calc", "", layer_conf)

    def test_calculation(self):
        self.assertEqual(len(self.calc.job_structure), 3)
        self.assertEqual(Job.select().count(), 22)

        self.assertListEqual(
            self.calc.job_structure,
            [
                {"coef": 1, "layer_id": 1},
                {"coef": -1, "layer_id": 1},
                {"coef": 1, "layer_id": 2},
            ],
        )

    def test_job_query(self):
        calc: Calculation = Calculation.select().first()
        jobs = 0
        for l in calc.layers:
            jobs += l.jobs.count()
        self.assertEqual(jobs, 43)  # Contains duplicates
        self.assertEqual(calc.jobs.count(), 22)
