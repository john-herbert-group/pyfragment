from peewee import IntegrityError

from fragment.calculations.models import Job, Layer
from fragment.mods.models import Stack
from fragment.systems.models import System
from tests._util import (
    DBTestCase,
    TestCOMMod,
    TestFragmenter,
    TestMBEFragmenter,
    TestQMBackend,
    make_system,
)


class LayerTestCases(DBTestCase):
    def setUp(self):
        self.fragmenter = TestFragmenter("fragmenter", "", save_config=True)
        self.backend = TestQMBackend("qm", "", save_config=True)
        self.capper = TestCOMMod("capper", "", save_config=True)

        self.sys = make_system(atoms=6)
        self.view = self.fragmenter.aux_from_system(self.sys, order=2, save=True)

        self.l1 = Layer.create(
            view=self.view,
            backend__id=self.backend.conf_id,
            mod_stack=Stack.from_names("capper"),
        )
        self.l1.make_jobs()

        self.l2 = Layer.create(
            view=self.view,
            backend__id=self.backend.conf_id,
            mod_stack=Stack.from_names(),
        )
        self.l2.make_jobs()

    def test_create(self):
        self.assertEqual(self.l1.jobs.count(), 17)
        self.assertEqual(self.l2.jobs.count(), 17)

        self.l2.make_jobs()  # Only create jobs once
        self.assertEqual(self.l2.jobs.count(), 17)

        with self.assertRaises(IntegrityError):
            Layer.create(
                view=self.view,
                backend__id=self.backend.conf_id,
                mod_stack=Stack.from_names("capper"),
            )
        self.assertEqual(Job.select().count(), 17 * 2)

    def test_job_query(self):
        layer = Layer.get(1)
        self.assertEqual(layer.jobs.count(), 17)
        Job.create(
            system=System.create(key=(100)),  # Place holder
            mod_stack=layer.mod_stack,
            backend_=layer.backend_,
        )
        self.assertEqual(Layer.get(1).jobs.count(), 17)
        self.assertEqual(Job.select().count(), 17 * 2 + 1)

    def test_aux_views_only(self):
        """
        Layers were including primary fragment view coefs in addition to aux
        view_coefs. A ViewCoef.view == self.view clause was added to prevent this
        """
        fragmenterMBE = TestMBEFragmenter("fragmenterMBE", "", save_config=True)
        viewMBE = fragmenterMBE.aux_from_system(self.sys, order=2, save=True)

        layerMBE = Layer.create(
            view=viewMBE,
            backend__id=self.backend.conf_id,
            mod_stack=Stack.from_names("capper"),
        )
        layerMBE.make_jobs()

        self.assertEqual(len(viewMBE.fragments), layerMBE.jobs_w_coefs.count())
