from fragment.calculations.models import Worker
from tests._util import DBTestCase


class WorkerTestCases(DBTestCase, enable_tmpdir=True):
    def test_init(self):
        self.worker = Worker()
        self.worker.save()

        self.assertEqual(self.worker._meta.table_name, "worker")
        self.assertSetEqual(
            set(self.worker.info.keys()),
            {"architecture", "machine", "platform", "processor", "system"},
        )
