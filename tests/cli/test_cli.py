import re
import tempfile
import unittest
from os import chdir, path
from textwrap import dedent

from click.testing import CliRunner

import fragment.core.logging as flogging
from fragment.app.cli.cli import fragment
from fragment.examples.strategies import EXAMPLE_STRATEGY_PATH
from fragment.examples.systems import WATER6_SYS_PATH

# TODO: Figure out how to attach this to Unittest's stdout
ISO_DATETIME_RE = re.compile(r"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d+")
WHITESPACE_RE = re.compile(r"[ \t]+")


def clean_string(dirty_string):
    new_str = dedent(dirty_string)
    # Replace dates
    new_str = re.sub(ISO_DATETIME_RE, "DATE_TIME", new_str)
    # Replace multiple tabs and spaces
    new_str = re.sub(WHITESPACE_RE, " ", new_str)
    return new_str


class CLITestCases(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.runner = CliRunner()

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()
        flogging.teardown_logger()

    def test_cli(self):
        """
        This test is written as a monolythic sequence because each step builds
        on itself
        """
        chdir(self.tmp_dir.name)

        # TEST INIT
        res = self.runner.invoke(fragment, ["-p", self.tmp_dir.name, "project", "init"])

        if res.exception:
            raise res.exception
        self.assertEqual(res.exit_code, 0)
        self.assertTrue(path.exists(path.join(self.tmp_dir.name, "fragment.db")))

        # TEST SYSTEM
        res = self.runner.invoke(
            fragment,
            [
                "system",
                "add",
                "--name",
                "water6",
                "--note",
                "Water 6 test system",
                WATER6_SYS_PATH,
            ],
        )
        if res.exception:
            raise res.exception
        self.assertEqual(res.exit_code, 0)

        res = self.runner.invoke(
            fragment,
            [
                "system",
                "list",
            ],
        )
        if res.exception:
            raise res.exception
        self.assertEqual(res.exit_code, 0)
        # print(res.stdout)
        self.assertEqual(
            clean_string(res.stdout),
            clean_string(
                """\
        
        water6:

            Water 6 test system

            ATOMS: 18
            VIEWS: 0

        """
            ),
        )

        res = self.runner.invoke(fragment, ["system", "info", "water6"])
        # print(res.stdout)
        if res.exception:
            raise res.exception
        self.assertEqual(res.exit_code, 0)
        self.maxDiff = None
        self.assertEqual(
            clean_string(res.stdout),
            clean_string(
                """\

        NAME: water6

        NOTE: Water 6 test system

        CREATED: 2021-08-05 16:15:25.485315

        CHARGE/MULT: 0/1

        GEOMETRY:

            ID  ATOM    X (Å)   Y (Å)   Z (Å)   CHARGE  META

            1   O       -1.13   -1.75   -0.42    0      None
            2   H       -0.23   -1.49   -0.66    0      None
            3   H       -1.06   -2.68   -0.22    0      None
            4   O       -0.25    1.61   -1.29    0      None
            5   H       -1.00    1.16   -1.69    0      None
            6   H       -0.15    2.41   -1.81    0      None
            7   O        1.69   -0.23    1.71    0      None
            8   H        0.79    0.07    1.68    0      None
            9   H        2.05    0.15    2.51    0      None
            10  O       -0.86    0.52    1.22    0      None
            11  H       -0.69    1.09    0.47    0      None
            12  H       -1.13   -0.31    0.83    0      None
            13  O       -2.31    0.05   -2.14    0      None
            14  H       -2.00   -0.69   -1.62    0      None
            15  H       -3.27    0.00   -2.08    0      None
            16  O        1.29   -0.70   -0.98    0      None
            17  H        1.64   -0.60   -0.09    0      None
            18  H        0.95    0.16   -1.20    0      None


        """
            ),
        )

        # TEST STRATEGY
        res = self.runner.invoke(fragment, ["strategy", "add", EXAMPLE_STRATEGY_PATH])
        if res.exception:
            raise res.exception
        # print(res.stdout)
        self.assertEqual(res.exit_code, 0)

        res = self.runner.invoke(fragment, ["strategy", "list"])
        if res.exception:
            raise res.exception
        # print(res.stdout)
        self.assertEqual(res.exit_code, 0)
        # print(res.stdout)
        self.assertEqual(
            clean_string(res.stdout),
            clean_string(
                """\

        QM_BACKENDS:

                    xyz: XYZ Dummy backend

        MODS:

            rcapper_default: The default RCapper accepting the default values

        FRAGMENTERS:

            supersystem: Supersystem "fragmenter". It does nothing.
            water_default: Splits system in water molecules

        """
            ),
        )

        res = self.runner.invoke(fragment, ["calc", "list"])
        if res.exception:
            raise res.exception
        self.assertEqual(res.exit_code, 0)
        self.assertEqual(
            clean_string(res.stdout),
            clean_string(
                """\
        test_calc_1__water6:    Test calc 1 with system water6
        """
            ),
        )

        res = self.runner.invoke(fragment, ["calc", "info", "test_calc_1__water6"])
        # print(res.stdout)
        self.assertEqual(res.exit_code, 0)
        self.assertEqual(
            clean_string(res.stdout),
            clean_string(
                """\

        NAME: test_calc_1__water6

        NOTE: Test calc 1 with system water6

            Calculation has incomplete jobs
        LAYERS:

            ( 1) water6__supersystem__order-1 (1 frags) → xyz: l:1

                INCOMPLETE JOBS: 1
        """
            ),
        )
