from pkg_resources import resource_filename

EXAMPLE_STRATEGY_PATH = resource_filename(__name__, "example.yaml")
WATER6_STRATEGY_PATH = resource_filename(__name__, "water6_test.yaml")
