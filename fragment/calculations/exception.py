class CalculationError(Exception):
    pass


class IncompleteJobsError(CalculationError):
    pass


class JobAlreadyRun(CalculationError):
    pass


class JobNotFinished(CalculationError):
    pass


class JobNotMarkedForRunning(CalculationError):
    pass


class NotTheRightWorker(CalculationError):
    pass


class NoJobInputFile(CalculationError):
    pass


class NoSubmittedJob(CalculationError):
    pass


class WorkerNotConfigured(Exception):
    pass
