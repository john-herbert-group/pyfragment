import platform
from datetime import datetime
from functools import cached_property
from typing import Dict, Iterable, List, Optional, Tuple
from uuid import UUID, uuid4

import networkx as nx
import peewee
from codenamegenerator import generate_codenames
from peewee import (
    SQL,
    BooleanField,
    CharField,
    DateTimeField,
    ForeignKeyField,
    IntegerField,
    UUIDField,
    chunked,
)
from playhouse.sqlite_ext import JSONField

from fragment.backends.util import QMBackend
from fragment.calculations.common import JobStatus
from fragment.calculations.exception import IncompleteJobsError
from fragment.conf.models import ConfigModel
from fragment.core.legacy_PIETree import Key, UnrepresentableException, child_eq_coefs
from fragment.core.PIETree import PIETree
from fragment.core.quickPIE import quickNode
from fragment.db.mixins import NamedAndNotedMixin
from fragment.db.models import BaseModel
from fragment.mods.abstract import PreTemplateBase
from fragment.mods.mixins import ModableMixin
from fragment.mods.models import Stack
from fragment.properties.core import PropertySet
from fragment.properties.mixins import PropertyMixin
from fragment.systems.models import System, View, ViewCoef


def generate_workernames(num: int = 1) -> List[str]:
    names = generate_codenames(suffix="periodic_elements", num=num)
    return [n.title() for n in names]


class Worker(BaseModel):
    """
    Accounting class for hardware
    """

    name: str = CharField(max_length=60, default=lambda: generate_workernames()[0])
    cores: int = IntegerField()
    hostname: str = CharField()
    info: Dict[str, str] = JSONField()
    last_seen: datetime = DateTimeField(default=datetime.now)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs.get("__no_default__", 0) == 1:
            return

        self.cores = self.cores or 1  # One core is our default
        if not self.hostname:
            self.hostname = platform.node()
            self.info = self.get_info()

    @staticmethod
    def get_hostname() -> str:
        return platform.node()

    @staticmethod
    def get_info() -> int:
        return {
            "architecture": platform.architecture(),
            "machine": platform.machine(),
            "platform": platform.platform(),
            "processor": platform.processor(),
            "system": platform.system(),
        }

    @property
    def jobs_handled(self):
        return Job.select().where(Job.worker == self).count()

    @property
    def jobs_failed(self):
        return (
            Job.select()
            .where(Job.worker == self, Job.status == JobStatus.FAILED)
            .count()
        )

    @property
    def jobs_succeeded(self):
        return (
            Job.select()
            .where(Job.worker == self, Job.status == JobStatus.COMPLETED)
            .count()
        )


class Job(BaseModel, ModableMixin, PropertyMixin):
    """
    Composite of individual fragments
    """

    SUPPORTED_MODS = (PreTemplateBase,)

    key: UUID = UUIDField(unique=True, default=uuid4)
    system: System = ForeignKeyField(System)
    backend_: ConfigModel = ForeignKeyField(ConfigModel)
    status: JobStatus = IntegerField(default=JobStatus.PENDING)
    worker: Worker = ForeignKeyField(Worker, null=True)
    files_collected: bool = BooleanField(default=False)
    start_time: datetime = DateTimeField(null=True)
    end_time: datetime = DateTimeField(null=True)

    class Meta:
        # primary_key = CompositeKey('system', 'backend_', 'mod_stack')
        constraints = [SQL("UNIQUE ('system_id', 'backend__id', 'mod_stack_id')")]

    def __repr__(self):
        return f"{self.__class__.__name__}(system={self.System._key}, backend='{self.backend.name}', mod_stack={self.mod_stack.key}, status={self.status})"

    # @property
    # def id(self) -> JobID:
    #     return (self.system_id, self.backend__id, self.mod_stack_id)

    @property
    def wall_time(self):
        try:
            return (self.end_time - self.start_time).total_seconds()
        except:
            return None

    @cached_property
    def backend(self) -> QMBackend:
        from fragment.backends.util import QMBackend

        return QMBackend.obj_from_conf(self.backend_) if self.backend_ else None

    @cached_property
    def modded_system(self) -> System:
        applicator = PreTemplateBase.get_applicator(self.mods, self.system.supersystem)
        return applicator(self.system)

    @classmethod
    def exclude_complete(cls, query, rerun_failed=False) -> peewee.SelectBase:
        if rerun_failed:
            return query.where(
                (cls.status == JobStatus.PENDING) | (cls.status != JobStatus.FAILED)
            )
        else:
            return query.where(cls.status == JobStatus.PENDING)

    @classmethod
    def annotate_properties(
        cls, tree: PIETree, backend: QMBackend, mods: Stack
    ) -> None:
        """Annotates View's DiGraph with property data. Returns a copy of the dep tree.

        The following properties are added to the dep tree:

        * `props`: Value set of job properties
        """
        for n_d in chunked(tree, 500):
            db_ids = [d["db_id"] for _, d in n_d if d["coef"] != 0]
            props = (
                cls.select(System._key, cls.properties_)
                .join(System)
                .where(
                    cls.system_id << db_ids,
                    cls.backend__id == backend.conf_id,
                    cls.mod_stack_id == mods.id,
                )
            )

            # Make sure we have the data we need before we do anything expensive
            if len(db_ids) != props.count():
                raise IncompleteJobsError("Tree cannoted be annoted; Not data")

            for n, p in props.tuples():
                tree[n]["props"] = p

    @staticmethod
    def annotate_child_approx(tree: nx.DiGraph):
        for n, d in tree:
            # TODO: Extend this to all properties
            try:
                equ = child_eq_coefs(tree, n)
            except UnrepresentableException:
                continue

            props = Layer.properties_from_tree(tree, nodes=equ)
            d["approx_props"] = props.to_dict()


class Layer(BaseModel, ModableMixin, PropertyMixin):
    SUPPORTED_MODS = (PreTemplateBase,)

    complete: bool = BooleanField(default=False)
    view: View = ForeignKeyField(View)
    backend_: ConfigModel = ForeignKeyField(ConfigModel)

    class Meta:
        indexes = (
            (("view", "backend_", "mod_stack"), True),
            # Does this mean that the values must be unique?
        )

    @classmethod
    def from_names(cls, view_name: str, backend_name: str, mod_names: Iterable):
        from fragment.backends.util import QMBackend

        backend = QMBackend.obj_from_name(backend_name)
        view = View.from_name(view_name)
        mod_stack = Stack.from_names(*mod_names)

        layer, created = cls.get_or_create(
            view=view, backend__id=backend.conf_id, mod_stack=mod_stack
        )
        if created:
            layer.make_jobs()
        return layer

    @classmethod
    def get_or_create(cls, **kwargs) -> Tuple["Layer", bool]:
        layer, created = super().get_or_create(**kwargs)
        if created:
            layer.make_jobs()
        return layer, created

    def make_jobs(self):
        for batch in peewee.chunked(self.view.fragments, 200):
            (
                Job.insert_many(
                    ((f, self.backend_, self.mod_stack) for f in batch),
                    fields=[Job.system, Job.backend_, Job.mod_stack],
                ).on_conflict_ignore()
            ).execute()

    @cached_property
    def backend(self) -> QMBackend:
        from fragment.backends.util import QMBackend

        return QMBackend.obj_from_conf(self.backend_) if self.backend_ else None

    @property
    def jobs(self):
        """
        TODO: Replace this with a manager
        """
        return Job.select().where(
            Job.system_id << self.view.fragments.frag_query.select(System.id),
            Job.backend_ == self.backend_,
            Job.mod_stack == self.mod_stack,
        )

    @property
    def jobs_w_coefs(self):
        return (
            self.jobs.join(System)
            .join(ViewCoef)
            .where(
                ViewCoef.view == self.view
            )  # see `tests.calculations.test_layering.LayerTestCases.test_aux_views_only`
            .select(Job.properties, ViewCoef.coef)
        )

    @staticmethod
    def properties_from_tree(
        tree: PIETree, nodes: Optional[Iterable[quickNode]] = None
    ) -> PropertySet:
        """Returns a single PropertySet from an anottated tree"""

        if nodes is None:
            nodes = (
                (
                    n,
                    d["coef"],
                )
                for n, d in tree
                if d["coef"] != 0
            )

        # TODO: Make this so it doesn't do all properties
        accumulator = PropertySet.zeros_all_props(tree.target)
        for n, coef in nodes:
            vs = PropertySet.from_dict(tree[n]["props"])
            accumulator.add_into(vs, coef)
        return accumulator

    @property
    def properties(self) -> PropertySet:
        if self.properties_:
            return super().properties
        if self.jobs.where(Job.status != JobStatus.COMPLETED).exists():
            raise IncompleteJobsError("Jobs incomplete")

        # TODO: Make it possible to pre-cache the view?
        tree = self.view.dep_tree
        Job.annotate_properties(tree, self.backend, self.mod_stack)
        props = self.properties_from_tree(tree)
        self.properties_ = props.to_dict()
        return props


class Calculation(BaseModel, NamedAndNotedMixin, PropertyMixin):
    complete: str = BooleanField(default=False)

    # Information store in json instead of
    # many-to-many table for simplicity
    job_structure: List[Dict[int, int]] = JSONField(default=lambda: {})

    @classmethod
    def from_conf(cls, name, note, layer_conf):
        layers: List[Layer] = []
        for l_conf in layer_conf:
            layers.append(
                {
                    "coef": 1,
                    "layer": Layer.from_names(
                        l_conf["view"], l_conf["backend"], l_conf.get("mods", ())
                    ),
                }
            )

        cls.make_intermediate_layers(layers)

        layer_ids = [{"coef": l["coef"], "layer_id": l["layer"].id} for l in layers]

        return cls.create(name=name, note=note, job_structure=layer_ids)

    @staticmethod
    def make_intermediate_layers(layers: List[Layer]) -> None:
        if len(layers) < 2:
            return

        added_layers: List[Tuple[int, Layer]] = []
        for i in range(len(layers) - 1):
            geom_layer = layers[i]["layer"]
            theory_layer = layers[i + 1]["layer"]

            new_layer, created = Layer.get_or_create(
                view=geom_layer.view,
                mod_stack=geom_layer.mod_stack,
                backend__id=theory_layer.backend__id,
            )
            added_layers.append((i + 1, {"coef": -1, "layer": new_layer}))

        # Add them in reverse order so as not to have to calculate
        # new indecies
        for l in reversed(added_layers):
            layers.insert(l[0], l[1])

    @cached_property
    def layers(self) -> List[Layer]:
        """
        TODO: Opimize
        """
        return [Layer.get(id=l["layer_id"]) for l in self.job_structure]

    def add_layer(self, layer: Layer):
        if not self.job_structure:  # The simplest case is no jobs
            self.job_structure = [{"coef": 1, "layer_id": layer.id}]
            return

        last_layer = Layer.get(self.job_structure[-1]["layer_id"])

        # Make an intermediate layer
        int_layer, _ = Layer.get_or_create(
            view_id=last_layer.view_id,
            mod_stack_id=last_layer.mod_stack_id,
            backend__id=layer.backend__id,
        )

        self.job_structure.append({"coef": -1, "layer_id": int_layer.id})
        self.job_structure.append({"coef": 1, "layer_id": layer.id})

    @cached_property
    def coefs(self) -> List[int]:
        return [l["coef"] for l in self.job_structure]

    @property
    def properties(self) -> PropertySet:
        if self.properties_:
            return super().properties
        atoms = set.union(*(l.view.dep_tree.target for l in self.layers))
        accumulator = PropertySet.zeros_all_props(atoms)
        for c, l in zip(self.coefs, self.layers):
            accumulator.add_into(l.properties, c)
        self.properties_ = accumulator.to_dict()
        return accumulator

    @property
    def jobs(self) -> Iterable[Job]:  # Technically a ModelSelect by good enough
        # for most use cases
        job_query = None
        for layer in self.layers:
            if job_query is None:
                job_query = layer.jobs
                continue
            job_query = job_query.union(layer.jobs)
        return job_query
