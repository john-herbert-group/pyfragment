from pkg_resources import resource_filename

RESOURCE_DIR = resource_filename(__name__, "")
STRATEGY_SCHEMA_PATH = resource_filename(__name__, "strategy.yaml")
CALCULATION_SCHEMA_PATH = resource_filename(__name__, "calculation.yaml")
