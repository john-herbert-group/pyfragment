import pydantic


class FragmentBaseModel(pydantic.BaseModel):
    class Config:
        orm_mode = True


class NamedAndNotedModel(FragmentBaseModel):
    name: str
    note: str
