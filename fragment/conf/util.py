from fragment.conf.models import ConfigModel
from fragment.registry import REGISTRY


class ConfigDoesNotExist(Exception):
    pass


class SavableConfig:
    CONFIG_TYPE = None

    def __init__(
        self, name, note, *args, conf_id=None, save_config=False, **kwargs
    ) -> None:
        self.name = name
        self.note = note
        self.args = list(args)
        self.kwargs = kwargs
        self.conf_id = conf_id
        if save_config:
            self.save_config()

    @classmethod
    def get_or_create(
        cls,
        name,
        note,
        *args,
        config_type=None,
        save_config=None,  # Remove from kwargs since it's removed by __init__
        **kwargs,
    ) -> "SavableConfig":
        try:
            config = cls.obj_from_name(
                name,
                config_type=config_type,
            )
        except ConfigDoesNotExist:
            return cls(name, note, *args, save_config=True, **kwargs)

        # Check that the requested configs match
        try:
            # test_cfg = config.__class__(config.name, config.name, *args, **kwargs)
            # assert test_cfg.args == config.args
            # assert test_cfg.kwargs == config.kwargs
            pass
        except AssertionError as e:
            print(list(test_cfg.args), test_cfg.kwargs)
            print(config.args, config.kwargs)
            raise ValueError(
                f"The requested `{cls.CONFIG_TYPE}` conflicts"
                f" with an existing item in the DB: {name}"
            ) from e

        return config

    @classmethod
    def obj_from_name(cls, name, config_type=None) -> "SavableConfig":
        config_type = config_type or cls.CONFIG_TYPE
        try:
            conf = ConfigModel.get(
                name=name,
                config_type=config_type,
            )
        except ConfigModel.DoesNotExist:
            raise ConfigDoesNotExist(
                f"Unknown {config_type.name.lower()} named '{name}'"
            )
        else:
            return cls.obj_from_conf(conf)

    @classmethod
    def obj_from_conf(cls, conf: ConfigModel) -> "SavableConfig":
        RetClass = cls
        RetClass = REGISTRY.get(conf.config_class)

        return RetClass(
            conf.name,
            conf.note,
            *conf.args,
            conf_id=conf.id,
            **conf.kwargs,
            save_config=False,
        )

    def save_config(self):
        if self.conf_id is None:
            conf_model = ConfigModel.create(
                name=self.name,
                note=self.note,
                config_type=self.CONFIG_TYPE,
                config_class=self.__class__.__name__,
                args=self.args,
                kwargs=self.kwargs,
            )
            self.conf_id = conf_model.id
        else:
            (
                ConfigModel.update(
                    {ConfigModel.args: self.args, ConfigModel.kwargs: self.kwargs}
                ).where(ConfigModel.id == self.conf_id)
            ).execute()
