import enum
from typing import Dict, List

from peewee import CharField
from playhouse.sqlite_ext import JSONField

from fragment.db.mixins import NamedAndNotedMixin, SlugField
from fragment.db.models import BaseModel


class ConfigType(enum.Enum):
    QM_BACKEND = 0
    FRAGMENTER = 1
    MOD = 2


class ConfigModel(BaseModel, NamedAndNotedMixin):
    """
    This class is used to store and regenerate config objects
    for future use.
    """

    name: str = SlugField(max_length=255, null=False, index=True, unique=False)
    config_type: str = CharField(max_length=30)
    config_class: str = CharField(max_length=30)
    args: List = JSONField(default=lambda: [])
    kwargs: Dict = JSONField(default=lambda: {})

    class Meta:
        indexes = ((("name", "config_type"), True),)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(name='{self.name}', config_type='{self.config_type}')"
