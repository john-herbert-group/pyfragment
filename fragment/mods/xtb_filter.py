import logging
from itertools import combinations
from typing import Dict, Tuple

import networkx as nx
import qcelemental as qcel

from fragment.backends.abstract import QMBackend
from fragment.backends.common import RunContext
from fragment.backends.libxtb import LibXTBBackend
from fragment.calculations.common import JobStatus
from fragment.calculations.models import Job
from fragment.core import PIETree
from fragment.core.PIE_common import ROOT
from fragment.core.quickPIE import mk_quickPlan_dirty, quickNode
from fragment.mods.abstract import AuxFragGenBase, PostAuxFragBase
from fragment.systems.models import System, View, ViewCoef

log = logging.getLogger(__name__)

kcalmol_to_hartree = qcel.constants.conversion_factor("kcal / mol", "hartree")
DEFAULT_THRESHOLDS = {3: 0.25, 4: 0.05}


def energy_from_system(backend: QMBackend, system: System) -> Tuple[quickNode, float]:
    ctx = RunContext(0, "tmp", system, backend_id=backend.conf_id, cores=1)
    energy = backend.run_ctx(ctx).properties["total_energy"]
    return system.key, energy


class EnergyModBase:
    supersystem: System = None
    backend: QMBackend

    def __init__(
        self,
        name: str,
        note: str,
        backend: str = None,
        thresholds: Dict[int, float] = None,
        **kwargs
    ) -> None:
        # Call the master constructor to make this saveable
        super().__init__(name, note, backend=backend, thresholds=thresholds, **kwargs)
        self.backend_name = backend
        if thresholds:
            self.thresholds = {
                int(k): v * kcalmol_to_hartree for k, v in thresholds.items()
            }
        else:
            self.thresholds = DEFAULT_THRESHOLDS

    def build_energy_cache(self, supersystem: System):
        self.backend = QMBackend.obj_from_name(self.backend_name)
        if not self.backend.FILELESS:
            log.warn("Filtering on file-based QM backend.")

        if self.supersystem and self.supersystem.id == supersystem.id:
            # print("Using existing cache")
            return
        self.supersystem = supersystem

        self.energy_cache = dict()
        completed = (
            View.select()
            .join(ViewCoef)
            .join(System)
            .join(Job, on=(Job.system == ViewCoef.fragment))
            .select(System._key, Job.properties_["total_energy"])
            .where(
                View.supersystem == self.supersystem,
                Job.status == JobStatus.COMPLETED,
                Job.backend_ == self.backend.conf_id,
            )
            .distinct()
        )

        for k, te in completed.tuples().iterator():
            if te is None:
                continue
            self.energy_cache[k] = te

    def int_energy(self, order: int, *systems: System):
        supersys = System.merge(*systems)

        ss_energy = self.energy_from_system(supersys)
        ap_energy = self.approx_energy(order, supersys, *systems)

        return ss_energy - ap_energy

    def approx_energy(self, order: int, supersystem: System, *system: System) -> float:
        primaries = [s.key for s in system]

        plan = mk_quickPlan_dirty(
            (frozenset.union(*d) for d in combinations(primaries, order - 1))
        )

        energy = 0.0
        for n, c in plan.items():
            energy += self.energy_from_system(self.supersystem.subset(n)) * c
        return energy

    def energy_from_system(self, system: System) -> float:
        try:
            return self.energy_cache[system.key]
        except KeyError:
            k, v = energy_from_system(self.backend, system)
            self.energy_cache[k] = v
            return v


class XTBEnergyMod(EnergyModBase, AuxFragGenBase):
    backend: LibXTBBackend
    backend_name: str
    energy_cache: Dict[quickNode, int]

    def setUp(self, supersystem: System, pfrags: View, order: int, m: int):
        self.build_energy_cache(supersystem)

    def run(self, *systems: System) -> bool:
        order = len(systems)

        if order == 1:
            return True

        thresh = self.thresholds.get(order, 0)
        if not thresh:
            return True

        int_energy = self.int_energy(order, *systems)
        if abs(int_energy) >= thresh:
            return True
        else:
            return False


class XTBChildrenEnergyMod(EnergyModBase, AuxFragGenBase):
    backend: LibXTBBackend
    backend_name: str
    energy_cache: Dict[quickNode, int]
    m: int
    order: int

    def setUp(self, supersystem: System, pfrags: View, order: int, m: int):
        self.m = m
        self.order = order
        self.build_energy_cache(supersystem)

    def run(self, *systems: System) -> bool:
        order = len(systems)
        # _M = max(order - self.m, 0)
        _M = self.m

        if order == 1:
            return True

        thresh = self.thresholds.get(order, 0)
        if not thresh:
            return True

        # Make child systems
        bad = 0
        for children in combinations(systems, order - 1):
            try:
                child_IE = self.int_energy(order - 1, *children)
            except AttributeError:
                child_IE = 0.0
            if abs(child_IE) < thresh:
                bad += 1

                if bad > _M:
                    return False

        return True


class XTBChildrenEnergyProductMod(EnergyModBase, AuxFragGenBase):
    backend: LibXTBBackend
    backend_name: str
    energy_cache: Dict[quickNode, int]

    def __init__(
        self,
        name: str,
        note: str,
        backend: str = None,
        thresholds: Dict[int, float] = None,
        **kwargs
    ) -> None:
        super().__init__(name, note, backend=backend, thresholds=thresholds, **kwargs)
        # Convert from (kcal/mol)**2
        for k in self.thresholds:
            self.thresholds[k] *= kcalmol_to_hartree

    def setUp(self, supersystem: System, pfrags: View, order: int, m: int):
        self.build_energy_cache(supersystem)

    def run(self, *systems: System) -> bool:
        order = len(systems)

        if order == 1:
            return True

        thresh = self.thresholds.get(order, 0)
        if not thresh:
            return True

        # Make child systems
        child_IEs = [
            self.int_energy(order - 1, *cs) for cs in combinations(systems, order - 1)
        ]
        products = [abs(a * b) for a, b in combinations(child_IEs, 2)]

        return sum(products) >= thresh


class EnergyNodeTrimming(EnergyModBase, PostAuxFragBase):
    """
    Generates fragments with full-supersytem basis as ghost atoms
    """

    def __init__(
        self,
        name: str,
        note: str,
        backend: str = None,
        thresholds: Dict[int, float] = None,
        **kwargs
    ) -> None:
        # Call the master constructor to make this saveable
        super().__init__(name, note, backend=backend, thresholds=thresholds, **kwargs)
        self.backend_name = backend
        self.thresholds = thresholds or DEFAULT_THRESHOLDS
        self.thresholds = {
            int(k): v * kcalmol_to_hartree for k, v in self.thresholds.items()
        }

    def setUp(self, supersystem: System, pfrags: View, m: int):
        self.build_energy_cache(supersystem)

    def get_int_energy(self, tree: PIETree, k: quickNode) -> None:
        d = tree[k]
        if d["order"] not in self.thresholds:
            return

        thresh = self.thresholds.get(d["order"], None)

        # Get child eq from tree
        child_eq = tree.child_equivalent(k)

        energy = self.energy_from_system(self.supersystem.subset(k))

        for nk, nd in child_eq:
            energy -= self.energy_from_system(self.supersystem.subset(nk)) * nd["coef"]

        if abs(energy) > thresh:
            return
        else:
            tree.replace_with_children(k, child_eq)

    def run(self, view: View):
        tree = view.dep_tree
        for k in nx.topological_sort(tree.tree):
            if k == ROOT:
                continue
            self.get_int_energy(tree, k)

        # NOW UPDATE THE VIEW. This is getting anoying
        tree.clean_zeros()
        systems = []
        coefs = []
        for k, d in tree:
            systems.append(self.supersystem.subset(k))
            coefs.append(d["coef"])

        del view.fragments  # Delete the FragmentManager
        view.dep_tree = tree
        view.fragments.add(*systems, coefs=coefs)
        return view
