from abc import ABC, abstractmethod
from typing import Any, Iterable, List, Tuple

from fragment.conf.models import ConfigType
from fragment.conf.util import SavableConfig
from fragment.systems.models import System, View


class ModBaseClass(SavableConfig, ABC):
    """Mods alter data in a fragment workflow

    Abstract base class other mods

    Attributes:
        CONFIG_TYPE: see :class:`.SavableConfig`
    """

    CONFIG_TYPE = ConfigType.MOD

    def setUp(self):
        """Prepairs the mod

        Setup may be used to pre-calculate important factors
        which may be used into the run function. For example
        KDTrees can be pre-calculated in advanced.
        """
        pass

    @abstractmethod
    def run(self):
        """Executes the mod

        The arguements and return values will depend on the specific event
        """
        pass

    @classmethod
    def get_applicator(
        cls, mods: Iterable["ModBaseClass"], *setUp_args, **setUp_kwargs
    ) -> "ModApplicator":
        """Returns a function which will evaluate all mods in the stack

        Args:
            mods (Iterable[ModBaseClass]): Mods to consider. Any mods which do
                not apply in this context will be filtered out.
            *setUp_args: args which will be passed to the
                :class:`~.ModBaseClass.setUp` function.
            **setUp_kwargs: kwargs which will be passed to the
                :class:`~.ModBaseClass.setUp`

        Returns:
            ModApplicator: Function which executes all mods

        Todo:
            * Handle input/output from filter functions
        """

        applicator = ModApplicator(cls, *mods)
        applicator.setUp(*setUp_args, **setUp_kwargs)
        return applicator


class FilterBaseClass(ModBaseClass):
    """Governs control-flow in fragment workflows"""

    @classmethod
    def get_applicator(
        cls, mods: Iterable["ModBaseClass"], *setUp_args, **setUp_kwargs
    ) -> "FilterApplicator":
        """Returns a filter function which returns a Boolean

        Args:
            mods (Iterable[FilterBaseClass]): Mods to consider. Any mods which
                do not apply in this context will be filtered out.
            *setUp_args: args which will be passed to the
                :class:`~.ModBaseClass.setUp` function.
            **setUp_kwargs: kwargs which will be passed to the
                :class:`~.ModBaseClass.setUp`

        Returns:
            FilterApplicator: Function which executes all mods

        Todo:
            * Handle input/output from filter functions
        """

        applicator = FilterApplicator(cls, *mods)
        applicator.setUp(*setUp_args, **setUp_kwargs)
        return applicator


class ModApplicator:
    """Applies a mod of a specific type"""

    mods: List[ModBaseClass]
    configured: bool

    def __init__(self, mod_class, *mods: ModBaseClass) -> None:
        self.mods = [m for m in mods if isinstance(m, mod_class)]
        self.configured = False

    def __call__(self, start_val: Any) -> Any:
        if not self.configured:
            raise AttributeError("ModApplicator has not been configured")

        input_val = start_val
        for m in self.mods:
            input_val = m.run(input_val)
        return input_val

    def setUp(self, *args: Any, **kwargs: Any) -> None:
        self.configured = True
        for m in self.mods:
            try:
                m.setUp(*args, **kwargs)
            except:
                print(f"Error adding {m}")
                raise

    def is_empty(self) -> bool:
        return not len(self.mods)


class FilterApplicator(ModApplicator):
    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        if not self.configured:
            raise AttributeError("FilterApplicator has not been configured")

        for m in self.mods:
            if not m.run(*args, **kwargs):
                return False
        return True

    def filter(self, arg_list: List[Tuple[Any, ...]]) -> Any:
        for args in arg_list:
            if self(*args):
                yield args


class PreTemplateBase(ModBaseClass):
    """Pre-template event handler

    This event is triggered before the system is passed to the backend to be
    templated. The :meth:`.run` function SHOULD NOT SAVE the fragment. These
    changes will be transient, but repeatable which prevents saving throusands
    of capping and ghost atoms in the DB.

    An example of a pre-template mod would be a capping method takes in a system
    and adds hydrogens to compensate for severed bonds.

    Attributes:
        supersystem (:obj:`System`): The other system used to generate caps.
            This is generally the supersystem of a given fragment.
    """

    def setUp(self, supersystem: System):
        """Setup the pre-template filter

        Args:
            supersystem (System): Reference system for the run method
        """
        self.supersystem = supersystem

    @abstractmethod
    def run(self, system: System) -> System:
        """Modifies :obj:`system`

        Run function should modify, but not save, the
        provided system which will then be passed to the
        backend's tempalte

        Args:
            system (System): sub-system of the provided supersytem in the setUp
            phase

        Returns:
            System:  modified, un-saved system
        """
        pass


class PostPrimaryFragBase(ModBaseClass):
    """Post-primary fragment generation event handler

    This event is triggered after primary fragments have been generated and
    before auxiliary fragments are created. This event is supplied the
    :class:`~fragment.system.moduls.View` produced by primary fragmentation and
    the reslult of this mod is passed to the aux. fragmentation process

    Attributes:
        supersystem (:obj:`System`): The other system used to generate caps.
            This is generally the supersystem of a given fragment.
    """

    def setUp(self, supersystem: System):
        """Setup the Post-primary fragment mod

        Args:
            supersystem (System): Reference system for the run method
        """
        self.supersystem = supersystem

    @abstractmethod
    def run(self, view: View) -> View:
        """Modifies :obj:`view`

        This function should accpet a view and modify it as needed

        Args:
            view (View): view to modify

        Returns:
            View:  modified view
        """
        pass


class PostAuxFragBase(ModBaseClass):
    """Post-auxiliary fragment generation event handler

    This event is triggered after auxiliary fragments have been generated and
    before this view is saved. This event is supplied the
    :class:`~fragment.system.moduls.View` produced by auxiliary fragmentation.

    Attributes:
        supersystem (:obj:`System`): The other system used to generate caps.
            This is generally the supersystem of a given fragment.
        pfrags (:obj:`View`): The primary fragments which were used to generate
            the auxiliary fragments in this view.
    """

    def setUp(self, supersystem: System, pfrags: View, m: int):
        """Setup the Post-primary fragment mod

        Args:
            supersystem (System): Reference system for the run method
            pfrags (View): The primary fragments for the supersystem
        """
        self.supersystem = supersystem
        self.pfrags = pfrags

    @abstractmethod
    def run(self, view: View) -> View:
        """Modifies :obj:`view`

        This function should accpet a view and modify it as needed.

        Args:
            view (View): view to modify

        Returns:
            View:  modified view
        """
        pass


class AuxFragGenBase(FilterBaseClass):
    """Auxiliary fragment generation filter

    This event is triggered during aux fragment generation. If :meth:`.run`
    returns True, the supplied systems are combined to form a root auxiliary
    fragment.

    Attributes:
        supersystem (:obj:`System`): The other system used to generate caps.
            This is generally the supersystem of a given fragment.
        pfrags (:obj:`View`): The primary fragments which were used to generate
            the auxiliary fragments in this view.
    """

    def setUp(self, supersystem: System, pfrags: View, order: int, m: int):
        """Setup the auxiliary fragment filter

        Args:
            supersystem (System): Reference system for the run method
            pfrags (View): The primary fragments for the supersystem
            order (int): Order of the fragmentation
        """
        self.supersystem = supersystem
        self.pfrags = pfrags

    @abstractmethod
    def run(*systems: System) -> bool:
        """Enable or disable auxiliary fragmentation

        Filter out systems and prevent them from forming root
        auxiliary fragments.

        Args:
            *systems: Primary fragment that form the aux fragment

        Returns:
            bool: Include system in aux-fragment generation
        """
        pass


class PreRunBase(FilterBaseClass):
    """Pre-run event handler

    This event is triggered before running each calculation. If the filter
    returns False, the calculation is not run

    Args:
        FilterBaseClass ([type]): [description]
    """

    @abstractmethod
    def run(*systems: System) -> bool:
        """Enable or disable auxiliary fragmentation

        Filter out systems and prevent them from forming root
        auxiliary fragments.

        Args:
            *systems: Systems to be filtered

        Returns:
            bool: Include system in aux-fragment generation
        """
        pass
