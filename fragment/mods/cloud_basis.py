from copy import deepcopy

from fragment.core.PIE_common import Key
from fragment.core.PIETree import PIETree
from fragment.mods.abstract import PostAuxFragBase
from fragment.systems.common import AtomType
from fragment.systems.models import System, View
from fragment.util.spatial import neighbor_cloud


def swap_nodes(tree: PIETree, old_key: Key, new_key: Key):
    d = deepcopy(tree[old_key])
    tree.new_node(new_key, 0)
    d.pop("data")  # Remove data set
    tree[new_key].update(**d)  # Use.update(**d, ) data

    in_edges = list(tree.tree.in_edges(old_key))
    for _in in in_edges:
        tree.tree.add_edge(_in[0], new_key)
        tree.tree.remove_edge(*_in)
    del in_edges

    out_edges = list(tree.tree.out_edges(old_key))
    for _out in out_edges:
        tree.tree.add_edge(new_key, _in[1])
        tree.tree.remove_edge(*_out)
    del out_edges

    tree.tree.remove_node(old_key)


class UseCloudBasisMod(PostAuxFragBase):
    """
    Generates fragments with basis of ghost atoms
    """

    def __init__(self, name, note, cutoff=5.0, **kwargs) -> None:
        super().__init__(
            name,
            note,
            cutoff=cutoff,
            **kwargs,
        )

        self.cutoff = cutoff

    def setUp(self, supersystem: System, pfrags: View, m: int):
        self.k = len(supersystem.atoms)
        return super().setUp(supersystem, pfrags, m)

    def run(self, view: View):
        real_to_ghost = {
            a.id: a.convert_to(AtomType.GHOST) for a in view.supersystem.atoms
        }

        for f in view.fragments:
            old_key = f.key
            ghost_cloud = neighbor_cloud(
                f, self.supersystem, cutoff=self.cutoff, k=self.k
            )
            ghosts = [a.id for a in ghost_cloud]

            # Update the system
            f.atoms.add(*[real_to_ghost[i] for i in ghosts])
            f.introspect_key()

            # Update the tree
            swap_nodes(view.dep_tree, old_key, f.key)

        return view
