from typing import Iterable

from peewee import ForeignKeyField, Model

from fragment.mods.abstract import ModBaseClass
from fragment.mods.models import Stack


class UnsupportedModError(Exception):
    """Triggered when a un-supported mod is supplied"""

    pass


class ModableMixin(Model):
    """Mixen to tie a mod :class:`~fragment.mods.models.Stack` to a model

    Attributes:
        mod_stack (:obj:`fragment.mods.models.Stack`): Instance of mods for this
            model instance. If no mods are specified this is the null
            :class:`.Stack`.
        SUPPORTED_MODS (:obj:`tuple` of :obj:`ModBaseClass`): Tuple of supported
            mod classes. This should correspond to evens which may be triggered
            by this class.
    """

    mod_stack = ForeignKeyField(Stack, default=lambda: Stack.from_names())  # Null stack
    SUPPORTED_MODS = tuple()

    def __init__(self, *args, **kwargs):
        """Pass-through constructor which type checks the mod_stack parameter

        Args:
             *args: Other arguments.
             mod_stack (:obj:`.Stack`, optional): Mod stack instance. If left
                 empty, the null stack will be used.
             **kwargs: Other keyword argumetns.
        """

        if kwargs.get("__no_default__", 0) == 1:
            # Return as we are restoring from the DB
            return super().__init__(*args, **kwargs)

        mod_stack = kwargs.get("mod_stack", None)
        if mod_stack:
            for m in mod_stack.mods:
                if not isinstance(m, self.SUPPORTED_MODS):
                    raise UnsupportedModError(
                        f"{self.__class__} does not support mods of type {m.__class__}"
                    )
        return super().__init__(*args, **kwargs)

    @property
    def mods(self) -> Iterable[ModBaseClass]:
        """Shortcut for :attr:`.Stack.mod_stack.mods`"""
        return self.mod_stack.mods

    def save(self, **kwargs):
        """Saves :attr:`.mod_stack` in addition to `self`

        Args:
            **kwargs: keyword arguments
        """
        if self.mod_stack.id is None:
            self.mod_stack.save()
        return super().save(**kwargs)
