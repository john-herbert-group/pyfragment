import logging
from collections import defaultdict
from itertools import combinations
from typing import Dict, Optional, Union

import numpy as np

from fragment.mods.abstract import AuxFragGenBase
from fragment.systems.models import System, View
from fragment.util.indexing import KeyToIndex
from fragment.util.spatial import MAX_VALUE, COM_dm, closest_approach_dm

log = logging.getLogger(__name__)


class DistanceMod(AuxFragGenBase):
    """Ditance Filter

    TODO: Fix bug that adds MOD postfix to filters and change the name of the class accordingly to something more appropriate

    Attributes:
        name (str): name of the mod
        note (str): human-readable note on this mod
        distance (float): distance cut off for screening/filtering aux_fragments
    """

    dm: np.ndarray
    fk: KeyToIndex

    def __init__(
        self,
        name: str,
        note: str,
        min_distance: Optional[Union[float, Dict[int, float]]] = None,
        max_distance: Optional[Union[float, Dict[int, float]]] = None,
        distance: Optional[float] = None,  # DEPRICATED: Alias for max_distance
        method: Optional[str] = None,
        **kwargs,
    ) -> None:
        # Convert keys to ints
        if isinstance(min_distance, dict):
            min_distance = {int(k): v for k, v in min_distance.items()}
        if isinstance(max_distance, dict):
            max_distance = {int(k): v for k, v in max_distance.items()}

        # Call the master constructor to make this saveable
        super().__init__(
            name,
            note,
            min_distance=min_distance,
            max_distance=max_distance,
            distance=distance,
            method=method,
            **kwargs,
        )

        # Method
        self.method = (method if not method is None else "com").lower()

        # Min distance cutoff
        if isinstance(min_distance, float):
            self.min_distances = defaultdict(lambda: min_distance)
        elif min_distance is None:
            self.min_distances = defaultdict(lambda: MAX_VALUE)
        else:
            self.min_distances = defaultdict(lambda: MAX_VALUE, min_distance.items())

        # Max distance cutoff
        if not (distance is None):  # Depricated
            max_distance = distance
            log.warning("`distance` keyword is depricated. Use `max_distance`")

        if isinstance(max_distance, float):
            self.max_distances = defaultdict(lambda: max_distance)
        elif max_distance is None:
            self.max_distances = defaultdict(lambda: MAX_VALUE)
        else:
            self.max_distances = defaultdict(lambda: MAX_VALUE, max_distance.items())

        # Check that this mod will do something
        if max_distance is None and min_distance is None:
            raise ValueError("Either `min_stance` or `max_distance` must be supplied")

    def setUp(self, supersystem: System, pfrags: View, order: int, m: int):
        self.fk = KeyToIndex.from_Systems(pfrags.fragments)

        if self.method == "com":
            self.dm = COM_dm(pfrags, self.fk)
        else:
            self.dm = closest_approach_dm(pfrags, self.fk)

        return super().setUp(supersystem, pfrags, order, m)

    def run(self, *systems: System) -> bool:
        order = len(systems)

        if order == 1:  # Base case
            return True

        # pair-wise check the distances
        d = np.array(
            [
                self.dm[self.fk(frag1), self.fk(frag2)]
                for frag1, frag2 in combinations(systems, 2)
            ]
        )

        min_d = self.min_distances[order]
        if min_d and d.min() >= min_d:
            return False

        max_d = self.max_distances[order]
        if max_d and d.max() >= max_d:
            return False

        return True
