from copy import deepcopy

import networkx as nx

from fragment.core.PIE_common import Key
from fragment.core.PIETree import PIETree
from fragment.mods.abstract import PostAuxFragBase, PreTemplateBase
from fragment.systems.common import AtomType
from fragment.systems.models import System, View


def swap_nodes(tree: PIETree, old_key: Key, new_key: Key):
    d = deepcopy(tree[old_key])
    tree.new_node(new_key, 0)
    d.pop("data")  # Remove data set
    tree[new_key].update(**d)  # Use.update(**d, ) data

    in_edges = list(tree.tree.in_edges(old_key))
    for _in in in_edges:
        tree.tree.add_edge(_in[0], new_key)
        tree.tree.remove_edge(*_in)
    del in_edges

    out_edges = list(tree.tree.out_edges(old_key))
    for _out in out_edges:
        tree.tree.add_edge(new_key, _in[1])
        tree.tree.remove_edge(*_out)
    del out_edges

    tree.tree.remove_node(old_key)


class UseSupersystemBasisMod(PostAuxFragBase):
    """
    Generates fragments with full-supersytem basis as ghost atoms

    Depricated. Use ClusterBasisMod. Updating the keys is confusing.
    """

    def run(self, view: View):
        real_to_ghost = {
            a.id: a.convert_to(AtomType.GHOST) for a in view.supersystem.atoms
        }

        all_real_atoms = set(view.supersystem._key)

        for f in view.fragments:
            old_key = f.key
            real_atoms = set(old_key)
            ghosts = all_real_atoms.difference(real_atoms)

            # Update the system
            f.atoms.add(*[real_to_ghost[i] for i in ghosts])
            f.introspect_key()

            # Update the tree
            swap_nodes(view.dep_tree, old_key, f.key)

        return view


class ClusterBasisMod(PreTemplateBase):
    """
    Generates fragments with full-supersytem basis as ghost atoms
    """

    def setUp(self, supersystem: System):
        super().setUp(supersystem)
        self.real_to_ghost = {
            a.id: a.convert_to(AtomType.GHOST) for a in supersystem.atoms
        }
        self.all_real_atoms = set(supersystem._key)

    def run(self, system: System):
        # Makes the assumption that every atom has an ID
        # Perhaps we need set operations
        existing = {a.id for a in system.atoms}
        ghosts = self.all_real_atoms.difference(existing)

        # Update the system
        system.atoms.add(*[self.real_to_ghost[i] for i in ghosts])

        return system
