import numpy as np
import qcelemental as qcel

from fragment.mods.abstract import PreTemplateBase
from fragment.systems.models import Atom, System
from fragment.util.spatial import neighbors


class RCapsMod(PreTemplateBase):
    """R Capper

    TODO: Add math for capping

    Attributes:
        name (str): name of the mod
        note (str): human-readable note on this mod
        tolerance (float): fudge factor used when determining if a bond exists
        cutoff (float): maximum distance to search for capping atoms
        k (int): Number of nearest neighbor atoms to consider
        ignore_charged (bool): prevents capping of atoms that are charged (i.g. metal centers)
    """

    def __init__(
        self, name, note, tolerance=1.2, cutoff=3.0, k=8, ignore_charged=False, **kwargs
    ) -> None:
        super().__init__(
            name,
            note,
            tolerance=tolerance,
            k=k,
            cutoff=cutoff,
            ignore_charged=ignore_charged,
            **kwargs,
        )

        self.tolerance = tolerance
        self.cutoff = cutoff
        self.H_radius = qcel.covalentradii.get("H", units="angstrom")
        self.k = k
        self.ignore_charged = ignore_charged

    def expected_bond_length(self, a1: Atom, a2: Atom, tolerance=1.2) -> float:
        return (a1.covalent_radius + a2.covalent_radius) * self.tolerance

    def cap_position(
        self, inner_atom: Atom, outer_atom: Atom, distance: float = 0.0
    ) -> np.array:
        # Create new atom at a shorter distance from the inner
        # atom. The bond length should be about that of a hydrogen
        if not distance:
            distance = Atom.distance(inner_atom, outer_atom)

        _shortening = (outer_atom.covalent_radius + self.H_radius) / (distance)
        return inner_atom.r + _shortening * (outer_atom.r - inner_atom.r)

    def run(self, frag: System) -> System:
        capped_atom_ids = set()  # Let's keep track so we don't
        # add a proxy for two atoms
        capping_atoms = []

        for innner_index, outer_index_list, distances in neighbors(
            frag, self.supersystem, cutoff=self.cutoff, k=self.k
        ):
            inner_atom = frag.atoms[innner_index]

            # Don't cap if charged
            if self.ignore_charged and inner_atom.charge != 0:
                continue

            for outer_index, distance in zip(outer_index_list, distances):
                outer_atom = self.supersystem.atoms[outer_index]

                # Don't make caps from phantom atoms
                if inner_atom.is_placeholder or outer_atom.is_placeholder:
                    continue

                # Don't make caps that are too far away
                if distance > self.expected_bond_length(
                    inner_atom, outer_atom, tolerance=self.tolerance
                ):
                    continue

                # Do not cap if charged
                if self.ignore_charged and outer_atom.charge != 0:
                    continue

                # Prevent double counting
                # NOTE: This method has issues with bidentate ligands around
                #       metal atoms
                if outer_index in capped_atom_ids:
                    raise Exception(
                        f"Atom (id={outer_atom.id}, t={outer_atom.t}) has already been accounted for when capping system {frag.id}"
                    )
                capped_atom_ids.add(outer_index)

                rcap = self.cap_position(inner_atom, outer_atom, distance=distance)
                cap = Atom(
                    "H",
                    r=rcap,
                    meta={
                        "cap_for": [inner_atom.id, outer_atom.id],
                        "proxy_for": outer_atom.id,
                    },
                )
                capping_atoms.append(cap)

        # Generate capping group
        frag.atoms.add(*capping_atoms)
        return frag
