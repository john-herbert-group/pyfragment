from functools import cached_property
from typing import Iterable, Tuple

from peewee import (
    EXCLUDED,
    CompositeKey,
    ForeignKeyField,
    IntegerField,
    IntegrityError,
    Value,
)

from fragment.conf.models import ConfigModel, ConfigType
from fragment.db.mixins import OrderedKeyField
from fragment.db.models import BaseModel
from fragment.mods.util import ModBaseClass


class Stack(BaseModel):
    """A collection of multiple :class:`mods <fragment.mods.abstract.ModBaseClass>`

    Attributes:
        key (:obj:`~fragment.db.mixin.KeyField`): Unique key specifiying the DB
        ID and order of the member mods
    """

    key: Tuple[int] = OrderedKeyField(
        index=True, unique=True
    )  # Reference to the mod-items

    def __repr__(self):
        return f"{self.__class__.__name__}(key={self.key})"

    def __init__(self, *mods, key=None, **kwargs):
        super().__init__(key=key, **kwargs)
        if kwargs.get("__no_default__", 0) == 1:
            return

        self.mods = mods
        if key is None:
            key = self.make_key(mods)
            if None in key:
                raise ValueError("Can only create Stack from saved Mods")
            self.key = key

    @classmethod
    def from_names(cls, *mod_names: str) -> "Stack":
        """Creates a :class:`.Stack` from :attr:`.ModBaseClass.name` identifiers.

        Args:
            *mod_names (str): ordered list of mod names

                Note: Mods must be saved in the DB to be accesses

        Raises:
            ValueError: if a requested mod does not exist in the DB.

        Returns:
            :obj:`.Stack`: New or existing stack instance
        """
        mods = list(
            ConfigModel.select().where(
                ConfigModel.name << mod_names, ConfigModel.config_type == ConfigType.MOD
            )
        )
        if len(mod_names) != len(mods):
            raise ValueError(
                f"One or more Mods with the names {', '.join(mod_names)} does not exist."
            )
        sort_order = {m_n: i for i, m_n in enumerate(mod_names)}
        mods.sort(key=lambda m: sort_order[m.name])
        key = cls.make_key(mods)
        stack = cls.get_or_create__FIXED(key=key)
        stack.mods = cls.get_Mods(mods)
        return stack

    @classmethod
    def get_or_create__FIXED(cls, key=None, **kwargs) -> "Stack":
        """Because PeeWee is broken we will do :meth:`peewee.Model.get_or_create`
        by hand

        Returns:
            :obj:`.Stack`: New or existing model instance
        """
        try:
            stack = cls.get(key=Value(key, unpack=False), **kwargs)
        except cls.DoesNotExist:
            stack = Stack(key=key, **kwargs)
            stack.save()
        return stack

    @staticmethod
    def make_key(mods: Iterable[ModBaseClass]) -> Tuple[int]:
        """Creates savable key for a list of mods.

        Args:
            mods (Iterable[ModBaseClass]): list of saved mods.

        Returns:
            `tuple` of `int`: Ordered tuple of the mods' DB ids.
        """
        key = []
        for m in mods:
            if isinstance(m, ConfigModel):
                key.append(m.id)
            else:
                key.append(m.conf_id)
        return tuple(key)

    def save(self, **kwargs):
        """Saves :attr:`.Stack.mods` in addition to `self`"""
        try:
            ret = super().save(**kwargs)
        except IntegrityError:
            self.id = self.get_or_create__FIXED(key=self.key).id
            ret = None

        (
            ModItem.insert_many(
                [(self.id, m, i) for i, m in enumerate(self.key)],
                fields=[ModItem.stack, ModItem.mod, ModItem.order],
            ).on_conflict(
                conflict_target=[ModItem.stack, ModItem.mod],
                update={ModItem.order: EXCLUDED.order},
            )
        ).execute()
        return ret

    @cached_property
    def mods(self) -> Iterable[ModBaseClass]:
        """Mods associated with this stack

        Returns:
            Iterable[ModBaseClass]: associated (and instantiated) mods
        """
        mods = (
            ModItem.select()
            .where(ModItem.stack == self)
            .order_by(ModItem.order)
            .prefetch(ConfigModel)
        )
        return self.get_Mods(m.mod for m in mods)

    @staticmethod
    def get_Mods(mods: Iterable["ModItem"]) -> Iterable[ModBaseClass]:
        """Converts an iterable :obj:`.ModItem` into instances of :obj:`ModBaseClass`

        Args:
            mods (Iterable[ModItem]): mod items from the database to convert

        Returns:
            Iterable[ModBaseClass]: Instantiated list of :obj:`ModBaseClass`
        """
        from fragment.mods.util import ModBaseClass

        return [ModBaseClass.obj_from_conf(m) for m in mods]


class ModItem(BaseModel):
    stack: Stack = ForeignKeyField(Stack, backref="_mods")
    mod: ConfigModel = ForeignKeyField(ConfigModel)
    order: int = IntegerField()

    def __repr__(self):
        return f"{self.__class__.__name__}(stack={self.stack.id}, mod={self.mod.id}, order={self.order})"

    class Meta:
        primary_key = CompositeKey("stack", "mod")
