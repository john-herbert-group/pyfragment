from typing import Dict, TextIO

from fragment.systems.models import Atom, System, SystemLabel


def XYZRead(file: TextIO, name: str, note: str, charges: Dict[int, int]) -> SystemLabel:
    num_atoms = 0
    atoms = []

    for i, l in enumerate(file):
        if i == 0:  # Number of atoms
            num_atoms = int(l)
            continue
        if i == 1:  # Comment line
            continue

        l_parts = l.split()
        t = l_parts[0]
        x = float(l_parts[1])
        y = float(l_parts[2])
        z = float(l_parts[3])
        try:
            # Charges are 1-based indexed
            # We lose 2 lines from count/comment
            charge = charges[i - 1]
        except KeyError:
            charge = 0
        atoms.append(Atom(t, (x, y, z), charge=charge))

    if num_atoms != len(atoms):
        raise Exception("XYZ input contains a different number of atoms than promised.")

    sys = System(atoms=atoms)
    return SystemLabel(system=sys, name=name, note=note)
