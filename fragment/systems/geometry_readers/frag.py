from io import TextIOWrapper
from typing import Dict, List

from fragment.systems.models import Atom, System, SystemLabel


def FragRead(
    file: TextIOWrapper, name: str, note: str, charges: Dict[int, int]
) -> SystemLabel:
    atoms: List[Atom] = []
    frag_group = 1
    atoms_in_group = 0

    for i, line in enumerate(file):
        line = line.strip()
        if i == 0:  # Number of atoms
            num_atoms = int(line)
            continue
        if i == 1:  # Comment line
            continue
        if line.startswith("---"):
            if atoms_in_group:
                frag_group += 1
            continue
        if not line:
            break
        atoms_in_group += 1
        atoms.append(atom_from_line(line, frag_group))

    for a_id, charge in charges.items():
        atoms[a_id - 1].charge = charge

    if num_atoms != len(atoms):
        raise Exception("XYZ input contains a different number of atoms than promised.")
    system = System(atoms=atoms)
    return SystemLabel(system=system, name=name, note=note)


def atom_from_line(line: str, frag_group: int) -> Atom:
    """
    Creates an atom from a PDB lines
    """
    data = line.split()
    t = data[0]
    x = float(data[1])
    y = float(data[2])
    z = float(data[3])

    return Atom(t, (x, y, z), meta={"frag_group": frag_group})
