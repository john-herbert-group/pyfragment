import os
from hashlib import sha1
from io import StringIO
from typing import Dict, Iterable, Set

from fragment.registry import REGISTRY
from fragment.systems.models import Atom, AtomToSystem, System, SystemLabel

from .pdb import PDBRead
from .xyz import XYZRead


class UnsupportedFileType(Exception):
    pass


class SystemExists(Exception):
    pass


class SystemDoesNotExist(Exception):
    pass


def hash_file_contents(fs_content: str) -> str:
    return sha1(fs_content.encode("utf-8")).hexdigest()


def atom_selector(atoms: Iterable[Atom], selectors: str) -> Set[int]:
    pass


def system_selector(name: str) -> Set[int]:
    aq = AtomToSystem.select(AtomToSystem.atom).join(System).where(System.name == name)
    return {a for a in aq.scalar()}


def group_selector(atoms: Iterable[Atom], selectors: str) -> Set[int]:
    pass


def read_geometry(
    name: str,
    note: str = None,
    charges: Dict[int, int] = None,
    path=None,
    source=None,
    ext=None,
    save=True,
) -> System:
    # Normalize the file source
    # This could (and probably should) be it's own function
    close_source = False
    if source:
        if ext is None:
            raise TypeError("Extension must be provided to use source")
        if isinstance(source, str):
            source = StringIO(source)
    elif path:
        if not os.path.isfile(path):
            raise FileNotFoundError("Input file `{}` does not exist.".format(path))
        source = open(path)
        close_source = True
        ext = os.path.splitext(path)[1][1:]
    else:
        raise TypeError("`path` or `source` must be provided")

    source_str = source.read()
    source.seek(0)
    hash = hash_file_contents(source_str)
    note = note or ""

    # Check for a duplicate file
    label_q = SystemLabel.select(SystemLabel.name, SystemLabel.hash).where(
        SystemLabel.hash == hash
    )
    if label_q.exists():
        name = label_q[0].name
        if close_source:
            source.close()
        raise SystemExists(
            f"System with identical contents already exists with the name '{name}'"
        )

    if note is None:
        note = ""

    # Get geometry reader function and use it
    geometry_reader = REGISTRY.get_from_namespace("read", ext.lower())

    if geometry_reader is None:
        raise UnsupportedFileType(
            'Fragment does not support reading ".{}" geometry files.'.format(ext)
        )

    sys_label: SystemLabel = geometry_reader(source, name, note, charges=charges or {})
    sys: System = sys_label.system
    sys_label.hash = hash

    if save:
        sys.save()
        sys_label.save()

    # Close file only if we opened it
    if close_source:
        source.close()

    return sys
