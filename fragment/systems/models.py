from copy import deepcopy
from functools import cached_property
from itertools import chain
from typing import Any, Dict, Iterable, Iterator, List, MutableSet, Tuple, Union

import networkx as nx
import numpy as np
from peewee import (
    EXCLUDED,
    JOIN,
    CharField,
    CompositeKey,
    FloatField,
    ForeignKeyField,
    IntegerField,
    Value,
    chunked,
)
from playhouse.sqlite_ext import JSONField
from qcelemental import covalentradii
from qcelemental import periodictable as ptable
from qcelemental import vdwradii
from scipy.spatial import KDTree

from fragment.conf.models import ConfigModel
from fragment.core.legacy_PIETree import DEPENDS_ON
from fragment.core.PIETree import ROOT, PIETree
from fragment.core.quickPIE import quickNode, quickNodeList, quickNodeSet, quickPlan
from fragment.core.util import reset_cache
from fragment.db.bootstrap import db
from fragment.db.mixins import BulkAddMixin, KeyField, NamedAndNotedMixin
from fragment.db.models import AddSession, BaseModel

from .common import AtomType, RelationType, ViewType

###########################################################
#                       ATOM MODEL                        #
###########################################################


VALENCE_SIZE_BY_PERIOD = [2, 8, 8, 18, 18, 32, 32]
CORE_E_BY_PERIOD = [0, 2, 10, 18, 36, 54, 86]


class Atom(BaseModel, BulkAddMixin):
    # ssid: int = IntegerField(null=True) # ID relative to the supersystem
    # May not be unique in a system
    t: str = CharField(2)
    x: float = FloatField()
    y: float = FloatField()
    z: float = FloatField()
    charge: int = IntegerField(default=0)
    type: AtomType = IntegerField(default=AtomType.PHYSICAL)
    meta: Dict[str, Any] = JSONField(null=True)

    def __init__(self, t=None, r=None, **kwargs):
        super().__init__(t=t, **kwargs)
        if kwargs.get("__no_default__", 0) == 1:
            # Return is we are restoring from the DB
            return

        if not r is None and "x" in kwargs:
            raise TypeError("Cannot specify 'r' and x,y,z for an atom")
        if not r is None:
            self.x, self.y, self.z = r

    def convert_to(self, atom_type: AtomType) -> "Atom":
        if self.type == atom_type:
            return self

        # Check the database for an atom that meets our requirements
        a1 = (
            Atom.select()
            .where(Atom.type == atom_type)
            .join(AtomToAtom, on=(Atom.id == AtomToAtom.a1_id))
            .where(AtomToAtom.a2 == self)
        )
        a2 = (
            Atom.select()
            .where(Atom.type == atom_type)
            .join(AtomToAtom, on=(Atom.id == AtomToAtom.a2_id))
            .where(AtomToAtom.a1 == self)
        )
        other = a1.union(a2).first()
        if other:
            return other

        # OK, so let's make a proxy instead
        other = deepcopy(self)
        other.id = None
        other.type = atom_type
        other.save()
        AtomToAtom.create(a1_id=self.id, a2_id=other.id)
        return other

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(id={self.id}, {self.t}, {tuple(self.r)}, charge={self.charge}, type={self.type})"

    @classmethod
    @db.atomic()
    def save_many(cls, atoms: Iterable["Atom"], session: AddSession = None) -> None:
        if session is None:
            session = AddSession.start_session()

        # Collect only the uninitiated atom objects
        to_add = {}
        for i, a in enumerate(atoms):
            if not a.id is None:
                continue
            a._session = session
            a._session_index = i
            to_add[i] = a  # Store such that we can cross ref

        # Add the items in bulk
        cls.bulk_create(to_add.values(), 100)

        # Get the newly added items back
        atom_ids = (
            cls.select(cls.id, cls._session_index)
            .where(cls._session == session)
            .tuples()
        )

        for atom_id, session_index in atom_ids.iterator():
            to_add[session_index].id = atom_id

        # Do not return atoms because python passes by
        # reference and the calling scope has this info
        return

    @property
    def r(self) -> np.ndarray:
        """Cartesian coordinates of the atom

        Returns:
            np.arry: Cartesian coodinate in 3D space
        """
        return np.array([self.x, self.y, self.z])

    @r.setter
    def r(self, val):
        self.x, self.y, self.z = val

    # Atomic Data
    @property
    def is_placeholder(self):
        return self.type in (AtomType.GHOST, AtomType.POINT_CHARGE, AtomType.DUMMY)

    @property
    def atomic_number(self) -> int:
        """Atomic Number"""
        return ptable.to_atomic_number(self.t)

    @property
    def electrons(self) -> int:
        """Number of bound electrons"""
        if self.is_placeholder:
            return 0
        return self.atomic_number - self.charge

    @property
    def core_electrons(self) -> int:
        return min(CORE_E_BY_PERIOD[ptable.to_period(self.t) - 1], self.electrons)

    @property
    def valence_e(self) -> int:
        return self.electrons - self.core_electrons

    @property
    def max_valence(self) -> int:
        return VALENCE_SIZE_BY_PERIOD[ptable.to_period(self.t) - 1]

    @property
    def Z(self) -> int:
        return ptable.to_Z(self.t)

    @property
    def mass(self) -> float:
        """Atomic mass in a.u."""
        if self.is_placeholder:
            return 0.0
        return ptable.to_mass(self.t)

    @property
    def covalent_radius(self) -> float:
        """Covalent radius in Å"""
        return covalentradii.get(self.t, units="angstrom")

    @property
    def vdw_radius(self) -> float:
        """van der Waals radius in Å"""
        return vdwradii.get(self.t, units="angstrom")

    @staticmethod
    def distance(a1: "Atom", a2: "Atom") -> np.ndarray:
        return np.linalg.norm(a1.r - a2.r)


###########################################################
#                     SYSTEM MODEL                        #
###########################################################


class AtomManager(MutableSet):
    def __init__(self, system: "System", atoms: List[Atom] = None) -> None:
        self.system = system
        self.clear_add_queue()
        if atoms is None:
            if system.id is None:
                self._atoms = []
            else:
                self._atoms = [r.atom for r in system.atomtosystem_set.prefetch(Atom)]
        else:
            self._atoms = list(atoms)
            for a in atoms:
                if a.id is None:
                    self.new_atoms.append(a)

    def __repr__(self) -> str:
        self._sort_atoms()
        return f"{self.__class__.__name__}({self._atoms.__repr__()})"

    def __contains__(self, x: object) -> bool:
        # Not particularly efficient.
        # Let's not do this oodls of times
        return x in self._atoms

    def __iter__(self) -> Iterator[Atom]:
        self._sort_atoms()
        return self._atoms.__iter__()

    def _sort_atoms(self):
        """
        TODO: Add some caching code so we don't
              do this unnecesarily
              Then again this does not take too long
              and I doubt there will be more than 1000 atoms.
              Peanuts really
        """
        self._atoms.sort(
            key=lambda a: (a.type, a.id or 100000)
        )  # Un-indexed atoms go last

    def __len__(self) -> int:
        return len(self._atoms)

    def __getitem__(self, val) -> Atom:
        """Make subscriptable

        Technically not part of :class:`.MutableSet` but this make interacting
        with atoms much easier for mods.
        """
        if isinstance(val, tuple):
            return [self._atoms[i] for i in val]
        return self._atoms[val]

    def add(self, *atoms: Union[Atom, int]) -> None:
        for atom in atoms:
            if isinstance(atom, int):
                atom = Atom(id=atom)  # Support atom IDs
                # Placeholder. will not be saved
                # TODO: Retrieve atom or enfoce this
            elif atom.id is None:
                self.new_atoms.append(atom)
            elif atom in self:
                continue  # No duplication

            self._atoms.append(atom)

        # Reset cache for everything that is not this AtomManager
        # NOTE: I am mildly uncomfortable with how much this class
        #       manages the state of the System
        reset_cache(
            self.system,
            keep=lambda k: hasattr(self.system, k) and getattr(self.system, k) is self,
        )

    def discard(self, value: Atom) -> None:
        self._atoms.remove(value)
        if value.id is None:
            self.new_atoms.remove(value)
        else:
            AtomToSystem.delete().where(
                AtomToSystem.atom == value, AtomToSystem.system == self.system
            ).execute()

    def clear_add_queue(self):
        self.new_atoms = []

    def filter(self, ids=None, type=None, saved=None):
        def _f(a):
            if not (ids is None) and not (a.id in ids):
                return False
            if not (type is None) and a.type != type:
                return False
            if not (saved is None) and not (a.id is None):
                return False
            return True

        return (a for a in self if _f(a))

    def count(self, **kwargs) -> int:
        return sum(1 for _ in self.filter(**kwargs))

    def make_key(self) -> quickNode:
        id_set = set()
        for v in self._atoms:
            if isinstance(v, int):
                id_set.add(v)
            elif isinstance(v.id, int):
                id_set.add(v.id)
            else:
                raise AttributeError("Unsaved atoms in system")
        return frozenset(id_set)


class System(BaseModel):
    _key: quickNode = KeyField(index=True, unique=True, column_name="key")

    @cached_property
    def key(self):
        self.introspect_key()
        return self._key

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(id={self.id}, {self.key})"

    def __init__(self, atoms: Iterable[Atom] = None, **kwargs):
        super().__init__(**kwargs)
        if kwargs.get("__no_default__", 0) == 1:
            return

        if atoms:
            self.atoms = AtomManager(self, atoms=atoms)
            # self.introspect_key()

    @classmethod
    def merge(cls, *systems: "System") -> "System":
        key = frozenset.union(*(s.key for s in systems))
        atoms = chain(*(sys.atoms for sys in systems))
        ret = cls(atoms)

        # TODO: Remove proxy_for atoms
        #       method should prioritize physical atoms

        # Pre-calculate the key
        ret._key = frozenset(key)
        ret.key = ret._key

        return ret

    def subset(self, atom_ids: Iterable[int]) -> "System":
        _aid = tuple((self.db_to_system_idx(i) for i in atom_ids))
        ret = self.__class__(self.atoms[_aid])

        # Pre-calculate the key
        ret._key = frozenset(atom_ids)
        ret.key = ret._key

        return ret

    def subsystem(self, atom_ids: Iterable[int]) -> "System":
        _aid = tuple(atom_ids)
        ret = self.__class__(self.atoms[_aid])

        # Pre-calculate the key
        ret._key = frozenset(atom_ids)
        ret.key = ret._key

        return ret

    def save(self, **kwargs):
        """
        Overwrite save logic to account for new atoms
        """

        # Handle new relations
        new_relations = []
        if self.id is None:
            new_relations = [AtomToSystem(atom=a, system=self) for a in self.atoms]
        else:
            new_relations = [
                AtomToSystem(atom=a, system=self) for a in self.atoms if a.id is None
            ]

        if self.atoms.new_atoms:
            Atom.save_many(self.atoms.new_atoms)
        self.introspect_key()

        ret = super().save(**kwargs)

        # Now add the relationships
        if new_relations:
            AtomToSystem.bulk_create(new_relations)
        self.atoms.clear_add_queue()

        return ret

    @classmethod
    @db.atomic()
    def save_many(cls, systems: Iterable["System"], session: AddSession = None):
        # from time import time # DEBUG
        # start = time() # DEBUG
        if session is None:
            session = AddSession.start_session()

        new_systems = []
        # At this point, all atoms should have an ID
        for sys in systems:
            # only operate on un-instantiated systems
            if not (sys.id is None):
                continue

            new_systems.append(sys)

        Atom.save_many(chain(*(s.atoms for s in new_systems)), session=session)
        # print(f"-- Atoms added in {time() - start:.4f} s"); start = time() # DEBUG

        # Refresh keys now that atoms have been added
        to_add = {}
        for sys in new_systems:
            sys.introspect_key()
            # It is possible that we have added the same fragment
            try:
                to_add[sys._key].append(sys)
            except KeyError:
                to_add[sys._key] = [sys]

        # Get list of atoms and create them
        if to_add:
            for sys_chunk in chunked(((k,) for k in to_add.keys()), 500):
                (
                    cls.insert_many(sys_chunk, fields=[cls._key]).on_conflict_ignore()
                ).execute()

        # print(f"-- Adding Systems in {time() - start:.4f} s"); start = time() # DEBUG

        # Query for the newly created keys
        added_systems_query = (
            cls.select(cls.id, cls._key).where(
                cls._key << [Value(k, unpack=False) for k in to_add.keys()]
            )
        ).tuples()
        # print(f"-- Retreiving Systems in {time() - start:.4f} s"); start = time() # DEBUG

        new_rels = []
        # Update the system ids
        for s_id, k in added_systems_query:
            for a_id in k:  # Create the new system relationship
                new_rels.append((s_id, a_id))
            for new_sys in to_add[k]:
                new_sys.id = s_id

        # Create the atom-system relationships
        if new_rels:
            for rels_chunk in chunked(new_rels, 500):
                (
                    AtomToSystem.insert_many(
                        rels_chunk,
                        fields=[AtomToSystem.system_id, AtomToSystem.atom_id],
                    ).on_conflict_ignore()
                ).execute()
        # print(f"-- Adding system rels in {time() - start:.4f} s"); start = time() # DEBUG

    #################################
    #####  SYSTEM RELATIONSHIPS #####
    #################################

    @property
    def supersystem(self) -> "System":
        """
        The supersystem is the whole system as read from the input file.
        """
        try:
            # This feel like it could be a more clever query
            # but for tonight works is better than clever
            return self.related_on_right.where(
                SystemToSystem.type == RelationType.SUPERSYSTEM
            )[0].left
        except IndexError:
            return self  # This system is it's own parent

    ############################
    #####  ATOM MANAGEMENT #####
    ############################

    @cached_property
    def atoms(self) -> AtomManager:
        return AtomManager(self)

    def partial_charges(self) -> List[float]:
        return [a.meta.get("partial_charge", 0.0) for a in self.atoms]

    #################################
    #####   SYSTEM PROPERTIES   #####
    #################################

    @property
    def charge(self) -> int:
        """
        A good mechanism for calculating this has not yet been
        determine so we are assuming all fragments are neutral
        This can be overwritten at a per-implementation level
        """
        return sum(a.charge for a in self.atoms if not a.is_placeholder)

    @cached_property
    def multiplicity(self) -> int:
        """
        As with change, there is not general concept for how to set
        this although input is always welcome.
        """
        electrons = sum(a.electrons for a in self.atoms)  # Start with charge==0

        if electrons % 2 == 0:
            return 1
        else:
            return 2

    @cached_property
    def r_matrix(self) -> np.ndarray:
        """Matrix of atomic coordinates. Does not include ghost atoms"""
        r_matrix = np.zeros((self.atoms.count(), 3), "float")
        for row, a in zip(r_matrix, self.atoms):
            row[:] = a.r[:]
        return r_matrix

    @cached_property
    def masses(self) -> np.ndarray:
        """Matrix of masses. Does not include ghost atoms"""
        return np.fromiter((a.mass for a in self.atoms), dtype="float")

    @cached_property
    def charge_matrix(self) -> np.ndarray:
        a_nums = np.zeros((self.atoms.count(), 1), "int")
        for num, a in zip(a_nums, self.atoms):
            num[:] = a.charge
        return a_nums

    @cached_property
    def atom_number_matrix(self) -> np.ndarray:
        a_nums = np.zeros((self.atoms.count(), 1), "int")
        for num, a in zip(a_nums, self.atoms):
            num[:] = a.atomic_number
        return a_nums

    @property
    def mass(self):
        return self.masses.sum()

    @property
    def COM(self) -> np.ndarray:
        # It annoys me that the masses must be reshaped
        MWCoord: np.ndarray = np.multiply(self.masses.reshape(-1, 1), self.r_matrix)
        COM = np.sum(MWCoord, axis=0)
        return COM / self.mass

    def moment_of_inertia_tensor(self) -> np.ndarray:
        I = np.zeros((3, 3))
        r_COM = self.r_matrix - self.COM
        r_COM_2 = r_COM**2

        # Diagonal elements
        I[0, 0] = np.sum(self.masses * (r_COM_2[:, 1] + r_COM_2[:, 2]))
        I[1, 1] = np.sum(self.masses * (r_COM_2[:, 0] + r_COM_2[:, 2]))
        I[2, 2] = np.sum(self.masses * (r_COM_2[:, 0] + r_COM_2[:, 1]))

        # Off diagonal elements
        I[0, 1] = I[1, 0] = -np.sum(self.masses * r_COM[:, 0] * r_COM[:, 1])
        I[0, 2] = I[2, 0] = -np.sum(self.masses * r_COM[:, 0] * r_COM[:, 2])
        I[1, 2] = I[2, 1] = -np.sum(self.masses * r_COM[:, 1] * r_COM[:, 2])

        return I

    def canonical_coords(self) -> "System":
        I = self.moment_of_inertia_tensor()
        e_val, e_vec = np.linalg.eig(I)

        # Sort eigenvalues. Largest -> Smallest = Z, Y, X
        sort_idx = np.argsort(e_val)

        # Rotate along x, y to align z then rotate along z
        rot_mat = e_vec[:, sort_idx]
        r_COM = self.r_matrix - self.COM
        return r_COM @ rot_mat

    @cached_property
    def KDTree(self) -> KDTree:
        return KDTree(self.r_matrix)

    @property
    def coef(self) -> int:
        """
        TODO: Debug why annotating the fragment query does not
              do this automatically
        """
        if hasattr(self, "viewcoef"):
            return self.viewcoef.coef
        return 0

    # Handle the indexing disparity between
    # the system and the database
    @cached_property
    def db_to_system_idx_map(self) -> Dict[int, int]:
        return {a.id: i for i, a in enumerate(self.atoms)}

    @cached_property
    def system_to_db_idx_map(self) -> List[int]:
        return sorted((a.id for a in self.atoms))

    def db_to_system_idx(self, db_idx) -> int:
        return self.db_to_system_idx_map[db_idx]

    def system_to_db_idx(self, sys_idx) -> int:
        return self.system_to_db_idx_map[sys_idx]

    def introspect_key(self):
        self._key = self.atoms.make_key()

    @cached_property
    def bonds(self) -> nx.Graph:
        return Bond.get_bond_graph(self.id)


###########################################################
#                  SUPERSYSTEM MODEL                      #
###########################################################


class SystemLabel(BaseModel, NamedAndNotedMixin):
    system: System = ForeignKeyField(System, backref="labels")
    hash: str = CharField(
        max_length=40, null=True, index=True
    )  # Hash of the source file
    source_path: str = CharField(default="")  # Path to our source file

    def __init__(self, system: str = None, name: str = None, **kwargs):
        super().__init__(system=system, name=name, **kwargs)
        if kwargs.get("__no_default__", 0) == 1:
            return

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"id={self.id}, name='{self.name}',"
            f"note='{self.note}', source_path='{self.source_path}'"
        )

    def save(self, **kwargs):
        if self.system.id is None:
            self.system.save()
        return super().save(**kwargs)


###########################################################
#                      VIEW MODELS                        #
###########################################################


class FragmentManager(MutableSet):
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(new_systems={self.unlinked_systems})"

    def __init__(self, view: "View") -> None:
        self.view = view
        self.clear_add_queue()
        self.all_query = (
            System.select(System, ViewCoef.coef)
            .join(ViewCoef)
            .where(ViewCoef.view == self.view)
        )
        self.frag_query = self.all_query

    def clear_add_queue(self) -> None:
        self.unlinked_systems = []

    def __contains__(self, system: System) -> bool:
        if system in self.frag_query:
            return True
        if system in (s for s in self.unlinked_systems):
            return True
        return False

    def __iter__(self) -> Iterator[System]:
        return chain(self.frag_query, (s for s in self.unlinked_systems))

    @property
    def all(self) -> Iterator[System]:
        return chain(self.all_query, self.unlinked_systems)

    def __len__(self) -> int:
        """
        Accounts for existing fragments
        # TODO: Handle zero coefs
        """
        unkeyed = sum(1 for s in self.unlinked_systems if (s.key is None))
        in_db = self.frag_query.count()
        keys_to_add = set(s.key for s in self.unlinked_systems if not (s.key is None))
        existing = len(
            self.frag_query.where(
                ViewCoef.fragment._key << [Value(k, unpack=False) for k in keys_to_add]
            )
        )
        return unkeyed + in_db + len(keys_to_add) - existing

    def add(self, *systems: System, coefs: List[int] = None) -> None:
        """
        Duplicates are OK, but coefs must be consolidated when
        added to the database
        """
        if coefs is None:
            coefs = [1 for _ in range(len(systems))]
        if len(systems) != len(coefs):
            raise ValueError(
                "The number of coefficients must equal the number of fragments"
            )
        for s, c in zip(systems, coefs):
            # recreate this to make consistent with api
            s.viewcoef = ViewCoef(view=self.view, system=s, coef=c)
            self.unlinked_systems.append(s)

    def discard(self, system: System) -> None:
        to_delete = None
        for i, s in enumerate(self.unlinked_systems):
            if s is system or s == system:
                to_delete = i
                break
        if not (to_delete is None):
            del self.unlinked_systems[to_delete]
            return

        (
            ViewCoef.delete().where(
                ViewCoef.fragment == system, ViewCoef.view == self.view
            )
        ).execute()

    @property
    def new_systems(self):
        return (s for s in self.unlinked_systems if s.id is None)

    @property
    def new_view_coefs(self) -> List[Tuple[int, ...]]:
        if self.view.id is None:
            KeyError("Cannot create coefs for unsaved View")
        coefs: Dict[int, Tuple[int, int]] = {}
        for s in self.unlinked_systems:
            if s.id is None:
                raise KeyError("Cannot create ViewCoefs for un-saved Systems")
            try:
                coefs[s.id] = (s.viewcoef.coef + coefs[s.id][0], s.viewcoef.order)
            except KeyError:
                coefs[s.id] = (s.viewcoef.coef, s.viewcoef.order)

        return [(self.view.id, s_id, c, o) for s_id, (c, o) in coefs.items()]

    @property
    def keys(self) -> quickNodeSet:
        return {f.key for f in self}

    @property
    def sets(self) -> quickNodeList:
        return [k for k in self.keys]


class View(BaseModel, NamedAndNotedMixin):
    order: int = IntegerField(null=True)
    type: int = IntegerField()
    parent: "View" = ForeignKeyField("self", null=True)
    fragmenter: ConfigModel = ForeignKeyField(ConfigModel, null=True)
    supersystem: System = ForeignKeyField(System, backref="views", null=True)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(name='{self.name}', note='{self.note}', order={self.order})"

    def __init__(self, fragments=None, coefs=None, **kwargs) -> None:
        super().__init__(**kwargs)
        if kwargs.get("__no_default__", 0) == 1:
            return

        if fragments:
            self.fragments.add(*fragments, coefs=coefs)

    @cached_property
    def fragments(self) -> FragmentManager:
        return FragmentManager(self)

    @db.atomic()
    def save(self, **kwargs):
        System.save_many(self.fragments.new_systems)
        if self.supersystem:
            self.supersystem.save()
        super().save(**kwargs)
        # Create the ViewCoefs
        # Handle conflicts by adding
        new_view_coefs = self.fragments.new_view_coefs
        if new_view_coefs:
            for coef_chunk in chunked(self.fragments.new_view_coefs, 500):
                add_query = ViewCoef.insert_many(
                    coef_chunk,
                    fields=[
                        ViewCoef.view,
                        ViewCoef.fragment,
                        ViewCoef.coef,
                        ViewCoef.order,
                    ],
                ).on_conflict(
                    conflict_target=[ViewCoef.fragment, ViewCoef.view],
                    update={ViewCoef.coef: ViewCoef.coef + EXCLUDED.coef},
                )
                add_query.execute()
        # Clear our cached views now that everything is done
        del self.fragments

        # Create parent-child relationships
        if self.supersystem:
            for ss_chunk in chunked(
                (
                    (self.supersystem.id, f.id, RelationType.SUPERSYSTEM)
                    for f in self.fragments
                ),
                500,
            ):
                SystemToSystem.insert_many(
                    ss_chunk,
                    fields=[
                        SystemToSystem.left,
                        SystemToSystem.right,
                        SystemToSystem.type,
                    ],
                ).on_conflict_ignore().execute()

        # Save dep_tree if applicable:
        if self.type == ViewType.AUXILIARY and not self._has_dep_tree():
            self._save_dep_tree(self.dep_tree)

    @cached_property
    def dep_tree(self) -> PIETree:
        if self.type != ViewType.AUXILIARY:
            raise AttributeError("PIETrees only be generated for Aux Views")

        if self._has_dep_tree():
            return self._get_dep_tree()
        return self._make_dep_tree()

    def _has_dep_tree(self) -> bool:
        return (
            FragmentToFragment.select().where(
                FragmentToFragment.view == self,
                FragmentToFragment.type == RelationType.DEPENDS_ON,
            )
        ).exists()

    def _make_dep_tree(self) -> PIETree:
        plan = self.quickPlan
        primaries: quickNodeList = {p[0] for p in self.primary_frag_keys.tuples()}

        tree = PIETree.from_quickPlan(plan, primaries)
        tree.clean_zeros()
        self._update_db_ids(tree)  # Technically could be passed by PIEPlan
        # but the PIE solver is not set up for this

        self._save_dep_tree(tree)
        return tree

    def _update_db_ids(self, tree: PIETree) -> None:
        query = (
            System.select(System.id, System._key)
            .join(ViewCoef, on=(ViewCoef.fragment_id == System.id))
            .where(ViewCoef.view == self)
        )
        for sid, key in query.tuples():
            tree[key]["db_id"] = sid

    def _save_dep_tree(self, tree: PIETree, force=False) -> None:
        if not force and self._has_dep_tree():
            return  # Don't save two trees

        self._update_db_ids(tree)

        _tree = tree.tree

        # Edge data
        FtF = FragmentToFragment
        for edges in chunked(_tree.edges, 500):
            edge_data = [
                (
                    tree[e1].get("db_id", None),
                    tree[e2]["db_id"],
                    self.id,
                    RelationType.DEPENDS_ON.value,
                )
                for e1, e2 in edges
            ]
            FragmentToFragment.insert_many(
                edge_data, fields=[FtF.left, FtF.right, FtF.view_id, FtF.type]
            ).on_conflict_ignore().execute()

    def _get_dep_tree(self) -> PIETree:
        node_query = (
            ViewCoef.select(System.id, System._key, ViewCoef.coef)
            .join(System, on=(ViewCoef.fragment_id == System.id))
            .where(ViewCoef.view == self)
        )

        left_sys = System.alias("left")
        right_sys = System.alias("right")
        edge_query = (
            FragmentToFragment.select(left_sys._key, right_sys._key)
            .where(
                FragmentToFragment.view == self,
                FragmentToFragment.type == RelationType.DEPENDS_ON.value,
            )
            .join(
                left_sys,
                JOIN.LEFT_OUTER,
                on=(FragmentToFragment.left_id == left_sys.id),
            )
            .join(
                right_sys,
                JOIN.LEFT_OUTER,
                on=(FragmentToFragment.right_id == right_sys.id),
            )
        )

        # Get parent data to calculate roots
        # TODO: Refactor all these queries so that we can DRY
        parents: quickNodeList = [p[0] for p in self.primary_frag_keys.tuples()]

        tree = PIETree.from_primaries(parents)
        for sid, key, coef in node_query.tuples():
            tree.update_or_add(key, coef)
            tree[key]["db_id"] = sid

        tree.tree.add_edges_from(
            ((lk or ROOT, rk) for lk, rk in edge_query.tuples()), type=DEPENDS_ON
        )

        tree.update_target()
        tree.clean_zeros()

        return tree

    @cached_property
    def quickPlan(self) -> quickPlan:
        if self.type != ViewType.AUXILIARY:
            raise AttributeError("PIEPlans can only be generated for Aux Views")

        # Reconstructs the PIEPlan this view represents
        coef_query = (
            ViewCoef.select(System._key, ViewCoef.coef)
            .where(ViewCoef.view == self)
            .join(System, on=(System.id == ViewCoef.fragment_id))
        )

        return {n: c for n, c in coef_query.tuples()}

    # ------------------------------------------------
    #                QUERIES
    # ------------------------------------------------

    @property
    def primary_frag_keys(self):
        return (
            ViewCoef.select(System._key)
            .where(ViewCoef.view_id == self.parent_id)
            .join(System, on=(System.id == ViewCoef.fragment_id))
        )

    @cached_property
    def COM_r_matrix(self) -> np.ndarray:
        """Matrix of atomic coordinates. Does not include ghost atoms"""
        r_matrix = np.zeros((len(self.fragments), 3), "float")
        for row, f in zip(r_matrix, self.fragments):
            row[:] = f.COM[:]
        return r_matrix


###########################################################
#                   RELATION MODELS                       #
###########################################################


class AtomToAtom(BaseModel):
    # TODO: Merge this with Bond model
    a1: Atom = ForeignKeyField(Atom)
    a2: Atom = ForeignKeyField(Atom)


class Bond(BaseModel):
    a1: Atom = ForeignKeyField(Atom)
    a2: Atom = ForeignKeyField(Atom)
    system: System = ForeignKeyField(System)
    length: float = FloatField()
    order: int = IntegerField(default=0)

    @classmethod
    def save_graph(cls, sys: System, G: nx.Graph, save_zero=True) -> None:
        sys_id = sys.id
        bonds = [
            Bond(
                a1_id=G.nodes[a1]["db_id"],
                a2_id=G.nodes[a2]["db_id"],
                system_id=sys_id,
                length=d["length"],
                order=d["order"],
            )
            for a1, a2, d in G.edges(data=True)
        ]
        cls.bulk_create(bonds)

    @classmethod
    def get_bond_graph(cls, sys_id: int) -> nx.Graph:
        bond_query = cls.select(
            Bond.a1_id,
            Bond.a2_id,
            Bond.length,
            Bond.order,
        ).where(Bond.system_id == sys_id)
        G = nx.Graph()

        for a1, a2, l, o in bond_query.tuples():
            G.add_edge(a1, a2, length=l, order=o)
        return G

    @classmethod
    def mk_bond_graph(cls, sys: System, save=True) -> nx.Graph:
        tolerance = 1.2
        cutoff = 3.0
        k = 8

        G = nx.Graph()
        kd = sys.KDTree
        bonds: List["Bond"] = []

        # ADD ATOMS AS NODES
        for idx, a in enumerate(sys.atoms):
            a: Atom
            G.add_node(
                idx,
                db_id=a.id,
                atom=a,
                charge=a.charge,
                valence=a.valence_e,
                bonds_needed=min(a.valence_e, a.max_valence - a.valence_e),
                column=ptable.to_group(a.t),
            )

        # ADD BONDS TO GRAPH
        for a1_idx, a1 in enumerate(sys.atoms):
            a2_d_list, a2_idx_list = kd.query(a1.r, k=k, distance_upper_bound=cutoff)
            for a2_d, a2_idx in zip(a2_d_list, a2_idx_list):
                if a1_idx <= a2_idx:
                    continue
                a2 = sys.atoms[a2_idx]
                expected = a1.covalent_radius + a2.covalent_radius
                if a2_d < expected * tolerance:
                    # A very broad definition of a bond :)
                    G.add_edge(
                        a1_idx,
                        a2_idx,
                        order=0,
                        length=a2_d,
                        score=cls.get_score(
                            a2_d,
                            0,
                            abs(ptable.to_period(a1.t) - ptable.to_period(a2.t)),
                        ),
                    )

        # INCREASE BOND ORDER UNTIL DONE
        bm = 1
        while bm > 0:
            bonds = []
            for a1, a2, d in G.edges(data=True):
                if G.nodes[a1]["bonds_needed"] == 0 or G.nodes[a2]["bonds_needed"] == 0:
                    continue
                bonds.append((a1, a2, d["score"]))
            if not bonds:
                break

            bonds.sort(key=lambda x: x[2])
            bm = cls.start(G, bonds[0][0])

        if save:
            cls.save_graph(sys, G)
        return G

    @classmethod
    def get_score(cls, length: float, order: int, period_delta: int) -> float:
        return length + 0.15 * length * (order + period_delta)

    @classmethod
    def score_from_edge(cls, G: nx.Graph, a1_idx, a2_idx):
        e = G[a1_idx][a2_idx]
        a1 = G.nodes[a1_idx]["atom"]
        a2 = G.nodes[a2_idx]["atom"]
        return cls.get_score(
            e["length"],
            e["order"],
            abs(ptable.to_period(a1.t) - ptable.to_period(a2.t)),
        )

    @classmethod
    def start(cls, G: nx.Graph, a_idx: int) -> int:
        bonds_made = 0
        a = G.nodes[a_idx]
        while a["bonds_needed"] > 0:
            bonds = cls.get_bonds(G, a_idx)  # Get newly scored bonds
            if not bonds:  # Prevent infinite loops
                break
            a1_idx, a2_idx, _score = bonds[0]

            # Make the bond
            bonds_made += 1
            cls.make_bond(G, a1_idx, a2_idx)

            # Make bonds on linked atom
            a_next = a_idx if a_idx != a1_idx else a2_idx
            bonds_made += cls.start(G, a_next)
        return bonds_made

    @classmethod
    def get_bonds(cls, G: nx.Graph, a1_idx: int):
        bonds = []
        for a2_idx in G[a1_idx]:
            a2 = G.nodes[a2_idx]
            if a2["bonds_needed"] > 0:
                bonds.append((a1_idx, a2_idx, G[a1_idx][a2_idx]["score"]))
        bonds.sort(key=lambda x: x[2])
        return bonds

    @classmethod
    def make_bond(cls, G: nx.Graph, a1_idx: int, a2_idx: int):
        G.nodes[a1_idx]["bonds_needed"] -= 1
        G.nodes[a2_idx]["bonds_needed"] -= 1
        G[a1_idx][a2_idx]["order"] += 1
        G[a1_idx][a2_idx]["score"] = cls.score_from_edge(G, a1_idx, a2_idx)


class AtomToSystem(BaseModel):
    atom: Atom = ForeignKeyField(Atom)
    system: System = ForeignKeyField(System)

    class Meta:
        primary_key = CompositeKey("atom", "system")


class SystemToSystem(BaseModel):
    left: System = ForeignKeyField(System, backref="related_on_left")
    right: System = ForeignKeyField(System, backref="related_on_right")
    type: int = IntegerField()


class FragmentToFragment(BaseModel):
    """Similar to SystemToSystem but this is for a specific View"""

    type: int = IntegerField(choices=RelationType)
    view: View = ForeignKeyField(View)
    left: System = ForeignKeyField(System, null=True)
    right: System = ForeignKeyField(System, null=True)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.type}, {self.view_id}, {self.left_id}, {self.right_id})"

    class Meta:
        primary_key = CompositeKey("type", "view", "left", "right")


class ViewCoef(BaseModel):
    fragment: System = ForeignKeyField(System)
    view: View = ForeignKeyField(View, backref="coefs")
    coef: int = IntegerField(choices=RelationType)
    order: int = IntegerField(null=True)
    # A set of primary frags responsible for this

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(fragment_id={self.fragment.id}, view_id={self.view.id}, coef={self.coef})"

    class Meta:
        primary_key = CompositeKey("fragment", "view")
