import os
from abc import ABC, abstractstaticmethod
from pathlib import Path
from typing import List

from playhouse.migrate import *

from fragment.db.models import Migration


def get_migrations() -> List["BaseMigration"]:
    return BaseMigration.__subclasses__()


def fake_migrations(db: Database):
    BaseMigration.fake_migrations(db, get_migrations())


def run_migrations(db: Database, project_path: Path):
    BaseMigration.run_migrations(db, get_migrations(), project_path)


def has_pending_migrations():
    migration_names = [m.NAME for m in get_migrations()]
    num_applied = Migration.select().where(Migration.name.in_(migration_names)).count()

    if len(migration_names) == num_applied:
        return False
    return True


class UnappliedMigrations(Exception):
    pass


class BaseMigration(ABC):
    NAME: str
    ORDER: int

    def __init__(self) -> None:
        pass

    @abstractstaticmethod
    def run(self, migrator: SchemaMigrator, project_path: Path):
        pass

    @staticmethod
    def run_migrations(
        db: Database, migrations: List["BaseMigration"], project_path: Path
    ):
        """
        Alters the DB and records this in the migration table
        """
        migration_list = sorted(migrations, key=lambda x: x.ORDER)
        migrator = SqliteMigrator(db)

        with db.atomic():
            for migration in migration_list:
                db_record = (
                    Migration.select().where(Migration.name == migration.NAME).first()
                )
                if db_record:
                    print(f"Migration '{migration.NAME}' is already applied.")
                    continue
                print(f"Applying migration '{migration.NAME}'...", end="")
                migration.run(migrator, project_path)
                Migration.create(name=migration.NAME)
                print(" DONE")

    @staticmethod
    def fake_migrations(db: Database, migrations: List["BaseMigration"]):
        """
        Creates migration entry in the DB without running it
        """
        with db.atomic():
            for migration in migrations:
                if (
                    not Migration.select()
                    .where(Migration.name == migration.NAME)
                    .exists()
                ):
                    Migration.create(name=migration.NAME)


class AddWorkerTable(BaseMigration):
    NAME = "add_worker_table"
    ORDER = 1

    @staticmethod
    def run(migrator: SchemaMigrator, project_path: Path):
        """
        Adds worker table for tracking parallel job status
        """
        from fragment.calculations.models import WorkerBase

        WorkerBase.create_table()
        worker_id_field = ForeignKeyField(WorkerBase, field=AutoField(), null=True)

        migrate(
            migrator.add_column("job", "worker_id", worker_id_field),
        )


class AddCollectedToJob(BaseMigration):
    NAME = "add_collected_to_job"
    ORDER = 2

    @staticmethod
    def run(migrator: SchemaMigrator, project_path: Path):
        """
        Adds a files_collected column to the job field and renames the
        last_heartbeat column to last_sean column on the worker model
        """
        collected_field = BooleanField(default=False)
        active_field = BooleanField(default=False)

        migrate(
            migrator.add_column("job", "files_collected", collected_field),
            migrator.add_column("worker", "active", active_field),
            migrator.rename_column("worker", "last_heartbeat", "last_seen"),
        )

        # Assume that all job files were collected if it was completed
        from fragment.calculations.models import Job, JobStatus

        (
            Job.update({Job.files_collected: True}).where(
                Job.status == JobStatus.COMPLETED
            )
        ).execute()

        # Move the log files from logs/jobs to the local work directory
        old_log_path = project_path / "logs" / "jobs"
        new_log_path = project_path / "qm_calcs"
        for root, _, files in os.walk(old_log_path):
            job_slug = root[(len(old_log_path) + 1) :]
            olp = old_log_path / job_slug  # The current log path
            nlp = new_log_path / job_slug  # The new log path
            for file in files:
                os.renames(
                    olp / file,
                    nlp / file,
                )


class RemoveParentIds(BaseMigration):
    NAME = "remove_ViewCoef_parent_ids"
    ORDER = 2

    @staticmethod
    def run(migrator: SchemaMigrator, project_path: Path):
        """Removes the unused paren_ids column from the ViewCoef model"""

        migrate(
            migrator.drop_column("viewcoef", "parent_ids"),
        )


class CreateFragmentToFragmentTable(BaseMigration):
    NAME = "create_FragmentToFragmentTable"
    ORDER = 3

    @staticmethod
    def run(migrator: SchemaMigrator, project_path: Path):
        from fragment.systems.models import FragmentToFragment

        FragmentToFragment.create_table()


class AddBondTable(BaseMigration):
    NAME = "add_bond_table"
    ORDER = 4

    @staticmethod
    def run(migrator: SchemaManager, project_path: Path):
        from fragment.systems.models import Bond, SystemLabel

        Bond.create_table()

        for sl in SystemLabel.select():
            Bond.mk_bond_graph(sl.system)


class AddAtomToAtom(BaseMigration):
    NAME = "add_atom_to_atom"
    ORDER = 5

    @staticmethod
    def run(migrator: SchemaManager, project_path: Path):
        from fragment.systems.models import AtomToAtom

        AtomToAtom.create_table()
