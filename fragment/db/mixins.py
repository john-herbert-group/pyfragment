from datetime import datetime
from typing import Iterable

from peewee import *

from fragment.core.quickPIE import quickNode
from fragment.core.util import validate_slug
from fragment.db.models import AddSession


class SlugField(CharField):
    def db_value(self, value):
        validate_slug(value)
        return super().db_value(value)


class OrderedKeyField(CharField):
    def db_value(self, value):
        if isinstance(value, Iterable):
            store_str = ",".join((str(v) for v in value))
        else:
            store_str = str(value)
        return super().db_value(store_str)

    def python_value(self, value) -> quickNode:
        if value:
            ids_str = value.split(",")
            return tuple(int(id) for id in ids_str)
        else:
            return tuple()


class KeyField(CharField):
    def db_value(self, value):
        if isinstance(value, Iterable):
            store_str = ",".join((str(v) for v in sorted(value)))
        else:
            store_str = str(value)
        return super().db_value(store_str)

    def python_value(self, value) -> quickNode:
        if value:
            ids_str = value.split(",")
            return frozenset((int(id) for id in ids_str))
        else:
            return frozenset([])


class NamedAndNotedMixin(Model):
    name = SlugField(max_length=255, null=False, index=True, unique=True)
    note = CharField(max_length=500, null=False, default="")
    created = DateTimeField(null=False, default=datetime.now)

    @classmethod
    def from_name(cls, name):
        return cls.get(cls.name == name)


class BulkAddMixin(Model):
    _session = ForeignKeyField(AddSession, field="id", null=True)
    _session_index = IntegerField(null=True)
