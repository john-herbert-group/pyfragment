from peewee import CharField, Model

from fragment.db.bootstrap import db


class BaseModel(Model):
    class Meta:
        database = db


class AddSession(BaseModel):
    @classmethod
    def start_session(cls) -> int:
        return cls.create()


class Migration(BaseModel):
    name: str = CharField(unique=True)
