# DB initialization deffered until runtime
from peewee import DatabaseProxy

db = DatabaseProxy()
MEM_CONNECT_STR = ":memory:"
DATABASE_FILE_NAME = "fragment.db"


def bootstrap_db():
    # Do initial setup of databse

    # If tables have been created, do nothing
    from fragment.db.model_list import MODEL_LIST

    create_tables = True
    for model in MODEL_LIST:
        table_name = model._meta.table_name
        if db.table_exists(table_name):
            # NOTE: Missing tables could mean there needs to be a migration
            create_tables = False
            break

    from fragment.db.migrations import (
        UnappliedMigrations,
        fake_migrations,
        has_pending_migrations,
    )

    if create_tables:
        db.create_tables(MODEL_LIST)
        fake_migrations(db)
    else:
        if has_pending_migrations():
            raise UnappliedMigrations(
                "The database has unapplied migrations and will be unusable"
            )

    from fragment.conf.models import ConfigModel, ConfigType
    from fragment.fragmenting.supersystem import SupersystemFragmenter

    if (
        not ConfigModel.select()
        .where(
            ConfigModel.name == "supersystem",
            ConfigModel.config_type == ConfigType.FRAGMENTER,
        )
        .exists()
    ):
        SupersystemFragmenter(
            "supersystem", 'Supersystem "fragmenter". It does nothing.'
        ).save_config()
