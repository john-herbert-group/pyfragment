import fragment.calculations.models as _calc_models
import fragment.conf.models as _conf_models
import fragment.mods.models as _mod_models
import fragment.systems.models as _system_models
from fragment.db.models import BaseModel

MODEL_LIST = BaseModel.__subclasses__()
