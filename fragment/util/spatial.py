from array import array
from itertools import chain, combinations
from typing import List, Tuple, Union

import networkx as nx
import numpy as np
from scipy.spatial import Delaunay
from scipy.spatial.distance import pdist, squareform

from fragment.systems.models import Atom, System, View
from fragment.util.indexing import KeyToIndex

MAX_VALUE = 1e12


def neighbors(
    inside: System,
    outside: System,
    cutoff=2.0,
    k=10,
) -> List[Tuple[int, List[int], List[float]]]:
    """Returns nearest neighboors between two systems

    .. note::
        Because of the way atom indexing works, this result may be invalidated
        upon the insertion of atoms into :obj:`inside` or :obj:`outside`.

    .. note::
        This code requires that all the atoms are saved and have a DB ID.
        This constraint may be relatexed in the future. This might be an
        argument for Atom UUIDs

    Args:
        inside (:class:`~fragment.systems.models.System`): Reference system
        outside (:class:`~fragment.systems.models.System`): System of atoms in
            which to search for nearest neighbor
        cuttoff (float): Distance at which to stop searching for atoms
        k (int): Number of neighbooring atoms to consider

    Returns:
        List[Tuple[int, List[int], List[float]]]: Returns a list of tuples
            containing the local ID of an internal atom followed by a list
            of outside local atom IDs and alist of the distances. If an
            internal atom does not have any nearest neighboors, no entry is
            returned.
    """
    # Handle system overlap
    # TODO: Handle proxy_for meta property
    inside_db_ids = set(a.id for a in inside.atoms)
    outside_db_ids = set(a.id for a in outside.atoms)
    outside_db_ids = outside_db_ids.difference(inside_db_ids)

    distance_list, internal_id_list = outside.KDTree.query(
        inside.r_matrix,
        distance_upper_bound=cutoff,
        k=k,
    )

    ret_data = []
    for inner_local_id, neighbor_info in enumerate(
        zip(internal_id_list, distance_list)
    ):
        outer_db_ids = []
        distances = []
        for oi_id, d in zip(*neighbor_info):
            # Are we out of neighbors?
            if id == outside.KDTree.n:
                break

            # Are we an outer atom or just a local inner atom?
            try:
                odb_id = outside.system_to_db_idx(oi_id)
            except IndexError:
                continue
            if not odb_id in outside_db_ids:
                continue

            # If we servived, add it!
            outer_db_ids.append(oi_id)
            distances.append(d)

        if outer_db_ids:
            ret_data.append((inner_local_id, outer_db_ids, distances))

    return ret_data


def neighbor_cloud(inside: System, outside: System, **kwargs) -> List[Atom]:
    """Returns all atoms within a given distance of the system

    Args:
        inside (:class:`~fragment.systems.models.System`): Reference system
        outside (:class:`~fragment.systems.models.System`): System of atoms in
            which to search for nearest neighbor
        **kwargs: Keyword arguments passed to :func:`neighbors`

    Returns:
        A list of atoms from outside which are in a certain cutoff of atoms in
        the inner atoms
    """
    nn = neighbors(inside, outside, **kwargs)
    neighbor_ids = set()
    for row in nn:
        neighbor_ids = neighbor_ids.union(set(row[1]))

    return outside.atoms[tuple(neighbor_ids)]


def COM_dm(pfrags: View, fk: KeyToIndex) -> np.array:
    """Calculates the distance between the centers of mass between pfrags"""
    # TODO: switch to compressed form
    return squareform(pdist(pfrags.COM_r_matrix))


def closest_approach_dm(pfrags: View, fk: KeyToIndex) -> np.array:
    """Calculates the closst approach between fragments in pfrags"""
    dm = np.zeros((fk.size, fk.size))

    # There is probably a better way
    for f1, f2 in combinations(pfrags.fragments, 2):
        d = MAX_VALUE  # Start high
        for a1 in f1.atoms:
            for a2 in f2.atoms:
                _d = Atom.distance(a1, a2)
                if _d < d:
                    d = _d

        i1 = fk(f1)
        i2 = fk(f2)
        dm[i1, i2] = d
        dm[i2, i1] = d
    return dm


def Delaunay_graph(points: np.array) -> nx.Graph:
    tri = Delaunay(points)
    G = nx.Graph()
    G.add_edges_from(
        ((i, j) for (i, j) in chain(*(combinations(s, 2) for s in tri.simplices)))
    )

    # import matplotlib.pyplot as plt

    # ax = plt.axes(projection="3d")
    # for i, j in G.edges:
    #     _p = points[[i, j], :]
    #     ax.plot(_p[:, 0], _p[:, 1], _p[:, 2], color="blue")
    # plt.show()

    return G


def view_Delaunay_graph(view: View) -> nx.Graph:
    G = Delaunay_graph(view.COM_r_matrix)

    fk = KeyToIndex((f.key for f in view.fragments))
    dm = closest_approach_dm(view, fk)
    for i, j, d in G.edges(data=True):
        d["length"] = dm[i, j]
    return G


def system_Delaunay_graph(system: System) -> nx.Graph:
    G = Delaunay_graph(system.r_matrix)
    dm = squareform(pdist(system.r_matrix))

    for i, j, d in G.edges(data=True):
        d["length"] = dm[i, j]
    return G
