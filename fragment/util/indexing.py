from typing import Any, Dict, Iterator, List

from fragment.core.PIE_common import Key
from fragment.systems.models import Atom, System


class KeyToIndex:
    key_to_index: Dict[Key, int]
    index_to_key: List[Key]

    def __init__(self, keys: Iterator[Key]):
        self.index_to_key = list(keys)
        self.key_to_index = {k: i for i, k in enumerate(self.index_to_key)}

    @classmethod
    def from_Systems(cls, sys: Iterator[System]) -> "KeyToIndex":
        return cls((s.key for s in sys))

    def __call__(self, k: Any) -> int:
        # if isinstance(k, Key): # Cannot type-check with subscripted generics
        if isinstance(k, tuple):
            return self.key_to_index[k]
        elif isinstance(k, System):
            return self.key_to_index[k.key]
        else:
            raise KeyError(f"{self.__class__.__name__} cannot be indexed with {k}.")

    def __len__(self) -> int:
        return len(self.key_to_index)

    def back(self, i: int) -> Key:
        return self.index_to_key[i]

    @property
    def size(self) -> int:
        return len(self)


class IDToIndex:
    id_to_index: Dict[int, int]
    index_to_id: List[int]

    def __init__(self, ids: Iterator[int]):
        self.index_to_id = list(ids)
        self.id_to_index = {k: i for i, k in enumerate(self.index_to_id)}

    @classmethod
    def from_Atoms(cls, atoms: Iterator[Atom]) -> "KeyToIndex":
        return cls((a.id for a in atoms))

    def __call__(self, k: Any) -> int:
        if isinstance(k, int):
            return self.id_to_index[k]
        elif isinstance(k, Atom):
            return self.id_to_index[k.id]
        else:
            raise KeyError(f"{self.__class__.__name__} cannot be indexed with {k}.")

    def __len__(self) -> int:
        return len(self.id_to_index)

    def back(self, i: int) -> Key:
        return self.index_to_id[i]

    @property
    def size(self) -> int:
        return len(self)
