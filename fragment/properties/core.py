from itertools import product
from typing import (
    Any,
    ClassVar,
    Dict,
    Iterable,
    Iterator,
    List,
    Optional,
    Set,
    Tuple,
    Type,
    Union,
)

import numpy as np


class AvailableProperty(object):
    """
    Defines the an agreed-upon interface for QM backends to
    extract and implement properties. QM backends are not required
    to implement all properties.
    """

    name: str
    type: Type
    use_coef: bool
    help: str

    def __init__(self, name: str, t: Type, use_coef: bool, help: str) -> None:
        self.name = name
        self.type = t
        self.use_coef = use_coef
        self.help = help


# Master property list used to construct extractors and adders and validators
MASTER_PROP_LIST: Dict[str, AvailableProperty] = {}


def add_property(name: str, t: Type, use_coef: bool, help: str) -> None:
    MASTER_PROP_LIST[name] = AvailableProperty(name, t, use_coef, help)


def get_property(name: str) -> None:
    return MASTER_PROP_LIST[name]


def remove_property(name: str) -> None:
    del MASTER_PROP_LIST[name]


NDIndexable = Union[np.ndarray, slice]


class PartialMatrix:
    # Class variables
    dtype: str  # Numpy type for matrix element
    window: ClassVar[Tuple[int, int]]
    extensive: ClassVar[Tuple[bool, bool]]

    # Object variables
    elements: List[int]
    el_types: List[int]
    lookup: Dict[int, int]
    data: np.ndarray

    def __init__(
        self, elements: List[int], el_types: List[int], data: np.ndarray
    ) -> None:
        if len(elements) != len(el_types):
            raise ValueError("Atom list and types are different lengths")
        if data.shape != self.get_size(len(elements)):
            raise ValueError(
                f"Numpy matrix is an incompatible shapes: {data.shape} != {self.get_size(len(elements))}"
            )

        self.elements = elements
        self.el_types = el_types
        self.data = data
        self.lookup = {a: i for i, a in enumerate(elements)}

    @classmethod
    def get_size(cls, nelements: int) -> Tuple[int, int]:
        return (
            cls.window[0] * nelements if cls.extensive[0] else cls.window[0],
            cls.window[1] * nelements if cls.extensive[1] else cls.window[1],
        )

    @classmethod
    def zeros(cls, elements: Iterable[int]) -> "PartialMatrix":
        nelements = len(elements)
        return cls(
            sorted(elements),
            [-1] * nelements,
            np.zeros(cls.get_size(nelements), dtype=cls.dtype),
        )

    @classmethod
    def union_zeros(cls, A: "PartialMatrix", B: "PartialMatrix") -> "PartialMatrix":
        # Check that we are talking about the same thing
        if not type(A) is cls or not type(B) is cls:
            raise ValueError(
                f"Cannot add two matrices representing different properties."
            )

        elements = sorted(set.union(set(A.elements), set(B.elements)))

        # I'm unsure how to resolve this union of types
        # This could become a bottleneck
        A_type_lookup = {i: t for i, t in zip(A.elements, A.el_types)}
        B_type_lookup = {i: t for i, t in zip(B.elements, B.el_types)}

        el_types = []
        for a in elements:
            A_t = A_type_lookup.get(a, None)
            B_t = B_type_lookup.get(a, None)

            if A_t is None:
                el_types.append(B_t)
            elif B_t is None:
                el_types.append(A_t)
            else:
                if A_t <= B_t:
                    el_types.append(A_t)
                else:
                    el_types.append(B_t)

        # Construct the new object and replace types
        new = cls.zeros(elements)
        new.el_types = el_types
        return new

    def axis_indexs(self, elements: Set[int], axis: int):
        """
        .. NOTE: Assumes elements are valid and in the matrix
        """
        if not self.extensive[axis]:
            return slice(None, None, None)

        window = self.window[axis]
        idxs = np.zeros(len(elements) * window, dtype="int")

        i = 0
        for a in elements:
            idx = self.lookup[a]
            for delta in range(window):
                idxs[i] = idx * window + delta
                i += 1
        return idxs

    def get_mask(self, elements: Set[int]) -> Tuple[NDIndexable, NDIndexable]:
        """
        .. NOTE: Assumes elements are valid and in the matrix
        """
        # Assuming two dimensions (no tensors)...
        D1 = self.axis_indexs(elements, 0)
        D2 = self.axis_indexs(elements, 1)

        if isinstance(D1, slice) or isinstance(D2, slice):
            return (D1, D2)

        size = len(D1) * len(D2)
        D1_prod = np.zeros(size, dtype="int")
        D2_prod = np.zeros(size, dtype="int")

        for i, (d1, d2) in enumerate(product(D1, D2)):
            D1_prod[i] = d1
            D2_prod[i] = d2
        return (D1_prod, D2_prod)

    def add_into(
        self, A: "PartialMatrix", coef: int, allowed_types: Optional[Set[int]] = None
    ) -> None:
        """
        Adds contents of `A` into `self` into.

        .. NOTE: This is a mutating method!
        """

        # Check that we are talking about the same thing
        if type(self) != type(A):
            raise ValueError(f"Cannot add two matrices with different types")

        # Get elements in question
        A_elements = set(A.elements)
        elements = set.intersection(A_elements, set(self.elements))
        if allowed_types:
            allowed_A_elements = {
                a for a, t in zip(A.elements, A.el_types) if t in allowed_types
            }
            elements.difference_update(allowed_A_elements)

        # Short circuite null case
        if not elements:
            return

        # Do actual addition :)
        self_mask = self.get_mask(elements)
        A_mask = A.get_mask(elements)

        self.data[self_mask] += coef * A.data[A_mask]

        # Does not return A. This is a mutator
        return None

    def accumulate_into(self, Bs: Iterable["PartialMatrix"], coefs: Iterable[int]):
        for B, c in zip(Bs, coefs):
            self.add_into(B, c)
        return None

    def __add__(self, A: "PartialMatrix") -> "PartialMatrix":
        C = self.__class__.union_zeros(self, A)
        C.add_into(self, 1)
        C.add_into(A, 1)
        return C

    def to_dict(self) -> Dict[str, Any]:
        return dict(
            elements=self.elements,
            el_types=[t for t in self.el_types],
            shape=self.data.shape,
            dtype=self.data.dtype.name,
            data=self.data.ravel().tolist(),
        )

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> "PartialMatrix":
        return cls(
            d["elements"],
            [int(t) for t in d["el_types"]],
            np.array(d["data"], dtype=cls.dtype).reshape(tuple(d["shape"])),
        )


class PropertySet(object):
    properties: Set[str]
    property_values: Dict[str, Any]

    def __init__(self, properties: Dict[str, Any]) -> None:
        self.properties = set(properties.keys())
        self.property_values = properties

    def validate_types(self) -> None:
        for p, v in self.property_values.items():
            prop_def = get_property(p)
            if not isinstance(v, prop_def.type):
                raise ValueError(
                    f"Invalide property type of {type(v)} for property {p}"
                )

    def add_property(self, name: str, value: Any) -> None:
        # TODO: validate that it is the correct type?
        self.properties.add(name)
        self.property_values[name] = value

    @classmethod
    def zeros(cls, keys: Iterable[str], elements: List[int]) -> "PropertySet":
        """
        Initialize an empty properties instance
        """
        props = {}
        for k in keys:
            try:
                prop_def = get_property(k)
            except KeyError:
                raise ValueError(f"Unknown property: {k}")

            if prop_def.type is int:
                props[k] = 0
            elif prop_def.type is float:
                props[k] = 0.0
            elif issubclass(prop_def.type, PartialMatrix):
                props[k] = prop_def.type.zeros(elements)
            else:
                raise ValueError(f"Unknown property type {prop_def.type}")
        return cls(props)

    @classmethod
    def zeros_all_props(cls, elements: List[int]) -> "PropertySet":
        return cls.zeros(MASTER_PROP_LIST.keys(), elements)

    @classmethod
    def union_zeros(cls, P1: "PropertySet", P2: "PropertySet") -> "PropertySet":
        properties = P1.properties.intersection(P2.properties)
        elements: Set[int] = set()
        for p in properties:
            prop_def = get_property(p)
            if issubclass(prop_def.type, PartialMatrix):
                elements.update(P1[p].elements)
                elements.update(P2[p].elements)

        return cls.zeros(properties, sorted(elements))

    def add_into(self, P: "PropertySet", coef: int) -> None:
        """
        Adds properties P into self. Deletes properties not in both
        """
        prop_rm = self.properties.difference(P.properties)
        self.properties.intersection_update(P.properties)

        # Remove non-existant properties
        for p in prop_rm:
            del self.property_values[p]

        # Update propertis
        for p in self.properties:
            prop_def = get_property(p)
            _coef = coef if prop_def.use_coef else 1
            if issubclass(prop_def.type, PartialMatrix):
                self[p].add_into(P[p], _coef)
            else:
                # Bypass the setters for in-place addition
                self.property_values[p] += _coef * P[p]

    def accumulate_into(self, Ps: Iterable["PropertySet"], coefs: int) -> None:
        for P, coef in zip(Ps, coefs):
            self.add_into(P, coef)

    def __add__(self, P: "PropertySet") -> "PropertySet":
        """
        Returns new value set with the sum of the common
        properties between sets
        """
        C = self.union_zeros(self, P)
        C.add_into(self, 1)
        C.add_into(P, 1)
        return C

    def __sub__(self, P: "PropertySet") -> "PropertySet":
        C = self.union_zeros(self, P)
        C.add_into(self, 1)
        C.add_into(P, -1)
        return C

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.property_values})"

    def __getitem__(self, key: str) -> Any:
        return self.property_values[key]

    def __iter__(self) -> Iterator[Tuple[str, Any]]:
        return ((k, v) for k, v in self.property_values.items())

    def __contains__(self, item: Any) -> bool:
        return item in self.properties

    def to_dict(self) -> Dict[str, Any]:
        d = {}
        for k, v in self.property_values.items():
            if isinstance(v, PartialMatrix):
                d[k] = v.to_dict()
            else:
                d[k] = v
        return d

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> "PropertySet":
        properties = {}
        for k, v in d.items():
            prop_def = get_property(k)
            if issubclass(prop_def.type, PartialMatrix):
                properties[k] = prop_def.type.from_dict(v)
            else:
                properties[k] = prop_def.type(v)
        return cls(properties)
