# # # # # # # # # # # # # # # # # # # # # # # # # #
#  Definitions for globally available properties  #
# # # # # # # # # # # # # # # # # # # # # # # # # #

from fragment.properties.core import PartialMatrix, add_property

# Add master list of properties
add_property(
    name="cpu_time",
    t=float,
    use_coef=False,
    help="CPU time for a calculation in seconds.",
)

add_property(
    name="wall_time",
    t=float,
    use_coef=False,
    help="Wall time for a calculation in seconds.",
)

add_property(
    name="basis_functions",
    t=int,
    use_coef=False,
    help="Number of basis functions in calculation",
)

add_property(
    name="total_energy",
    t=float,
    use_coef=True,
    help="Total energy of QM system in Hartree",
)

add_property(
    name="total_scf_energy",
    t=float,
    use_coef=True,
    help="Total SCF energy before the addition of correlation corrections Hartree",
)

add_property(
    name="dft_exchange", t=float, use_coef=True, help="DFT exchange energy in Hartree"
)

add_property(
    name="dft_correlation",
    t=float,
    use_coef=True,
    help="DFT correlation contribution to total energy Hartree",
)

add_property(
    name="MP2_correlation",
    t=float,
    use_coef=True,
    help="Couple cluster triples correlation correlation contribution to total energy Hartree",
)

add_property(
    name="CCSD_correlation",
    t=float,
    use_coef=True,
    help="Couple cluster singles and doubles correlation contribution to total energy Hartree",
)

add_property(
    name="CC_T_correlation",
    t=float,
    use_coef=True,
    help="Couple cluster triples correlation correlation contribution to total energy Hartree",
)

add_property(
    name="hf_exchange",
    t=float,
    use_coef=True,
    help="Total Hartree Fock exchange energy in Hartree",
)

add_property(
    name="total_correlation_energy",
    t=float,
    use_coef=True,
    help="Hartree Fock exchange energy in Hartree",
)

add_property(
    name="kinetic_energy", t=float, use_coef=True, help="Kinetic energy in Hartree"
)

add_property(
    name="total_coulomb_energy",
    t=float,
    use_coef=True,
    help="Coulomb energy in Hartree",
)

add_property(
    name="nuclear_attraction",
    t=float,
    use_coef=True,
    help="Nuclear attraction Energy in Hartree",
)

add_property(
    name="nuclear_repulsion",
    t=float,
    use_coef=True,
    help="Nuclear attraction energy in Hartree",
)

add_property(
    name="one_electron_int",
    t=float,
    use_coef=True,
    help="Sum of one-electron integrals in Hartree",
)

add_property(
    name="two_electron_int",
    t=float,
    use_coef=True,
    help="Sum of one-electron integrals in Hartree",
)

add_property(
    name="total_enthalpy",
    t=float,
    use_coef=True,
    help="Total enthalpy of QM system in kcal/mol",
)

add_property(
    name="total_entropy",
    t=float,
    use_coef=True,
    help="Total entropy of QM system kcal/mol/K",
)

add_property(
    name="total_gibbs",
    t=float,
    use_coef=True,
    help="Total Gibbs free energy of QM system in kcal/mol",
)


class SCFGradient(PartialMatrix):
    window = (3, 1)
    extensive = (False, True)
    dtype = "float64"


add_property(
    name="scf_gradient",
    t=SCFGradient,
    use_coef=True,
    help="Cartesian gradient of the SCF electronic energy",
)
