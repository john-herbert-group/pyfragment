from peewee import *
from playhouse.sqlite_ext import JSONField

from fragment.properties.core import PropertySet

PROPERTIES_CACHE = "_properties_cache"


class PropertyMixin(Model):
    properties_ = JSONField(default=lambda: {})

    @property
    def properties(self) -> PropertySet:
        if not hasattr(self, PROPERTIES_CACHE):
            self._properties_cache = PropertySet.from_dict(self.properties_)
        return self._properties_cache

    @properties.setter
    def properties(self, val: PropertySet):
        self._properties_cache = val
        self.properties_ = val.to_dict()

    @properties.deleter
    def properties(self):
        del self.properties_
        del self._properties_cache

    def save(self, *args, **kwargs):
        # Update the properties value with the current changes
        if hasattr(self, PROPERTIES_CACHE):
            prop_dict = self._properties_cache.to_dict()
            if self.properties_ != prop_dict:
                self.properties_ = prop_dict
        return super().save(*args, **kwargs)
