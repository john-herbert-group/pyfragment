import subprocess
from typing import List, Tuple

import qcelemental as qcel

from fragment.backends.abstract import QMShellCommand
from fragment.backends.common import RunContext
from fragment.properties.extraction import calc_property


class MOPACBackend(QMShellCommand):
    DEFAULT_TEMPLATE_PARAMS = {
        "template": """\
            PM7 XYZ PRECISE NOSYM NOREOR GEO-OK


            {geometry}
        """,
        "atom": "{symbol}    {x} {y} {z}",
        "ghost_atom": None,
    }
    FILE_MANIFEST = {"input": ".mop", "output": ".out"}
    RUN_CMD = "MOPAC2016.exe"

    @classmethod
    def is_available(cls):
        try:
            return (
                subprocess.run(
                    cls.RUN_CMD,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                    input=b"\n",
                ).returncode
                == 0
            )
        except:
            return False

    def get_run_cmd(self, ctx: RunContext) -> Tuple[str, List[str]]:
        return self.RUN_CMD, [ctx.files["input"]]

    @calc_property(source="re_file", patterns=[r"TOTAL ENERGY\s+=\s+(-?\d+\.\d+)\sEV"])
    def prop_total_energy(self, ctx, m, _):
        return float(m[1]) / qcel.constants.hartree2ev

    @calc_property(
        source="re_file", patterns=[r"COMPUTATION TIME\s+=\s+(-?\d+\.\d+)\sSECONDS"]
    )
    def prop_cpu_time(self, ctx, m, _):
        return float(m[1])
