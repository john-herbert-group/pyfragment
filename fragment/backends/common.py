from datetime import datetime
from pathlib import Path
from typing import Any, Dict, Optional, TextIO

from fragment.calculations.common import JobStatus
from fragment.properties.core import PropertySet
from fragment.properties.extraction import EXTRACTOR_MAPPING, DataSource, Extractor
from fragment.systems.models import System


class RunContext:
    id: int
    name: str
    workpath: Path
    cores: int
    system: System
    files: Dict[str, Path]
    open_files: Dict[str, TextIO]
    scratch: Dict[Any, Any]  # Scratch space for running the calc
    start_time: datetime
    end_time: datetime

    def __init__(
        self,
        id: int,
        name: str,
        system: System,
        workpath: Optional[Path] = None,
        backend_id: Optional[int] = None,
        cores: int = 1,
    ) -> None:
        self.id = id
        self.name = name
        self.workpath = workpath
        self.system = system
        self.cores = cores
        self.backend_id = backend_id

        self.start_time = None
        self.end_time = None
        self.files = dict()
        self.open_files = dict()
        self.scratch = dict()

    def open_file(self, tag: str, mode="r") -> TextIO:
        if tag in self.open_files:
            return self.open_files[tag]
        self.open_files[tag] = self.files[tag].open(mode)
        return self.open_files[tag]

    def close_file(self, tag: str) -> None:
        f = self.open_file.get(tag, None)
        if f is None:
            return
        f.close()
        del self.open_files[tag]

    def close_files(self) -> None:
        for f in self.open_files.values():
            f.close()
        self.open_files = dict()


class Result:
    id: int
    name: str
    workpath: Optional[Path]
    status: JobStatus
    files: Dict[str, Path]
    start_time: datetime
    end_time: datetime
    properties: PropertySet

    def __init__(
        self,
        id: int,
        name: str,
        status: JobStatus,
        start_time: datetime,
        end_time: datetime,
        workpath: Optional[Path] = None,
        files: Optional[Dict[str, str]] = None,
        properties: Optional[PropertySet] = None,
    ) -> None:
        self.id = id
        self.name = name
        self.workpath = workpath
        self.status = status
        self.start_time = start_time
        self.end_time = end_time
        self.files = files or dict()
        self.properties = properties or PropertySet({})

    @classmethod
    def fromRunContext(cls, ctx: RunContext, status, properties=None) -> "Result":
        return cls(
            ctx.id,
            ctx.name,
            status,
            ctx.start_time,
            ctx.end_time,
            workpath=ctx.workpath,
            files={k: f for k, f in ctx.files.items() if f.exists()},
            properties=properties,
        )
