import time
from datetime import datetime
from functools import partial
from typing import Any, Callable, Dict, Union, cast

from fragment.backends.abstract import QMBackend
from fragment.backends.common import RunContext
from fragment.backends.exceptions import SCFConvergenceError
from fragment.properties.extraction import calc_property
from fragment.schemas.common import FragmentBaseModel
from fragment.schemas.strategy import (
    PYSCF_PROC_LIST,
    PySCF_CCSD,
    PySCF_CCSD_T,
    PySCF_DFMP2,
    PySCF_MP2,
    PySCF_Procedure,
)
from fragment.systems.common import AtomType

try:
    # Conditional import just in case
    import pyscf
except ImportError:
    pyscf = None


# Supported Methods
def HF(mol, args: PySCF_Procedure):
    from pyscf import scf

    driver = scf.HF if args.driver is None else getattr(scf, args.driver)
    scf = driver(mol)
    if args.use_newton:
        scf = scf.newton()
    scf.run(conv_tol=args.conv_tol, direct_scf_tol=args.direct_scf_tol)

    if not scf.converged:
        raise SCFConvergenceError("HF SCF procedure didn't converge")
    return scf, dict(
        total_energy=scf.e_tot,
        total_scf_energy=scf.e_tot,
        one_electron_int=scf.scf_summary["e1"],
        two_electron_int=scf.scf_summary["e2"],
        nuclear_repulsion=scf.scf_summary["nuc"],
    )


def KS(mol, args: PySCF_Procedure):
    from pyscf import dft

    driver = dft.KS if args.driver is None else getattr(dft, args.driver)
    scf = driver(mol)
    if args.use_newton:
        scf = scf.newton()
    scf.xc = args.xc
    scf.run(conv_tol=args.conv_tol, direct_scf_tol=args.direct_scf_tol)

    if not scf.converged:
        raise SCFConvergenceError("KS SCF procedure didn't converge")

    return scf, dict(
        total_energy=scf.e_tot,
        total_scf_energy=scf.e_tot,
        one_electron_int=scf.scf_summary["e1"],
        dft_exchange=scf.scf_summary["exc"],
        total_coulomb_energy=scf.scf_summary["coul"],
        nuclear_repulsion=scf.scf_summary["nuc"],
    )


def MP2(mol, args: PySCF_MP2):
    from pyscf import mp

    scf, results = HF(mol, args)
    mp2_driver = mp.MP2 if args.driver is None else getattr(mp, args.mp2_driver)
    mp2 = mp2_driver(scf)
    mp2.conv_tol = args.conv_tol
    # mp2.direct_scf_tol = args.direct_scf_tol
    mp2.run()

    results.update(total_energy=mp2.e_tot, total_correlation_energy=mp2.e_corr)
    return (mp2, results)


def DFMP2(mol, args: PySCF_DFMP2):
    from pyscf.mp import dfmp2_native

    scf, results = HF(mol, args)
    dfmp2_driver = (
        dfmp2_native.DFMP2
        if args.driver is None
        else getattr(dfmp2_native, args.mp2_driver)
    )

    if args.auxbasis:
        dfmp2 = dfmp2_driver(scf)
    else:
        dfmp2 = dfmp2_driver(scf, auxbasis=args.auxbasis)

    dfmp2.conv_tol = args.conv_tol
    dfmp2.direct_scf_tol = args.direct_scf_tol
    dfmp2.run()

    results.update(total_energy=dfmp2.e_tot, total_correlation_energy=dfmp2.e_corr)
    return (dfmp2, results)


def CCSD(mol, args: PySCF_CCSD):
    from pyscf import cc

    scf, results = HF(mol, args)
    ccsd = cc.CCSD(scf)
    ccsd.run(conv_tol=args.conv_tol)
    results.update(
        total_energy=ccsd.e_tot,
        total_correlation_energy=ccsd.e_corr,
        CCSD_correlation=ccsd.e_corr,
        MP2_correlation=ccsd.emp2,
    )
    return (ccsd, results)


def CCSD_T(mol, args: PySCF_CCSD_T):
    ccsd, results = CCSD(mol, args)
    et = ccsd.ccsd_t()

    results["total_energy"] += et
    results["total_correlation_energy"] += et
    results["CC_T_correlation"] = et
    return (ccsd, results)


PROCEDURES = {
    "HF": HF,
    "KS": KS,
    "DFT": KS,
    "MP2": MP2,
    "DFMP2": DFMP2,
    "RIMP2": DFMP2,
    "CCSD": CCSD,
    "CCSD(T)": CCSD_T,
}


def get_procedure(proc_dict: Dict) -> PySCF_Procedure:
    class _PROC_GETTER(FragmentBaseModel):
        proc: PYSCF_PROC_LIST

    # Pydantic magic
    return _PROC_GETTER(proc=proc_dict).proc


class PySCFBackend(QMBackend):
    procedure_args: PySCF_Procedure
    procedure: Callable

    def __init__(
        self,
        *args: Any,
        procedure: Union[PySCF_Procedure, Dict[Any, Any]],
        **kwargs: Any,
    ) -> None:
        if not isinstance(procedure, PySCF_Procedure):
            procedure = get_procedure(procedure)
        cast(procedure, PySCF_Procedure)
        super().__init__(*args, procedure=procedure.dict(), **kwargs)
        self.procedure_args = procedure
        self.procedure = partial(
            PROCEDURES[self.procedure_args.method.upper()],
            args=self.procedure_args,
        )

    @classmethod
    def is_available(cls) -> bool:
        return not pyscf is None

    def setup_calc(self, ctx: RunContext):
        super().setup_calc(ctx)

        sys = ctx.system
        physical = [(a.t, a.r) for a in sys.atoms.filter(type=AtomType.PHYSICAL)]
        ghost = [("ghost:" + a.t, a.r) for a in sys.atoms.filter(type=AtomType.GHOST)]
        ctx.scratch["mol"] = pyscf.M(
            atom=physical + ghost,
            basis=self.procedure_args.basis,
            charge=sys.charge,
            spin=(sys.multiplicity - 1),
            unit="AU",
            symmetry=False,
            verbose=0,
        )

    def run_calc(self, ctx: RunContext):
        with pyscf.lib.misc.with_omp_threads(ctx.cores):
            ctx.start_time = datetime.now()
            cpu0 = time.process_time()
            _, res = self.procedure(ctx.scratch["mol"])
            ctx.scratch["cpu_time"] = time.process_time() - cpu0
            ctx.end_time = datetime.now()
            ctx.scratch.update(**res)

    def cleanup_calc(self, ctx: RunContext):
        super().cleanup_calc(ctx)

    @calc_property(source="context")
    def prop_cpu_time(self, ctx: RunContext):
        return ctx.scratch.get("cpu_time", None)

    @calc_property(source="context")
    def prop_total_energy(self, ctx: RunContext):
        return ctx.scratch.get("total_energy", None)

    @calc_property(source="context")
    def prop_total_scf_energy(self, ctx: RunContext):
        return ctx.scratch.get("total_scf_energy", None)

    @calc_property(source="context")
    def prop_CCSD_correlation(self, ctx: RunContext):
        return ctx.scratch.get("CCSD_correlation", None)

    @calc_property(source="context")
    def prop_CC_T_correlation(self, ctx: RunContext):
        return ctx.scratch.get("CC_T_correlation", None)

    @calc_property(source="context")
    def prop_MP2_correlation(self, ctx: RunContext):
        return ctx.scratch.get("MP2_correlation", None)

    @calc_property(source="context")
    def prop_total_correlation_energy(self, ctx: RunContext):
        return ctx.scratch.get("total_correlation_energy", None)

    @calc_property(source="context")
    def prop_one_electron_int(self, ctx: RunContext):
        return ctx.scratch.get("one_electron_int", None)

    @calc_property(source="context")
    def prop_two_electron_int(self, ctx: RunContext):
        return ctx.scratch.get("two_electron_int", None)

    @calc_property(source="context")
    def prop_dft_exchange(self, ctx: RunContext):
        return ctx.scratch.get("dft_exchange", None)

    @calc_property(source="context")
    def prop_total_coulomb_energy(self, ctx: RunContext):
        return ctx.scratch.get("total_coulomb_energy", None)

    @calc_property(source="context")
    def prop_nuclear_repulsion(self, ctx: RunContext):
        return ctx.scratch.get("nuclear_repulsion", None)
