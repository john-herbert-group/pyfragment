import time
from datetime import datetime
from os import environ
from typing import Any, Optional, Tuple, Union

from qcelemental import constants

from fragment.backends.abstract import QMBackend
from fragment.backends.common import RunContext
from fragment.properties.extraction import calc_property
from fragment.schemas.strategy import XTBMethods


class LibXTBBackend(QMBackend):
    method: str
    save_output: bool
    accuracy: float

    def __init__(
        self,
        *args: Any,
        method: Optional[Union[XTBMethods, str]] = None,
        accuracy: float = 0.01,
        save_output: bool = False,
        **kwargs: Any,
    ) -> None:
        method = method.lower() if method else "gfn2"
        super().__init__(
            *args, method=method, save_output=save_output, accuracy=accuracy, **kwargs
        )
        self.method = method
        self.save_output = save_output
        self.accuracy = accuracy

        self._configured = False
        self._interface = None
        self._libxtb = None

        if self.save_output:
            self.FILELESS = False
            self.FILE_MANIFEST = {"output": ".log"}

    @classmethod
    def is_available(cls) -> bool:
        try:
            import xtb as _

            return True
        except:
            return False

    def _import_libxtb(self, cores: int) -> Tuple[Any, Any]:
        """
        Sets the relevent evirnment variables and loads xTB

        ..node:: Once loaded, the number of cores used by xtb cannot be changed
            even if we reload the module :(
        """

        # Return modules if we've already loaded them
        if self._configured:
            return self._interface, self._libxtb

        environ["OMP_NUM_THREADS"] = str(cores) + ",1"
        environ["MKL_NUM_THREADS"] = str(cores)

        import xtb.interface as interface
        import xtb.libxtb as libxtb

        self._configured = True
        self._interface = interface
        self._libxtb = libxtb

        return interface, libxtb

    def setup_calc(self, ctx: RunContext):
        super().setup_calc(ctx)

        interface, libxtb = self._import_libxtb(ctx.cores)

        methods = {
            "gfn2": interface.Param.GFN2xTB,
            "gfn1": interface.Param.GFN1xTB,
            "gfn0": interface.Param.GFN0xTB,
            "ipea": interface.Param.IPEAxTB,
            "gfnff": interface.Param.GFNFF,
        }

        calc = interface.Calculator(
            methods[self.method],
            ctx.system.atom_number_matrix,
            ctx.system.r_matrix / constants.bohr2angstroms,
            charge=ctx.system.charge,
        )
        calc.set_accuracy(self.accuracy)
        if self.save_output:
            calc.set_output(str(ctx.files["output"]))
            calc.set_verbosity(libxtb.VERBOSITY_FULL)
        else:
            # calc.set_verbosity(libxtb.VERBOSITY_FULL)
            calc.set_verbosity(libxtb.VERBOSITY_MUTED)
        # TODO: Setup external charges
        ctx.scratch["calculator"] = calc

    def run_calc(self, ctx: RunContext):
        ctx.start_time = datetime.now()
        cpu0 = time.process_time()
        res = ctx.scratch["calculator"].singlepoint()
        ctx.scratch["result"] = res
        ctx.scratch["cpu_time"] = time.process_time() - cpu0
        ctx.end_time = datetime.now()

    def cleanup_calc(self, ctx: RunContext):
        super().cleanup_calc(ctx)
        if self.save_output:
            ctx.scratch["calculator"].release_output()

    @calc_property(source="context")
    def prop_total_energy(self, ctx: RunContext):
        res = ctx.scratch.get("result", None)
        if res:
            return res.get_energy()

    @calc_property(source="context")
    def prop_cpu_time(self, ctx: RunContext):
        return ctx.scratch.get("cpu_time", None)
