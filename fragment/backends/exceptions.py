class QMBackendException(Exception):
    pass


class BackendUnavailable(QMBackendException):
    pass


class UnknownQMBackend(QMBackendException):
    pass


class BackendRunException(QMBackendException):
    pass


class ProcessFailed(BackendRunException):
    pass


class NoOutputFile(BackendRunException):
    pass


class ConfigurationError(BackendRunException):
    pass


class CommandNotFound(BackendRunException):
    pass


class SCFConvergenceError(BackendRunException):
    pass
