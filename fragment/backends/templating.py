from textwrap import dedent

from fragment.systems.models import Atom, AtomType, System

DEFAULT_SYSTEM = """\
    {name}: n_atoms={n_atoms}; {info}

    {charge} {mult}
    {geometry}
"""
DEFAULT_ATOM = " {symbol}    {x} {y} {z}"
DEFAULT_GHOST_ATOM = "@{symbol}    {x} {y} {z}"


class SystemTemplate(object):
    def __init__(
        self,
        template=None,
        atom=None,
        ghost_atom=None,
    ) -> None:
        self.template = dedent(template) if template else None
        self.atom = atom
        self.ghost_atom = ghost_atom

    def override(self, **kwargs):
        for k, v in kwargs.items():
            if v is None:
                continue
            setattr(self, k, v)

    def system_to_string(self, sys: System, **kwargs) -> str:
        atom_strs = [self.atom_to_string(a) for a in sys.atoms]
        geometry = "\n".join(atom_s for atom_s in atom_strs if not atom_s is None)

        # TODO: add additional fields to the template
        fragment_str = self.template.format(
            name=kwargs.get("name", ""),
            info=kwargs.get("info", ""),
            cores=kwargs.get("cores", "1"),
            n_atoms=len(sys.atoms),
            charge=sys.charge,
            mult=sys.multiplicity,
            geometry=geometry,
        )
        return fragment_str

    def atom_to_string(self, a: Atom) -> str:
        if a.type == AtomType.GHOST:
            template = self.ghost_atom
        else:
            template = self.atom

        if not template:
            return

        return template.format(symbol=a.t, x=a.x, y=a.y, z=a.z)


def get_default_template():
    return SystemTemplate(
        template=DEFAULT_SYSTEM,
        atom=DEFAULT_ATOM,
        ghost_atom=DEFAULT_GHOST_ATOM,
    )
