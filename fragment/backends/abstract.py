import logging
import os
import subprocess
from datetime import datetime
from pathlib import Path
from typing import IO, Any, ClassVar, Dict, List, Optional, Tuple

import fragment.properties.properties as _
from fragment.backends.common import Result, RunContext
from fragment.backends.exceptions import (
    BackendRunException,
    ConfigurationError,
    NoOutputFile,
    ProcessFailed,
)
from fragment.backends.templating import SystemTemplate
from fragment.calculations.common import JobStatus
from fragment.conf.models import ConfigType
from fragment.conf.util import SavableConfig
from fragment.properties.core import PropertySet
from fragment.properties.extraction import PropertyExtractorMixin, calc_property
from fragment.systems.models import System

log = logging.getLogger(__name__)


class QMBackend(PropertyExtractorMixin, SavableConfig):
    # Class variables
    CONFIG_TYPE = ConfigType.QM_BACKEND
    FILELESS: ClassVar[bool] = True  # Potentially fileless
    EXTRACTABLE_FILES: ClassVar[bool] = tuple()
    FILE_MANIFEST: ClassVar[Dict[str, str]] = dict()

    @classmethod
    def is_available(cls) -> bool:
        return True

    @property
    def is_fileless(self):
        if self.FILE_MANIFEST:
            return False
        return self.FILELESS

    def run(
        self,
        id: int,
        name: str,
        system: System,
        cores: int,
        workpath: Optional[Path] = None,
    ) -> Result:
        ctx = RunContext(id, name, system, workpath=workpath, cores=cores)
        return self.run_ctx(ctx)

    def run_ctx(self, ctx: RunContext) -> Result:
        if not self.is_fileless and not ctx.workpath.is_absolute():
            raise ConfigurationError(
                "Context has a relative workpath: " + str(ctx.workpath)
            )
        try:
            self.setup_calc(ctx)
            self.run_calc(ctx)
        except Exception as e:
            raise e
        finally:
            res = self.gather_results(ctx)
            self.cleanup_calc(ctx)
        return res

    def setup_calc(self, ctx: RunContext):
        # NOTE: It's possible to have an empty FILE_MANIFEST and still be
        #       FILELESS due to non-saved scratch files
        if not self.is_fileless:
            try:
                ctx.scratch["old_path"] = os.getcwd()
            except FileNotFoundError:
                pass

            ctx.workpath.mkdir(parents=True, exist_ok=True)
            os.chdir(ctx.workpath)

        ctx.files = self.file_manifest(ctx.name, ctx.workpath)

    def file_manifest(self, filename_stub: str, workpath: Path) -> Dict[str, Path]:
        return {
            k: workpath / f"{filename_stub}{ext}"
            for k, ext in self.FILE_MANIFEST.items()
        }

    def run_calc(self, ctx: RunContext):
        pass

    def determine_success(self, ctx: RunContext):
        for file in ctx.files.values():
            if not file.exists():
                raise NoOutputFile(f"Process did not create an output file {file}")

    @calc_property(source="context")
    def prop_wall_time(self, ctx: RunContext):
        if ctx is None:  # Really only happens in testing
            return None
        if ctx.start_time is None or ctx.end_time is None:
            return None
        return (ctx.end_time - ctx.start_time).total_seconds()

    def sources_from_ctx(self, ctx: RunContext) -> List[Any]:
        sources = []

        # Handle files inside RunContexts
        ctx.close_files()  # Start from a clean slate

        # Open the run context and add it's file to list of sources
        for tag in self.EXTRACTABLE_FILES:
            if ctx.files[tag].exists():
                sources.append(ctx.open_file(tag))
        return sources

    def gather_results(self, ctx) -> Result:
        try:
            self.determine_success(ctx)
        except BackendRunException:
            status = JobStatus.FAILED
        else:
            status = JobStatus.COMPLETED

        try:
            return Result.fromRunContext(
                ctx, status, self.get_properties(ctx, self.sources_from_ctx(ctx))
            )
        finally:
            ctx.close_files()

    def cleanup_calc(self, ctx: RunContext):
        ctx.close_files()
        if ctx.scratch.get("old_path", None):
            os.chdir(ctx.scratch["old_path"])


class QMShellCommand(QMBackend):
    CONFIG_TYPE = ConfigType.QM_BACKEND
    DEFAULT_TEMPLATE_PARAMS = {}
    FILELESS = False
    FILE_MANIFEST = {"input": ".inp", "output": ".out", "log": ".log"}
    EXTRACTABLE_FILES = ("output",)
    STDOUT_FILE = "log"
    STDERR_FILE = "log"
    RUN_CMD = None

    def __init__(self, *args, **kwargs) -> None:
        # Pass along arguments and make them saveable
        super().__init__(*args, **kwargs)
        self.make_template(**kwargs)

    def make_template(self, **kwargs) -> None:
        # Set defaults
        self.template = SystemTemplate(**self.DEFAULT_TEMPLATE_PARAMS)
        # Overwrite with user-supplied values
        self.template.override(**kwargs)

    @classmethod
    def is_available(cls):
        result = subprocess.run(["which", cls.RUN_CMD], text=True, capture_output=True)
        return result.returncode == 0

    def get_run_cmd(self, ctx: RunContext) -> Tuple[str, List[str]]:
        """Returns the command-line program required to run an external
        QM application

        .. note:: This function assumes the QM process will be run on a single
            node with multiple CPUs

        Argumens:
            input_path (str): The path to an existing input file
            output_path (str): The path to the not-yet-written output file
            cores(int): Number of threads/CPUs to run the application

        Returns:
            str: QM Command
            List[str]: Arguments
        """
        raise NotImplementedError("The backend should implement this function")

    def run_calc(self, ctx: RunContext):
        cmd = ctx.scratch["run_cmd"]
        args = ctx.scratch["run_args"]

        ctx.start_time = datetime.now()
        proc = subprocess.Popen(
            [cmd] + args,
            stdout=ctx.open_file(self.STDOUT_FILE, "w"),
            stderr=ctx.open_file(self.STDERR_FILE, "w"),
        )
        ctx.scratch["proc"] = proc
        proc.wait()
        ctx.end_time = datetime.now()
        ctx.close_files()

    def determine_success(self, ctx: RunContext):
        """Raise exception if the QM Job failed

        Args:
            output_path (int): Expected output path of the QM process
            returncode (int): Shell return code
            stdout (str): String of the STDOUT content
            stderror (str): String of STDERR content

        Raises:
            ProcessFailed: Raised if non-zero return code
            NoOutputFile: Raise if no output file was created
        """
        try:
            proc: subprocess.Popen = ctx.scratch["proc"]
        except KeyError:
            raise BackendRunException("The executable was never called.")
        if proc.returncode is None:
            raise BackendRunException("Calculation is still running")
        if proc.returncode != 0:
            raise ProcessFailed("Process returned a non-zero exit code")
        super().determine_success(ctx)

    def setup_calc(self, ctx: RunContext):
        super().setup_calc(ctx)

        # Get the run command
        ctx.scratch["run_cmd"], ctx.scratch["run_args"] = self.get_run_cmd(ctx)

        # Write the input file
        if ctx.files["input"].exists():
            return
        with ctx.files["input"].open("w") as f:
            f.write(
                self.template.system_to_string(
                    ctx.system,
                    num_atoms=len(ctx.system.atoms),
                    name=ctx.name,
                    time=datetime.now().isoformat(),
                    system_id=ctx.system.id,
                    backend=self.name,
                    cores=ctx.cores,
                )
            )

    def file_manifest(self, filename_stub: str, workpath: Path) -> Dict[str, Path]:
        if not "log" in self.FILE_MANIFEST:
            self.FILE_MANIFEST["log"] = ".log"
        return super().file_manifest(filename_stub, workpath)
