import subprocess
from os import environ
from typing import List, Optional, Tuple

from fragment.backends.abstract import QMShellCommand
from fragment.backends.common import RunContext
from fragment.properties.extraction import calc_property


class XTBBackend(QMShellCommand):
    DEFAULT_TEMPLATE_PARAMS = {
        "template": """\
            {n_atoms}
            Fragment {name}; Charge={charge}; Multiplicity={mult}
            {geometry}
        """,
        "atom": "{symbol}    {x} {y} {z}",
        "ghost_atom": None,
    }
    FILE_MANIFEST = {"input": ".xyz", "output": ".out"}
    STDOUT_FILE = "output"
    RUN_CMD = "xtb"

    def __init__(self, *args, memory: Optional[str] = None, **kwargs) -> None:
        super().__init__(*args, memory=memory, **kwargs)
        self.memory = memory or 8

    @classmethod
    def is_available(cls):
        try:
            return (
                subprocess.run(
                    [cls.RUN_CMD, "file_does_not_exits"],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                ).returncode
                == 1
            )
        except:
            return False

    def setup_calc(self, ctx: RunContext) -> None:
        super().setup_calc(ctx)
        environ["OMP_STACKSIZE"] = f"{self.memory}G"
        environ["OMP_NUM_THREADS"] = f"{ctx.cores},1"

    def get_run_cmd(self, ctx: RunContext) -> Tuple[str, List[str]]:
        charge = ctx.system.charge
        return self.RUN_CMD, [ctx.files["input"], "--charge", str(charge)]

    @calc_property(source="re_file", patterns=[r"TOTAL ENERGY\s+(-?\d+\.\d+)\sEh"])
    def prop_total_energy(self, ctx, m, _):
        return float(m[1])

    @calc_property(
        source="re_file",
        patterns=[r"cpu-time:\s+(\d+) d,\s+(\d+) h,\s+(\d+) min,\s+(\d+.\d+) sec"],
    )
    def prop_cpu_time(self, ctx, m, _):
        times = [float(m[1]) * 86400, float(m[2]) * 3600, float(m[3]) * 60, float(m[4])]
        return sum(times)

    @calc_property(source="re_file", patterns=[r"TOTAL ENTHALPY\s+(-?\d+.\d+) Eh"])
    def prop_total_enthalpy(self, ctx, m, _):
        """
        Properties from frequency calcualtions
        Enthalphy reported in Eh
        """
        return float(m[1])

    @calc_property(
        source="re_file", patterns=[r"TOT\s+\d+.\d+\s+\d+.\d+\s+(\d+.\d+)\s+\d+.\d+"]
    )
    def prop_total_entropy(self, ctx, m, _):
        """
        Entropy reported in cal/mol*K
        """
        return float(m[1])
