import re
import subprocess
from os import environ
from typing import Any, TextIO

import numpy as np

from fragment.backends.abstract import QMShellCommand
from fragment.backends.common import RunContext
from fragment.properties.extraction import calc_property
from fragment.properties.properties import SCFGradient
from fragment.systems.common import AtomType

ENERGY_SEARCH_RE = r"^\s*method[ \t]*=?[ \t]*([^\s]*)"  # Connat pre-compile
ENERGY_EXPRESSIONS = {
    "mp2": re.compile(r"MP2[ \t]+[tT]otal [eE]nergy =[ \t]+(-?\d+.\d+)"),
    "rimp2": re.compile(r"RIMP2[ \t]+total [eE]nergy =[ \t]+(-?\d+.\d+)"),
    "ccsd": re.compile(r"CCSD [tT]otal [eE]nergy[ \t]+=[ \t]+(-?\d+.\d+)"),
    "ccsd(t)": re.compile(r"CCSD\(T\) [tT]otal [eE]nergy[ \t]+=[ \t]+(-?\d+.\d+)"),
    "default": re.compile(r"Total energy in the final basis set =[ \t]+(-?\d+.\d+)"),
}
CORRELATION_EXPRESSION = {
    "mp2": re.compile(r"MP2[ \t]+correlation energy =[ \t]+(-?\d+.\d+)"),
    "rimp2": re.compile(r"RIMP2[ \t]+correlation energy =[ \t]+(-?\d+.\d+)"),
    "ccsd": re.compile(r"CCSD correlation energy[ \t]+=[ \t]+(-?\d+.\d+)"),
    "ccsd(t)": re.compile(r"CCSD\(T\) correlation energy[ \t]+=[ \t]+(-?\d+.\d+)"),
}
DEFAULT_ENERGY_EXPRESSION = ENERGY_EXPRESSIONS["default"]
ZERO = 1e-9


class QChemBackend(QMShellCommand):
    DEFAULT_TEMPLATE_PARAMS = {
        "template": """\
            ! Name: {name}
            $molecule
            {charge} {mult} 
            {geometry}
            $end

            $rem
                basis 6-31G
                method HF
                job_type sp
            $end
        """,
        "atom": "{symbol}    {x:.8f} {y:.8f} {z:.8f}",
        "ghost_atom": "@{symbol}    {x:.8f} {y:.8f} {z:.8f}",
    }
    RUN_CMD = "qchem"

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        # Configure the energy expression based on the template
        m = re.search(ENERGY_SEARCH_RE, self.template.template.lower(), re.MULTILINE)

        if m is None:
            self.prop_total_energy.patterns = [DEFAULT_ENERGY_EXPRESSION]
        else:
            self.prop_total_energy.patterns = [
                ENERGY_EXPRESSIONS.get(m[1], DEFAULT_ENERGY_EXPRESSION)
            ]
            if m[1] in CORRELATION_EXPRESSION:
                self.prop_total_correlation_energy.patterns = [
                    CORRELATION_EXPRESSION[m[1]]
                ]

    def setup_calc(self, ctx: RunContext):
        environ["QCSCRATCH"] = str(ctx.workpath)
        return super().setup_calc(ctx)

    @classmethod
    def is_available(cls):
        try:
            return (
                subprocess.run(
                    cls.RUN_CMD, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
                ).returncode
                == 255
            )
        except:
            return False

    def get_run_cmd(self, ctx: RunContext) -> None:
        return self.RUN_CMD, [
            "-nt",
            str(ctx.cores),
            ctx.files["input"],
            ctx.files["output"],
        ]

    @calc_property(source="re_file")  # Patterns configured at runtime
    def prop_total_energy(self, ctx: RunContext, m, _):
        return float(m[1])

    @calc_property(
        source="re_file", patterns=[r"Total [eE]nthalpy\s*:\s+(-?\d+.\d+)\s*"]
    )
    def prop_total_enthalpy(self, ctx: RunContext, m, _):
        """
        Properties from frequency calcualtions
        Enthalphy reported in kcal/mol
        """
        return float(m[1])

    @calc_property(
        source="re_file",
        patterns=[r"SCF +energy in the final basis set = +(-?\d+.\d+)"],
    )
    def prop_total_scf_energy(self, ctx: RunContext, m, _):
        return float(m[1])

    @calc_property(
        source="re_file",
        patterns=[r"DFT +Exchange +Energy = +(-?\d+.\d+)"],
    )
    def prop_dft_exchange(self, ctx: RunContext, m, _):
        res = float(m[1])
        if abs(res) > ZERO:
            return res

    @calc_property(
        source="re_file",
        patterns=[r"DFT +Correlation +Energy = +(-?\d+.\d+)"],
    )
    def prop_dft_correlation(self, ctx: RunContext, m, _):
        res = float(m[1])
        if abs(res) > ZERO:
            return res

    @calc_property(
        source="re_file",
        patterns=[r"Total +Coulomb +Energy = +(-?\d+.\d+)"],
    )
    def prop_total_coulomb_energy(self, ctx: RunContext, m, _):
        res = float(m[1])
        if abs(res) > ZERO:
            return res

    @calc_property(
        source="re_file",
        patterns=[
            r"HF +Exchange +Energy = +(-?\d+.\d+)",
            r"Alpha +Exchange +Energy = +(-?\d+.\d+)",
        ],
    )
    def prop_hf_exchange(self, ctx: RunContext, m, stream):
        if m[0].startswith("HF"):
            return float(m[1])
        if m[0].startswith("Alph"):
            alpha = float(m[1])
            beta_str = stream.readline()
            m2 = re.match(r" Beta +Exchange +Energy = +(-?\d+.\d+)", beta_str)
            if m2:
                return alpha + float(m2[1])

    @calc_property(source="re_file")
    def prop_total_correlation_energy(self, ctx: RunContext, m, _):
        return float(m[1])

    @calc_property(
        source="re_file",
        patterns=[r"Kinetic + Energy = +(-?\d+.\d+)"],
    )
    def prop_kinetic_energy(self, ctx: RunContext, m, _):
        res = float(m[1])
        if abs(res) > ZERO:
            return res

    @calc_property(
        source="re_file",
        patterns=[r"Nuclear Repulsion Energy = +(-?\d+.\d+)"],
    )
    def prop_nuclear_repulsion(self, ctx: RunContext, m, _):
        return float(m[1])

    @calc_property(
        source="re_file", patterns=[r"Nuclear Attr(actions|\.) + Energy = +(-?\d+.\d+)"]
    )
    def prop_nuclear_attraction(self, ctx: RunContext, m, _):
        res = float(m[2])
        if abs(res) > ZERO:
            return float(m[2])

    @calc_property(
        source="stream",
    )
    def prop_scf_gradient(self, ctx: RunContext, line: str, stream: TextIO):
        if line.strip() != "Gradient of SCF Energy":
            return

        # TODO: This will likely be spun off into it's own helper function
        atom_map = []
        atom_type_map = []
        for a in ctx.system.atoms:
            if a.type == AtomType.PHYSICAL:
                if a.id is None:
                    raise AttributeError("Atoms must be saved to form partial matrix.")
                atom_map.append(a.id)
                atom_type_map.append(a.type.value)
            elif a.type == AtomType.PROXY:
                try:
                    atom_map.append(a.meta["proxy_for"])
                    atom_type_map.append(a.type.value)
                except:
                    raise AttributeError(
                        "Proxy atoms must have `proxy_for` meta attribute set"
                    )
        grad = np.zeros((3, len(atom_map)), dtype=SCFGradient.dtype)

        # TODO: Do some math to pre-allocate our matrix
        uncollected = len(atom_map)
        while True:
            if uncollected <= 0:  # Break when we are done
                break
            idxs = [int(i) - 1 for i in stream.readline().split()]
            xs = [float(i) for i in stream.readline().split()[1:]]
            ys = [float(i) for i in stream.readline().split()[1:]]
            zs = [float(i) for i in stream.readline().split()[1:]]

            grad[0, idxs] += xs
            grad[1, idxs] += ys
            grad[2, idxs] += zs

            uncollected -= len(idxs)

        return SCFGradient(atom_map, atom_type_map, grad)

    @calc_property(
        source="re_file",
        patterns=[r"One-Electron +Energy = +(-?\d+.\d+)"],
    )
    def prop_one_electron_int(self, ctx: RunContext, m, _):
        return float(m[1])

    @calc_property(
        source="re_file", patterns=[r"Total [eE]ntropy\s*:\s+(-?\d+.\d+)\s*"]
    )
    def prop_total_entropy(self, ctx: RunContext, m, _):
        return float(m[1])

    @calc_property(source="re_file", patterns=[r"(\d+\.\d+)s\(cpu\)"])
    def prop_cpu_time(self, ctx: RunContext, m, _):
        return float(m[1])
