from fragment.backends.abstract import QMShellCommand


class XYZBackend(QMShellCommand):
    DEFAULT_TEMPLATE_PARAMS = {
        "template": """\
            {n_atoms}
            Fragment {name}; Charge={charge}; Multiplicity={mult}
            {geometry}
        """,
        "atom": "{symbol}    {x} {y} {z}",
        "ghost_atom": None,
    }
    FILE_MANIFEST = {
        "input": ".xyz",
    }
