from fragment.backends.abstract import QMBackend
from fragment.registry import REGISTRY


def get_backend(backend_name: str) -> QMBackend:
    return REGISTRY.get_from_namespace("backend", backend_name.lower())
