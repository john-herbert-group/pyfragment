from typing import Optional

from fragment.core.PIETree import PIETree
from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.systems.models import System, View


class SupersystemFragmenter(AbstractFragmenter):
    def __init__(self, *args, **kwargs) -> None:
        # Order is always 1
        super().__init__(*args, **kwargs)

    def generate_primary_fragments(
        self, supersystem: System, view: Optional[View] = None
    ) -> View:
        view = super().generate_primary_fragments(supersystem, view)
        view.fragments.add(supersystem)
        return view

    def generate_aux_fragments(self, primary_view: View, order=1, **kwargs):
        view = self.new_aux_view(primary_view, order=1)
        view.fragments.add(primary_view.supersystem)

        # Handle the dependency graph
        key = primary_view.supersystem._key
        tree = PIETree.from_primaries([key], add=True)
        view.dep_tree = tree
        return view
