from typing import Optional

from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.systems.models import System, View


class RawFragmenter(AbstractFragmenter):
    def generate_primary_fragments(
        self, system: System, view: Optional[View] = None
    ) -> View:
        p_frags: View = super().generate_primary_fragments(system, view)

        # Check that we have fragment data on all the atoms
        groups = set()
        for a in system.atoms:
            if not "frag_group" in a.meta:
                raise Exception("Atoms in system lack fragment group information")
            groups.add(a.meta["frag_group"])

        group_list = list(groups)
        group_list.sort()
        group_lookup = {g: i for i, g in enumerate(group_list)}
        fragments = [System() for _ in group_list]

        for a in system.atoms:
            fragments[group_lookup[a.meta["frag_group"]]].atoms.add(a)

        p_frags.fragments.add(*fragments)
        return p_frags
