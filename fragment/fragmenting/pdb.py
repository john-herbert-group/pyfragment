import logging
from typing import Optional

from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.systems.models import System, View

log = logging.getLogger(__name__)


class PDBFragmenter(AbstractFragmenter):
    """
    TODO: Add hetero-atom fragmentation scheme
    """

    def __init__(self, *args, window: int = 1, **kwargs) -> None:
        super().__init__(*args, window=window, **kwargs)
        self.window = window

    def generate_primary_fragments(
        self, system: System, view: Optional[View] = None
    ) -> View:
        p_frags = super().generate_primary_fragments(system, view)
        # Check that we have residue data on all the atoms
        for a in system.atoms:
            if not "frag_group" in a.meta:
                raise Exception("Atoms in system lack residue information")

        num_residues = max(*(a.meta["frag_group"] for a in system.atoms))
        residues = [
            [] for _ in range(num_residues)
        ]  # initial primary fragments (before window adjustment)
        for a in system.atoms:
            residues[a.meta["frag_group"] - 1].append(
                a
            )  # adds atoms of residue number to primary fragment

        if [] in residues:
            system_label = system.labels.first()
            logging.warning(f'PDB system "{system_label.name}" contains empty residues')

        # Combine into windowed fragments
        # TODO: Perform distance based globbing
        windowed_frags = [[] for _ in range((num_residues - self.window + 1))]
        for w in range(0, (num_residues - self.window + 1)):
            for x in range(self.window):
                windowed_frags[w] += residues[w + x]

        # Add primary fragments and celebrate!
        # NOTE: These are only added if the fragments are not empty
        p_frags.fragments.add(*(System(f) for f in windowed_frags if f))
        return p_frags
