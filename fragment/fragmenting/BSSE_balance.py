from itertools import combinations
from typing import Dict

from fragment.core.PIE_common import ROOT
from fragment.core.PIETree import PIETree, roots_overlap
from fragment.fragmenting.water import WaterFragmenter
from fragment.mods.abstract import FilterApplicator
from fragment.systems.common import AtomType
from fragment.systems.models import System, View


class BSSEBalancedFragmenter(WaterFragmenter):
    """
    BSSE Balanced Fragmenter

    MBE calculation done with each correction done in the sambe basis set.

    For example, all four-body corrections will include all terms in that
    four-body basis set.
    """

    def generate_aux_fragments(
        self, primary_view: View, order: int, app: FilterApplicator = None
    ) -> View:
        if not app.is_empty():
            raise Exception(f"{self.__class__.__name__} cannot be used with filters.")
        if roots_overlap([n.key for n in primary_view.fragments]):
            raise Exception(
                f"{self.__class__.__name__} cannot be used with overlapping fragments."
            )

        real_to_ghost = {
            a.id: a.convert_to(AtomType.GHOST) for a in primary_view.supersystem.atoms
        }

        # Make dep tree with monomer energies
        tree = PIETree.from_primaries(primary_view.fragments.sets, add=True)

        for o in range(2, order + 1):
            self.add_correction(tree, primary_view, o, real_to_ghost)

        # Create the actual view (WHY HASN'T THIS BEEN MERGED WITH PIE TREES)
        view = self.new_aux_view(primary_view, order=order)
        systems = []
        coefs = []

        ghosted_supersystem = System(
            list(primary_view.supersystem.atoms) + list(real_to_ghost.values())
        )
        for k, d in tree:
            systems.append(System(ghosted_supersystem.supersystem.atoms.filter(ids=k)))
            coefs.append(d["coef"])

        view.fragments.add(*systems, coefs=coefs)
        view.dep_tree = tree

        return view

    def add_correction(
        self, tree: PIETree, pfrags: View, order: int, real_to_ghost: Dict[int, System]
    ) -> None:
        for frags in combinations(pfrags.fragments, order):
            primaries = {f.key for f in frags}
            n_body = frozenset.union(*primaries)

            n_body_key = tree.new_node(n_body, 1)
            tree.tree.add_edge(ROOT, n_body_key)

            sub_tree = PIETree.from_MBE_primary_frags(
                [frozenset(p) for p in primaries], order - 1
            )

            ghosted_map = {}
            for k, d in sub_tree:
                nd = d["data"]
                coef = d["coef"]
                ghosted = nd.union({real_to_ghost[i].id for i in n_body.difference(nd)})
                ghosted_map[k] = tree.new_node(ghosted, coef=-1 * coef)

            # Add edges
            for u, v in sub_tree.tree.edges():
                if u == ROOT:
                    tree.tree.add_edge(n_body_key, ghosted_map[v])
                    continue
                tree.tree.add_edge(ghosted_map[u], ghosted_map[v])
