from itertools import combinations
from typing import Any, Set

import networkx as nx
from typing_extensions import Self

from fragment.core.PIE_common import ROOT
from fragment.core.PIETree import PIETree
from fragment.core.quickPIE import quickNode, quickNodeSet
from fragment.fragmenting.water import WaterFragmenter
from fragment.systems.common import AtomType
from fragment.systems.models import System, View


class GhostAwarePIETree(PIETree):
    ghost_set: Set[int]

    def __init__(self, *args: Any) -> None:
        self.ghost_set = set()
        super().__init__(*args)

    @classmethod
    def from_primatives(cls, primatives: quickNodeSet, ghosts: Set[int]) -> Self:
        _self = super().from_primatives(primatives)
        _self.ghost_set.update(ghosts)
        return _self

    def skip_add(self, k: quickNode, coef: int) -> bool:
        if k.issubset(self.ghost_set):
            return False
        return super().skip_add(k, coef)


class MBCPFragmenter(WaterFragmenter):
    """
    Many Body Counterpois Correction

    This should be used as a correction to a traditional MBE calculation. This code
    has not be verified with the GMBE.

    The MBCP is done by creating monomers/trimers/etc with a single fragment real
    monomer and the requeset number of ghost monomers and then a GMBE calculation is
    performed (fragments that only have ghost atoms are ignored). This method
    approximates the sum of differences between the monomer energies and the monomer
    calculations performedin the full-cluster basis set
    """

    def generate_aux_fragments(self, primary_view: View, order=2, **kwargs):
        real_to_ghost = {
            a.id: a.convert_to(AtomType.GHOST) for a in primary_view.supersystem.atoms
        }
        ghost_atom_ids = {a.id for a in real_to_ghost.values()}

        # Make the dep tree
        tree = PIETree.from_primaries([p.key for p in primary_view.fragments], add=True)

        # Create CP Ghosts
        for real_f in primary_view.fragments:
            sub_tree = GhostAwarePIETree.from_primatives(
                {real_f.key}, ghosts=ghost_atom_ids
            )

            for ghosts in combinations(primary_view.fragments, order - 1):
                # Don't work with fragments wich contain the real fragments
                skip = False
                for g in ghosts:
                    if real_f.key == g.key:
                        skip = True
                        break
                if skip:
                    continue

                # Now go through and do the MBCP
                _frag_data = set(real_f.key)
                for g in ghosts:
                    _frag_data.update((real_to_ghost[i].id for i in g.key))
                frag_data = frozenset(_frag_data)
                sub_tree.expand(frozenset(frag_data))

            # Add all the nodes!
            for k, d in sub_tree:
                # This may include the original key. Is this an issue?
                tree.new_node(k, -1 * d["coef"])

            for u, v in sub_tree.tree.edges():
                tree.tree.add_edge(u, v)
            tree.tree.remove_edge(ROOT, real_f.key)
            tree[real_f.key]["coef"] += 1

        # Create the actual view (WHY HASN'T THIS BEEN MERGED WITH PIE TREES)
        view = self.new_aux_view(primary_view, order=order)
        systems = []
        coefs = []

        ghosted_supersystem = System(
            list(primary_view.supersystem.atoms) + list(real_to_ghost.values())
        )
        for k, d in tree:
            systems.append(System(ghosted_supersystem.supersystem.atoms.filter(ids=k)))
            coefs.append(d["coef"])

        view.fragments.add(*systems, coefs=coefs)
        view.dep_tree = tree

        return view
