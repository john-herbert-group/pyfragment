from typing import Optional

import numpy as np

from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.systems.models import AtomType, System, View


class WaterFragmenter(AbstractFragmenter):
    def generate_primary_fragments(
        self, system: System, view: Optional[View] = None
    ) -> View:
        O_atoms = tuple(
            a for a in system.atoms.filter(type=AtomType.PHYSICAL) if a.t == "O"
        )
        H_atoms = tuple(
            a for a in system.atoms.filter(type=AtomType.PHYSICAL) if a.t == "H"
        )

        # TODO: Support non-OH atoms
        if 2 * len(O_atoms) != len(H_atoms) or len(O_atoms) + len(
            H_atoms
        ) != system.atoms.count(type=AtomType.PHYSICAL):
            raise Exception("The system does not contain a 1:2 O to H ratio.")

        view = super().generate_primary_fragments(system, view)
        # This could be improved with a KDTree but it's fine for now
        for O in O_atoms:
            _H = sorted(H_atoms, key=lambda x: np.linalg.norm(O.r - x.r))
            view.fragments.add(System([O, _H[0], _H[1]]))
        return view
