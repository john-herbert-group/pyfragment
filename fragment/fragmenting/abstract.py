from abc import ABC, abstractmethod
from itertools import combinations
from time import process_time
from typing import Callable, List, Optional

from fragment.conf.models import ConfigType
from fragment.conf.util import SavableConfig
from fragment.core.PIETree import PIETree, roots_overlap
from fragment.core.quickPIE import compress_up, quickNodeSet
from fragment.db.bootstrap import db
from fragment.mods.abstract import (
    AuxFragGenBase,
    FilterApplicator,
    PostAuxFragBase,
    PostPrimaryFragBase,
)
from fragment.mods.mixins import UnsupportedModError
from fragment.mods.models import Stack
from fragment.systems.models import System, View, ViewType


class AbstractFragmenter(SavableConfig, ABC):
    CONFIG_TYPE = ConfigType.FRAGMENTER
    SUPPORTED_MODS = (
        PostPrimaryFragBase,
        PostAuxFragBase,
        AuxFragGenBase,
    )

    def __init__(
        self,
        name,
        note,
        mods=None,
        combinator: Optional[str] = None,
        bu_missing: Optional[int] = 0,
        **kwargs,
    ) -> None:
        with db.atomic():
            self.mod_stack = Stack.from_names(*(mods or []))
            for m in self.mod_stack.mods:
                if not isinstance(m, self.SUPPORTED_MODS):
                    raise UnsupportedModError(
                        (
                            f"{self.__class__} does not support "
                            "mods of type {m.__class__}"
                        )
                    )

        self.name = name
        self.combinator = combinator
        self.bu_missing = bu_missing
        super().__init__(
            name,
            note,
            mods=mods,
            combinator=self.combinator,
            bu_missing=self.bu_missing,
            **kwargs,
        )

    @abstractmethod
    def generate_primary_fragments(
        self, system: System, view: Optional[View] = None
    ) -> View:
        """User-extendabnle function which creates primary fragments from the
        supersystem.

        The fragments generated in this function will be the basis for
        auxiliary fragment generation. Higher-level aggrigation (such
        as grouping primary fragments with nearest neighbors) should be
        done with mods.

        Args:
            system (:class:`~fragment.systems.models.System`): Superystem
            to be fragmented
            view (:class:`~fragment.systems.models.view`): View to fill with fragments

        Returns:
            View: Fragment subsystems with all view coefs set to 1
        """
        if view is None:
            view = self.new_primary_view(system)
        return view

    def new_primary_view(self, supersystem: System) -> View:
        """Returns new view configured for use as primary view

        Args:
            supersystem (:class:`~fragment.systems.models.System`):
                Supersystem to be fragmented

        Returns:
            View: Empty primary view
        """
        return View(
            name=f"{supersystem.labels[0].name}__{self.name}__primary",
            note=f"Auto generated primary view of system '{supersystem.labels[0].name}' using fragmenter {self.name}",
            type=ViewType.PRIMARY,
            order=0,
            supersystem=supersystem,
            fragmenter_id=self.conf_id,
        )

    def new_aux_view(self, primary_view, order) -> View:
        """Returns an view configured for use as an auxiliary view.

        Args:
            primary_view (:class:`~fragment.systems.models.View`):
                Primary view on which to base the new view
            order (int): Number of combinations which will be use

        Returns:
            View: Empty auxiliary view
        """
        supersystem: System = primary_view.supersystem
        return View(
            name=f"{supersystem.labels[0].name}__{self.name}__order-{order}",
            note=f"system '{supersystem.labels[0].name}' fragmented with '{self.name}' at order {order}",
            type=ViewType.AUXILIARY,
            order=order,
            parent=primary_view,
            fragmenter_id=self.conf_id,
            supersystem=supersystem,
        )

    def primary_fragments(self, system: System, view: Optional[View] = None) -> View:
        """Runs :meth:`.generate_primary_fragments` and applies mods

        This function generate the primary fragments (the smallest base systems
        in a view). Grouping of primary fragments should be left to mods or
        the :meth:`.generate_aux_fragments`.

        .. note::
            This method should not be overwritten when extending
            :class:`~fragment.fragmenting.abstract.AbstractFragmenter`
            to develop new fragmentation methods. Use
            :meth:`.generate_primary_fragments`

        Args:
            system (:class:`fragment.systems.models.System`): The system to be
                fragmented

        Returns:
            :class:`~fragment.systems.models.View`: View containing the primary
                fragments. All view coefs are equal to 1.
        """
        p_frags = self.generate_primary_fragments(system, view=view)
        applicator = PostPrimaryFragBase.get_applicator(self.mod_stack.mods, system)
        return applicator(p_frags)

    def aux_fragments(self, primary_view: View, order: int) -> View:
        """Runs :meth:`.generate_aux_fragments` and applies mods and filters

        Given a list of primary fragments, it creates larager systems using
        groups of primary fragments (goverend by `order`) and calculates
        the overlap between the superfragments.

        .. note::
            This method should not be overwritten when extending
            :class:`~fragment.fragmenting.abstract.AbstractFragmenter`
            to develop new fragmentation methods. Use
            :meth:`.generate_aux_fragments`

        Args:
            primary_view (:class:`~fragment.systems.models.View`): View
                containing the fragments to be grouped.
            order (int): The number of primary fragments to group when creating
                the primary fragments

        Returns:
            :class:`~fragment.systems.View`: View containing auxialiary
                fragments. All view coefs are weighted properly to satisfy
                the principle of inclusion/exclusion.
        """
        aux_filter = AuxFragGenBase.get_applicator(
            self.mod_stack.mods,
            primary_view.supersystem,
            primary_view,
            order,
            self.bu_missing,
        )
        mod_fn = PostAuxFragBase.get_applicator(
            self.mod_stack.mods, primary_view.supersystem, primary_view, self.bu_missing
        )

        aux_frags = self.generate_aux_fragments(primary_view, order, app=aux_filter)
        return mod_fn(aux_frags)

    def get_or_create_aux(self, system: System, order: int, save=False) -> View:
        try:
            return View.get(
                supersystem=system,
                fragmenter_id=self.conf_id,
                order=order,
                type=ViewType.AUXILIARY,
            )
        except View.DoesNotExist:
            return self.aux_from_system(system, order, save=save)

    def aux_from_system(self, system: System, order: int, save=False) -> View:
        """Convenience function which generates primary fragments and aux
        fragments in a single call.
        """
        p_frags = (
            View.select().where(
                View.supersystem == system,
                View.type == ViewType.PRIMARY,
                View.fragmenter == self.conf_id,
            )
        ).first()

        if p_frags is None:
            p_frags = self.primary_fragments(system)
            if save:
                p_frags.save()

        aux_frags = self.aux_fragments(p_frags, order)
        if save:
            aux_frags.save()
        return aux_frags

    #### AUX FRAGMENTS ####
    def generate_aux_fragments(
        self,
        primary_view: View,
        order: int,
        app: FilterApplicator = None,
    ) -> View:
        """Workhorse function which groups for generating aux fragments

        Args:
            primary_view (:class:`.View`): Primary view on which to base the
                primary fragments
            order (int): Fragment generation order
            app (FilterApplicator, optional): Filter function for aux fragment
                creation. Defaults to None.

        Raises:
            Exception: Raised when order is insuffiencet to create aux fragments
            Exception: Raised when atoms do not have a DB id.

        Returns:
            View: View containing auxiliary fragments with properly weighted
                view coeficients.
        """

        # Validate the inputs and make sure we can do this
        if len(primary_view.fragments) < order:
            raise Exception("The number of primary fragments is less than the order.")

        for f in primary_view.fragments:
            if f.key is None:
                raise Exception("Cannot perform fragmentation on un-instantiated atoms")

        if self.combinator is None:
            tree = self._auto_aux_fragments(primary_view, order, app)
        elif self.combinator == "top_down":
            tree = self._overlapping_aux_fragments(primary_view, order, app)
        elif self.combinator == "bottom_up":
            tree = self._bottom_up_aux_fragments(primary_view, order, app)
        elif self.combinator == "mbe":
            tree = self._quick_aux_fragments(primary_view, order)
        else:
            raise ValueError(f"Unknow fragmentation combinator '{self.combinator}'")

        # Validate
        if not tree.is_complete():
            raise ValueError("Fragmentation scheme does not reproduce the supersystem.")

        return self.aux_frags_from_tree(primary_view, tree, order)

    def aux_frags_from_tree(
        self, primary_view: View, tree: PIETree, order: int
    ) -> View:
        aux_frags = self.new_aux_view(primary_view, order)
        prim_frags = primary_view.fragments.sets
        coefs = []
        systems = []

        for node, data in tree:
            # Annotate with primary fragment information
            primaries = [p for p in prim_frags if data["data"].issuperset(p)]
            data["primaries"] = primaries
            data["order"] = len(primaries)

            # Extract that information to to build View
            systems.append(System(aux_frags.supersystem.atoms.filter(ids=node)))
            coefs.append(data["coef"])

        aux_frags.fragments.add(*systems, coefs=coefs)
        for f in aux_frags.fragments:
            f.viewcoef.order = tree[f.key]["order"]
        aux_frags.dep_tree = tree  # This needs to change

        return aux_frags

    def _auto_aux_fragments(
        self, p_view: View, order: int, app: FilterApplicator
    ) -> PIETree:
        if app.is_empty() and not roots_overlap(p_view.fragments.keys):
            tree = self._quick_aux_fragments(p_view, order)
        else:
            tree = self._overlapping_aux_fragments(p_view, order, app)
        return tree

    def _quick_aux_fragments(
        self,
        p_view: View,
        order: int,
    ) -> PIETree:
        return PIETree.from_MBE_primary_frags(p_view.fragments.sets, order)

    def _bottom_up_aux_fragments(
        self,
        p_view: View,
        order: int,
        app: FilterApplicator,
    ) -> PIETree:
        """Adds nodes layer by layer.

        Only fragments with all parents in the tree are allowed to be added.
        For example ab + ac + bc -> abc is allowed but ab + ac -!> abc
        """
        tree = PIETree.from_primaries(p_view.fragments.sets, add=True)
        # Congratulations! We have level one completed!

        if order != 1:
            # Add remaining levels
            self._add_bottom_up_level(p_view.fragments, tree, app, order, 2)
        return tree

    def _add_bottom_up_level(
        self,
        p_frags: List[System],
        tree: PIETree,
        filter: FilterApplicator,
        o: int,  # Target order
        m: int,  # The current level
    ) -> PIETree:
        # TODO: Find a more efficient way to do this
        _new_hl: quickNodeSet = set()
        new_ll_nodes = 1  # New low-level nodes
        new_hl_nodes = 1  # New high-level nodes
        MAX_ITER = 1
        # _M = max(m - 2, 0)
        _M = self.bu_missing
        # print(f"{m} -> {_M}")
        itr = 1

        while new_ll_nodes and new_hl_nodes and MAX_ITER >= itr:
            to_add: quickNodeSet = set()
            to_add_missing: quickNodeSet = set()

            # This is a brute force check. We can probably do this much more
            # efficiently with the Tree (finally, a use!)
            for com in combinations(p_frags, m):
                # Check that all parents exist in the tree. They don't have to have
                # non-zero coefs (?) but should be there
                mc = 0
                missing = set()
                hl_key = frozenset.union(*(c.key for c in com))
                if hl_key in _new_hl:
                    continue  # Don't duplicate work

                skip = False
                for children in combinations(com, m - 1):
                    pk = frozenset.union(*(c.key for c in children))
                    if not pk in tree:
                        mc += 1
                        missing.add(pk)
                        if mc > _M:
                            skip = True
                            # break

                if not skip and filter(*com):
                    to_add_missing.update(missing)
                    to_add.add(frozenset.union(*(c.key for c in com)))

            # Add the missing high-level terms and new keys
            prev_nodes = len(tree.tree)

            for k in to_add:
                tree.expand(k)

            new_hl_nodes = len(to_add)
            new_ll_nodes = len(tree.tree) - prev_nodes - new_hl_nodes
            _new_hl.update(to_add)  # Keep track of which HL nodes exist
            # print(
            #     f"itr={itr}\tm={m}\tnhl={new_hl_nodes}\tnll={new_ll_nodes}\tmll={len(to_add_missing)}"
            # )
            itr += 1

        # Just keep going until we have nothing left to add
        if m == o or len(_new_hl) == 0:
            return tree
        else:
            return self._add_bottom_up_level(p_frags, tree, filter, o, m + 1)

    def _overlapping_aux_fragments(
        self, p_view: View, order: int, app: FilterApplicator
    ) -> PIETree:
        # TODO: Implement without complete combos.
        # May be simpler and cheaper to check if fragment is in tree
        # Do the old reliable way :)
        tree = PIETree.from_primaries(p_view.fragments.sets)

        root_fragments = complete_combos(p_view.fragments, order, app)
        for k in compress_up(root_fragments):
            tree.expand(k)
        tree.clean_zeros()

        return tree


def complete_combos(
    p_frags: List[System],
    order: int,
    filter: Callable,
) -> quickNodeSet:
    fragments = set()

    for frags in combinations(p_frags, order):
        fragments.update(add_fragments(filter, *frags))

    return fragments


def add_fragments(filter: Callable, *comps: System) -> quickNodeSet:
    order = len(comps)
    key = frozenset.union(*(c.key for c in comps))

    if order == 1:
        return set((comps[0].key,))

    if filter(*comps):  # Keep if True
        return set((key,))

    return complete_combos(comps, order - 1, filter)
