from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.registry import REGISTRY


class UnknownFragmenter(Exception):
    pass


def get_fragmenter(fragmenter_name: str) -> AbstractFragmenter:
    return REGISTRY.get_from_namespace("fragmenter", fragmenter_name.lower())
