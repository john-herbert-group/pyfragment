from typing import Dict, Optional

from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.systems.models import System, View


class CompoundFragmenter(AbstractFragmenter):
    def __init__(
        self, *args, fragments: Optional[Dict[str, str]] = None, **kwargs
    ) -> None:
        super().__init__(*args, fragmenters=fragments, **kwargs)
        self.fragmenters = fragments or {}
        self._fragmenters_dict: Dict["str", AbstractFragmenter] = None

        self._default: AbstractFragmenter = None
        self._last: AbstractFragmenter = None

    def generate_primary_fragments(
        self, system: System, view: Optional[View] = None
    ) -> View:
        p_frags: View = super().generate_primary_fragments(system, view)

        fragmenters = self.get_fragmenters()

        sub_fragments = {}
        last_group = 0
        for a in system.atoms:
            if not "frag_group" in a.meta:
                raise Exception("Atoms in system lack fragment group information")
            fg = a.meta["frag_group"]
            if fg > last_group:
                last_group = fg
            try:
                sub_fragments[fg].atoms.add(a)
            except KeyError:
                sub_fragments[fg] = System(atoms=[a])

        if self._last:
            self._last.primary_fragments(sub_fragments[last_group], p_frags)
            del sub_fragments[last_group]

        for k, sys in sub_fragments.items():
            _fragmenter = self._fragmenters_dict.get(k, self._default)
            _fragmenter.primary_fragments(sys, p_frags)
        return p_frags

    def get_fragmenters(self) -> Dict[str, AbstractFragmenter]:
        """Idempotent function"""
        if self._fragmenters_dict:
            return self._fragmenters_dict

        self._fragmenters_dict = {}

        # Handle default
        d_name = self.fragmenters.get("default", "supersystem")
        self._default = AbstractFragmenter.obj_from_name(d_name)

        # Handle last
        l_name = self.fragmenters.get("last", None)
        if l_name:
            self._last = AbstractFragmenter.obj_from_name(l_name)

        for idx, f_name in self.fragmenters.items():
            try:
                int_idx = int(idx)
                self._fragmenters_dict[int_idx] = AbstractFragmenter.obj_from_name(
                    f_name
                )
            except ValueError:
                pass
