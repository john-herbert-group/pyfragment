from typing import List, Optional

import numpy as np
from peewee import Value, chunked

from fragment.backends.abstract import QMBackend
from fragment.calculations.models import Job, Layer
from fragment.core.legacy_PIETree import UnrepresentableException
from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.mods.models import Stack
from fragment.properties.core import (
    MASTER_PROP_LIST,
    AvailableProperty,
    PropertySet,
    add_property,
)
from fragment.systems.common import ViewType
from fragment.systems.models import System, SystemLabel, View


class JobPipeline:
    """A :class:`.JobPipeline` pipeline describes how properties are
    calculated for a :class:`~fragment.system.models.System`.

    This model combines a :class:`~fragment.backends.abstract.QMBackend`
    with a :class:`~fragment.mods.models.Stack` to represent the processes of
    capping and running the QM code.

    Arguments:
        backend: The name (from the strategy file)
            :class:`~fragment.backends.abstract.QMBackend`
        mod_stack (Optional[List[str]]): Optional list of
            :class:`~fragment.mods.abstract.ModBaseClass` names

    Attributes:
        backend (QMBackend): backend object
        mod_stack (Stack): Mod stack to be run before the
            :class:`~fragment.backends.abstract.QMBackend` is run

    Todo:
        * Replace with graph-based workflow

    """

    def __init__(self, backend: str, mod_stack: Optional[List[str]] = None) -> None:
        self.backend: QMBackend = QMBackend.obj_from_name(backend)

        mod_stack = mod_stack if mod_stack else []
        self.mod_stack: Stack = Stack.from_names(*mod_stack)

    def __hash__(self) -> int:
        return hash((self.backend.conf_id, self.mod_stack.id))

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}('{self.backend.name}', {self.mod_stack})"


class FragmentationPipeline:
    """A :class:`~fragment.core.query.FragmentationPipeline` describes all the steps
    required to properly fragment a :class:`fragment.systems.models.System`

    Arguments:
        fragmenter: Name of the
            :class:`~fragment.fragmenting.abstract.AbstractFragmenter`
        order:
            fragmentation order

    Attributes:
        fragmenter (AbstractFragmenter): instance of the abstract fragmenter to use
        order: fragmentation order
    """

    def __init__(self, fragmenter: str, order: int) -> None:
        self.fragmenter: AbstractFragmenter = AbstractFragmenter.obj_from_name(
            fragmenter
        )
        self.order: int = order

    def __hash__(self) -> int:
        return hash((self.fragmenter.conf_id, self.order))

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}('{self.fragmenter.name}', {self.order})"


def get_view(system_name: str, fpipe: FragmentationPipeline) -> View:
    """Returns a :obj:`~fragment.systems.models.View` matching a :class:`.FragmentationPipeline`

    Arguments:
        system_name: Name of the system (specified in the strategy file)
        fpipe: Pipeline describing how the system should be
            fragmented

    Returns:
        ~fragment.systems.models.View: View object from the database

    Raises:
        QueryError: A :class:`fragment.systems.View` matching the requirements does not exists in
            the database
    """
    return (
        View.select()
        .join(System)
        .join(SystemLabel)
        .where(
            SystemLabel.name == system_name,
            View.type == ViewType.AUXILIARY,
            View.fragmenter_id == fpipe.fragmenter.conf_id,
            View.order == fpipe.order,
        )
    ).first()


def annotate_view(view: View, jpipe: JobPipeline) -> None:
    """
    Annotates the view's :attr:`fragment.systems.models.View.dep_tree` with all
    available information matching the a :obj:`.JobPipeline`

    To work, all jobs must be completed and a
    :class:`~fragment.calculations.models.Layer` must exist in the database matching the view and the pipeline.

    Arguments:
        view: View to annotate
        jpipe: Job pipeline to use when querying for properties

    Raises:
        Exception: A set of calculations matching the job pipeline does not exist

    Examples:
        Assuming you have an existing job pipeline, :obj:`pipeline`, and view
        :obj:`view`, running this method will wllow the following::

            annotate_view(view, pipeline)

            # Now you can access properties associated with this pipeline e.g.
            view.dep_tree[(1,2,3)]['properties'][pipeline]['total_energy']
    """
    layer: Layer = (
        Layer.select()
        .where(
            Layer.view == view,
            Layer.backend__id == jpipe.backend.conf_id,
            Layer.mod_stack == jpipe.mod_stack,
        )
        .first()
    )
    if layer:
        jobs_query = layer.jobs.join(System).select(System._key, Job.properties_)

        for k, p in jobs_query.tuples():
            d = view.dep_tree[k]

            if "properties" not in d:
                d["properties"] = {}

            view.dep_tree[k]["properties"][jpipe] = PropertySet.from_dict(p)
    else:
        # Do the calculation in a more general way. May be slower
        jobs_query = (
            Job.select(System._key, Job.properties_)
            .where(
                Job.backend__id == jpipe.backend.conf_id,
                Job.mod_stack == jpipe.mod_stack,
            )
            .join(System)
        )

        for batch in chunked(view.dep_tree.tree.nodes, 500):
            res = jobs_query.where(
                System._key << [Value(k, unpack=False) for k in batch]
            )

            for k, p in res.tuples():
                d = view.dep_tree[k]

                if "properties" not in d:
                    d["properties"] = {}

                view.dep_tree[k]["properties"][jpipe] = PropertySet.from_dict(p)


def annotate_ceq_deltas(__view: View, __properties: Optional[List[str]] = None) -> None:
    """Calculates child-equivalent deltas for specific properties

    Arguments:
        __view (~fragment.systems.models.View): View to annotate
        __properties (List[str]): List of properties for which deltas should be calculated

    Returns:
        None: Method modifies :code:`view.dep_tree[key]['properties']`

    Suppose a simple system has a three node PIE tree containing AB, A, and B. In
    approximate terms any physical property of AB can be approximated as
    :math:`AB \\approx A + B` . This approximation is imperfect and that error can be
    denoted as

    .. math::
        \\delta AB = AB - A - B

    and thought of as how much new information is gained from doing the full AB
    calculation instead of the simpler A and B calculations.

    This method adds new delta properties to each node's 'property' attribute. This
    property is prefixed with "delta\\_". This method will annotate all existing
    pipelines. If a node cannot be represented by its children, the full value
    of the property will be used.

    Examples:

        Assuming a :obj:`view` with three nodes with keys (0,) , (1,), and (0, 1) and an
        already run and annotated pipeline :obj:`pipeline`, this method can be ran
        as::

            # Note the pipeline isn't specified
            annotate_ceq_deltas(view, ['total_energy'])

        This calculates a 'delta_total_energy' property for *all* pipelines::

            # Access new information (the interaction energy) of the (0,1) calc.
            d_total_energy = view[(0,1)]['properties'][pipeline]['delta_total_energy']

            # Check our work
            te0 = view[(0,)]['properties'][pipeline]['total_energy']
            te1 = view[(1,)]['properties'][pipeline]['total_energy']
            te01 = view[(0,1)]['properties'][pipeline]['total_energy']

            assert d_total_energy == te01 - te0 - te1  # True

        Any unrepresentable nodes have the full value::

            # Unrepresentable nodes use the full value
            total_energy = view[(0,)]['properties'][pipeline]['total_energy']
            d_total_energy = view[(0,)]['properties'][pipeline]['delta_total_energy']

            assert total_energy == d_total_energy  # True
    """

    if __properties is None:
        properties = [
            (p, "delta_" + p)
            for p, v in MASTER_PROP_LIST.items()
            if v.use_coef == True and not p.startswith("delta_")
        ]
    else:
        properties = [
            (p, "delta_" + p)
            for p in __properties
            if p in MASTER_PROP_LIST and not p.startswith("delta_")
        ]

    for prop, delta in properties:
        if delta in MASTER_PROP_LIST:
            continue
        add_property(
            name=delta,
            t=MASTER_PROP_LIST[prop].type,
            use_coef=False,
            help="Delta of `{}`".format(prop),
        )

    tree = __view.dep_tree

    for n, d in tree:
        try:
            child_eq = tree.child_equivalent(n)
            for backend_key, bp in d["properties"].items():
                for prop, delta in properties:
                    try:
                        val = bp[prop]
                    except KeyError:
                        continue

                    try:
                        for cn, cd in child_eq:
                            val -= (
                                tree[cn]["properties"][backend_key][prop] * cd["coef"]
                            )
                        bp.add_property(delta, val)
                    except KeyError:
                        bp.add_property(delta, np.NaN)

        except UnrepresentableException:
            for bp in d["properties"].values():
                for prop, delta in properties:
                    try:
                        bp.add_property(delta, bp[prop])
                    except KeyError:
                        pass
            continue
