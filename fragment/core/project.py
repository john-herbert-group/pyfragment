import bz2
import os
import shutil
import tarfile
from abc import ABC, abstractmethod
from functools import wraps
from os import chdir, getcwd
from pathlib import Path
from typing import Any, Callable, Dict, Iterable, List, Optional, Tuple

import peewee
from peewee import Database, DatabaseError, IntegrityError, fn
from playhouse.sqlite_ext import SqliteExtDatabase
from yaml import load

from fragment.conf.models import ConfigModel
from fragment.mods.models import Stack

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

import logging

import fragment.schemas.strategy as schema
from fragment.backends.abstract import QMBackend
from fragment.backends.common import Result, RunContext
from fragment.backends.util import get_backend
from fragment.calculations.common import JobStatus
from fragment.calculations.models import Calculation, Job, Layer, Worker
from fragment.db import migrations
from fragment.db.bootstrap import DATABASE_FILE_NAME, MEM_CONNECT_STR, bootstrap_db, db
from fragment.fragmenting.abstract import AbstractFragmenter
from fragment.fragmenting.util import get_fragmenter
from fragment.mods.util import get_mod
from fragment.systems.geometry_readers.util import (
    SystemDoesNotExist,
    SystemExists,
    read_geometry,
)
from fragment.systems.models import Bond, System, SystemLabel, View

log = logging.getLogger(__name__)


# Paths in file archive
SOURCE_PATH = "sources"
CALC_PATH = "qm_calcs"
FILE_ARCHIVE_NAME = "file_archive.tar"
COMPRESSED_ARCHIVE_NAME = "file_archive.tar.bz2"

# These are not in the file archive
SCRATCH_PATH = "scratch"
UPLOAD_PATH = "uploads"
LOG_PATH = "logs"


class DuplicateModelError(Exception):
    pass


class DatabaseManagerBase(ABC):
    context_db: Database
    db_options: Dict[Any, Any]

    def init(self, **db_options: Any):
        self.db_options = db_options
        pass

    def migrate(self, basepath: Path):
        with self:
            migrations.run_migrations(self.context_db, basepath)

    def fake_migrations(self):
        with self:
            migrations.fake_migrations(self.context_db)

    @abstractmethod
    def cleanup_lifecycle(self):
        self._cleaned = True

    @abstractmethod
    def start_session(self) -> "DatabaseManagerBase":
        pass

    @abstractmethod
    def cleanup_session(self):
        pass

    def __enter__(self) -> "DatabaseManagerBase":
        return self.start_session()

    def __exit__(self, type_, value, traceback):
        self.cleanup_session()

    def __call__(self, fn: Callable) -> Callable:
        @wraps(fn)
        def wrapper(*args, **kwargs):
            with self:
                with db.atomic():
                    return fn(*args, **kwargs)

        return wrapper

    def __del__(self):
        self.cleanup_lifecycle()
        if not hasattr(self, "_cleaned"):
            raise ResourceWarning(
                "LifeCycle not cleaned up. Be sure to call "
                "`.cleanup_lifecycle() when finished`"
            )


class FileDatabaseManager(DatabaseManagerBase):
    """Manages the lifecycle of a fragment job for a job stored
    on the filesystem

    Contains convenience methods for common operations
    """

    basepath: Path
    db_file: Path
    db_options: Dict[Any, Any]
    context_db: Database

    def __init__(
        self, basepath: Path, db_filename: str = None, **db_options: any
    ) -> None:
        self.basepath = Path(basepath)
        self.session_count = 0
        if db_filename is None:
            db_filename = DATABASE_FILE_NAME
        self.db_file = self.basepath / db_filename
        self.db_options = db_options

        # Rotate to a new database
        self.context_db = None
        if self.db_file.exists():
            self.bind_db()

    def bind_db(self):
        self.context_db = SqliteExtDatabase(self.db_file, **self.db_options)
        db.initialize(self.context_db)

    def init(self):
        if self.db_file.exists():
            raise FileExistsError("The database file already exists.")
        self.bind_db()

        # Create file and table without context check
        try:
            self.context_db.connect()
            bootstrap_db()
        finally:
            self.context_db.close()

        # Assume this DB has all previous migrations
        self.fake_migrations()

    def start_session(self):
        if self.session_count == 0:
            if self.context_db is None:
                raise DatabaseError(f"The database `{self.db_file}` does not exist")
            self.context_db.connect()
        self.session_count += 1
        return self

    def cleanup_session(self):
        self.session_count -= 1
        if self.session_count < 0:
            raise Exception("sessions count cannot be less than 0")
        if self.session_count == 0:
            # db.close()
            self.context_db.close()

    def cleanup_lifecycle(self):
        """Always close our db connection"""
        super().cleanup_lifecycle()
        if self.context_db:
            self.context_db.close()
        self.session_count = 0


class TransientDatabaseManager(FileDatabaseManager):
    def __init__(self, **db_options: Any) -> None:
        super(**db_options)
        self.context_db = SqliteExtDatabase(
            ":memory:",
        )
        # Pass this off to our model instances
        db.initialize(self.context_db)
        self.context_db.connect()
        self.__inited = False

    def init(self):
        if self.__inited:
            return

        with self:
            bootstrap_db()
            self.fake_migrations()
        self.__inited = True

    def start_session(self):
        pass

    def cleanup_session(self):
        pass

    def cleanup_lifecycle(self):
        super().cleanup_lifecycle()
        self.context_db.close()  # Destroy the DB


def with_dbm(fn: Callable) -> Callable:
    @wraps(fn)
    def wrapper(project: "Project", *args, **kwargs):
        if not isinstance(project, Project):
            raise ValueError("`dbm` decorator can only be used on Project methods")
        if not project.is_configured:
            raise ValueError("Project has not been initialized.")
        with project.dbm:
            # with project.dbm.context_db.atomic():
            return fn(project, *args, **kwargs)

    return wrapper


class Project:
    basepath: Path
    scratchpath: Path
    _file_archive: Path
    _compressed_archive: Path
    log_path: Path

    dbm: DatabaseManagerBase

    is_configured: bool
    is_compressed: bool

    def __init__(
        self,
        basepath: Optional[str] = None,
        db_file: Optional[str] = None,
        in_memory_db: bool = False,
        DBMClass: Optional[DatabaseManagerBase] = None,
    ) -> None:
        db_file = db_file or DATABASE_FILE_NAME
        self.configure_paths(basepath)
        self.configure_db(db_file, DBMClass, in_memory_db)

        # Configure to handle .tar.bz2 compressed file
        if self._file_archive.exists():
            has_archive = True
            self.is_compressed = False
        elif (self._compressed_archive).exists():
            has_archive = True
            self.is_compressed = True
        else:
            has_archive = False
            self.is_compressed = False

        self.is_configured = has_archive and self.is_configured

    def configure_paths(self, basepath: str) -> None:
        self.basepath = Path(basepath)

        self.scratchpath = self.basepath / SCRATCH_PATH
        self.uploadpath = self.basepath / UPLOAD_PATH
        self.log_path = self.basepath / LOG_PATH

        self._file_archive = self.basepath / FILE_ARCHIVE_NAME
        self._compressed_archive = self.basepath / COMPRESSED_ARCHIVE_NAME

    def configure_db(self, db_file: str, DBMClass, in_memory_db) -> None:
        if in_memory_db:
            self.db_file = MEM_CONNECT_STR
            self.dbm = TransientDatabaseManager()
            self.is_configured = True
        else:
            DBMClass = DBMClass or FileDatabaseManager
            self.db_file = self.basepath / db_file
            self.dbm = DBMClass(self.basepath, db_file)
            self.is_configured = self.db_file.exists()

    def init(self):
        self.basepath.mkdir(parents=True, exist_ok=True)
        self.scratchpath.mkdir(exist_ok=True)
        self.log_path.mkdir(exist_ok=True)
        self.dbm.init()

        # Make directories'
        with self.open_archive("x", force=True) as _:
            pass  # We just want an empty tar
        self.is_configured = True

    def cleanup(self):
        self.dbm.cleanup_lifecycle()

    # PROJECT SUMMARY AND REPORTING FUNCTIONS
    def jobs_summary(self, jobs_query=None) -> Dict[JobStatus, int]:
        counts = {s: 0 for s in JobStatus}
        if jobs_query is None:
            jobs_query = Job.select()

        # TODO: Add optional job filter
        stats = jobs_query.select(Job.status, fn.COUNT().alias("count")).group_by(
            Job.status
        )

        for s, c in stats.tuples():
            counts[s] = c
        return counts

    def calculations_summary(self) -> Tuple[int, int]:
        # Do this is the db_query?
        # TODO: Get calculation summary statistics
        completed = 0
        calcs: Iterable[Calculation] = Calculation.select()
        for calc in calcs:
            if not calc.jobs.where(Job.status != JobStatus.COMPLETED).exists():
                completed += 1

        return Calculation.select().count(), completed

    def systems_summary(self) -> int:
        return SystemLabel.select().count()

    @property
    def file_archive(self) -> Path:
        if self.is_compressed:
            return self._compressed_archive
        return self._file_archive

    def open_archive(self, mode="a", force=False) -> tarfile.TarFile:
        if not self.is_configured and not force:
            raise RuntimeError("Project has not been initialized.")
        if self.is_compressed:
            if mode == "r":  # We are allowed to read from .tar.bz2 files
                mode = "r:bz2"
            else:
                raise RuntimeError("Cannot write to compressed archive")
        return tarfile.open(self.file_archive, mode=mode)

    def consolodate_files(self, rm_uploads=False):
        """Consolodates files from uploads into file_archive.tar

        Arguments:
            file_archive (bool): Remove Uploads folder once done
        """

        # Don't consolodate if there are no uploads
        if not self.uploadpath.exists():
            return

        with self.open_archive("a") as arch:
            # Loop through uploads
            for dirname, dirnames, filenames in os.walk(self.uploadpath):
                cur_dir = Path(dirname)
                for fn in filenames:
                    _, ext = os.path.splitext(fn)
                    if ext == ".tar":
                        with tarfile.open(cur_dir / fn) as source_arch:
                            for tinfo in source_arch:
                                f = source_arch.extractfile(tinfo)
                                arch.addfile(tinfo, f)
        if rm_uploads:
            shutil.rmtree(self.uploadpath)

    def compress(self, chunk_size=100):
        """Compresses file archive to bz2 format

        Arguments:
            chunk_size (int): Size in MB
        """
        if self.is_compressed:
            return

        chunk_size *= 1000000
        # Method that transfers this file-by-file
        # with tarfile.open(self._file_archive, mode="r|*") as arch:
        #     with tarfile.open(self._compressed_archive, mode="w|bz2") as comp:
        #         for tinfo in arch:
        #             comp.addfile(tinfo, arch.extractfile(tinfo))

        # Handle files that haven't been bundled yet
        self.consolodate_files(True)  # Remove

        with self._file_archive.open("rb") as arch:
            with bz2.open(self._compressed_archive, "wb") as comp:
                while True:
                    res = arch.read(chunk_size)
                    if not res:
                        break
                    comp.write(res)

        self._file_archive.unlink()
        self.is_compressed = True

    def uncompress(self, chunk_size=100):
        """Uncompress file archive from bz2 format

        Arguments:
            chunk_size (int): Size in MB
        """

        chunk_size *= 1000000
        with bz2.open(self._compressed_archive, "rb") as comp:
            with self._file_archive.open("wb") as arch:
                while True:
                    res = comp.read(chunk_size)
                    if not res:
                        break
                    arch.write(res)

        self._compressed_archive.unlink()
        self.is_compressed = False

    # Job management

    @with_dbm
    def job_path(self, job: Job) -> Path:
        # Get job's supersystem id
        key = str(job.key)[:2]
        worker_id = job.worker_id or 0
        backend_id = job.backend__id
        mod_stack_id = job.mod_stack_id
        supersystem_id = str(job.system.supersystem.id)

        return (
            Path(CALC_PATH)
            / str(worker_id)
            / str(supersystem_id)
            / str(backend_id)
            / str(mod_stack_id)
            / key
        )

    @with_dbm
    def job_name(self, job: Job) -> str:
        return f"frag_{job.system_id}.{job.backend__id}.{job.mod_stack_id}"

    @with_dbm
    def contexts_batch(
        self,
        jobs: Iterable[Job],  # preferrebly a query
        worker: Optional[Worker] = None,
    ) -> List[RunContext]:
        if worker is None:
            worker_id = 0
            cores = 1
        else:
            worker_id = worker.id
            cores = worker.cores

        (
            Job.update(worker_id=worker_id, status=JobStatus.RUNNING).where(
                Job.id << [j.id for j in jobs]
            )
        ).execute()

        # Prefetch systems and atoms for the query
        if isinstance(jobs, peewee.BaseModelSelect):
            jobs = jobs.prefetch(System, ConfigModel, Stack)

        # We do not retrieve the updated jobs so update the local instance
        rctx = []
        for job in jobs:
            job.worker_id = worker_id
            job.status = JobStatus.RUNNING  # We are here so why not?
            job.system.atoms  # Preload atoms
            rctx.append(
                RunContext(
                    job.id,
                    self.job_name(job),
                    job.modded_system,
                    workpath=self.job_path(job),
                    backend_id=job.backend__id,
                    cores=cores,
                )
            )
        return rctx

    def get_run_context(
        self,
        job: Job,
        worker: Optional[Worker] = None,
    ) -> RunContext:
        return self.contexts_batch([job], worker=worker)[0]

    @with_dbm
    def archive_batch(
        self,
        results: Iterable[Result],
        archive_files: bool = True,
        delete_scratch=True,
        archive: Optional[tarfile.TarFile] = None,
    ):
        for res in results:
            self.update_job(res, archive_files)

        if archive_files:
            if archive is None:
                with self.open_archive("a") as arch:
                    self.archive_files(results, arch, delete_scratch)
            else:
                self.archive_files(results, archive, delete_scratch)

    def update_job(self, res: Result, files_collected: bool):
        props = (
            res.properties
            if isinstance(res.properties, Dict)
            else res.properties.to_dict()
        )
        (
            Job.update(
                {
                    Job.status: res.status,
                    Job.start_time: res.start_time,
                    Job.end_time: res.end_time,
                    Job.properties_: props,
                    Job.files_collected: files_collected,
                }
            ).where(Job.id == res.id)
        ).execute()

    def archive_files(
        self, results: Iterable[Result], archive: tarfile.TarFile, delete_scratch=True
    ):
        for res in results:
            if not res.files:
                continue
            job_rel_path = str(res.workpath)
            rel_start = job_rel_path.find(CALC_PATH)
            job_rel_path = job_rel_path[rel_start:]
            archive.add(res.workpath, job_rel_path)
            if delete_scratch:
                shutil.rmtree(res.workpath)

    @with_dbm
    def archive_result(self, res: Result, **kwargs) -> None:
        self.archive_batch([res], **kwargs)

    @with_dbm
    def run_job(
        self,
        job: Job,
        worker: Optional[Worker] = None,
        backend: Optional[QMBackend] = None,
        archive: Optional[tarfile.TarFile] = None,
        delete_scratch: bool = True,
        rerun: bool = False,
    ) -> Result:
        if job.status != JobStatus.PENDING and not rerun:
            return

        ctx = self.get_run_context(job, worker)
        ctx.workpath = self.scratchpath / ctx.workpath

        if backend is None:
            backend = job.backend

        res = backend.run_ctx(ctx)
        self.archive_result(res, archive=archive, delete_scratch=delete_scratch)
        return res

    @with_dbm
    def run_layer(
        self, layer: Layer, worker: Optional[Worker] = None, delete_scratch: bool = True
    ):
        backend = layer.backend

        with self.open_archive("a") as arch:
            for job in Job.exclude_complete(layer.jobs):
                self.run_job(
                    job,
                    worker=worker,
                    backend=backend,
                    archive=arch,
                    delete_scratch=delete_scratch,
                )
        layer.properties
        layer.save()

    @with_dbm
    def run_calc(
        self,
        calc_name: str,
        worker: Optional[Worker] = None,
        delete_scratch: bool = True,
    ):
        calc: Calculation = Calculation.get(name=calc_name)

        with self.open_archive("a") as arch:
            backend = None
            for job in Job.exclude_complete(calc.jobs):
                # Cache the backend
                if backend is None or backend.conf_id != job.backend__id:
                    backend = job.backend

                self.run_job(
                    job,
                    worker=worker,
                    backend=backend,
                    archive=arch,
                    delete_scratch=delete_scratch,
                )

        calc.properties
        calc.save()

    # Top level functions
    @with_dbm
    def add_system(
        self, input_file: str, name: str, note: str, charges: Optional[Dict[int, int]]
    ) -> System:
        file = Path(input_file)
        ext = file.suffix[1:]

        with file.open("r") as f:
            try:
                system = read_geometry(name, note, source=f, ext=ext, charges=charges)
            except IntegrityError as e:
                raise DuplicateModelError(f"System with name '{name}' already exists!")

        Bond.mk_bond_graph(system)

        archive_path = f"{SOURCE_PATH}/{name}.{ext}"
        with self.open_archive() as tf:
            tf.add(file, arcname=archive_path)
        return system

    @with_dbm
    def add_calculations(self, calcs: List[schema.CalculationModel]):
        # Select the Systems we will be working with
        system_names = set()
        for c in calcs:
            if isinstance(c.system, list):
                system_names.update(set(c.system))
            else:
                system_names.add(c.system)
        sys_query = System.select(System, SystemLabel.name.alias("name")).join(
            SystemLabel
        )
        if "ALL" not in system_names:
            sys_query = sys_query.where(SystemLabel.name.in_(system_names))
        all_systems = {s.systemlabel.name: s for s in sys_query}

        # Check that we have all the requested system names
        missing_s = []
        for sn in system_names:
            if sn == "ALL":
                continue
            if sn not in all_systems:
                missing_s.append(sn)
        if missing_s:
            raise SystemDoesNotExist(f"Reference to unknown system(s): {missing_s}")

        # Select the backends used in these calculations
        backends: Dict[str, QMBackend] = {}
        fragmenters: Dict[str, AbstractFragmenter] = {}
        views: Dict[Tuple[str, str, int], View] = {}

        for c in calcs:
            # Handle views
            systems: List[str]
            if isinstance(c.system, list):  # View is a list of systems
                systems = c.system
            elif c.system == "ALL":  # Apply to all Systems
                systems = [s for s in all_systems.keys()]
            else:
                systems = [c.system]

            for s in systems:
                system_name = all_systems[s].systemlabel.name
                calc_name = c.name + "__" + system_name

                # If a calculation with the same name exists, quit
                if Calculation.select().where(Calculation.name == calc_name):
                    continue

                # Start a new calculation
                calc = Calculation(
                    name=calc_name, note=c.note + " with system " + system_name
                )
                for l in c.layers:
                    b = l.backend
                    if b not in backends:
                        backend = QMBackend.obj_from_name(b)
                        backends[b] = backend
                    else:
                        backend = backends[b]

                    f = l.view.fragmenter
                    if f not in fragmenters:
                        fragmenter: AbstractFragmenter = (
                            AbstractFragmenter.obj_from_name(f)
                        )
                        fragmenters[f] = fragmenter
                    else:
                        fragmenter = fragmenters[f]

                    # Fragment the system
                    view_key = (s, l.view.fragmenter, l.view.order)
                    if view_key not in views:
                        view = fragmenter.get_or_create_aux(
                            all_systems[s], l.view.order, save=True
                        )
                        views[view_key] = view
                    else:
                        view = views[view_key]

                    # Create the layer and jobs
                    layer, _ = Layer.get_or_create(
                        view=view,
                        mod_stack=Stack.from_names(*l.mods if l.mods else []),
                        backend_=backend.conf_id,
                    )
                    calc.add_layer(layer)
                calc.save()

    @with_dbm
    def add_strategy_file(self, file_path: str):
        # Get the current directory since all paths are relative to the
        # strategy file
        file = Path(file_path)
        if not file.exists():
            raise ValueError(f"The file '{file_path}' does not exist")
        try:
            current_path = getcwd()
        except:
            current_path = None

        try:
            chdir(file.parent)
            with file.open("r") as f:
                data = load(f, Loader=Loader)
            strategy = schema.Strategy(**data)

            for mod in strategy.mods or []:
                get_mod(mod.mod_name).get_or_create(
                    mod.name,
                    mod.note,
                    save_config=True,
                    **mod.dict(
                        exclude={"mod_name", "name", "note"}, exclude_unset=True
                    ),
                )
            for sys in strategy.systems or []:
                try:
                    self.add_system(sys.source, sys.name, sys.note, sys.charges)
                except SystemExists:
                    pass
            for frag in strategy.fragmenters or []:
                get_fragmenter(frag.fragmenter).get_or_create(
                    frag.name,
                    frag.note,
                    save_config=True,
                    **frag.dict(
                        exclude={"fragmenter", "name", "note"}, exclude_unset=True
                    ),
                )
            for backend in strategy.backends or []:
                get_backend(backend.program).get_or_create(
                    backend.name,
                    backend.note,
                    save_config=True,
                    **backend.dict(
                        exclude={"program", "name", "note"}, exclude_unset=True
                    ),
                )
            if strategy.calculations:
                self.add_calculations(strategy.calculations)
        finally:
            if current_path:
                chdir(current_path)
