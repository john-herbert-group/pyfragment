from functools import cached_property
from typing import Callable


#### CACHING ####
def reset_cache(obj: object, keep: Callable = None):
    """
    Removes all cached values so they can be recalculated
    """
    ObjClass = obj.__class__
    to_delete = []
    for k in obj.__dict__.keys():
        attr = getattr(ObjClass, k, None)
        if isinstance(attr, cached_property):
            if not (keep is None) and keep(k):
                continue
            to_delete.append(k)

    for k in to_delete:
        delattr(obj, k)


def invalidate_cache(method):
    """
    Removes all cached values so they can be recalculated
    """

    def wrapper(_self, *args, **kwargs):
        reset_cache(_self)
        return method(_self, *args, **kwargs)

    return wrapper


#### VALIDATORS ####
class ValidationError(Exception):
    pass


def validate_slug(value):
    for c in value:
        c: str
        if c.isalnum():
            continue
        if c in "-_.":
            continue
        raise ValidationError(
            f"Name {value} is not a valid slug. Should contain only letters, numbers, '-', '_', and '.'."
        )
