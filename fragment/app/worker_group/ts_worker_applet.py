import asyncio
from typing import IO, Optional

from fragment.app.worker_group.common import WorkerAppletBase


class TaskSpoolerWorker(WorkerAppletBase):
    def __init__(
        self,
        worker_host: str,
        worker_port: Optional[int] = 22,
        provisioning_script: Optional[str] = None,
        cores: Optional[int] = None,
    ):
        super().__init__(cores)
        self.worker_host = worker_host
        self.worker_port = worker_port
        self.provisioning_script = provisioning_script or ""

    async def spawn_worker_proc(self, log_stream: IO) -> asyncio.subprocess.Process:
        # Setup the ssh command
        ssh_cmd = [
            "ssh",
            "-p",
            str(self.worker_port),
            self.worker_host,
        ]

        # Setup the fragment command
        fragment_cmd = (
            f"ts fragment "
            f"--cores {self.cores} "
            f" worker "
            f"--server-host {self.server_host} "
            f"--server-port {self.server_port} "
        )

        # Now combine the two :)
        ssh_cmd.append(self.provisioning_script + "\n" + fragment_cmd)

        return await asyncio.create_subprocess_exec(
            *ssh_cmd,
            stdout=log_stream,
            stderr=log_stream,
            # start_new_session=True
        )
