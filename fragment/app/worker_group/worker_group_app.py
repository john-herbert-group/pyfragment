import logging
from os import path
from pathlib import Path
from typing import List

import uvloop
from async_collie import App
from async_collie.resources import Provides

from fragment.app.worker_group.common import WorkerAppletBase

log = logging.getLogger(__name__)


class WorkerGroupApp(App):
    server_host: str = Provides()
    server_port: int = Provides()
    log_level: str = Provides()
    worker_log_path: Path = Provides()

    def __init__(
        self,
        server_host: str,
        server_port: str,
        basepath: str,
        workers: List[WorkerAppletBase],
        log_level: str = None,
    ) -> None:
        super().__init__()

        self.server_host = server_host
        self.server_port = server_port
        self.basepath = Path(basepath).absolute()
        self.workers = workers
        self.worker_log_path = self.basepath / "logs" / "workers"
        self.log_level = log_level.upper() if log_level else "INFO"

    def run(self):
        uvloop.install()
        return super().run()

    async def create_applets(self):
        for d, worker in enumerate(self.workers):
            worker.startup_delay = d * 0.5
            self.add_applet(worker)
