import asyncio
from typing import IO

from fragment.app.worker_group.common import WorkerAppletBase


class LocalWorker(WorkerAppletBase):
    async def spawn_worker_proc(self, log: IO):
        args = [
            "--log-level",
            self.log_level,
            "--cores",
            str(self.cores),
            "worker",
            "--server-host",
            self.server_host,
            "--server-port",
            str(self.server_port),
        ]

        return await asyncio.create_subprocess_exec(
            "fragment", *args, stdout=log, stderr=log
        )
