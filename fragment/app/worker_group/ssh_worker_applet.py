import asyncio
import re
from typing import IO, Optional

from fragment.app.worker_group.common import WorkerAppletBase


class SSHWorker(WorkerAppletBase):
    PID_FIND = re.compile(r"PID: (\d+)$")

    def __init__(
        self,
        worker_host: str,
        worker_port: Optional[int] = 22,
        provisioning_script: Optional[str] = None,
        proxy_host: Optional[str] = "localhost",
        proxy_port: Optional[int] = 9000,  # This needs to be carefully handled
        cores: Optional[int] = None,
    ):
        super().__init__(cores)
        self.worker_host = worker_host
        self.worker_port = worker_port
        self.provisioning_script = provisioning_script or ""
        self.proxy_host = proxy_host
        self.proxy_port = proxy_port

    async def spawn_worker_proc(self, log_stream: IO) -> asyncio.subprocess.Process:
        # Setup the ssh command
        ssh_cmd = [
            "ssh",
            "-R",
            f"{self.proxy_port}:{self.proxy_host}:{self.server_port}",
            "-p",
            str(self.worker_port),
            self.worker_host,
        ]

        # Setup the fragment command
        fragment_cmd = (
            f"fragment --cores {self.cores} worker "
            f"--server-host {self.proxy_host} "
            f"--server-port {self.proxy_port} "
        )

        # Now combine the two :)
        ssh_cmd.append(self.provisioning_script + "\n" + fragment_cmd)

        return await asyncio.create_subprocess_exec(
            *ssh_cmd,
            stdout=log_stream,
            stderr=log_stream,
            # start_new_session=True
        )

    async def terminate_proc(self):
        with open(self.worker_log_file, "r") as lf:
            m = None
            for l in lf:
                m = re.search(self.PID_FIND, l)
                if m:
                    break

            if not m:
                await super().terminate_proc()  # I'm sorry it had to come to this
                return

            # "Now kill it from afar"
            kill_pid = await asyncio.create_subprocess_exec(
                "ssh",
                "-p",
                str(self.worker_port),
                self.worker_host,
                f"kill {m[1]}",
                stdout=asyncio.subprocess.DEVNULL,
                stderr=asyncio.subprocess.DEVNULL,
            )
            await kill_pid.wait()
