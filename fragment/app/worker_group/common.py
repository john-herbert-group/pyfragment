import asyncio
import logging
from functools import cached_property
from pathlib import Path
from typing import IO, Optional
from uuid import uuid4

import async_collie as ac

log = logging.getLogger(__name__)


class WorkerAppletBase(ac.Applet):
    server_host: str = ac.Requires()
    server_port: int = ac.Requires()
    worker_log_path: Path = ac.Requires()
    log_level: str = ac.Requires()

    proc: asyncio.subprocess.Process = None

    def __init__(
        self,
        cores: Optional[int] = None,
        work_path: Optional[str] = None,
        shared_path: Optional[bool] = None,
    ) -> None:
        rand_name = self.__class__.__name__ + "-" + str(uuid4()).split("-")[0]
        super().__init__(rand_name, loop=False)
        self.cores = cores
        self.startup_delay = 0
        self.work_path = work_path
        self.shared_path = shared_path

    async def main(self):
        await asyncio.sleep(self.startup_delay)
        self.worker_log_path.mkdir(parents=True, exist_ok=True)
        with self.worker_log_file.open("w") as lf:
            self.proc = await self.spawn_worker_proc(lf)
            log.info(f"{self.name} started with a " f"PID {self.proc.pid} 👷⛏")
            await self.proc.wait()

    @cached_property
    def worker_log_file(self) -> Path:
        # Make the log files in squential order
        log_file_id = 0
        for f in self.worker_log_path.iterdir():
            try:
                log_num = int(f.name.split("-")[0])
            except ValueError:
                log_num = 0
            if log_num >= log_file_id:
                log_file_id = log_num + 1

        return self.worker_log_path / (str(f"{log_file_id:03d}-") + self.name + ".log")

    async def spawn_worker_proc(self, log: IO) -> asyncio.subprocess.Process:
        return

    async def soft_stop(self):
        await super().soft_stop()

        if self.proc and self.proc.returncode is None:
            await self.terminate_proc()
            try:
                await ac.DeadmanSwitch.run_with_timeout(self.proc.wait(), timeout=10)
            except TimeoutError:
                pass  # Move to the hard stop

    async def terminate_proc(self):
        self.proc.terminate()

    async def cleanup(self):
        await super().cleanup()

        if self.proc is None:
            pass  # The worker was never started
        elif self.proc.returncode is None:
            log.warning(f"{self.name} was force down 🙅")
            self.proc.kill()
        elif self.proc.returncode != 0:
            log.error(f"{self.name} crashed: see {self.worker_log_file} 🤯")
            self.app_soft_stop.set()
        else:
            log.info(f"{self.name} has shutdown and gone home ‍️👷🏠")
