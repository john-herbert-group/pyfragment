import asyncio
import logging
import tarfile
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from typing import List

import async_collie as ac

import fragment.schemas.calculation as dbs
from fragment.app.worker.applet_common import HTTPXApplet
from fragment.schemas.calculation import BatchResult

log = logging.getLogger(__name__)


def bundle(workpath: Path, archive: Path, files: List[Path]) -> Path:
    archive = workpath / "uploads" / archive
    if archive.exists():
        return archive  # Just in this is a resubmission
    archive.parent.mkdir(parents=True, exist_ok=True)
    with tarfile.open(archive, "x") as arch:
        for f in files:
            arch.add(f, f.relative_to(workpath))

    return archive


class ReportApplet(HTTPXApplet):
    # REQUIRES
    workpath: Path = ac.Requires()
    worker_id: int = ac.Requires()

    # PROVIDES
    results_queue: asyncio.Queue = ac.Provides()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, loop=True, **kwargs)

    async def create_provides(self) -> None:
        await super().create_provides()
        self.results_queue = asyncio.Queue(maxsize=2)
        self.pool = ThreadPoolExecutor()

    async def cleanup_provides(self) -> None:
        await super().cleanup_provides()
        self.pool.shutdown()

    async def main(self):
        results_batch, start_time, end_time = await self.results_queue.get()

        res = [dbs.Result.from_orm(r) for r in results_batch]
        data = BatchResult(results=res, start_time=start_time, end_time=end_time)

        log.info(f"Reporting {len(res)} jobs")

        # Upload files first so the server doesn't shut down early
        to_bundle = [r.workpath for r in results_batch if r.files]
        if to_bundle:
            archive_name = "worker_arch-{}.jobs{}-{}.tar".format(
                self.worker_id, results_batch[0].id, results_batch[-1].id
            )
            archive = await asyncio.get_running_loop().run_in_executor(
                self.pool, bundle, self.workpath, Path(archive_name), to_bundle
            )

            # This part should be async but aiofile does not mesh with httpx
            # TODO: Add worker thread to handle this correctly
            with archive.open("rb") as f:
                await self.post(
                    "/calculation/upload_archive", files={"archive": (archive_name, f)}
                )

        # Report the actual job as done now that the files are uploaded
        try:
            await self.post("/calculation/report_work", data=data.json())
        except:
            # Try to report this later
            await self.results_queue.put((results_batch, start_time, end_time))

        self.results_queue.task_done()

    async def soft_stop(self):
        await super().soft_stop()
        await self.results_queue.join()
