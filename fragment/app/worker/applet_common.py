import logging
from time import time
from typing import Callable

import async_collie as ac
import httpx

log = logging.getLogger(__name__)


class HTTPXApplet(ac.Applet):
    # REQUIREMENTS
    client: httpx.AsyncClient = ac.Requires()

    async def _http_call(
        self, method: Callable, endpoint: str, *args, **kwargs
    ) -> httpx.Response:
        s = time()
        try:
            return await method(endpoint, *args, **kwargs)
        finally:
            t = time() - s
            timeout = min(self.client.timeout.as_dict().values())
            msg = f"{method.__name__}/'{endpoint}' took {t:.5f} s/{timeout} s"
            if t > 0.8 * timeout:
                log.warning(msg)
            else:
                log.debug(msg)

    async def get(self, enpoint: str, *args, **kwargs) -> httpx.Response:
        return await self._http_call(self.client.get, enpoint, *args, **kwargs)

    async def post(self, enpoint: str, *args, **kwargs) -> httpx.Response:
        return await self._http_call(self.client.post, enpoint, *args, **kwargs)

    async def put(self, enpoint: str, *args, **kwargs) -> httpx.Response:
        return await self._http_call(self.client.put, enpoint, *args, **kwargs)
