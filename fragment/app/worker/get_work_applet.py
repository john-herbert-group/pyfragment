import asyncio
import logging
from pathlib import Path
from typing import Dict, Set

import async_collie as ac

import fragment.schemas.db as dbs
from fragment.backends.abstract import QMBackend
from fragment.backends.common import RunContext
from fragment.conf.models import ConfigModel
from fragment.schemas.calculation import Batch
from fragment.systems.models import Atom, System

from .applet_common import HTTPXApplet

log = logging.getLogger(__name__)


class GetWorkApplet(HTTPXApplet):
    # REQUIREMENTS
    cores: int = ac.Requires()
    exec_queue: asyncio.Queue = ac.Requires()

    # PROVIDES
    backend_ids: Set[int] = ac.Provides()
    backends: Dict[int, QMBackend] = ac.Provides()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, loop=True, **kwargs)

    async def create_provides(self) -> None:
        await super().create_provides()
        self.backend_ids = set()
        self.backends = dict()

    async def main(self) -> None:
        try:
            resp = await self.get("/worker/next_task")
        except Exception:
            await asyncio.sleep(1)
            return

        if resp.status_code != 200:
            await self.event(ac.Events.HALT)
            self.soft_stop_event.set()
            return
        next_task = resp.json()["next"]

        if next_task == "halt":
            await self.event(ac.Events.HALT)
            self.soft_stop_event.set()
            return
        elif next_task == "sleep":
            log.warning("SLEEPING FOR 1 s")
            await asyncio.sleep(1)
            return
        elif next_task != "run_jobs":
            raise Exception(f"Unknown task `{next_task}`")

        resp = await self.get(
            "/calculation/get_work",
        )

        batch = Batch(**resp.json())
        if batch.jobs:
            log.info(f"Got {len(batch.jobs)} jobs")
        else:
            return
        backend_ids = {j.backend_id for j in batch.jobs}
        for bid in backend_ids.difference(self.backend_ids):
            await self.get_backend(bid)

        # TODO: Add proper DB Systems and Atoms
        ctx_batch = []
        for j in batch.jobs:
            system = self.get_system(j.system)
            workpath = Path(j.workpath)
            ctx = RunContext(
                **j.dict(exclude={"workpath", "system"}),
                system=system,
                cores=self.cores,
                workpath=workpath,
            )
            ctx_batch.append(ctx)
        await self.exec_queue.put(ctx_batch)
        await self.exec_queue.join()  # There should only be one job in the queue

    async def get_backend(self, bid: int) -> QMBackend:
        resp = await self.get("/models/config/" + str(bid))
        conf_data = dbs.ConfigModel(**resp.json())

        conf = ConfigModel(**conf_data.dict())
        backend = QMBackend.obj_from_conf(conf)

        self.backend_ids.add(backend.conf_id)
        self.backends[backend.conf_id] = backend
        return backend

    def get_system(self, system_data: dbs.System) -> System:
        atoms = [Atom(**a.dict()) for a in system_data.atoms]
        # System ID excluded. This is a moded system
        return System(atoms, **system_data.dict(exclude={"atoms", "id"}))
