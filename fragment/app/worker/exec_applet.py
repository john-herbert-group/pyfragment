import asyncio
import logging
from concurrent.futures import ProcessPoolExecutor
from pathlib import Path
from time import time
from typing import Dict, List

import async_collie as ac

from fragment.backends.abstract import QMBackend
from fragment.backends.common import Result, RunContext
from fragment.calculations.common import JobStatus

log = logging.getLogger(__name__)


def run_ctx_batch(
    ctx_batch: List[RunContext], backends: Dict[int, QMBackend], workpath: str
) -> List[Result]:
    backend: QMBackend = None
    results: List[Result] = []
    workpath = Path(workpath)

    for ctx in ctx_batch:
        if ctx.backend_id != backend:
            backend = backends[ctx.backend_id]
        if not backend.is_fileless:
            ctx.workpath = workpath / ctx.workpath

        try:
            results.append(backend.run_ctx(ctx))
        except:
            results.append(Result(id=ctx.id, name=ctx.name, status=JobStatus.FAILED))
    return results


class ExecApplet(ac.Applet):
    # REQUIRES
    backends: Dict[int, QMBackend] = ac.Requires()
    workpath: Path = ac.Requires()
    results_queue: asyncio.Queue = ac.Requires()

    # PROVIDES
    exec_queue: asyncio.Queue = ac.Provides()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, loop=True, **kwargs)

    async def create_provides(self) -> None:
        await super().create_provides()
        self.exec_queue = asyncio.Queue(maxsize=2)
        self.pool = ProcessPoolExecutor()

    async def cleanup_provides(self) -> None:
        await super().cleanup_provides()
        self.pool.shutdown()

    async def main(self):
        ctx_batch: List[RunContext] = await self.exec_queue.get()

        start_time = time()
        results = await asyncio.get_running_loop().run_in_executor(
            self.pool,
            run_ctx_batch,
            ctx_batch,
            self.backends,  # Copy or a reference?
            self.workpath,
        )

        await self.results_queue.put(
            (
                results,
                start_time,
                time(),
            )
        )
        log.debug(f"{self.results_queue.qsize()} batches in queue")

        # Mark this job as done so we can properly shut down
        self.exec_queue.task_done()

    async def soft_stop(self):
        await super().soft_stop()
        await self.exec_queue.join()
