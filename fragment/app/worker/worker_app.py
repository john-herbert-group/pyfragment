import logging
import os
from datetime import datetime
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Optional

import async_collie as ac
import httpx

from fragment.app.worker.applet_common import HTTPXApplet
from fragment.app.worker.exec_applet import ExecApplet
from fragment.app.worker.get_work_applet import GetWorkApplet
from fragment.app.worker.report_work_applet import ReportApplet
from fragment.calculations.models import Worker
from fragment.schemas.workers import ServerConfig, ServerStatus, WorkerAssignment

from .heartbeat_applet import HeartbeatApplet

log = logging.getLogger(__name__)


class WorkerApp(ac.App, HTTPXApplet):
    client: httpx.AsyncClient = ac.Provides()
    worker_id: int = ac.Provides()
    config: ServerConfig = ac.Provides()
    cores: int = ac.Provides()
    workpath: Path = ac.Provides()

    hostname: str
    port: int
    server_config: ServerStatus

    def __init__(
        self,
        server_hostname: str,
        server_port: int,
        cores: int = 1,
        workerpath: Optional[str] = None,
        **kwargs,
    ) -> None:
        super().__init__(**kwargs)
        self.server_hostname = server_hostname
        self.server_port = server_port

        if workerpath:
            self.tmpdir = None
            self.workpath = Path(workerpath)
        else:
            self.tmpdir = TemporaryDirectory()
            self.workpath = Path(self.tmpdir.name)

        self.cores = cores
        self.hostname = Worker.get_hostname()
        self.info = Worker.get_info()

        # Configure for setup
        self.client = None
        self.config = None

    async def create_provides(self) -> None:
        await super().create_provides()

        log.info("CONNECTING TO SERVER:")
        log.info(f"  SERVER HOSTNAME: {self.server_hostname}")
        log.info(f"  SERVER PORT: {self.server_port}")

        if self.client is None:
            self.client = httpx.AsyncClient(
                base_url=f"http://{self.server_hostname}:{self.server_port}/",
                timeout=10,
            )

        resp = await self.post(
            "/worker/register",
            json={"cores": self.cores, "hostname": self.hostname, "info": self.info},
        )
        if resp.status_code != 200:
            self.app_soft_stop.set()
            return

        # Configure the worker based on what the server has to say
        reg = WorkerAssignment(**resp.json())
        self.config = reg.server_config
        self.worker_id = reg.id
        self.client.headers["X-Worker-ID"] = str(self.worker_id)

        log.info("WORKER REGISTERED:")
        log.info(f"  NAME: {reg.name}")
        log.info(f"  ID: {self.worker_id}")
        log.info(f"  HOSTNAME: {self.hostname}")
        log.info(f"  START TIME: {datetime.now()}")
        log.info(f"  NUM CORES: {self.cores}")
        log.info(f"  PID: {os.getpid()}")
        log.info(f"  WORKING DIR: {self.workpath}")

    async def cleanup_provides(self) -> None:
        await super().cleanup_provides()
        if isinstance(self.worker_id, int) and self.worker_id:
            # Don't clean up if we never contatected the  server
            try:
                await self.post("/worker/deregister")
            except httpx.ConnectError:
                pass
        if self.tmpdir:
            self.tmpdir.cleanup()
        await self.client.aclose()

    async def cleanup(self) -> None:
        log.info(f"Worker has shutdown 😴")
        return await super().cleanup()

    async def create_applets(self) -> None:
        await super().create_applets()
        self.add_applet(HeartbeatApplet("heartbeat", critical=True))
        self.add_applet(
            GetWorkApplet(
                "get_work",
                critical=True,
                depends_on=["exec"],
            )
        )
        self.add_applet(ExecApplet("exec", critical=True, depends_on=["report"]))
        self.add_applet(ReportApplet("report", critical=True))
