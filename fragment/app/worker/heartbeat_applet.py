import asyncio
import logging

import async_collie as ac

from fragment.app.worker.applet_common import HTTPXApplet
from fragment.schemas.workers import ServerConfig

log = logging.getLogger(__name__)


class HeartbeatApplet(HTTPXApplet):
    config: ServerConfig = ac.Requires()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, loop=True, **kwargs)
        self.skips = 0  # Internal skips counter

    async def main(self) -> None:
        await asyncio.sleep(self.config.heartbeat_freq)
        try:
            await self.status_check()
            self.skips = 0
        except asyncio.CancelledError as e:
            raise e
        except Exception as e:
            print(e)
            log.error(f"Heartbeat error. {e}")
            self.skips += 1
            log.warning(
                f"Missed {self.skips} of {self.config.heartbeat_skips} " "heartbeats 💔"
            )

            if self.config.heartbeat_skips <= self.skips:
                await self.event(ac.Events.HALT)
            else:
                log.debug(
                    f"Missed {self.config.heartbeat_skips} heartbeats. Stopping 💔🛑"
                )

    async def status_check(self) -> None:
        resp = await self.post("/worker/check_in")
        if resp.status_code != 200:
            raise Exception("There was an error in during the heartbeat")

        if resp.json()["halt"]:
            log.info("Recieved shutdown signal from from heartbeat")
            await self.event(ac.Events.HALT)
