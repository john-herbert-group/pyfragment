import logging
from pathlib import Path
from socket import gethostname

import async_collie as ac
import httpx
import uvloop

from fragment.app.server.common import get_server_conf_path, write_server_conf
from fragment.app.server.status_watcher_applet import StatusWatcherApplet
from fragment.app.server.uvicorn_applet import UvicornApplet
from fragment.schemas.workers import ServerConfig

log = logging.getLogger(__name__)


class ServerApp(ac.App):
    server_host: str = ac.Provides()
    server_port: int = ac.Provides()
    config: ServerConfig = ac.Provides()
    client: httpx.AsyncClient = ac.Provides()

    def __init__(
        self,
        server_host: str,
        server_port: str,
        basepath: str,
        log_level: str = None,
        uvicorn_log_level: str = None,
        **kwargs,
    ) -> None:
        super().__init__()

        self.server_host = server_host
        self.server_port = server_port

        self.config = ServerConfig(
            hostname=gethostname(),
            bind_to=self.server_host,
            port=server_port,
            basepath=str(Path(basepath).absolute()),
            log_level=log_level.upper() if log_level else "INFO",
            uvicorn_log_level=(
                uvicorn_log_level.upper() if uvicorn_log_level else "INFO"
            ),
            **kwargs,
        )
        self.config_record_path = get_server_conf_path(self.config.basepath)

    def run(self):
        uvloop.install()
        return super().run()

    async def create_provides(self) -> None:
        await super().create_provides()
        self.client = httpx.AsyncClient(
            base_url=f"http://{self.server_host}:{self.server_port}/"
        )
        write_server_conf(self.config.basepath, self.config)

    async def cleanup_provides(self) -> None:
        await super().cleanup_provides()
        await self.client.aclose()
        self.config_record_path.unlink(missing_ok=True)

    async def create_applets(self):
        # self.add_applet(DaphneApplet("server", critical=True))
        self.add_applet(UvicornApplet("server", critical=True))
        self.add_applet(
            StatusWatcherApplet("status", critical=True, depends_on=["server"])
        )
