import asyncio
import logging
from os import environ
from typing import Any, Dict

import async_collie as ac
import httpx

from fragment.app.server.common import server_shutdown, server_status
from fragment.schemas.workers import ServerConfig

log = logging.getLogger(__name__)


class DaphneApplet(ac.Applet):
    server_up: asyncio.Event = ac.Provides()
    server_down: asyncio.Event = ac.Provides()

    server_host: str = ac.Requires()
    server_port: int = ac.Requires()
    config: ServerConfig = ac.Requires()
    client: httpx.AsyncClient = ac.Requires()

    async def create_provides(self) -> None:
        await super().create_provides()
        self.server_up = asyncio.Event()
        self.server_down = asyncio.Event()

    async def main(self):
        self.server_proc = await self.spawn_server_proc()
        log.info(f"Server configured with PID {self.server_proc.pid} ⚙️")

        try:
            await ac.DeadmanSwitch.run_with_timeout(
                self.await_server_start(), self.config.server_startup_timeout
            )
        except TimeoutError:
            log.error(
                f"The server could not be reached for {self.startup_timeout} "
                "so I'm gonna shut everything down 🛑💾"
            )
            await self.event(ac.Events.HALT)

        await self.server_proc.wait()

    async def spawn_server_proc(self) -> asyncio.subprocess.Process:
        verbosity = {"warning": 0, "info": 1, "debug": 2}[
            self.config.uvicorn_log_level.lower()
        ]
        return await asyncio.create_subprocess_exec(
            "daphne",
            "-b",
            self.server_host,
            "-p",
            str(self.server_port),
            "-v",
            str(verbosity),
            "-s",
            "FRAGMENT",
            "fragment.app.api.main:app",
            env={
                "PATH": environ["PATH"],
                "FRAGMENT_BASEPATH": self.config.basepath,
                "FRAGMENT_CONFIG": self.config.json(exclude={"basepath"}),
            },
        )

    async def await_server_start(self) -> Dict[str, Any]:
        log.info(f"Waiting for start on {self.server_host}:{self.server_port} ⏳")
        while True:
            await asyncio.sleep(0.1)

            # Check the server to make sure it's not crashed
            if not self.server_proc.returncode is None:
                log.error(f"The server crashed 💥")
                await self.event(ac.Events.HALT)
                return

            try:
                await server_status(self.client)
                break
            except httpx.ConnectError:
                pass

        log.info(
            "The server is up and listing on "
            f"{self.server_host}:{self.server_port} 👂"
        )
        self.server_up.set()

    # ===========================================================================
    #                            SHUTDOWN CODE
    # ===========================================================================

    async def soft_stop(self):
        await super().soft_stop()
        try:
            await server_shutdown(self.client)
            await self.await_0_workers(self.config.server_shutdown_timeout)
        except httpx.ConnectError:
            pass

        if self.server_proc.returncode is None:
            self.server_proc.terminate()
            try:
                await ac.DeadmanSwitch.run_with_timeout(self.server_proc.wait(), 5)
            except TimeoutError:
                pass  # Proceed to the hard shutdown

    async def await_0_workers(self, timeout=10):
        shutdown_timer = ac.DeadmanSwitch(timeout)
        while True:
            await asyncio.sleep(0.5)
            try:
                workers = (await server_status(self.client)).active_workers
                shutdown_timer.check()
            except httpx.ConnectError:
                return
            except TimeoutError:
                return

            if workers == 0:
                return

    async def cleanup(self):
        # For the love of god don't let the server zombie on
        if self.server_proc.returncode is None:
            log.warning("Bringing the server down hard 🍃")
            self.server_proc.kill()
        else:
            log.info("The server has stoped ✋")
        self.server_down.set()
