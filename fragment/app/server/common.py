import json
import logging
import socket
from lib2to3.pytree import BasePattern
from pathlib import Path

import httpx

from fragment.calculations.common import JobStatus
from fragment.schemas.workers import ServerConfig, ServerState, ServerStatus

log = logging.getLogger(__name__)


def get_port() -> int:
    try:
        s = socket.socket()
        s.bind(("", 0))
        return s.getsockname()[1]
    finally:
        s.close()


def get_server_conf_path(base_path: Path):
    return Path(base_path) / ".server_conf.json"


def write_server_conf(base_path: Path, config: ServerConfig) -> None:
    scp = get_server_conf_path(base_path)
    with scp.open("w") as f:
        json.dump(config.dict(), f, indent=2, sort_keys=True)


def read_server_conf(base_path: Path) -> ServerConfig:
    scp = get_server_conf_path(base_path)
    with scp.open("r") as f:
        return ServerConfig(**json.load(f))


async def server_status(client: httpx.AsyncClient) -> ServerStatus:
    status_data = await client.get("/status")
    status_data.raise_for_status()
    return ServerStatus(**status_data.json())


async def server_state(client: httpx.AsyncClient) -> ServerState:
    status_data = await client.get("/status/state")
    status_data.raise_for_status()
    return ServerState(**status_data.json())


async def server_shutdown(client: httpx.AsyncClient) -> ServerStatus:
    status_data = await client.post("/status/halt")
    status_data.raise_for_status()
    return ServerStatus(**status_data.json())


def server_status_sync(host: str, port: int) -> ServerStatus:
    status_data = httpx.get(f"http://{host}:{port}/status")
    status_data.raise_for_status()
    return ServerStatus(**status_data.json())


def server_state_sync(host: str, port: int) -> ServerState:
    states_data = httpx.get(f"http://{host}:{port}/status/state")
    states_data.raise_for_status()
    return ServerState(**states_data.json())


def server_halt_sync(host: str, port: int) -> ServerStatus:
    status_data = httpx.post(f"http://{host}:{port}/status/halt")
    status_data.raise_for_status()
    return ServerStatus(**status_data.json())


def print_status_sync(host: str, port: int) -> None:
    try:
        status = server_status_sync(host, port)
    except Exception as e:
        log.info(f"Could not connect to server at {host}:{port}")
        exit()

    _print_status(status)


async def print_status(client: httpx.AsyncClient) -> None:
    try:
        status = await server_status(client)
    except:
        log.info(f"Could not connect to server at {client.base_url}")

    _print_status(status)


def _print_status(status: ServerStatus) -> None:
    log.info(f"Batch Size: {status.batch_size}")
    log.info(
        f"Jobs (reported/prepaired): {status.jobs_reported}/{status.jobs_prepaired}"
    )
    log.info(f"Batch Queue Size: {status.batch_queue_items}")
    log.info(f"Report Queue Size: {status.report_queue_items}")
    log.info(f"DB Use Queue: {status.async_queue_length}")
    log.info(f"Work Done: {status.work_done}")
    log.info("Jobs Status:")
    log.info(f"  PENDING: {status.jobs[JobStatus.PENDING]}")
    log.info(f"  RUNNING: {status.jobs[JobStatus.RUNNING]}")
    log.info(f"  COMPLETED: {status.jobs[JobStatus.COMPLETED]}")
    log.info(f"  FAILED: {status.jobs[JobStatus.FAILED]}")

    if status.workers:
        log.info(f"Workers ({len(status.workers)}):")
        for w in status.workers:
            log.info(
                f'  {w.id}: "{w.hostname}" ({w.reported}/{w.got}) last seen {w.last_seen}'
            )
