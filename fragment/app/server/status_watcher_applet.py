import asyncio
import logging
from time import time

import async_collie as ac
import httpx

from fragment.app.server.common import server_state
from fragment.app.worker.applet_common import HTTPXApplet
from fragment.schemas.workers import ServerConfig, ServerState

log = logging.getLogger(__name__)


class StatusWatcherApplet(HTTPXApplet):
    client: httpx.AsyncClient = ac.Requires()
    config: ServerConfig = ac.Requires()
    server_up: asyncio.Event = ac.Requires()
    server_down: asyncio.Event = ac.Requires()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, loop=True, **kwargs)

    async def configure(self) -> None:
        await super().configure()
        await self.server_up.wait()
        self.unreachable_switch = ac.DeadmanSwitch(
            self.config.server_unreachable_timeout
        )
        self.workerless_switch = ac.DeadmanSwitch(self.config.workerless_timeout)

    async def main(self):
        await asyncio.sleep(self.config.status_poll_freq)

        if self.server_down.is_set():
            raise ac.ExitMainLoop()

        try:
            s = time()
            state = await server_state(self.client)
            log.debug(
                "{} with {} workers. Query took {:.5f} s".format(
                    "Wrapping up work" if state.work_done else "Still working",
                    state.active_workers,
                    time() - s,
                )
            )
            self.unreachable_switch.press()
        except httpx.ConnectError:
            await self.handle_unreachable()
            return
        except httpx.TimeoutException:
            await self.handle_unreachable()
            return

        await self.check_workforce(state)
        await self.check_workload(state)

    async def handle_unreachable(self):
        # Check for server timeouts
        try:
            self.unreachable_switch.check()
        except TimeoutError:
            log.error(
                f"Server has been unresponsive for "
                f"{self.config.server_unreachable_timeout}. Shutting down ⌛️"
            )
            await self.event(ac.Events.HALT)
            return

    async def check_workload(self, state: ServerState):
        if state.work_done and state.jobs_prepaired == state.jobs_reported:
            log.info("🎉 All jobs are finished. Shutting down 🎉")
            await self.event(ac.Events.HALT)
        return

    async def check_workforce(self, state: ServerState):
        if state.active_workers != 0:
            self.workerless_switch.press()
            return

        try:
            self.workerless_switch.check()
        except TimeoutError:
            await self.event(ac.Events.HALT)
            log.info(
                f"No workers seen in {self.config.workerless_timeout} s "
                "so I'm gonna stop!️ 👀"
            )
