import asyncio
import json
import logging
import threading
from collections import deque
from concurrent.futures.thread import ThreadPoolExecutor
from datetime import datetime, timedelta
from functools import partial, wraps
from math import ceil
from os import environ
from time import time
from typing import Awaitable, Callable, Deque, Dict, Iterable, List, Optional, Tuple

import peewee
from playhouse.sqlite_ext import SqliteExtDatabase

import fragment.backends.common as qm
from fragment.backends.common import Result, RunContext
from fragment.calculations.common import JobStatus
from fragment.calculations.models import Job, Worker
from fragment.core.project import FileDatabaseManager, Project
from fragment.schemas.calculation import Batch, BatchResult
from fragment.schemas.workers import Registration, ServerConfig

log = logging.getLogger(__name__)


def async_dbm(fn: Callable) -> Awaitable:
    @wraps(fn)
    async def async_wrapper(proj: "AsyncProject", *args, **kwargs):
        return await proj.run_in_pool(fn, proj, *args, **kwargs)

    return async_wrapper


class ThreadCentricDB(SqliteExtDatabase):
    def init(self, database, pragmas=None, timeout=5, **kwargs):
        self.thread_name = threading.current_thread().name
        return super().init(database, pragmas, timeout, **kwargs)

    def execute_sql(self, sql, *args, **kwargs):
        if self.thread_name != threading.current_thread().name:
            raise RuntimeError(
                f"This database belongs to the thread '{self.thread_name}' and cannot be accessed by '{threading.current_thread().name}'"
            )
        return super().execute_sql(sql, *args, **kwargs)


class ThreadedFDBM(FileDatabaseManager):
    thread_name: str

    def __init__(self, *args, **kwargs) -> None:
        self.thread_name = threading.current_thread().name
        super().__init__(*args, **kwargs)
        self.session_count += 1

    def check_thread(self):
        ct = threading.current_thread()
        if ct.name != self.thread_name:
            raise RuntimeError(
                f"This database belongs to the thread '{self.thread_name}' and cannot be accessed by '{ct.name}'"
            )

    def bind_db(self):
        self.check_thread()
        self.context_db = ThreadCentricDB(self.db_file, **self.db_options)
        from fragment.db.bootstrap import db

        db.initialize(self.context_db)

    def start_session(self):
        self.check_thread()
        return super().start_session()


class AsyncProject(Project):
    config: ServerConfig
    workers: Dict[int, Worker]

    # STATISTICS
    jobs_prepaired: int
    jobs_reported: int
    batch_sizes: Deque[int]
    exec_times: Deque[int]
    archive_times: Deque[int]

    # ASYNC CONSTRUCTS
    stop: asyncio.Event
    batch_queue: asyncio.Queue
    report_queue: asyncio.Queue

    job_batcher_task: asyncio.Task
    job_archiver_task: asyncio.Task

    def __init__(
        self, *args, config: Optional[Dict] = None, DBMClass=None, **kwargs
    ) -> None:
        dbm_class = DBMClass or ThreadedFDBM
        self.pool = ThreadPoolExecutor(1, thread_name_prefix="DATABASE")

        # Do the initialization in a different thread
        super().__init__(*args, DBMClass=dbm_class, **kwargs)

        self.config = ServerConfig(basepath=str(self.basepath), **config)

        self.workers = dict()
        self.timeout_delta = timedelta(
            0, self.config.heartbeat_freq * self.config.heartbeat_skips, 0
        )
        self.uploadpath.mkdir(exist_ok=True, parents=True)

        self.batch_sizes = deque([], 10)
        self.exec_times = deque([], 10)
        self.archive_times = deque([], 10)
        self.jobs_prepaired = 0
        self.jobs_reported = 0
        self.pool_queue = 0

    async def async_init(self) -> None:
        # Create queues and coroutines for getting work, reporting work
        self.stop = asyncio.Event()
        self.report_queue = asyncio.Queue()
        self.batch_queue = asyncio.Queue(
            maxsize=5  # Cap the max number of batches so we don't over-prep
        )

        loop = asyncio.get_running_loop()
        self.job_batcher_task: asyncio.Task = loop.create_task(
            self.job_batcher(Job.exclude_complete(Job.select()), self.batch_queue)
        )
        self.job_archiver_task: asyncio.Task = loop.create_task(
            self.job_archiver(self.report_queue)
        )

    async def async_cleanup(self) -> None:
        # Stop making more archive batches
        self.stop.set()

        # Shut down the batcher
        if not self.job_batcher_task.done():
            self.job_batcher_task.cancel()
            try:
                await self.job_batcher_task
            except asyncio.CancelledError:
                pass

        # Shut down the archiver
        try:
            await asyncio.wait_for(self.report_queue.join(), 5)
        except asyncio.TimeoutError:
            # Do a hard kill without clearing the queue
            log.error("Report queue could not clear before shutdown")

        if not self.job_archiver_task.done():
            self.job_archiver_task.cancel()
            try:
                await self.job_archiver_task
            except asyncio.CancelledError:
                pass

        # Mark unreported jobs as running
        await self.run_in_pool(
            Job.update(status=JobStatus.PENDING)
            .where(Job.status == JobStatus.RUNNING)
            .execute
        )

    def work_done(self) -> bool:
        return any(
            [
                self.stop.is_set(),
                self.batch_queue.qsize() == 0 and self.job_archiver_task.done(),
                self.job_batcher_task.done()
                and self.jobs_reported == self.jobs_prepaired,
            ]
        )

    async def job_batcher(
        self, job_query: peewee.SelectQuery, batch_queue: asyncio.Queue
    ) -> None:
        while not self.stop.is_set():
            # Get a bunch of jobs and mark them as running
            jobs: List[Job] = await self.run_in_pool(
                list, job_query.select().limit(self.config.batch_size)
            )

            if not jobs:
                return

            # Mark job as running so we don't query the jobs twice
            await self.run_in_pool(
                Job.update(status=JobStatus.RUNNING)
                .where(Job.id << [j.id for j in jobs])
                .execute
            )

            rctx = [await self.run_in_pool(self.get_run_context, j) for j in jobs]
            await batch_queue.put(Batch(jobs=sorted(rctx, key=lambda x: x.backend_id)))
            self.jobs_prepaired += len(rctx)
            log.debug(
                f"ADDED {len(rctx)} JOB(S) TO THE QUEUE (SIZE {batch_queue.qsize()})"
            )

    async def job_archiver(self, report_queue: asyncio.Queue) -> None:
        while True:
            b: BatchResult = await report_queue.get()
            run_time = (b.end_time - b.start_time).total_seconds()
            archive_time = 0.0
            contains_failed = False

            for _r in b.results:
                if _r.status == JobStatus.FAILED:
                    contains_failed = True
                r = qm.Result(**_r.dict())
                s = time()
                await self.run_in_pool(self.update_job, r, False)
                archive_time += time() - s
            self.jobs_reported += len(b.results)

            if not contains_failed:
                self.update_batch_size(archive_time, run_time, len(b.results))

            log.debug(f"ARCHIVED {len(b.results)} JOB(S) (SIZE {report_queue.qsize()})")
            report_queue.task_done()

    def get_run_context(self, job: Job, worker: Optional[Worker] = None) -> RunContext:
        job.system.atoms  # Preload atoms
        return RunContext(
            job.id,
            self.job_name(job),
            job.modded_system,
            workpath=self.job_path(job),
            backend_id=job.backend__id,
            cores=0,  # Worker will decide this
        )

    def configure_db(self, db_file: str, DBMClass, in_memory_db) -> None:
        DBMClass = DBMClass or FileDatabaseManager
        self.db_file = self.basepath / db_file
        db_init = self.pool.submit(DBMClass, self.basepath, db_file)
        self.dbm = db_init.result()
        self.is_configured = self.db_file.exists()

    @async_dbm
    def init(self):
        return super().init()

    async def run_in_pool(self, fn: Callable, *args, **kwargs):
        to_call = partial(fn, *args, **kwargs)
        self.pool_queue += 1  # Accounting tool to see how things go
        s = time()
        res = await asyncio.get_running_loop().run_in_executor(self.pool, to_call)
        self.pool_queue -= 1
        return res

    # WORKER MANAGEMENT CODE
    async def add_worker(self, reg: Registration) -> Worker:
        worker = Worker(cores=reg.cores, hostname=reg.hostname, info=reg.info)
        worker.got = 0
        worker.reported = 0
        await self.run_in_pool(worker.save)
        self.workers[worker.id] = worker
        return worker

    def get_worker(self, worker_id: int) -> Worker:
        return self.workers[worker_id]

    async def checkin_worker(self, worker_id: int, commit=False) -> Worker:
        worker = self.get_worker(worker_id)
        worker.last_seen = datetime.now()
        worker.active = True
        if commit:
            await self.run_in_pool(worker.save)
        return worker

    async def deregister_worker(self, worker_id: int) -> None:
        worker = self.get_worker(worker_id)
        worker.active = False
        await self.run_in_pool(worker.save)
        del self.workers[worker_id]

    def active_workers(self) -> Iterable[Worker]:
        time_cuttoff = datetime.now() - self.timeout_delta
        for worker in self.workers.values():
            if worker.last_seen > time_cuttoff:
                yield worker
            else:
                worker.active = False

    def num_active_workers(self) -> int:
        return sum((1 for _ in self.active_workers()))

    # def init(self):
    #     raise NotImplementedError("Method not implemented for async")

    async def cleanup(self):
        for w in self.workers.values():
            await self.run_in_pool(w.save)
        await self.run_in_pool(self.dbm.cleanup_lifecycle)

    # PROJECT SUMMARY AND REPORTING FUNCTIONS
    @async_dbm
    def jobs_summary(self, jobs_query=None) -> Dict[JobStatus, int]:
        return super().jobs_summary(jobs_query)

    def calculations_summary(self) -> Tuple[int, int]:
        raise NotImplementedError("Method not implemented for async")

    def systems_summary(self) -> int:
        raise NotImplementedError("Method not implemented for async")

    def consolodate_files(self):
        raise NotImplementedError("Method not implemented for async")

    # Job management
    @async_dbm
    def contexts_batch(
        self,
        jobs: Iterable[Job],  # preferrebly a query
        worker: Optional[Worker] = None,
    ) -> List[RunContext]:
        return super().contexts_batch(jobs, worker)

    async def archive_batch(self, results: Iterable[Result], **kwargs):
        results = list(results)

        # Run the actual method
        s = time()
        res = await self.run_in_pool(super().archive_batch, results, **kwargs)
        archive_time = time() - s

        # Accounting
        batch_size = len(results)
        exec_time = sum(((r.end_time - r.start_time).total_seconds() for r in results))
        self.update_batch_size(archive_time, exec_time, batch_size)

        return res

    def update_batch_size(
        self, _archive_time: float, _exec_time: float, _batch_size: int
    ):
        # Keep the 10 pushes worth of data
        self.archive_times.append(_archive_time)
        self.exec_times.append(_exec_time)
        self.batch_sizes.append(_batch_size)

        ra_ratio = sum(self.exec_times) / sum(self.archive_times)
        target = 5 * self.num_active_workers()
        avg_batch_size = sum(self.batch_sizes) / len(self.batch_sizes)

        # Ideal batch size is 5 times longer to run than archive for each worker
        correction = target / ra_ratio
        next_batch = min(ceil(avg_batch_size * correction), self.config.batch_size_max)
        next_batch = max(self.config.batch_size_min, next_batch)

        log.debug(f"RA RATIO: {target}/{ra_ratio}")
        log.debug(f"Correction: {correction}")
        log.debug(f"Next batch: {next_batch}")
        log.debug(f"Cur batch: {self.config.batch_size}")

        if self.config.batch_size != next_batch:
            self.config.batch_size = next_batch
            log.debug(f"CHANGING BATCH SIZE TO {next_batch}")


PROJECT = None


async def mk_project(
    basepath: Optional[str] = None, config: Optional[Dict] = None, make_db=False
) -> AsyncProject:
    if basepath is None:
        basepath = environ.get("FRAGMENT_BASEPATH", ".")
    if config is None:
        config = json.loads(environ.get("FRAGMENT_CONFIG", "{}"))

    project = AsyncProject(basepath=basepath, config=config, DBMClass=ThreadedFDBM)
    if not project.db_file.exists():
        if make_db:
            await project.init()
        else:
            raise ValueError(
                f"Database `{str(project.db_file.absolute())}` does not exist"
            )
    return project


async def get_project() -> AsyncProject:
    # Project should be created BEFORE the first client connects but
    # we allow this workflow for testing purposes
    global PROJECT
    if PROJECT is None:
        PROJECT = await mk_project()
    return PROJECT


def clear_project() -> None:
    global PROJECT
    PROJECT = None
