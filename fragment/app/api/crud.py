from typing import Dict

from peewee import fn

from fragment.calculations.common import JobStatus
from fragment.calculations.models import Job


def jobs_summary() -> Dict[JobStatus, int]:
    counts = {s: 0 for s in JobStatus}

    # TODO: Add optional job filter
    stats = Job.select(Job.status, fn.COUNT().alias("count")).group_by(Job.status)

    for s, c in stats.tuples():
        counts[s] = c
    return counts
