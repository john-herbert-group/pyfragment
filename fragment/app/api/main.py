import logging

from fastapi import FastAPI

import fragment.core.logging as flogging
from fragment.app.api.db import clear_project, get_project
from fragment.calculations.common import JobStatus
from fragment.calculations.models import Job

from .calculations import router as calculations_router
from .db_models import router as models_router
from .status import router as status_router
from .workers import router as workers_router

app = FastAPI()
app.include_router(models_router)
app.include_router(workers_router)
app.include_router(calculations_router)
app.include_router(status_router)

log = logging.getLogger(__name__)


@app.on_event("startup")
async def startup_event():
    proj = await get_project()

    if not flogging.LOGGING_CONFIGURED:
        flogging.configure_logger(proj.config.log_level, detailed=True)
    log.debug("INITIALIZING FRAGMENT SERVER")

    # Clean up any previous run
    await proj.run_in_pool(
        Job.update(status=JobStatus.PENDING)
        .where(Job.status == JobStatus.RUNNING)
        .execute
    )

    # Handle rerun failed
    if proj.config.rerun_failed:
        await proj.run_in_pool(
            Job.update(status=JobStatus.PENDING)
            .where(Job.status == JobStatus.FAILED)
            .execute
        )

    # Run async project init
    await proj.async_init()


@app.on_event("shutdown")
async def shutdown_event():
    log.debug("CLEANING FRAGMENT SERVER")
    proj = await get_project()
    await proj.async_cleanup()
    await proj.cleanup()
    # if not proj.dbm.context_db.is_stopped():
    #     proj.dbm.context_db.stop()
    clear_project()  # Set PROJECT var to None


# @app.exception_handler(peewee.OperationalError)
# def db_operational_error_handler(request: Request, exc: peewee.OperationalError):
#     return JSONResponse(
#         status_code=500,
#         content={'message': str(exc)}
#     )
