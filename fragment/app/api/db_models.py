from fastapi import APIRouter, Depends
from fastapi.exceptions import HTTPException

import fragment.schemas.db as db_models
from fragment.app.api.db import AsyncProject, get_project
from fragment.conf.models import ConfigModel
from fragment.systems.models import System

router = APIRouter(prefix="/models", tags=["models"])


@router.get("/system/{system_id}", response_model=db_models.System)
async def get_system(system_id: int, project: AsyncProject = Depends(get_project)):
    try:
        sys: System = await project.run_in_pool(System.get, system_id)
    except System.DoesNotExist:
        raise HTTPException(status_code=404, detail="System not found")
    return db_models.System.from_orm(sys)


@router.get("/config/{backend_id}", response_model=db_models.ConfigModel)
async def get_config(backend_id: int, project: AsyncProject = Depends(get_project)):
    try:
        conf: ConfigModel = await project.run_in_pool(ConfigModel.get, backend_id)
    except ConfigModel.DoesNotExist:
        raise HTTPException(status_code=404, detail="ConfigModel not found")

    return db_models.ConfigModel.from_orm(conf)
