import asyncio
from functools import partial
from uuid import uuid4

import aiofiles
from fastapi import APIRouter, Depends, File, UploadFile

from fragment.app.api.db import AsyncProject, get_project
from fragment.app.api.workers import get_worker
from fragment.calculations.models import Job, Worker
from fragment.schemas.calculation import Batch, BatchResult

# Note, not everything in this route requires the DB
router = APIRouter(prefix="/calculation", tags=["calculations", "jobs"])


@router.get("/get_work", response_model=Batch)
async def get_calculations(
    worker: Worker = Depends(get_worker),
    project: AsyncProject = Depends(get_project),
):
    if project.batch_queue.qsize() == 0:  # Clear the request
        return Batch(jobs=[])

    batch: Batch = await project.batch_queue.get()
    await project.run_in_pool(
        Job.update(worker_id=worker.id)
        .where(Job.id << [j.id for j in batch.jobs])
        .execute
    )  # Bind this batch to the worker
    project.batch_queue.task_done()
    worker.got += len(batch.jobs)
    return batch


@router.post("/report_work", dependencies=[Depends(get_worker)])
async def report_calculations(
    results: BatchResult,
    worker: Worker = Depends(get_worker),
    project: AsyncProject = Depends(get_project),
):
    worker.reported += len(results.results)
    await project.report_queue.put(results)


@router.post("/upload_archive", dependencies=[Depends(get_worker)])
async def upload_archive(
    archive: UploadFile = File(...),
    project: AsyncProject = Depends(get_project),
):
    dest_dir = project.uploadpath / str(uuid4())[0:2]
    mk_dest_dir = partial(dest_dir.mkdir, exist_ok=True, parents=True)
    await asyncio.get_running_loop().run_in_executor(None, mk_dest_dir)

    dest = dest_dir / archive.filename
    async with aiofiles.open(dest, "wb") as dest_f:
        while True:
            data = await archive.read(2048)
            if not data:
                break
            await dest_f.write(data)
