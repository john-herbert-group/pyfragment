import logging

from fastapi import APIRouter, Depends

from fragment.schemas.workers import ServerState, ServerStatus, WorkerStatus

from .db import AsyncProject, get_project

log = logging.getLogger(__name__)

# Note, not everything in this route requires the DB
router = APIRouter(prefix="/status", tags=["status"])


@router.get("", response_model=ServerStatus)
async def status(project: AsyncProject = Depends(get_project)):
    return ServerStatus(
        jobs=await project.jobs_summary(),
        workers=[
            WorkerStatus(
                id=w.id,
                hostname=w.hostname,
                last_seen=w.last_seen,
                got=w.got,
                reported=w.reported,
            )
            for w in project.active_workers()
        ],
        batch_size=project.config.batch_size,
        jobs_prepaired=project.jobs_prepaired,
        jobs_reported=project.jobs_reported,
        batch_queue_items=project.batch_queue.qsize(),
        report_queue_items=project.report_queue.qsize(),
        async_queue_length=project.pool_queue,
        work_done=project.work_done(),
    )


@router.get("/state", response_model=ServerState)
async def state(project: AsyncProject = Depends(get_project)):
    batcher_crash = False
    if project.job_batcher_task.done():
        e = project.job_batcher_task.exception()
        if e:
            raise e
    if project.job_archiver_task.done():
        e = project.job_archiver_task.exception()
        if e:
            raise e

    return ServerState(
        work_done=project.work_done() or batcher_crash,
        active_workers=project.num_active_workers(),
        jobs_prepaired=project.jobs_prepaired,
        jobs_reported=project.jobs_reported,
    )


@router.post("/halt", response_model=ServerStatus)
async def halt(project: AsyncProject = Depends(get_project)):
    project.stop.set()
    return await status(project=project)
