from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Header
from fastapi.params import Depends

from fragment.app.api.db import AsyncProject, get_project
from fragment.calculations.models import Worker
from fragment.schemas.workers import Registration, WorkerAssignment

# Note, not everything in this route requires the DB
router = APIRouter(prefix="/worker", tags=["workers"])


async def get_worker(
    x_worker_id: int = Header(None), project: AsyncProject = Depends(get_project)
) -> Worker:
    if x_worker_id is None:
        raise HTTPException(400, detail="Unknown worker")
    try:
        return await project.checkin_worker(x_worker_id)
    except KeyError:
        raise HTTPException(403, detail="Unknown worker")


@router.post("/register", response_model=WorkerAssignment)
async def register(
    reg: Registration,
    project: AsyncProject = Depends(get_project),
):
    worker = await project.add_worker(reg)

    return WorkerAssignment(
        id=worker.id,
        name=worker.name,  # Auto-generated
        server_config=project.config,  # Perhaps exclude some values
    )


@router.post("/check_in")
async def check_in(
    worker: Worker = Depends(get_worker), project: AsyncProject = Depends(get_project)
):
    """Checkin the worker with the project

    ..:note: The worker is not update in the DB
    """
    return {"halt": project.work_done()}


@router.post("/deregister")
async def register(
    worker: Worker = Depends(get_worker), project: AsyncProject = Depends(get_project)
):
    await project.deregister_worker(worker.id)


# Tast Management
@router.get("/next_task", dependencies=[Depends(get_worker)])
async def next_task(project: AsyncProject = Depends(get_project)):
    if project.stop.is_set():
        return {"next": "halt"}

    if project.batch_queue.qsize() == 0:
        if project.job_batcher_task.done():
            return {"next": "halt"}
        else:
            return {"next": "sleep"}
            # REPORT SLEEPING STATISTIC

    return {"next": "run_jobs"}
