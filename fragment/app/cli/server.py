from logging import getLogger

import click

import fragment.core.logging as flogging
from fragment.app.cli.util import ERROR
from fragment.app.server.common import (
    get_port,
    print_status_sync,
    read_server_conf,
    server_halt_sync,
    server_state_sync,
)
from fragment.app.server.server_app import ServerApp

log = getLogger(__name__)


@click.group(help="Spawn and manage the fragment server for parallel jobs")
@click.option(
    "--host", type=str, default="0.0.0.0", help="Host; env FRAGMENT_SERVER_HOST"
)
@click.option("--port", type=int, default=0, help="Port; env FRAGMENT_SERVER_PORT")
@click.pass_context
def server(ctx: click.Context, host, port):
    ctx.ensure_object(dict)
    log_level = ctx.obj["log_level"]

    # Configure logging
    if ctx.invoked_subcommand == "start":
        flogging.configure_logger(level_str=log_level, detailed=True)
        if not port:
            port = get_port()
    else:
        flogging.configure_logger(level_str=log_level)

    # Configure ports
    if not port:
        if ctx.invoked_subcommand == "start":
            port = get_port()
        elif ctx.invoked_subcommand != "await-startup":
            wp = ctx.obj["work_path"]
            try:
                conf = read_server_conf(wp)
            except FileNotFoundError:
                click.echo(
                    ERROR
                    + f"Could not find a running server in `{wp}`. Please provide a hostname and port."
                )
                exit()
            port = conf.port
            host = conf.hostname

    ctx.obj["server_host"] = host
    ctx.obj["server_port"] = port


@server.command(help="Starts fragment server running fragment's ASGI app")
@click.option("-r", "--rerun-failed", is_flag=True, help="Run Failed Jobs")
@click.option("--uvicorn-log-level", default="warning", help="Uvicorn server log level")
@click.option(
    "--heartbeat-freq",
    default=5,
    type=int,
    help="The frequency at which workers should be expected to check back in.",
)
@click.option(
    "--status-freq",
    default=2,
    type=int,
    help="How frequently to check in with the ASGI server.",
)
@click.option(
    "--workerless-timeout",
    default=10,
    type=int,
    help="How many seconds to wait before stoping if there are no workers.",
)
@click.option(
    "--batch-size",
    default=1,
    type=int,
    help="Number of jobs for each worker to handle at once",
)
@click.pass_context
def start(
    ctx,
    rerun_failed: bool,
    uvicorn_log_level: str,
    heartbeat_freq: float,
    status_freq: float,
    workerless_timeout: float,
    batch_size: int,
):
    server_host = ctx.obj["server_host"]
    server_port = ctx.obj["server_port"]
    if not server_port:
        server_port = get_port()
    project = ctx.obj["project"]
    log_level = ctx.obj["log_level"]

    if not project.is_configured:
        click.echo(ERROR + f"{project.basepath} is not a fragment project")
        exit(-1)

    server = ServerApp(
        server_host,
        server_port,
        basepath=project.basepath,
        log_level=log_level,
        uvicorn_log_level=uvicorn_log_level,
        rerun_failed=rerun_failed,
        heartbeat_freq=heartbeat_freq,
        status_poll_freq=status_freq,
        workerless_timeout=workerless_timeout,
        batch_size=batch_size,
    )
    server.run()


@server.command(help="Stop the server")
@click.pass_context
def stop(ctx):
    server_host = ctx.obj["server_host"]
    server_port = ctx.obj["server_port"]

    try:
        server_halt_sync(server_host, server_port)
    except Exception as e:
        print(ERROR + f"Could not connect to server at {server_host}:{server_port}")
        exit()


@server.command(help="Get server status")
@click.pass_context
def status(ctx):
    server_host = ctx.obj["server_host"]
    server_port = ctx.obj["server_port"]

    print_status_sync(server_host, server_port)


@server.command(help="Gets the port of a running fragment server")
@click.pass_context
def port(ctx):
    work_path = ctx.obj["work_path"]
    try:
        sc = read_server_conf(work_path)
        click.echo(str(sc.port))
    except FileNotFoundError:
        click.echo(ERROR + f"Coult not find running server in `{work_path}`")


@server.command(help="Gets the host of a running fragment server")
@click.pass_context
def host(ctx):
    work_path = ctx.obj["work_path"]
    try:
        sc = read_server_conf(work_path)
        click.echo(str(sc.hostname))
    except FileNotFoundError:
        click.echo(ERROR + f"Coult not find running server in `{work_path}`")


@server.command(help="Await server start")
@click.option("--timeout", type=float, default=2)
@click.option("-v", "--verbose", is_flag=True)
@click.pass_context
def await_startup(ctx, timeout: float, verbose: bool):
    from time import sleep, time

    server_host = ctx.obj["server_host"]
    server_port = ctx.obj["server_port"]
    work_path = ctx.obj["work_path"]

    start_time = time()
    ret_code = 1
    while time() - start_time < timeout:
        sleep(0.1)
        # Try to get the port if it's not defined yet
        if not server_port:
            try:
                sc = read_server_conf(work_path)
                server_host = sc.hostname
                server_port = sc.port
            except FileNotFoundError:
                break

        # Try to contact the server
        try:
            if verbose:
                log.info(f"Trying {server_host}:{server_port}")
            server_state_sync(server_host, server_port)
            ret_code = 0
            break
        except Exception as e:
            if verbose:
                print(e)
    if ret_code > 0:
        if server_port:
            log.error(f"Timeout for {server_host}:{server_port}")
        else:
            log.error(f"Timeout for server started in `{work_path}`")
    exit(ret_code)
