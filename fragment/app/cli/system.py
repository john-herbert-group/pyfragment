import click

from fragment.app.cli.util import ERROR, INFO, WARNING, SuperSystemDoesNotExists
from fragment.core.project import DuplicateModelError, Project
from fragment.systems.geometry_readers.util import SystemExists


@click.group(help="Commands for handling system geometry")
def system():
    pass


@system.command(help="Read in a geometry file")
@click.option(
    "--name",
    required=True,
    prompt=True,
    help="Name slug used to uniquily identify this system",
    callback=SuperSystemDoesNotExists,
)
@click.option("--note", required=False, prompt=True, help="A human readable comment")
@click.option(
    "-c",
    "--charges",
    required=False,
    help=(
        "List of atom charges in the file in a `id1:charge1,id2:charge2` format. "
        + "For XYZ files `id` is the 1-based index of the atom for how it appears in the file. "
        + "For PDB files `id` is the ID in the PDB file."
    ),
)
@click.argument("geometry_file", required=True, type=click.Path(exists=True))
@click.pass_context
def add(ctx, name, note, charges, geometry_file):
    project: Project = ctx.obj["project"]

    charges_dict = {}
    if charges:
        for cs in charges.split(","):
            atom, charge = cs.split(":")
            charges_dict[int(atom)] = int(charge)

    try:
        sys = project.add_system(geometry_file, name, note, charges=charges_dict)

    except DuplicateModelError:
        click.echo(WARNING + f"A system named {name} already exists.")
    except SystemExists as e:
        click.echo(ERROR + str(e))
    else:
        click.echo(
            INFO + f"A system named {name} with {sys.atoms.count()} was"
            " added to the project"
        )


@system.command(help="List systems")
@click.pass_context
def list(ctx):
    project: Project = ctx.obj["project"]
    from fragment.systems.models import System, SystemLabel

    with project.dbm:
        systems = SystemLabel.select().order_by("name").prefetch(System)

        click.echo()

        for sys in systems:
            click.echo(click.style(sys.name + ":\n", fg="blue", underline=True))
            click.echo(f"    {sys.note}\n")
            click.echo(f"    ATOMS: {sys.system.atoms.count()}")
            click.echo(f"    VIEWS: {sys.system.views.count()}")
            click.echo()


@system.command(help="Provide details on a specific system")
@click.argument("system_name", required=True)
@click.pass_context
def info(ctx, system_name: str):
    from fragment.calculations.models import Job
    from fragment.systems.models import System, SystemLabel

    project: Project = ctx.obj["project"]

    with project.dbm:
        name = None
        note = None
        created = None

        if system_name.lower().startswith("s:"):
            sys_id = int(system_name.split(":")[1])
            try:
                sys: System = System.get(id=sys_id)
            except System.DoesNotExist:
                click.echo(ERROR + f"System with ID {sys_id} does not exist")
                return

            name = f"S:{sys_id}"
            note = f"System {sys_id}"
        elif system_name.lower().startswith("j:"):
            job_id = int(system_name.split(":")[1])
            try:
                job: Job = Job.get(id=job_id)
            except Job.DoesNotExist:
                click.echo(ERROR + f"Job with ID {job_id} does not exist")
                return

            sys = job.modded_system
            name = f"J:{job_id}"
            note = f"System used for Job {job_id}"
        else:
            try:
                label: SystemLabel = SystemLabel.from_name(system_name)
            except SystemLabel.DoesNotExist:
                click.echo(ERROR + f"System with name '{system_name}' does not exist")
                return

            sys = label.system
            name = label.name
            note = label.note
            created = label.created

        click.echo()

        if name:
            click.echo(click.style("NAME:", underline=True) + f" {name}\n")
        if note:
            click.echo(click.style("NOTE:", underline=True) + f" {note}\n")
        if created:
            click.echo(click.style("CREATED:", underline=True) + f" {created}\n")

        click.echo(
            click.style("CHARGE/MULT:", underline=True)
            + f" {sys.charge}/{sys.multiplicity}\n"
        )

        click.echo(click.style("GEOMETRY:\n", underline=True))
        click.echo(
            "    "
            + click.style("ID", fg="blue", underline=True)
            + "\t"
            + click.style("ATOM", fg="blue", underline=True)
            + "\t"
            + click.style("X (Å)", fg="blue", underline=True)
            + "\t"
            + click.style("Y (Å)", fg="blue", underline=True)
            + "\t"
            + click.style("Z (Å)", fg="blue", underline=True)
            + "\t"
            + click.style("CHARGE", fg="blue", underline=True)
            + "\t"
            + click.style("META", fg="blue", underline=True)
            + "\n"
        )

        for a in sys.atoms:
            click.echo(
                f"    {a.id}\t{a.t}\t{a.x: .2f}\t{a.y: .2f}\t{a.z: .2f}\t{a.charge: }\t{a.meta:}"
            )
        click.echo()

        if sys.views.exists():
            click.echo(click.style("VIEWS:\n", underline=True))
            for v in sys.views.order_by("name"):
                click.echo(f"    {v.name}:\t{   v.note}")

        click.echo()
