import logging

import click

import fragment.core.logging as flogging

log = logging.getLogger(__name__)


@click.command(help="Starts local worker process")
@click.option(
    "--server-host",
    default="localhost",
    help="Hostname of the fragment server; env FRAGMENT_WORKER_SERVER_HOST",
)
@click.option(
    "--server-port",
    type=int,
    default=8000,
    help="Port; env FRAGMENT_WORKER_SERVER_PORT",
)
@click.option(
    "--work-path",
    type=str,
    required=False,
    help="Set the working directory for the worker",
)
# @click.option(
#     '--shared-path',
#     is_flag=True,
#     help='Use if the work path is the same as the fragment-server\'s work path'
# )
@click.pass_context
def worker(ctx, server_host, server_port, work_path):
    import async_collie as ac

    from fragment.app.worker.worker_app import WorkerApp

    log_level = ctx.obj["log_level"]

    flogging.configure_logger(level_str=log_level, detailed=True)

    app = WorkerApp(
        server_host,
        server_port,
        cores=ctx.obj["cores"],
        workerpath=work_path,
    )

    try:
        log.info(
            f"Starting worker connecting to {app.server_hostname}:{app.server_port}"
        )
        app.run()
    except ac.app.AppStartupError as e:
        log.error("Worker could not start: " + str(e.__cause__))
        exit(-1)
    except Exception as e:
        log.error(f"There was an {e.__class__.__name__}: {e}")
        raise e
        exit(-1)
