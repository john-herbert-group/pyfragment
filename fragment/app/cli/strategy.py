from typing import Any, Tuple

import click
from pydantic import ValidationError

from fragment.app.cli.util import INFO
from fragment.conf.util import ConfigDoesNotExist
from fragment.core.project import Project
from fragment.systems.geometry_readers.util import SystemDoesNotExist


@click.group(help="Manage calculation strategies")
def strategy():
    pass


@strategy.command(help="Imports strategy elements from a yaml file")
@click.argument("strat_files", required=True, nargs=-1, type=click.Path(exists=True))
@click.pass_context
def add(ctx, strat_files):
    project: Project = ctx.obj["project"]
    for f in strat_files:
        click.echo(INFO + f"Adding `{f}`... ", nl=False)
        try:
            project.add_strategy_file(f)
        except ValidationError as ve:
            # TODO: Handle ctx variable
            click.echo("FAILED")
            addressed = set()
            for e in reversed(ve.errors()):
                loc = e["loc"]
                if loc in addressed:
                    continue
                else:
                    addressed.add(loc)
                loc_str = loc_to_str(loc)
                msg = e["msg"]
                click.echo(
                    "  " + click.style(f"{loc_str}", underline=True) + f": {msg}"
                )
                exit(-1)
        except SystemDoesNotExist as e:
            click.echo("FAILED")
            click.echo("  " + str(e))
            exit(-1)
        except ConfigDoesNotExist as e:
            click.echo("FAILED")
            click.echo(" " + str(e))
            exit(-1)
        else:
            click.echo("DONE")


def loc_to_str(loc: Tuple[Any]):
    loc_str = ""
    last = None
    for l in loc:
        if isinstance(l, int):
            l_str = f"[{l}]"
        else:
            l_str = l

        if isinstance(l, str) and (not last is None):
            loc_str += "."
        loc_str += l_str
        last = l
    return loc_str


@strategy.command()
@click.pass_context
def list(ctx):
    project: Project = ctx.obj["project"]
    from fragment.conf.models import ConfigModel

    with project.dbm:
        configs = ConfigModel.select().order_by(
            ConfigModel.config_type.desc(), ConfigModel.name
        )

        last_type = None
        for conf in configs:
            if conf.config_type != last_type:
                last_type = conf.config_type
                click.echo()

                click.echo(
                    click.style(
                        conf.config_type[11:].upper() + "S:", fg="blue", underline=True
                    )
                    + ""
                )
                click.echo()

            click.echo(f"{conf.name:>18}: {conf.note}")

        click.echo()
