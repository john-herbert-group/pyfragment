import os

import click
from click.core import Context

from fragment.app.cli.util import ERROR, LOGGING_CHOICES, get_working_path
from fragment.core.logging import configure_logger, teardown_logger
from fragment.core.project import Project


@click.group()
@click.option(
    "-p",
    "--project-folder",
    required=False,
    type=click.Path(resolve_path=True),
    help="Project path to write the database and I/O files",
)
@click.option(
    "-l",
    "--log-level",
    default="info",
    type=click.Choice(LOGGING_CHOICES, case_sensitive=False),
    help="Logging level",
)
@click.option(
    "-c",
    "--cores",
    default=1,
    help="The number of CPU cores",
)
@click.pass_context
def fragment(ctx: Context, project_folder: str, log_level: str, cores: int):
    # Configure work path
    ctx.ensure_object(dict)
    ctx.obj["work_path"] = get_working_path(project_folder)
    ctx.obj["log_level"] = log_level
    ctx.obj["cores"] = cores

    project = Project(basepath=ctx.obj["work_path"])
    ctx.obj["project"] = project

    # Configure logging
    if not ctx.invoked_subcommand in ("server", "worker"):
        configure_logger(level_str=log_level)
    # else leave it up to server and worker

    if (
        not ctx.invoked_subcommand in ("project", "worker-group", "worker", "server")
        and not project.is_configured
    ):
        click.echo(ERROR + "workpath is not an initialized project")
        exit(-1)

    basepath = project.basepath
    if basepath:
        os.chdir(basepath)


@fragment.result_callback()
@click.pass_context
def fragment_cleanup(ctx, *args, **kwargs):
    # Cleanup logger
    teardown_logger()

    project: Project = ctx.obj["project"]
    project.cleanup()


from fragment.app.cli.project import project

fragment.add_command(project)

from fragment.app.cli.system import system

fragment.add_command(system)

from fragment.app.cli.strategy import strategy

fragment.add_command(strategy)

from fragment.app.cli.view import view

fragment.add_command(view)

from fragment.app.cli.calc import calc

fragment.add_command(calc)

from fragment.app.cli.server import server

fragment.add_command(server)

from fragment.app.cli.worker import worker

fragment.add_command(worker)

from fragment.app.cli.worker_group import worker_group

fragment.add_command(worker_group)


def main():
    fragment(auto_envvar_prefix="FRAGMENT")


if __name__ == "__main__":
    main()
