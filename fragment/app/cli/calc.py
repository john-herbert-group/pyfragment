from typing import Any, Iterable, List

import click

from fragment.app.cli.util import ERROR, INFO, SUCCESS, WARNING
from fragment.backends.abstract import QMBackend
from fragment.backends.common import Result
from fragment.calculations.models import (
    Calculation,
    IncompleteJobsError,
    Job,
    JobStatus,
    Layer,
    Worker,
)
from fragment.core.project import Project
from fragment.properties.core import PropertySet


@click.group(help="Calculation management")
def calc():
    pass


@calc.command(help="Lists all calculations")
@click.pass_context
def list(ctx):
    project: Project = ctx.obj["project"]
    with project.dbm:
        from fragment.calculations.models import Calculation

        calcs = Calculation.select().order_by(Calculation.name)
        for calc in calcs:
            click.echo(f"{calc.name}:\t{calc.note}")


@calc.command(help="Lists all calculations")
@click.argument("name", required=True)
@click.pass_context
def info(ctx, name):
    project: Project = ctx.obj["project"]
    from fragment.calculations.models import Calculation

    with project.dbm:
        calc = Calculation.get(Calculation.name == name)

        click.echo()
        click.echo(click.style("NAME:", underline=True) + f" {calc.name}\n")
        click.echo(click.style("NOTE:", underline=True) + f" {calc.note}\n")

        try:
            props = calc.properties
        except IncompleteJobsError:
            click.echo("    Calculation has incomplete jobs")
            props = None

        click.echo(click.style("LAYERS:\n", underline=True))

        for coef, layer in zip(calc.coefs, calc.layers):
            if coef < 0:
                color = "red"
            else:
                color = "green"
            coef_str = click.style(f"({coef: d}) ", fg=color)
            click.echo(
                "    "
                + coef_str
                + click.style(
                    f"{layer.view.name} ({len(layer.view.fragments)} frags)",
                    underline=True,
                )
                + " → "
                + click.style(f"{layer.backend.name}", underline=True)
                + f": l:{layer.id}"
            )
            try:
                for k, v in layer.properties:
                    click.echo(f"    {k:>18}: {v}")
            except IncompleteJobsError:
                incomplete_jobs = layer.jobs.select(
                    Job.status != JobStatus.COMPLETED
                ).count()
                click.echo(f"\n         INCOMPLETE JOBS: {incomplete_jobs}")
                continue
            except Exception as e:
                click.echo(ERROR + "There was an error:")
                click.echo(e)
                exit(-2)
            click.echo()

        if props:
            click.echo(click.style("PROPERTIES:\n", underline=True))

            if props:
                for k, v in props:
                    click.echo(f"    {k:>18}: {v}")

            click.echo()


@calc.command(help="Run all jobs in calculation")
@click.option("-r", "--rerun-failed", is_flag=True, help="Run Failed Jobs")
@click.option("-v", "--verbose", is_flag=True, help="Print out verbose errors")
@click.argument("calcs", required=True, nargs=-1, type=str)
@click.pass_context
def run(ctx, rerun_failed, verbose, calcs: List[str]):
    project: Project = ctx.obj["project"]
    with project.dbm:
        worker = Worker(cores=ctx.obj["cores"])
        worker.save()

    if "ALL" in calcs:
        calcs = ["ALL"]

    # TODO: Allow batching
    for calc_name in calcs:
        with project.dbm:
            try:
                if calc_name.lower().startswith("j:"):
                    # We are running a single job
                    job_id = int(calc_name.split(":")[1])
                    query = Job.select().where(Job.id == job_id)
                elif calc_name.lower().startswith("l:"):
                    layer_id = int(calc_name[2:])
                    query = Layer.get(layer_id).jobs
                elif calc_name == "ALL":
                    query = Job.select()
                else:
                    query = Calculation.get(name=calc_name).jobs
            except Exception as e:
                # raise e
                click.echo(WARNING + f"Unknown calculation '{calc_name}'. Skipping")
                continue

            click.echo(INFO + f"Running {calc_name}🏃", nl=False)

            backend: QMBackend = None
            with project.open_archive("a") as arch:
                for job in Job.exclude_complete(query, rerun_failed).iterator():
                    job: Job
                    if backend is None or backend.conf_id != job.backend__id:
                        backend = job.backend

                    res: Result = project.run_job(
                        job, worker=worker, backend=backend, archive=arch, rerun=True
                    )
                    project.dbm.context_db.commit()
                    if res.status != JobStatus.COMPLETED:
                        error_str = f"E(j:{job.id})"
                        click.echo(click.style(error_str, fg="red"), nl=False)
                    else:
                        click.echo(".", nl=False)
            click.echo()

        click.echo()
        with project.dbm:
            worker: Worker = Worker.get(worker.id)  # The project will
            # update without refreshing
        click.echo(INFO + f"{worker.jobs_handled} jobs ran")
        if worker.jobs_succeeded:
            click.echo(SUCCESS + f"{worker.jobs_succeeded} jobs succeeded 🥳")
        if worker.jobs_failed:
            click.echo(ERROR + f"{worker.jobs_failed} jobs have failed 🤬")


def value_or_NA(V: Any):
    if V is None:
        return V
    return V.value


@calc.command(help="Re-extracts properties for all completed")
@click.option("--all", is_flag=True, help="Extract properties from all completed jobs")
@click.pass_context
def collect_properties(ctx, all):
    click.echo(INFO + "Updating properties will overwrite any previouse values")
    con: str = click.prompt("Would you like to continue? [y/n]")

    if not con.lower().startswith("y"):
        return

    project: Project = ctx.obj["project"]
    fs_handler = ctx.obj["fs_handler"]
    worker = Worker()
    worker.configure(fs_handler)
    from fragment.calculations.models import Job, JobStatus

    with project.dbm:
        jobs_query: Iterable[Job] = Job.select().where(
            Job.status == JobStatus.COMPLETED
        )
        if not all:
            jobs_query: Iterable[Job] = jobs_query.where(Job.properties == {})
            click.echo(INFO + "Updating blank properties...")
        else:
            click.echo(INFO + "Updating all properties could take long time...")

        for job in jobs_query:
            job.update_properties_from_workdir(
                worker.job_filename_stub(job), worker.job_abs_workdir(job)
            )
            job.save()


@calc.command(help="Outputs a CSV summary of all completed calculations")
@click.argument("output_file")
def summary(output_file):
    import csv

    from fragment.calculations.models import Calculation, Job, Layer

    # TODO: Add hybrid property for completed
    rows: List[PropertySet] = []
    for calc in Calculation.select():
        try:
            rows.append(
                (
                    calc.name,
                    calc.get_properties(),
                )
            )
        except IncompleteJobsError:
            pass
    keys = set()
    for _, prop in rows:
        for k in prop.properties.keys():
            keys.add(k)

    # Print the header
    with open(output_file, "w") as f:
        writer = csv.writer(f)
        writer.writerow(["calc_name"] + [k for k in keys])
        for name, params in rows:
            writer.writerow(
                [name] + [value_or_NA(params.properties.get(k, None)) for k in keys]
            )
