from io import TextIOWrapper

import click

from fragment.app.worker_group.worker_group_app import WorkerGroupApp


@click.group(help="Starts multiple workers.", chain=True)
@click.option(
    "--server-host",
    default="localhost",
    type=str,
    help="Server host; env FRAGMENT_WORKER_GROUP_SERVER_HOST",
)
@click.option(
    "--server-port",
    default=8000,
    type=int,
    help="Server port; env FRAGMENT_WORKER_GROUP_SERVER_PORT",
)
@click.pass_context
def worker_group(ctx, server_host, server_port):
    ctx.ensure_object(dict)
    ctx.obj["server_host"] = server_host
    ctx.obj["server_port"] = server_port


@worker_group.result_callback()
@click.pass_context
def worker_group_exec(ctx, *args, **kargs):
    server_host = ctx.obj["server_host"]
    server_port = ctx.obj["server_port"]

    work_path = ctx.obj["work_path"]
    log_level = ctx.obj["log_level"]

    local_workers = ctx.obj.get("local_workers", [])
    ssh_workers = ctx.obj.get("ssh_workers", [])
    ts_workers = ctx.obj.get("ts_workers", [])

    workers = local_workers + ssh_workers + ts_workers

    group = WorkerGroupApp(
        server_host,
        server_port,
        work_path,
        workers,
        log_level,
    )
    group.run()


@worker_group.command(help="Starts an example worker on the local machine")
@click.option(
    "-w", "--nworkers", type=int, default=1, help="Number of local workers to launch"
)
@click.option("-n", "--cores", type=int, required=False, help="Number of CPUs")
# @click.option(
#     '--work-path',
#     type=str,
#     required=False,
#     help='Set the working directory for the worker'
# )
# @click.option(
#     '--shared-path',
#     is_flag=True,
#     help='Use if the work path is the same as the fragment-server\'s work path'
# )
@click.pass_context
def local(ctx, nworkers: int, cores: int):
    from fragment.app.worker_group.local_worker_applet import LocalWorker

    local_workers = [
        LocalWorker(cores=cores or ctx.obj["cores"]) for _ in range(nworkers)
    ]
    ctx.obj["local_workers"] = local_workers


@worker_group.command(
    help=(
        "Starts a cluster over SSH. NOTE: You must have password-free "
        "certificate access to use this feature"
    )
)
@click.option(
    "--host-file", type=click.File("r"), help="List of hostnames to use for the cluster"
)
@click.option("--worker-port", type=int, default=22, help="SSH port of the worker")
@click.option(
    "--proxy-port",
    type=int,
    default=9000,
    required=False,
    help=("Proxy tunnel port on the worker"),
)
@click.option(
    "-s",
    "--provisioning-script",
    type=click.File("r"),
    required=False,
    help=(
        "Provisioning scrip to run before executing fragment. NOTE: "
        "this script will use the native shell on the worker machine"
    ),
)
@click.option("-n", "--cores", required=False, help="Number of CPUs per worker")
@click.option(
    "--workers-per-host",
    default=1,
    type=int,
    help=("Number of worker apps to run on each host"),
)
@click.pass_context
def ssh(
    ctx,
    host_file: TextIOWrapper,
    worker_port: int,
    proxy_port: int,
    provisioning_script: TextIOWrapper,
    cores,
    workers_per_host,
):
    from fragment.app.worker_group.ssh_worker_applet import SSHWorker

    p_script = provisioning_script.read() if provisioning_script else ""
    worker_hosts = []

    for l in host_file:
        l = l.strip()
        if not l or l.startswith("#"):
            continue
        worker_hosts.append(l)

    workers = []
    for worker_host in worker_hosts:
        for _ in range(workers_per_host):
            workers.append(
                SSHWorker(
                    worker_host,
                    worker_port=worker_port,
                    provisioning_script=p_script,
                    proxy_port=proxy_port,
                    cores=cores or ctx.obj["cores"],
                )
            )
            proxy_port += 1
    ctx.obj["ssh_workers"] = workers


@worker_group.command(
    help=(
        "Starts a cluster over SSH using Task Spooler."
        "NOTE: You must have password-free access and task spooler to use this feature"
    )
)
@click.option(
    "--host-file", type=click.File("r"), help="List of hostnames to use for the cluster"
)
@click.option("--worker-port", type=int, default=22, help="SSH port of the worker")
@click.option(
    "-s",
    "--provisioning-script",
    type=click.File("r"),
    required=False,
    help=(
        "Provisioning scrip to run before executing fragment. NOTE: "
        "this script will use the native shell on the worker machine"
    ),
)
@click.option("-n", "--cores", required=False, help="Number of CPUs per worker")
@click.pass_context
def ts(
    ctx,
    host_file: TextIOWrapper,
    worker_port: int,
    provisioning_script: TextIOWrapper,
    cores,
):
    from fragment.app.worker_group.ts_worker_applet import TaskSpoolerWorker

    p_script = provisioning_script.read() if provisioning_script else ""
    worker_hosts = []

    for l in host_file:
        l = l.strip()
        if not l or l.startswith("#"):
            continue
        worker_hosts.append(l)

    workers = []
    for worker_host in worker_hosts:
        workers.append(
            TaskSpoolerWorker(
                worker_host,
                worker_port=worker_port,
                provisioning_script=p_script,
                cores=cores or ctx.obj["cores"],
            )
        )
    ctx.obj["ts_workers"] = workers
