import click

from fragment.core.project import Project


@click.group(help="View management and fragment generation")
def view():
    pass


@view.command(help="List all views")
@click.option("--primary", flag_value=True)
@click.pass_context
def list(ctx, primary):
    from fragment.systems.models import View, ViewType

    project: Project = ctx.obj["project"]
    with project.dbm:
        if primary:
            views = View.select()
        else:
            views = (
                View.select().where(View.type == ViewType.AUXILIARY).order_by(View.name)
            )

        for v in views:
            click.echo(f"{v.name}:\t{v.note}")
