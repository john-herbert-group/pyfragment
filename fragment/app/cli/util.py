import os
from pathlib import Path
from typing import List

import click
from click.core import Context
from click.exceptions import BadParameter

from fragment.core.project import Project
from fragment.core.util import ValidationError, validate_slug

# Status Strings
ERROR = click.style("ERROR: ", fg="red")
WARNING = click.style("WARNING: ", fg="yellow")
INFO = click.style("INFO: ", fg="blue")
SUCCESS = click.style("Success! ", fg="green")


LOGGING_CHOICES = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


def get_working_path(output_path: str) -> Path:
    if output_path is None:
        return Path(os.getcwd())
    return Path(output_path)


# VALIDATORS


def Slug(ctx: Context, param, value):
    try:
        validate_slug(value)
    except ValidationError as e:
        raise BadParameter(e.__str__())
    return value


def SuperSystemExists(ctx: Context, param, value):
    project: Project = ctx.obj["project"]
    from fragment.systems.models import SystemLabel

    value = Slug(ctx, param, value)
    with project.dbm:
        if not SystemLabel.select().where(SystemLabel.name == value).exists():
            raise BadParameter(f"This DB does not contain a system named '{value}'")
    return value


def SuperSystemDoesNotExists(ctx: Context, param, value):
    project: Project = ctx.obj["project"]
    from fragment.systems.models import SystemLabel

    value = Slug(ctx, param, value)
    with project.dbm:
        if SystemLabel.select().where(SystemLabel.name == value).exists():
            raise BadParameter(f"This DB already contains a system named '{value}'")
    return value


def _conf_exists(ctx: Context, name: str, config_type: str):
    project: Project = ctx.obj["project"]
    Slug(None, None, name)
    from fragment.conf.models import ConfigModel

    with project.dbm:
        if (
            not ConfigModel.select()
            .where(ConfigModel.name == name, ConfigModel.config_type == config_type)
            .exists
        ):
            raise BadParameter(f"{config_type} with name '{name}' does not exist.")
    return name


def FragmenterExists(ctx: Context, param, value):
    return _conf_exists(ctx, value, "fragmenter")


def CapperExists(ctx: Context, param, value):
    return _conf_exists(ctx, value, "capper")


def QMBackendExists(ctx: Context, param, value):
    return _conf_exists(ctx, value, "qm_backend")
