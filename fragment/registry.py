GEOMETRY_READERS = [
    "fragment.systems.geometry_readers.pdb.PDBRead",
    "fragment.systems.geometry_readers.xyz.XYZRead",
    "fragment.systems.geometry_readers.frag.FragRead",
]

QM_BACKENDS = [
    "fragment.backends.mopac.MOPACBackend",
    "fragment.backends.nwchem.NWChemBackend",
    "fragment.backends.orca.OrcaBackend",
    "fragment.backends.qchem.QChemBackend",
    "fragment.backends.xtb.XTBBackend",
    "fragment.backends.pyscf.PySCFBackend",
    "fragment.backends.libxtb.LibXTBBackend",
    "fragment.backends.xyz.XYZBackend",
]

FRAGMENTERS = [
    "fragment.fragmenting.water.WaterFragmenter",
    "fragment.fragmenting.pdb.PDBFragmenter",
    "fragment.fragmenting.compound.CompoundFragmenter",
    "fragment.fragmenting.supersystem.SupersystemFragmenter",
    "fragment.fragmenting.raw.RawFragmenter",
    "fragment.fragmenting.MBCP.MBCPFragmenter",
    "fragment.fragmenting.BSSE_balance.BSSEBalancedFragmenter",
]

MODS = [
    "fragment.mods.rcapper.RCapsMod",
    "fragment.mods.distance.DistanceMod",
    "fragment.mods.xtb_filter.XTBEnergyMod",
    "fragment.mods.xtb_filter.XTBChildrenEnergyMod",
    "fragment.mods.xtb_filter.XTBChildrenEnergyProductMod",
    "fragment.mods.xtb_filter.EnergyNodeTrimming",
    "fragment.mods.supersystem_basis.UseSupersystemBasisMod",
    "fragment.mods.supersystem_basis.ClusterBasisMod",
    "fragment.mods.cloud_basis.UseCloudBasisMod",
]


from importlib import import_module


class Registry(object):
    def __init__(self) -> None:
        self.namespaces = {}
        self.all = {}

    def add(self, cls_path, suffex):
        module_path, class_name = cls_path.rsplit(".", maxsplit=1)
        self.all[class_name] = module_path
        position = class_name.find(suffex)

        namespace = class_name[position:].lower()
        name = class_name[0:position].lower()
        try:
            self.namespaces[namespace][name] = class_name
        except KeyError:
            self.namespaces[namespace] = {name: class_name}

    def _import(self, module_path: str, class_name: str):
        module = import_module(module_path)
        return getattr(module, class_name)

    def get(self, class_name: str):
        return self._import(self.all[class_name], class_name)

    def get_from_namespace(self, namespace: str, name: str):
        return self.get(self.namespaces[namespace][name])


REGISTRY = Registry()
for reader in GEOMETRY_READERS:
    REGISTRY.add(reader, "Read")
for backend in QM_BACKENDS:
    REGISTRY.add(backend, "Backend")
for fragmenter in FRAGMENTERS:
    REGISTRY.add(fragmenter, "Fragmenter")
for mod in MODS:
    REGISTRY.add(mod, "Mod")
