import logging

# Configure the default logger
# The life-cycle handler will configure this
log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())
