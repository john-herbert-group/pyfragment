from setuptools import find_packages, setup

requirements = [
    "aiofiles",
    "numpy",
    "fastapi",
    "httpx",
    "scipy",
    "Click",
    "codenamegenerator",
    "uvicorn[standard]",
    "Asynchronous-Collie @ git+https://gitlab.com/dustinrb/asynchronous-collie.git",
    "peewee",
    "python-multipart",  # uploads
    "pydantic >= 1.10, <2.0",
    "qcelemental",
]

pyscf_requirements = ["pyscf"]

doc_requirements = [
    "sphinx",
    "sphinx-autodoc-typehints",
    "sphinxcontrib-katex",
    "furo",
]

test_requiements = ["black", "isort", "requests"]
dev_requirements = ["esbonio"]


setup(
    name="Fragment",
    version="0.2.0",
    python_requires=">=3.8",
    package_data={"fragment": ["py.typed"]},
    packages=find_packages(where=".", include=["fragment*"], exclude=["tests*"]),
    long_description=open("README.md").read(),
    install_requires=requirements,
    include_package_data=True,
    extras_require={
        "dev": dev_requirements + doc_requirements + test_requiements,
        "doc": doc_requirements,
        "pyscf": pyscf_requirements,
        "test": test_requiements,
    },
    entry_points="""
    [console_scripts]
    fragment=fragment.app.cli.__main__:main
    """,
    test_suite="tests",
)
