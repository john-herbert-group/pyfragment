# We've Moved

The new URL for this project is now https://gitlab.com/john-herbert-group/fragment

Please update your git remotes for you your local repositories.

# PyFragment

Fragmentation code for the Herbert Group

# Install 

Use the following script to get up and running with development. It assumes you have
a working Conda installation and that you are subscribed to conda-forge

```sh
# Clone the directory!
git clone git@gitlab.com:john-herbert-group/pyfragment.git
cd pyfragment

# Create a conda env and install depenencies!
conda env create -f environment.yml
conda activate fragment
python -m pip install --editable .

# Make sure everything is working!
python -m unittest discover
```

# Documentation

More documentation can be found at https://john-herbert-group.gitlab.io/pyfragment
