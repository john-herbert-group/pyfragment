Mods
====

Mods provide a user-configurable way of adjusting the control-flow
of the fragmentation process. Mods bind to a specific event in the 
fragmentation process and multiple mods can be stacked per calculation.
From the user's perspective, mods are specified in the strategy
file. Currently mods are only supported on the layer attribute of a 
calculation

This is a test

.. code-block:: yaml

   # Select lines from example strategy file
   mods: 
      foo:
         note: foo mod that does nothing
         mod: TestMod

   calculations:
      bar:
         note: the bare calculation
         layers:
               - view: view1
                 backend: backend1
                 mods: [foo]

There are two catagories of mods: mods and filters

Mods
----

Mods take in data and return a modified version of that data. This could be
adding additional atoms to a system or removing specific fragments from a
view. The general workflow for a mod is *data* --> *moded data*

Currently supported modification events are:

* :class:`Post Primary Fragments <fragment.mods.abstract.PostPrimaryFragBase>`:
  This event occures between primary fragment generation and auxiliary
  fragment generation. This is a chance to alter the primary view.
* :class:`Post Auxiliary Fragments <fragment.mods.abstract.PostAuxFragBase>`:
  This event occures after auxiliary fragment generation and before saving
  the fragments to the database. This is a chance to alter the auxiliary
  view.
* :class:`Pre Template <fragment.mods.abstract.PreTemplateBase>`: This event
  occures before the system is written to in input file offering a chance to
  alter the system.

More details on each mod can be found in the
:mod:`API documentation <fragment.mods.abstract>`.

Filters
-------

Filters take in an event-specific set of parameters and return a boolean value.
Fragment filters use the same convention as the python ``filter`` function:
True means include.

Currently supported filters are:

* :class:`Aux Fragment Generation <fragment.mods.abstract.AuxFragGenBase>`: This
  even occurse during auxiliary fragment generation. The filter is provided the
  primary fragments for each root auxiliary fragment.
* :class:`Pre Run <fragment.mods.abstract.PreRunBase>`: This even occurse before
  a :class:`~fragment.calculations.models.Job` is run. The filter is provided
  the job to be run.

More details on each filter can be found in the
:mod:`API documentation <fragment.mods.abstract>`.

Developing New Mods
-------------------

Mods should extend one of the event classes referenced in the previous two
sections. The general workflow is as follows:

1. Subclass an event to bind to from the
   :mod:`API documentation <fragment.mods.abstract>` and familiarize youself
   with the parameters passed to the :meth:`run` and :meth:`setUp` methods.
2. User-supplied configuration can be processed in the :meth:`__init__` function.
3. If values need to be pre-calculated and cached, this can be done in the
   :meth:`~fragment.mods.abstract.ModBaseClass.setUp` method.
4. Write your mod or filter code in
   :meth:`~fragment.mods.abstract.ModBaseClass.run` method. If you are writing a
   filter, this should return a boolean. If you are writing a mod, this should
   return the same type of object which was provided to the function.
5. Make your mod available to fragment by importing it into the
   :mod:`fragment.mods.util` module.
6. Make your mod available to the user by adding appropriate sections to the
   fragment/schemas/strategy.yaml file.