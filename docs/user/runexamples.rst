Examples
========
All examples discussed here can be found in the examples directory (pyfragment/fragment/examples)

Example 1a: Water 6 (XYZ)
-------------------------
Path to example files:
- Strategy: ``/pyfragment/fragment/examples/strategies/water6_test.yaml``
- XYZ: ``/pyfragment/fragment/examples/systems/water6.xyz``

First, lets create the database and set up our directory. Once in the directory where you wish to store your information, type: 
``fragment project init``
This should have created three things: fragment.db, file_archive.tar, and a directory titled scratch. The fragment.db file holds all of the information about the system and its corresponding fragments, and when the calculations are complete, your results. The file_archive.tar will compress and store all of the input and output files (if applicable) for calculations and can be decompressed to inscpect specific jobs if necessary. 

.. note:: If you are running jobs in parallel you will also need to create a logs directory.

Next, we need to add our strategy file. 
``fragment strategy add water6_test.yaml``
This strategy file will take the XYZ file for a cluster of 6 water atoms and create two subsystems containing 'left' and 'right' parts of the water cluster. An 8 Angstrom distance modification has been added to screen out terms that correspond to subsystems outside this range. The only calculation set up for this system is a MBE(2) with supersystem correction.

In order to run this example you will need to have QChem and MOPAC set up. To run all calculations, simply use:
``fragment calc run``

When jobs are complete you can either report results to your screen or generate a CSV. To report results to your screen use: 
``fragment calc info mbe2_at_PM7.HF3C__water6``
to generate a CSV:
``fragment calc summary``

.. note:: If you ever forget or do not know what the name of your calculation is, you can always see what calculations exsit in your database using the 'list' flag


Example 1b: Water 6 (frag)
--------------------------
TO DO: Add strategy for frag style input files

Example 2: Short Protein Sequence
---------------------------------
Path to example files:
- Strategy: ``/pyfragment/fragment/examples/strategies/tetrapeptide.yaml``
- XYZ: ``/pyfragment/fragment/examples/systems/tetrapeptide.pdb``
- Submission file: ``/pyfragment/fragment/examples/other/parallel.sh``

As in the other examples, we will need to initialize and add the strategy files.
``fragment project init``
and
``fragment strategy add protein.yaml``
In this example, there are two charged side chains. Each charged atom number and its charge is listed in the systems section of the strategy file.

When creating fragments each residue is broken into its own monomer unit. Monomer calculations are used directly for MBE(1), but for higher MBE orders monomer units are combined to create dimers, trimers, etc. If the covalent bond between two residues is severed, the RCaps mod added (named mod_rcaps in this strategy file) will ensure that hydrogen atoms are added to the fragments.

.. note:: Using a PDB file without modification will result in fragments that are split between the carbon of the previous residue and nitrogen of the current residue. The fragmenter uses residue number to determine what should be considered a monomer unit. A more appropriate fragment would retain the CO group from the previous residue and split the alpha carbon and CO bond. A script to make such a modification has been provided in the examples/other directory, more about this script is discussed elsewhere in our documentation.

Although this example is relatively small, it is useful to have the ability to run in parallel. You'll see that a submission file has been provided for reference as a way to submit a job that gives 7 workers 4 cores each (28 total). Before running in parallel you will want to create a logs directory to store the log files generated during each run.