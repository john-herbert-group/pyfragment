Quickstart Guide
================

Installation & Setup
--------------------

#. Clone directory
    ``git clone git@gitlab.com:john-herbert-group/pyfragment.git``
#. Navigate to directory
    ``cd pyfragment``
#. Set up environment & activate
    ``conda env create -f environment.yml``
    ``conda activate fragment``
#. Install dependencies
    ``pip install --editable .``
    If necessary update pip (``pip install --upgrade pip``)
#. Run unit tests to ensure that set up has worked correctly
    ``python -m unittest discover``

Basic Commands
--------------

.. note::
    At any time more information about commands can be requested using ``fragment --help`` or ``fragment <command> --help``.

#. Initalize database
    ``fragment project init``
    This database will store all of the information about each system the user provides including atom type, atom location, charge, etc.
    - A file titled "fragment.db" should now exist within the directory that it was initialized in. To traverse or query this database use `SQLite`_.
#. Add strategy files
    ``fragment strategy add <strat.yaml>``
    Strategy files contain all of the information about how calculations should be completed (include information on electronic structure program, system, mods, etc.). For more information & a strategy template please see the next section.
#. Run calculations
    ``fragment calc run``
    There are additional options: number of nodes ``-n <no. nodes (int)>``, restart failed calculations ``--rerun-failed``, run specific job ``j:JOB_ID`` or ``s:SYSTEM_ID``, ect. 
    .. note:: TO DO: ADD HELP OPTIONS FOR RUN
#. View summary of completed calculations
    ``fragment calc info <calculation name>``
    If you do not know the name of your calculation it can be looked up using ``fragment calc list``
    If there is no reported value (i.e. total energy or CPU time), then it may be necessary to check for failed jobs or other errors

.. _SQLite: https://www.sqlitetutorial.net/sqlite-cheat-sheet/