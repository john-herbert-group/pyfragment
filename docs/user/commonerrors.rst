Common Errors
=============

Naming Conflicts
----------------
When a system is added the user is prompted to add a name and given the option to add a note/comment. In order to generate fragments/add calculations for this file it must be referenced by the exact name provided when the system is added (including case). To list the system(s) within the database use ``fragment system list``.

Rerunning Failed Jobs
---------------------
Occasionally jobs will not terminate as expected and will need to be rerun, to rerun these jobs use ``fragment calc run-all --rerun-failed``.

.. note:: At this time there is not an easy way to regenerate a fragment if there is an issue such as incorrect spin/multiplicity combination. 