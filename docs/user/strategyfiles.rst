Strategy Files
==============
There are several sections that one should consider defining: backends, fragmenters, supersystems, views, and calculations. There are also additional modifications that can be defined as well, for more information on this see information under 'Mods' in the Developers section. 

.. note:: The names of implemented fragmenters, modifications, etc. are case sensitive.

System
------
Here the source file will be added and any charged atoms noted.

.. code-block:: yaml

    systems:
    -
        name: reactant
        source: r.pdb
        note: Reactant used to study enzymatic barrier heights
        charges:
            12: -1 #E6
            103: 1 #K46
            121: -1 #E64
            232: -1 #E90 
            329: 2 #Mg
    -
    ...

Backends
--------
Within the strategy file, the program, any notes and/or desired options available in the program should be included. The current implemenation of Fragment is interfaced with `QChem`_, `MOPAC`_, `NWChem`_, `xTB`_, and `ORCA`_.  Users have the capability to modify the default template for each program to suit their needs; if sepecific calulations are not being processed correctly, then please read the documentation for each program for available options (methods, basis sets, etc.) or modifications.

.. code-block:: yaml

    # Set up the type calculations desired from each program

    backends: 
    -
        pm6:
            program: MOPAC
            note: PM6 semi-emperical MOPAC backend
            options:
              template: |
                 PM6-D3H4 XYZ PRECISE NOSYM NOREOR 
                 {name}

                 {geometry}
    -
        hf3c:
            program: QChem
            note: HF3C HF/semi-emperical through QChem
            options:
                template: |
                    ! Name: {name}
                    $molecule
                    {charge} {mult}
                    {geometry}
                    $end

                    $rem
                      basis minix
                      method hf3c
                      job_type sp
                    $end
    -
    ...

Fragmenters
-----------

In this section of the strategy file, the user must indicate how the system provided is to be divided. There are serveral schemes available: frag, water, and PDB.

.. note:: In protein fragmentation, covalent bonds between residues are severed so the user must define a modification to cap these bonds. Currently, there is one modification for this already implemented titled "RCaps".

.. code-block:: yaml

    # Define how the system is to be fragmented

    fragmenters:
        PDB:
            fragmenter: PDB
            note: Fragmenter for protein systems

Modifications (Mods)
--------------------
Mods allow the user to specify different modfications that may be needed or are disired for calculations to accurate represent the system. There are serveral useful modifications that users may want to use. If covalent bonds between fragments are being severed (i.e. between protein resiudes or side-chain/backbone), hydrogen caps can be added using the rcap mod. Additionally, modifications to add a distance threshold can be added. The distance mod offers two different algorthims: center-of-mass (COM) and closest neighbor. Other modifications are available and new ones can easily be added (see developer information).

.. note:: If a distance threshold modification is added, make sure to add a corresponding fragmenter.

.. code-block:: yaml
    mods:
    -
    #Hydrogen capping mod
        name: mod_rcaps
        mod_name: RCaps
        tolerance: 1.2
        k: 8
        cutoff: 3.0
        ignore_charged: True # Makes sure that charged atoms are NOT capped
        note: Caps severed covalent bonds between residues
    -
    #COM mod
        name: distCutoff_com 
        mod_name: Distance
        method: COM
        note: Filter of 10 Angstroms
        distance: 10
    -
    #Closest mod
        name: distCutoff_closest 
        mod_name: Distance
        method: Closest
        note: Filter of 10 Angstroms
        distance: 10
    -
    ...

Calculations
------------

Using the information given above, now each calculation can be defined. Modifications (mods) should be included if necessary. In pyfragment it is possible to mix calculations from different programs (see example below).

It is also possible to add a supersystem (order = 1) calculation for the purpose of completing molecules-in-molecules (MIM) sytle calculations where the supersystem calculation serves as a long range correction for change in energy due to fragmentation. 


.. code-block:: yaml

    # Add each calculation with its corresponding view and backend

    calculations:
    -
        name: MBE1_hf3c
        system: reactant
        note: MBE(1) calculation at HF3c level
        layers:
        - 
          view: 
            order: 1
            fragmenter: PDB
          backend: hf3c
          mods: ["mod_rcaps"]
    -
        name: MBE3_hf3c_pm6
        system: reactant
        note: MBE(3) calculation at HF3c level with a supersystem correction using PM6
        layers:
        - 
          view: 
            order: 3
            fragmenter: PDB
          backend: hf3c
          mods: ["mod_rcaps"]
        - 
          view:  
            order: 1
            fragmenter: supersystem
          backend: pm6
    -
    ...

.. _QChem: https://www.q-chem.com/ 
.. _MOPAC: http://openmopac.net/ 
.. _ORCA: https://orcaforum.kofo.mpg.de/app.php/portal
.. _xTB: https://xtb-docs.readthedocs.io/en/latest/contents.html
.. _NWChem: https://nwchemgit.github.io/