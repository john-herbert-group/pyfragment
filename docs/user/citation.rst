Useful References & Citation
============================
Software Development & Testing: Dustin R. Broderick, Paige E. Bowling, 
Advisor: John M. Herbert

Relevant papers from our group:
K.-Y Liu and J. M. Herbert, J. Chem. Theory Comput. 16, 475–487 (2020).
J. M. Herbert, J. Chem. Phys. 151, 170901:1–38 (2019).
J. Liu, B. Rana, K.-Y. Liu, and J. M. Herbert, J. Phys. Chem. Lett. 10, 3877–3886 (2019).
K.-Y. Liu and J. M. Herbert, J. Chem. Phys. 147, 161729:1–13 (2017).
K. U. Lao, K.-Y. Liu, R. M. Richard, and J. M. Herbert. J. Chem. Phys. 144, 164105:1–15 (2016).
J. Liu and J. M. Herbert. J. Chem. Theory Comput. 12, 572–584 (2016).
R. M. Richard, K. U. Lao, and J. M. Herbert. Acc. Chem. Res. 47, 2828–2836 (2014).
R. M. Richard, K. U. Lao, and J. M. Herbert. J. Chem. Phys. 141, 014108:1–14 (2014).
R. M. Richard, K. U. Lao, and J. M. Herbert. J. Chem. Phys. 139, 224102:1–11 (2013).
R. M. Richard, K. U. Lao, and J. M. Herbert. J. Phys. Chem. Lett. 4, 2674–2680 (2013).
R. M. Richard and J. M. Herbert. J. Chem. Theory Comput. 9, 1408–1416 (2013).
R. M. Richard and J. M. Herbert. J. Chem. Phys. 137, 064113:1–17 (2012).

Funding:
This work was supported by the U.S. Department of Energy, Office of Basic Energy Sciences, Division of Chemical Sciences, Geosciences, and Biosciences under Award No. DESC0008550. Calculations were performed at the Ohio Supercomputer Center under project no. PAS-2091.