User documentation
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   quickstart
   strategyfiles
   commonerrors
   runexamples
   usefulscripts
   citation
