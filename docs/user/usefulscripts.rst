Useful Scripts
==============
All files discussed here can be found: ``/pyfragment/fragment/examples/other/``

Modify template
---------------
This script serves as a way to modify the current template for calculations. This is particularly useful if jobs are failing (i.e. not enough memory or cycles allocated).

Modify PDB
----------
This script takes in a PDB and the residue numbers that the user would like to keep and generates a PDB file that will retain the CO group from the previous residue and split the alpha carbon and CO bond. Heteroatoms (ligands and water) are not modified. Additionally, any atoms that have been marked as charged (like in Pymol output PDB files) are noted at the top so that this information can be included in your strategy file.