fragment.properties
===========================

Submodules
----------

fragment.properties.core
-------------------------------

.. automodule:: fragment.properties.core
   :members:
   :undoc-members:
   :show-inheritance:

fragment.properties.extraction
-------------------------------------

.. automodule:: fragment.properties.extraction
   :members:
   :undoc-members:
   :show-inheritance:

fragment.properties.properties
-------------------------------------

.. automodule:: fragment.properties.properties
   :members:
   :undoc-members:
   :show-inheritance:
