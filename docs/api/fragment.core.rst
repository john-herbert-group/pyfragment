=============
fragment.core
=============

fragment.core.PIE
=================

.. automodule:: fragment.core.PIETree
   :members:
   :undoc-members:
   :show-inheritance:

fragment.core.legacy_PIE
========================

.. automodule:: fragment.core.legacy_PIE
   :members:
   :undoc-members:
   :show-inheritance:

fragment.core.legacy_PIETree
============================

.. automodule:: fragment.core.legacy_PIETree
   :members:
   :undoc-members:
   :show-inheritance:

fragment.core.query
===================

A collection of methods for fragment calculation analysis

This module is meant to be used for dataset analysis and assumes that all jobs required
to do this analysis have successfully run and have the desired properties.

.. warning:: 

   This module is expirimental and may be changed or deleted without notice.

.. todo:: 

   * Add tests

Examples
--------

Suppose the there is a project generated using the following strategy file

.. code-block:: yaml

   systems:
      -
            name: water6
            note: Water 6 cluster at local minimum
            source: /path/to/water6.xyz

   backends: 
      -
            name: xTB.GFN2
            note: xTB/GFN2 backend
            program: libxtb
      -
            name: HF.6-31G_star
            note: HF/6-31G* SP w/ QChem
            program: QChem
            template: |
               ... insert HF template
   
   fragmenters:
      -
            name: waterMBE
            note: Fragmeter for water 
            fragmenter: Water

   calculations:
      -
            name: mbe4.cluster.HF
            system: ALL
            note: MBE(4) calculation using  HF/6-31G*
            layers:
               -
                  backend: HF.6-31G_star
                  view:
                        fragmenter: waterMBE
                        order: 4
      -
            name: mbe4.cluster.xTB
            system: ALL
            note: MBE(4) calculation using xTB
            layers:
               -
                  backend: xTB.GFN2
                  view:
                        fragmenter: waterMBE
                        order: 4

The following steps will extract each individual fragment calculation into a
CSV file for analysis after all the QM calculations have completed.

.. note::

   Because this code accesses the database it is assumed all calls to this 
   module will be run in a :class:`Project.dbm` context

   .. code-block:: python

      from fragment.core.project import Project
      proj = Project('.')  # Assuming we are running this in the same dir as fragment.db

      with proj.dbm:
         ...  # the code for this example here

The first step is to inport this library

.. code-block:: python

   from fragment.core.query import (
      FragmentationPipeline,
      JobPipeline,
      annotate_view,
      annotate_ceq_deltas,
      get_view,
   )

First we define a :class:`~fragment.core.query.FragmentationPipeline` which are the
abstract steps taken when breaking up a system. We can use that pipeline to 
select a :class:`~fragment.systems.models.View` for a specific system.

.. code-block:: python

   mbe4 = FragmentationPipeline("waterMBE", 4)  # The pipeline
   view = get_view('water6', mbe4)  # Water 6 fargmented using the mbe4 pipeline

At this point the view has informaiton about how the cluster was fragmented but it
has no information about what the energies of those fragments are. This information is
extracted by defining a job pipeline and then binding the job data to our view


.. code-block:: python

   xTB = JobPipeline("xTB.GFN2")
   HF = JobPipeline("HF.6-31G_star")

This defines two job pipelines using the above QM backends with no mods (e.g. no
capping schemes). The view can then be annotated with the job pipeline's results.

.. code-block:: python

   annotate_view(view, xTB)
   annotate_view(view, HF)

Property deltas can be calculated (see :func:`~fragment.core.query.annotate_ceq_deltas`)
. This gives the systematic improvement added by doing higher order fragment 
calculations.

.. code-block:: python

   annotate_ceq_deltas(view, ['total_energy'])

Finally, these properties can be written to a CSV (this process does not use code)
provided in this file.

.. code-block:: python

   import csv
   COLUMNS = [
      "system",
      "key",
      "order",
      "coef",
      "backend",
      "fragmenter",
      "total_energy",  # From annotate_view
      "delta_total_energy"  # Frome annotate_ceq_deltas
   ]
   FILENAME = "out.csv"

   with open(FILENAME, 'w') as FILE:
      CSV_WRITER = csv.writer(FILE)
      CSV_WRITER.writerow(COLUMNS)

      for n, d in view.dep_tree:
         for p in [xTB, HF]:
            # Slightly awkward because PropertySet does not support iteration
            row_data = { 
               k: v for k, v in d["properties"][p]
            }
            row_data.update(
               system=sys_name,
               key=n,
               order=d["order"],
               coef=d["coef"],
               fragmenter=view.fragmenter.name,
               backend=p.backend.name,
            )
            CSV_WRITER.writerow([row_data.get(c, None) for c in COLUMNS])

API
---

.. automodule:: fragment.core.query
   :members:
   :undoc-members:
   :show-inheritance:

fragment.core.util
==================

.. automodule:: fragment.core.util
   :members:
   :undoc-members:
   :show-inheritance:
