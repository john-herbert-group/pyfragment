fragment.backends
=========================

Submodules
----------

fragment.backends.abstract
------------------------------------------

.. automodule:: fragment.backends.abstract
   :members:
   :undoc-members:
   :show-inheritance:

fragment.backends.templating
-----------------------------------

.. automodule:: fragment.backends.templating
   :members:
   :undoc-members:
   :show-inheritance:

fragment.backends.util
-----------------------------

.. automodule:: fragment.backends.util
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: fragment.backends
   :members:
   :undoc-members:
   :show-inheritance:
