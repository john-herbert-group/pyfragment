fragment.systems.geometry\_readers
==========================================

Submodules
----------

fragment.systems.geometry\_readers.util
----------------------------------------------

.. automodule:: fragment.systems.geometry_readers.util
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: fragment.systems.geometry_readers
   :members:
   :undoc-members:
   :show-inheritance:
